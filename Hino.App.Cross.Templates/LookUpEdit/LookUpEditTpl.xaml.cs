﻿using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.App.Cross.Templates.LookUpEdit
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LookUpEditTpl : ContentView
    {
        public LookUpEditTpl()
        {
            InitializeComponent();
            txtSelectEnterprise.BindingContext = this;
            txtSelectEnterprise.Tap(SelectEnterpriseTap);
        }

        void SelectEnterpriseTap(object sender, EventArgs e)
        {
            var actions = new string[] { "Open in new tab", "Open in new window", "Copy link address", "Download link" };
            /*
            await PopupNavigation.Instance.PushAsync(
                new SearchSinglePage(new GEEnterprisesSearch
                {
                    KeyName = "Id",
                    Description = "RazaoSocial",
                    Details = "NomeFantasia",
                    Title = "Empresas"
                })
            );*/
        }
    }
}