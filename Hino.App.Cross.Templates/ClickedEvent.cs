﻿using Hino.App.Cross.Resources;
using Hino.App.Cross.Utils.Extensions;
using Hino.App.Cross.Utils.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using XF.Material.Forms.UI.Dialogs;

namespace Hino.App.Cross.Templates
{
    public static class ClickedEvent
    {
        public static void LongPressToCopy(this View pInput)
        {
            if (pInput != null)
            {
                Vapolia.Lib.Ui.Gesture.SetLongPressCommand(pInput,
                    new Command<Point>(async (s) =>
                    {
                        UtilsExtensions.ShortVibrate();
                        await UtilsExtensions.CopyValueToClipboard(pInput);
                    }));
            }
        }

        public static void Tap(this View pInput, EventHandler pEventClicked)
        {
            if (pInput != null)
            {
                TapGestureRecognizer TapReco = new TapGestureRecognizer();
                TapReco.Tapped += async (s, e) =>
                {
                    await pInput.FadeTo(0.3, 100);
                    await pInput.FadeTo(1, 100);
                    pEventClicked.Invoke(pInput, e);
                };

                pInput.GestureRecognizers.Add(TapReco);
            }
        }

        public async static Task ShowOptions(this View pInput, string[] pActions, EventHandler[] pEvents, EventHandler pCancel)
        {
            var actions = new List<string>();
            var events = new List<EventHandler>();

            for (int i = 0, length = pEvents.Length; i < length; i++)
                if (pEvents[i] != null)
                {
                    actions.Add(pActions[i]);
                    events.Add(pEvents[i]);
                }

            var result = await MaterialDialog.Instance.SelectActionAsync(
                title: Messages.SelectOption,
                actions: actions);

            if (result > -1)
                events[result].Invoke(pInput, new EventArgs());
            else
                pCancel?.Invoke(pInput, new EventArgs());
        }

        public static void LongPressPhoneOptions(this View pInput, 
            EventHandler pCallNumberClicked,
            EventHandler pSendMessageClicked,
            EventHandler pCopyNumberClicked,
            EventHandler pCancelClicked)
        {
            if (pInput != null)
            {
                Vapolia.Lib.Ui.Gesture.SetLongPressCommand(pInput,
                    new Command<Point>(async (s) => 
                    {
                        UtilsExtensions.ShortVibrate();
                        await ClickedEvent.ShowOptions(
                            pInput,
                            new string[] { 
                                Messages.Call,
                                Messages.SendMessage,
                                Messages.CopyText,
                            },
                            new EventHandler[] { 
                                pCallNumberClicked,
                                pSendMessageClicked,
                                pCopyNumberClicked,
                            },
                            pCancelClicked
                        ); 
                    }));
            }
        }

        public static void LongPressEmailOptions(this View pInput, 
            EventHandler pSendEmailClicked,
            EventHandler pCopyNumberClicked,
            EventHandler pCancelClicked)
        {
            if (pInput != null)
            {
                Vapolia.Lib.Ui.Gesture.SetLongPressCommand(pInput,
                    new Command<Point>(async (s) => 
                    {
                        UtilsExtensions.ShortVibrate();
                        await ClickedEvent.ShowOptions(
                            pInput,
                            new string[] { 
                                Messages.SendEmail,
                                Messages.CopyText,
                            },
                            new EventHandler[] {
                                pSendEmailClicked,
                                pCopyNumberClicked,
                            },
                            pCancelClicked
                        ); 
                    }));
            }
        }

        public static void LongPressBrowserOptions(this View pInput, 
            EventHandler pOpenBrowserClicked,
            EventHandler pCopyNumberClicked,
            EventHandler pCancelClicked)
        {
            if (pInput != null)
            {
                Vapolia.Lib.Ui.Gesture.SetLongPressCommand(pInput,
                    new Command<Point>(async (s) => 
                    {
                        UtilsExtensions.ShortVibrate();
                        await ClickedEvent.ShowOptions(
                            pInput,
                            new string[] { 
                                Messages.OpenBrowser,
                                Messages.CopyText,
                            },
                            new EventHandler[] {
                                pOpenBrowserClicked,
                                pCopyNumberClicked,
                            },
                            pCancelClicked
                        ); 
                    }));
            }
        }

        public static void LongPress(this View pInput, EventHandler pEventClicked)
        {
            if (pInput != null)
            {
                Vapolia.Lib.Ui.Gesture.SetLongPressCommand(pInput, 
                    new Command<Point>((s) => 
                    {
                        UtilsExtensions.ShortVibrate();
                        pEventClicked?.Invoke(pInput, new EventArgs()); 
                    }));
            }
        }

        public static void SwipeLeft(this View pInput, EventHandler pEventClicked)
        {
            if (pInput != null)
            {
                Vapolia.Lib.Ui.Gesture.SetSwipeLeftCommand(pInput,
                    new Command<Point>((s) => { pEventClicked.Invoke(pInput, new EventArgs()); }));
            }
        }

        public static void SwipeRight(this View pInput, EventHandler pEventClicked)
        {
            if (pInput != null)
            {
                Vapolia.Lib.Ui.Gesture.SetSwipeRightCommand(pInput,
                    new Command<Point>((s) => { pEventClicked.Invoke(pInput, new EventArgs()); }));
            }
        }
    }
}
