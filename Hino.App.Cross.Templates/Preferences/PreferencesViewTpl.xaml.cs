﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XF.Material.Forms.UI.Dialogs;

namespace Hino.App.Cross.Templates.Preferences
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PreferencesViewTpl : ContentView
    {
        #region IsNotBusy
        public bool IsNotBusy
        {
            get { return (bool)GetValue(IsNotBusyProperty); }
            set { SetValue(IsNotBusyProperty, value); }
        }

        public static readonly BindableProperty IsNotBusyProperty =
            BindableProperty.Create(nameof(IsNotBusy), typeof(bool), typeof(PreferencesViewTpl));
        #endregion

        #region IntervalSync
        public string LBLIntervalSyncPlaceholder
        {
            get { return (string)GetValue(LBLIntervalSyncPlaceholderProperty); }
            set { SetValue(LBLIntervalSyncPlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLIntervalSyncPlaceholderProperty =
            BindableProperty.Create(nameof(LBLIntervalSyncPlaceholder), typeof(string), typeof(PreferencesViewTpl));

        #endregion

        #region SyncWIFI
        public string LBLSyncWIFIPlaceholder
        {
            get { return (string)GetValue(LBLSyncWIFIPlaceholderProperty); }
            set { SetValue(LBLSyncWIFIPlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLSyncWIFIPlaceholderProperty =
            BindableProperty.Create(nameof(LBLSyncWIFIPlaceholder), typeof(string), typeof(PreferencesViewTpl));

        #endregion

        #region SyncOnStart
        public string LBLSyncOnStartPlaceholder
        {
            get { return (string)GetValue(LBLSyncOnStartPlaceholderProperty); }
            set { SetValue(LBLSyncOnStartPlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLSyncOnStartPlaceholderProperty =
            BindableProperty.Create(nameof(LBLSyncOnStartPlaceholder), typeof(string), typeof(PreferencesViewTpl));

        #endregion

        #region SyncOnSave
        public string LBLSyncOnSavePlaceholder
        {
            get { return (string)GetValue(LBLSyncOnSavePlaceholderProperty); }
            set { SetValue(LBLSyncOnSavePlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLSyncOnSavePlaceholderProperty =
            BindableProperty.Create(nameof(LBLSyncOnSavePlaceholder), typeof(string), typeof(PreferencesViewTpl));

        #endregion

        #region UseSecurity
        public string LBLUseSecurityPlaceholder
        {
            get { return (string)GetValue(LBLUseSecurityPlaceholderProperty); }
            set { SetValue(LBLUseSecurityPlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLUseSecurityPlaceholderProperty =
            BindableProperty.Create(nameof(LBLUseSecurityPlaceholder), typeof(string), typeof(PreferencesViewTpl));

        #endregion

        #region LastSync
        public string LBLLastSyncPlaceholder
        {
            get { return (string)GetValue(LBLLastSyncPlaceholderProperty); }
            set { SetValue(LBLLastSyncPlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLLastSyncPlaceholderProperty =
            BindableProperty.Create(nameof(LBLLastSyncPlaceholder), typeof(string), typeof(PreferencesViewTpl));

        public string LBLLastSyncHelperText
        {
            get { return (string)GetValue(LBLLastSyncHelperTextProperty); }
            set { SetValue(LBLLastSyncHelperTextProperty, value); }
        }

        public static readonly BindableProperty LBLLastSyncHelperTextProperty =
            BindableProperty.Create(nameof(LBLLastSyncHelperText), typeof(string), typeof(PreferencesViewTpl));
        #endregion

        #region ClearAll
        public string LBLClearAllPlaceholder
        {
            get { return (string)GetValue(LBLClearAllPlaceholderProperty); }
            set { SetValue(LBLClearAllPlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLClearAllPlaceholderProperty =
            BindableProperty.Create(nameof(LBLClearAllPlaceholder), typeof(string), typeof(PreferencesViewTpl));

        #endregion

        #region SyncAll
        public string LBLSyncAllPlaceholder
        {
            get { return (string)GetValue(LBLSyncAllPlaceholderProperty); }
            set { SetValue(LBLSyncAllPlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLSyncAllPlaceholderProperty =
            BindableProperty.Create(nameof(LBLSyncAllPlaceholder), typeof(string), typeof(PreferencesViewTpl));

        #endregion

        public bool SyncWIFI
        {
            get { return (bool)GetValue(SyncWIFIProperty); }
            set { SetValue(SyncWIFIProperty, value); }
        }

        public static BindableProperty SyncWIFIProperty =
                      BindableProperty.Create(nameof(SyncWIFI), typeof(bool),
                      typeof(PreferencesViewTpl), defaultBindingMode: BindingMode.TwoWay);

        public bool SyncOnSave
        {
            get { return (bool)GetValue(SyncOnSaveProperty); }
            set { SetValue(SyncOnSaveProperty, value); }
        }

        public static BindableProperty SyncOnSaveProperty =
                      BindableProperty.Create(nameof(SyncOnSave), typeof(bool),
                      typeof(PreferencesViewTpl), defaultBindingMode: BindingMode.TwoWay);

        public bool SyncOnStart
        {
            get { return (bool)GetValue(SyncOnStartProperty); }
            set { SetValue(SyncOnStartProperty, value); }
        }

        public static BindableProperty SyncOnStartProperty =
                      BindableProperty.Create(nameof(SyncOnStart), typeof(bool),
                      typeof(PreferencesViewTpl), defaultBindingMode: BindingMode.TwoWay);

        public bool UseSecurity
        {
            get { return (bool)GetValue(UseSecurityProperty); }
            set { SetValue(UseSecurityProperty, value); }
        }

        public static BindableProperty UseSecurityProperty =
                      BindableProperty.Create(nameof(UseSecurity), typeof(bool),
                      typeof(PreferencesViewTpl), defaultBindingMode: BindingMode.TwoWay);


        #region CanUseSecurity
        public bool CanUseSecurity
        {
            get { return (bool)GetValue(CanUseSecurityProperty); }
            set { SetValue(CanUseSecurityProperty, value); }
        }

        public static readonly BindableProperty CanUseSecurityProperty =
            BindableProperty.Create(nameof(CanUseSecurity), typeof(bool), typeof(PreferencesViewTpl));
        #endregion

        public int IntervalSync
        {
            get { return (int)GetValue(IntervalSyncProperty); }
            set { SetValue(IntervalSyncProperty, value); }
        }

        public static BindableProperty IntervalSyncProperty =
                      BindableProperty.Create(nameof(IntervalSync), typeof(int),
                      typeof(PreferencesViewTpl), defaultBindingMode: BindingMode.TwoWay);

        public DateTime LastSync
        {
            get { return (DateTime)GetValue(LastSyncProperty); }
            set { SetValue(LastSyncProperty, value); }
        }

        public static BindableProperty LastSyncProperty =
                      BindableProperty.Create(nameof(LastSync), typeof(DateTime),
                      typeof(PreferencesViewTpl), defaultBindingMode: BindingMode.TwoWay);

        public PreferencesViewTpl ()
        {
	        InitializeComponent ();
            _content.BindingContext = this;
        }

        private async void BtnIntervalSync_Clicked(object sender, EventArgs e)
        {
            var actions = new string[] {
                Cross.Resources.Messages.Minutes10,
                Cross.Resources.Messages.Minutes15,
                Cross.Resources.Messages.Minutes30,
                Cross.Resources.Messages.Hour,
                Cross.Resources.Messages.Never };
            
            var result = await MaterialDialog.Instance.SelectActionAsync(title: Cross.Resources.PageTitle.SyncInterval,
                                                                         actions: actions);

            switch (result)
            {
                case 0:
                    IntervalSync = 10;
                    break;
                case 1:
                    IntervalSync = 15;
                    break;
                case 2:
                    IntervalSync = 30;
                    break;
                case 3:
                    IntervalSync = 60;
                    break;
                case 4:
                    IntervalSync = -1;
                    break;
                default:
                    IntervalSync = 10;
                    break;
            }


        }

        public event EventHandler ClearAll;
        private void BtnClearAll_Clicked(object sender, EventArgs e) =>
            ClearAll?.Invoke(sender, e);

        public event EventHandler SyncAll;
        private void BtnSyncAll_Clicked(object sender, EventArgs e) =>
            SyncAll?.Invoke(sender, e);
    }
}