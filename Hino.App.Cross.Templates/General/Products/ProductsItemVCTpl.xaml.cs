﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.App.Cross.Templates.General.Products
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProductsItemVCTpl : ViewCell
    {
        public ProductsItemVCTpl()
        {
            InitializeComponent();
        }
    }
}