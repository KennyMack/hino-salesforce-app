﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.App.Cross.Templates.General
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EmailVCTpl : ContentView
    {
        #region EMAIL
        public string LBLEMAILPlaceholder
        {
            get { return (string)GetValue(LBLEMAILPlaceholderProperty); }
            set { SetValue(LBLEMAILPlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLEMAILPlaceholderProperty =
            BindableProperty.Create(nameof(LBLEMAILPlaceholder), typeof(string), typeof(EmailVCTpl));

        public string LBLEMAILHelperText
        {
            get { return (string)GetValue(LBLEMAILHelperTextProperty); }
            set { SetValue(LBLEMAILHelperTextProperty, value); }
        }

        public static readonly BindableProperty LBLEMAILHelperTextProperty =
            BindableProperty.Create(nameof(LBLEMAILHelperText), typeof(string), typeof(EmailVCTpl));

        public string LBLEMAILErrorText
        {
            get { return (string)GetValue(LBLEMAILErrorTextProperty); }
            set { SetValue(LBLEMAILErrorTextProperty, value); }
        }

        public static readonly BindableProperty LBLEMAILErrorTextProperty =
            BindableProperty.Create(nameof(LBLEMAILErrorText), typeof(string), typeof(EmailVCTpl));

        public bool EMAILHasError
        {
            get { return (bool)GetValue(EMAILHasErrorProperty); }
            set { SetValue(EMAILHasErrorProperty, value); }
        }

        public static readonly BindableProperty EMAILHasErrorProperty =
            BindableProperty.Create(nameof(EMAILHasError), typeof(bool), typeof(EmailVCTpl));

        public string EMAILText
        {
            get { return (string)GetValue(EMAILTextProperty); }
            set { SetValue(EMAILTextProperty, value); }
        }

        public static BindableProperty EMAILTextProperty =
                      BindableProperty.Create(nameof(EMAILText), typeof(string),
                      typeof(EmailVCTpl), defaultBindingMode: BindingMode.TwoWay);

        #endregion

        #region SITE
        public string LBLSITEPlaceholder
        {
            get { return (string)GetValue(LBLSITEPlaceholderProperty); }
            set { SetValue(LBLSITEPlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLSITEPlaceholderProperty =
            BindableProperty.Create(nameof(LBLSITEPlaceholder), typeof(string), typeof(EmailVCTpl));

        public string LBLSITEHelperText
        {
            get { return (string)GetValue(LBLSITEHelperTextProperty); }
            set { SetValue(LBLSITEHelperTextProperty, value); }
        }

        public static readonly BindableProperty LBLSITEHelperTextProperty =
            BindableProperty.Create(nameof(LBLSITEHelperText), typeof(string), typeof(EmailVCTpl));

        public string LBLSITEErrorText
        {
            get { return (string)GetValue(LBLSITEErrorTextProperty); }
            set { SetValue(LBLSITEErrorTextProperty, value); }
        }

        public static readonly BindableProperty LBLSITEErrorTextProperty =
            BindableProperty.Create(nameof(LBLSITEErrorText), typeof(string), typeof(EmailVCTpl));

        public bool SITEHasError
        {
            get { return (bool)GetValue(SITEHasErrorProperty); }
            set { SetValue(SITEHasErrorProperty, value); }
        }

        public static readonly BindableProperty SITEHasErrorProperty =
            BindableProperty.Create(nameof(SITEHasError), typeof(bool), typeof(EmailVCTpl));

        public string SITEText
        {
            get { return (string)GetValue(SITETextProperty); }
            set { SetValue(SITETextProperty, value); }
        }

        public static BindableProperty SITETextProperty =
                      BindableProperty.Create(nameof(SITEText), typeof(string),
                      typeof(EmailVCTpl), defaultBindingMode: BindingMode.TwoWay);

        #endregion



        public EmailVCTpl()
        {
            InitializeComponent();
            _content.BindingContext = this;
        }
    }
}