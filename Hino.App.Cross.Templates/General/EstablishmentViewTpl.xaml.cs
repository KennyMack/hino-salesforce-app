﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.App.Cross.Templates.General
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EstablishmentViewTpl : ContentView
	{
        #region RazaoSocial
        public string LBLRazaoSocialPlaceholder
        {
            get { return (string)GetValue(LBLRazaoSocialPlaceholderProperty); }
            set { SetValue(LBLRazaoSocialPlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLRazaoSocialPlaceholderProperty =
            BindableProperty.Create(nameof(LBLRazaoSocialPlaceholder), typeof(string), typeof(EstablishmentViewTpl));

        public string LBLRazaoSocialHelperText
        {
            get { return (string)GetValue(LBLRazaoSocialHelperTextProperty); }
            set { SetValue(LBLRazaoSocialHelperTextProperty, value); }
        }

        public static readonly BindableProperty LBLRazaoSocialHelperTextProperty =
            BindableProperty.Create(nameof(LBLRazaoSocialHelperText), typeof(string), typeof(EstablishmentViewTpl));
        #endregion

        #region NomeFantasia
        public string LBLNomeFantasiaPlaceholder
        {
            get { return (string)GetValue(LBLNomeFantasiaPlaceholderProperty); }
            set { SetValue(LBLNomeFantasiaPlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLNomeFantasiaPlaceholderProperty =
            BindableProperty.Create(nameof(LBLNomeFantasiaPlaceholder), typeof(string), typeof(EstablishmentViewTpl));

        public string LBLNomeFantasiaHelperText
        {
            get { return (string)GetValue(LBLNomeFantasiaHelperTextProperty); }
            set { SetValue(LBLNomeFantasiaHelperTextProperty, value); }
        }

        public static readonly BindableProperty LBLNomeFantasiaHelperTextProperty =
            BindableProperty.Create(nameof(LBLNomeFantasiaHelperText), typeof(string), typeof(EstablishmentViewTpl));
        #endregion

        #region Email
        public string LBLEmailPlaceholder
        {
            get { return (string)GetValue(LBLEmailPlaceholderProperty); }
            set { SetValue(LBLEmailPlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLEmailPlaceholderProperty =
            BindableProperty.Create(nameof(LBLEmailPlaceholder), typeof(string), typeof(EstablishmentViewTpl));

        public string LBLEmailHelperText
        {
            get { return (string)GetValue(LBLEmailHelperTextProperty); }
            set { SetValue(LBLEmailHelperTextProperty, value); }
        }

        public static readonly BindableProperty LBLEmailHelperTextProperty =
            BindableProperty.Create(nameof(LBLEmailHelperText), typeof(string), typeof(EstablishmentViewTpl));
        #endregion

        #region Phone
        public string LBLPhonePlaceholder
        {
            get { return (string)GetValue(LBLPhonePlaceholderProperty); }
            set { SetValue(LBLPhonePlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLPhonePlaceholderProperty =
            BindableProperty.Create(nameof(LBLPhonePlaceholder), typeof(string), typeof(EstablishmentViewTpl));

        public string LBLPhoneHelperText
        {
            get { return (string)GetValue(LBLPhoneHelperTextProperty); }
            set { SetValue(LBLPhoneHelperTextProperty, value); }
        }

        public static readonly BindableProperty LBLPhoneHelperTextProperty =
            BindableProperty.Create(nameof(LBLPhoneHelperText), typeof(string), typeof(EstablishmentViewTpl));
        #endregion

        #region CNPJCPF
        public string LBLCNPJCPFPlaceholder
        {
            get { return (string)GetValue(LBLCNPJCPFPlaceholderProperty); }
            set { SetValue(LBLCNPJCPFPlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLCNPJCPFPlaceholderProperty =
            BindableProperty.Create(nameof(LBLCNPJCPFPlaceholder), typeof(string), typeof(EstablishmentViewTpl));

        public string LBLCNPJCPFHelperText
        {
            get { return (string)GetValue(LBLCNPJCPFHelperTextProperty); }
            set { SetValue(LBLCNPJCPFHelperTextProperty, value); }
        }

        public static readonly BindableProperty LBLCNPJCPFHelperTextProperty =
            BindableProperty.Create(nameof(LBLCNPJCPFHelperText), typeof(string), typeof(EstablishmentViewTpl));
        #endregion

        #region PIS
        public string LBLPISPlaceholder
        {
            get { return (string)GetValue(LBLPISPlaceholderProperty); }
            set { SetValue(LBLPISPlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLPISPlaceholderProperty =
            BindableProperty.Create(nameof(LBLPISPlaceholder), typeof(string), typeof(EstablishmentViewTpl));

        public string LBLPISHelperText
        {
            get { return (string)GetValue(LBLPISHelperTextProperty); }
            set { SetValue(LBLPISHelperTextProperty, value); }
        }

        public static readonly BindableProperty LBLPISHelperTextProperty =
            BindableProperty.Create(nameof(LBLPISHelperText), typeof(string), typeof(EstablishmentViewTpl));
        #endregion

        #region COFINS
        public string LBLCOFINSPlaceholder
        {
            get { return (string)GetValue(LBLCOFINSPlaceholderProperty); }
            set { SetValue(LBLCOFINSPlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLCOFINSPlaceholderProperty =
            BindableProperty.Create(nameof(LBLCOFINSPlaceholder), typeof(string), typeof(EstablishmentViewTpl));

        public string LBLCOFINSHelperText
        {
            get { return (string)GetValue(LBLCOFINSHelperTextProperty); }
            set { SetValue(LBLCOFINSHelperTextProperty, value); }
        }

        public static readonly BindableProperty LBLCOFINSHelperTextProperty =
            BindableProperty.Create(nameof(LBLCOFINSHelperText), typeof(string), typeof(EstablishmentViewTpl));
        #endregion

        public string RazaoSocial
        {
            get { return (string)GetValue(RazaoSocialProperty); }
            set { SetValue(RazaoSocialProperty, value); }
        }

        public static BindableProperty RazaoSocialProperty =
                      BindableProperty.Create(nameof(RazaoSocial), typeof(string),
                      typeof(EstablishmentViewTpl), defaultBindingMode: BindingMode.TwoWay);

        public string NomeFantasia
        {
            get { return (string)GetValue(NomeFantasiaProperty); }
            set { SetValue(NomeFantasiaProperty, value); }
        }

        public static BindableProperty NomeFantasiaProperty =
                      BindableProperty.Create(nameof(NomeFantasia), typeof(string),
                      typeof(EstablishmentViewTpl), defaultBindingMode: BindingMode.TwoWay);

        public string Email
        {
            get { return (string)GetValue(EmailProperty); }
            set { SetValue(EmailProperty, value); }
        }

        public static BindableProperty EmailProperty =
                      BindableProperty.Create(nameof(Email), typeof(string),
                      typeof(EstablishmentViewTpl), defaultBindingMode: BindingMode.TwoWay);

        public string Phone
        {
            get { return (string)GetValue(PhoneProperty); }
            set { SetValue(PhoneProperty, value); }
        }

        public static BindableProperty PhoneProperty =
                      BindableProperty.Create(nameof(Phone), typeof(string),
                      typeof(EstablishmentViewTpl), defaultBindingMode: BindingMode.TwoWay);

        public string CNPJCPF
        {
            get { return (string)GetValue(CNPJCPFProperty); }
            set { SetValue(CNPJCPFProperty, value); }
        }

        public static BindableProperty CNPJCPFProperty =
                      BindableProperty.Create(nameof(CNPJCPF), typeof(string),
                      typeof(EstablishmentViewTpl), defaultBindingMode: BindingMode.TwoWay);

        public string PIS
        {
            get { return (string)GetValue(PISProperty); }
            set { SetValue(PISProperty, value); }
        }

        public static BindableProperty PISProperty =
                      BindableProperty.Create(nameof(PIS), typeof(string),
                      typeof(EstablishmentViewTpl), defaultBindingMode: BindingMode.TwoWay);

        public string COFINS
        {
            get { return (string)GetValue(COFINSProperty); }
            set { SetValue(COFINSProperty, value); }
        }

        public static BindableProperty COFINSProperty =
                      BindableProperty.Create(nameof(COFINS), typeof(string),
                      typeof(EstablishmentViewTpl), defaultBindingMode: BindingMode.TwoWay);

        public EstablishmentViewTpl ()
		{
			InitializeComponent ();
            _content.BindingContext = this;
        }

        private void MaterialTextField_TextChanged(object sender, TextChangedEventArgs e)
        {
            

            return;
        }
    }
}