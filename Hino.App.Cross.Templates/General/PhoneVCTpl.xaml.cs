﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.App.Cross.Templates.General
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PhoneVCTpl : ContentView
    {
        #region PHONE
        public string LBLPHONEPlaceholder
        {
            get { return (string)GetValue(LBLPHONEPlaceholderProperty); }
            set { SetValue(LBLPHONEPlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLPHONEPlaceholderProperty =
            BindableProperty.Create(nameof(LBLPHONEPlaceholder), typeof(string), typeof(PhoneVCTpl));

        public string LBLPHONEHelperText
        {
            get { return (string)GetValue(LBLPHONEHelperTextProperty); }
            set { SetValue(LBLPHONEHelperTextProperty, value); }
        }

        public static readonly BindableProperty LBLPHONEHelperTextProperty =
            BindableProperty.Create(nameof(LBLPHONEHelperText), typeof(string), typeof(PhoneVCTpl));

        public string LBLPHONEErrorText
        {
            get { return (string)GetValue(LBLPHONEErrorTextProperty); }
            set { SetValue(LBLPHONEErrorTextProperty, value); }
        }

        public static readonly BindableProperty LBLPHONEErrorTextProperty =
            BindableProperty.Create(nameof(LBLPHONEErrorText), typeof(string), typeof(PhoneVCTpl));

        public bool PHONEHasError
        {
            get { return (bool)GetValue(PHONEHasErrorProperty); }
            set { SetValue(PHONEHasErrorProperty, value); }
        }

        public static readonly BindableProperty PHONEHasErrorProperty =
            BindableProperty.Create(nameof(PHONEHasError), typeof(bool), typeof(PhoneVCTpl));

        public string PHONEText
        {
            get { return (string)GetValue(PHONETextProperty); }
            set { SetValue(PHONETextProperty, value); }
        }

        public static BindableProperty PHONETextProperty =
                      BindableProperty.Create(nameof(PHONEText), typeof(string),
                      typeof(PhoneVCTpl), defaultBindingMode: BindingMode.TwoWay);

        #endregion

        #region CELLPHONE
        public string LBLCELLPHONEPlaceholder
        {
            get { return (string)GetValue(LBLCELLPHONEPlaceholderProperty); }
            set { SetValue(LBLCELLPHONEPlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLCELLPHONEPlaceholderProperty =
            BindableProperty.Create(nameof(LBLCELLPHONEPlaceholder), typeof(string), typeof(PhoneVCTpl));

        public string LBLCELLPHONEHelperText
        {
            get { return (string)GetValue(LBLCELLPHONEHelperTextProperty); }
            set { SetValue(LBLCELLPHONEHelperTextProperty, value); }
        }

        public static readonly BindableProperty LBLCELLPHONEHelperTextProperty =
            BindableProperty.Create(nameof(LBLCELLPHONEHelperText), typeof(string), typeof(PhoneVCTpl));

        public string LBLCELLPHONEErrorText
        {
            get { return (string)GetValue(LBLCELLPHONEErrorTextProperty); }
            set { SetValue(LBLCELLPHONEErrorTextProperty, value); }
        }

        public static readonly BindableProperty LBLCELLPHONEErrorTextProperty =
            BindableProperty.Create(nameof(LBLCELLPHONEErrorText), typeof(string), typeof(PhoneVCTpl));

        public bool CELLPHONEHasError
        {
            get { return (bool)GetValue(CELLPHONEHasErrorProperty); }
            set { SetValue(CELLPHONEHasErrorProperty, value); }
        }

        public static readonly BindableProperty CELLPHONEHasErrorProperty =
            BindableProperty.Create(nameof(CELLPHONEHasError), typeof(bool), typeof(PhoneVCTpl));

        public string CELLPHONEText
        {
            get { return (string)GetValue(CELLPHONETextProperty); }
            set { SetValue(CELLPHONETextProperty, value); }
        }

        public static BindableProperty CELLPHONETextProperty =
                      BindableProperty.Create(nameof(CELLPHONEText), typeof(string),
                      typeof(PhoneVCTpl), defaultBindingMode: BindingMode.TwoWay);

        #endregion

        public PhoneVCTpl()
        {
            InitializeComponent();
            _content.BindingContext = this;
        }
    }
}