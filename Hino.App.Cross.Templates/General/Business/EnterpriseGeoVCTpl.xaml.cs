﻿using Hino.App.Cross.Utils.Demograph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.App.Cross.Templates.General.Business
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EnterpriseGeoVCTpl : ContentView
    {
        public IList<string> UFCHOICES => States.UFList;

        #region ZIPCODE
        public string LBLZIPCODEPlaceholder
        {
            get { return (string)GetValue(LBLZIPCODEPlaceholderProperty); }
            set { SetValue(LBLZIPCODEPlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLZIPCODEPlaceholderProperty =
            BindableProperty.Create(nameof(LBLZIPCODEPlaceholder), typeof(string), typeof(EnterpriseGeoVCTpl));

        public string LBLZIPCODEHelperText
        {
            get { return (string)GetValue(LBLZIPCODEHelperTextProperty); }
            set { SetValue(LBLZIPCODEHelperTextProperty, value); }
        }

        public static readonly BindableProperty LBLZIPCODEHelperTextProperty =
            BindableProperty.Create(nameof(LBLZIPCODEHelperText), typeof(string), typeof(EnterpriseGeoVCTpl));

        public string LBLZIPCODEErrorText
        {
            get { return (string)GetValue(LBLZIPCODEErrorTextProperty); }
            set { SetValue(LBLZIPCODEErrorTextProperty, value); }
        }

        public static readonly BindableProperty LBLZIPCODEErrorTextProperty =
            BindableProperty.Create(nameof(LBLZIPCODEErrorText), typeof(string), typeof(EnterpriseGeoVCTpl));

        public bool ZIPCODEHasError
        {
            get { return (bool)GetValue(ZIPCODEHasErrorProperty); }
            set { SetValue(ZIPCODEHasErrorProperty, value); }
        }

        public static readonly BindableProperty ZIPCODEHasErrorProperty =
            BindableProperty.Create(nameof(ZIPCODEHasError), typeof(bool), typeof(EnterpriseGeoVCTpl));

        public string ZIPCODEText
        {
            get { return (string)GetValue(ZIPCODETextProperty); }
            set { SetValue(ZIPCODETextProperty, value); }
        }

        public static BindableProperty ZIPCODETextProperty =
                      BindableProperty.Create(nameof(ZIPCODEText), typeof(string),
                      typeof(EnterpriseGeoVCTpl), defaultBindingMode: BindingMode.TwoWay);
        #endregion

        #region ADDRESS
        public string LBLADDRESSPlaceholder
        {
            get { return (string)GetValue(LBLADDRESSPlaceholderProperty); }
            set { SetValue(LBLADDRESSPlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLADDRESSPlaceholderProperty =
            BindableProperty.Create(nameof(LBLADDRESSPlaceholder), typeof(string), typeof(EnterpriseGeoVCTpl));

        public string LBLADDRESSHelperText
        {
            get { return (string)GetValue(LBLADDRESSHelperTextProperty); }
            set { SetValue(LBLADDRESSHelperTextProperty, value); }
        }

        public static readonly BindableProperty LBLADDRESSHelperTextProperty =
            BindableProperty.Create(nameof(LBLADDRESSHelperText), typeof(string), typeof(EnterpriseGeoVCTpl));

        public string LBLADDRESSErrorText
        {
            get { return (string)GetValue(LBLADDRESSErrorTextProperty); }
            set { SetValue(LBLADDRESSErrorTextProperty, value); }
        }

        public static readonly BindableProperty LBLADDRESSErrorTextProperty =
            BindableProperty.Create(nameof(LBLADDRESSErrorText), typeof(string), typeof(EnterpriseGeoVCTpl));

        public bool ADDRESSHasError
        {
            get { return (bool)GetValue(ADDRESSHasErrorProperty); }
            set { SetValue(ADDRESSHasErrorProperty, value); }
        }

        public static readonly BindableProperty ADDRESSHasErrorProperty =
            BindableProperty.Create(nameof(ADDRESSHasError), typeof(bool), typeof(EnterpriseGeoVCTpl));

        public string ADDRESSText
        {
            get { return (string)GetValue(ADDRESSTextProperty); }
            set { SetValue(ADDRESSTextProperty, value); }
        }

        public static BindableProperty ADDRESSTextProperty =
                      BindableProperty.Create(nameof(ADDRESSText), typeof(string),
                      typeof(EnterpriseGeoVCTpl), defaultBindingMode: BindingMode.TwoWay);

        #endregion

        #region DISTRICT
        public string LBLDISTRICTPlaceholder
        {
            get { return (string)GetValue(LBLDISTRICTPlaceholderProperty); }
            set { SetValue(LBLDISTRICTPlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLDISTRICTPlaceholderProperty =
            BindableProperty.Create(nameof(LBLDISTRICTPlaceholder), typeof(string), typeof(EnterpriseGeoVCTpl));

        public string LBLDISTRICTHelperText
        {
            get { return (string)GetValue(LBLDISTRICTHelperTextProperty); }
            set { SetValue(LBLDISTRICTHelperTextProperty, value); }
        }

        public static readonly BindableProperty LBLDISTRICTHelperTextProperty =
            BindableProperty.Create(nameof(LBLDISTRICTHelperText), typeof(string), typeof(EnterpriseGeoVCTpl));

        public string LBLDISTRICTErrorText
        {
            get { return (string)GetValue(LBLDISTRICTErrorTextProperty); }
            set { SetValue(LBLDISTRICTErrorTextProperty, value); }
        }

        public static readonly BindableProperty LBLDISTRICTErrorTextProperty =
            BindableProperty.Create(nameof(LBLDISTRICTErrorText), typeof(string), typeof(EnterpriseGeoVCTpl));

        public bool DISTRICTHasError
        {
            get { return (bool)GetValue(DISTRICTHasErrorProperty); }
            set { SetValue(DISTRICTHasErrorProperty, value); }
        }

        public static readonly BindableProperty DISTRICTHasErrorProperty =
            BindableProperty.Create(nameof(DISTRICTHasError), typeof(bool), typeof(EnterpriseGeoVCTpl));

        public string DISTRICTText
        {
            get { return (string)GetValue(DISTRICTTextProperty); }
            set { SetValue(DISTRICTTextProperty, value); }
        }

        public static BindableProperty DISTRICTTextProperty =
                      BindableProperty.Create(nameof(DISTRICTText), typeof(string),
                      typeof(EnterpriseGeoVCTpl), defaultBindingMode: BindingMode.TwoWay);

        #endregion

        #region NUM
        public string LBLNUMPlaceholder
        {
            get { return (string)GetValue(LBLNUMPlaceholderProperty); }
            set { SetValue(LBLNUMPlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLNUMPlaceholderProperty =
            BindableProperty.Create(nameof(LBLNUMPlaceholder), typeof(string), typeof(EnterpriseGeoVCTpl));

        public string LBLNUMHelperText
        {
            get { return (string)GetValue(LBLNUMHelperTextProperty); }
            set { SetValue(LBLNUMHelperTextProperty, value); }
        }

        public static readonly BindableProperty LBLNUMHelperTextProperty =
            BindableProperty.Create(nameof(LBLNUMHelperText), typeof(string), typeof(EnterpriseGeoVCTpl));

        public string LBLNUMErrorText
        {
            get { return (string)GetValue(LBLNUMErrorTextProperty); }
            set { SetValue(LBLNUMErrorTextProperty, value); }
        }

        public static readonly BindableProperty LBLNUMErrorTextProperty =
            BindableProperty.Create(nameof(LBLNUMErrorText), typeof(string), typeof(EnterpriseGeoVCTpl));

        public bool NUMHasError
        {
            get { return (bool)GetValue(NUMHasErrorProperty); }
            set { SetValue(NUMHasErrorProperty, value); }
        }

        public static readonly BindableProperty NUMHasErrorProperty =
            BindableProperty.Create(nameof(NUMHasError), typeof(bool), typeof(EnterpriseGeoVCTpl));

        public string NUMText
        {
            get { return (string)GetValue(NUMTextProperty); }
            set { SetValue(NUMTextProperty, value); }
        }

        public static BindableProperty NUMTextProperty =
                      BindableProperty.Create(nameof(NUMText), typeof(string),
                      typeof(EnterpriseGeoVCTpl), defaultBindingMode: BindingMode.TwoWay);

        #endregion

        #region COMPLEMENT
        public string LBLCOMPLEMENTPlaceholder
        {
            get { return (string)GetValue(LBLCOMPLEMENTPlaceholderProperty); }
            set { SetValue(LBLCOMPLEMENTPlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLCOMPLEMENTPlaceholderProperty =
            BindableProperty.Create(nameof(LBLCOMPLEMENTPlaceholder), typeof(string), typeof(EnterpriseGeoVCTpl));

        public string LBLCOMPLEMENTHelperText
        {
            get { return (string)GetValue(LBLCOMPLEMENTHelperTextProperty); }
            set { SetValue(LBLCOMPLEMENTHelperTextProperty, value); }
        }

        public static readonly BindableProperty LBLCOMPLEMENTHelperTextProperty =
            BindableProperty.Create(nameof(LBLCOMPLEMENTHelperText), typeof(string), typeof(EnterpriseGeoVCTpl));

        public string LBLCOMPLEMENTErrorText
        {
            get { return (string)GetValue(LBLCOMPLEMENTErrorTextProperty); }
            set { SetValue(LBLCOMPLEMENTErrorTextProperty, value); }
        }

        public static readonly BindableProperty LBLCOMPLEMENTErrorTextProperty =
            BindableProperty.Create(nameof(LBLCOMPLEMENTErrorText), typeof(string), typeof(EnterpriseGeoVCTpl));

        public bool COMPLEMENTHasError
        {
            get { return (bool)GetValue(COMPLEMENTHasErrorProperty); }
            set { SetValue(COMPLEMENTHasErrorProperty, value); }
        }

        public static readonly BindableProperty COMPLEMENTHasErrorProperty =
            BindableProperty.Create(nameof(COMPLEMENTHasError), typeof(bool), typeof(EnterpriseGeoVCTpl));

        public string COMPLEMENTText
        {
            get { return (string)GetValue(COMPLEMENTTextProperty); }
            set { SetValue(COMPLEMENTTextProperty, value); }
        }

        public static BindableProperty COMPLEMENTTextProperty =
                      BindableProperty.Create(nameof(COMPLEMENTText), typeof(string),
                      typeof(EnterpriseGeoVCTpl), defaultBindingMode: BindingMode.TwoWay);

        #endregion

        #region UF
        public string LBLUFPlaceholder
        {
            get { return (string)GetValue(LBLUFPlaceholderProperty); }
            set { SetValue(LBLUFPlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLUFPlaceholderProperty =
            BindableProperty.Create(nameof(LBLUFPlaceholder), typeof(string), typeof(EnterpriseGeoVCTpl));

        public string LBLUFHelperText
        {
            get { return (string)GetValue(LBLUFHelperTextProperty); }
            set { SetValue(LBLUFHelperTextProperty, value); }
        }

        public static readonly BindableProperty LBLUFHelperTextProperty =
            BindableProperty.Create(nameof(LBLUFHelperText), typeof(string), typeof(EnterpriseGeoVCTpl));

        public string LBLUFErrorText
        {
            get { return (string)GetValue(LBLUFErrorTextProperty); }
            set { SetValue(LBLUFErrorTextProperty, value); }
        }

        public static readonly BindableProperty LBLUFErrorTextProperty =
            BindableProperty.Create(nameof(LBLUFErrorText), typeof(string), typeof(EnterpriseGeoVCTpl));

        public bool UFHasError
        {
            get { return (bool)GetValue(UFHasErrorProperty); }
            set { SetValue(UFHasErrorProperty, value); }
        }

        public static readonly BindableProperty UFHasErrorProperty =
            BindableProperty.Create(nameof(UFHasError), typeof(bool), typeof(EnterpriseGeoVCTpl));

        public string UFText
        {
            get { return (string)GetValue(UFTextProperty); }
            set { SetValue(UFTextProperty, value); }
        }

        public static BindableProperty UFTextProperty =
                      BindableProperty.Create(nameof(UFText), typeof(string),
                      typeof(EnterpriseGeoVCTpl), defaultBindingMode: BindingMode.TwoWay);

        #endregion

        #region CITYNAME
        public string LBLCITYNAMEPlaceholder
        {
            get { return (string)GetValue(LBLCITYNAMEPlaceholderProperty); }
            set { SetValue(LBLCITYNAMEPlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLCITYNAMEPlaceholderProperty =
            BindableProperty.Create(nameof(LBLCITYNAMEPlaceholder), typeof(string), typeof(EnterpriseGeoVCTpl));

        public string LBLCITYNAMEHelperText
        {
            get { return (string)GetValue(LBLCITYNAMEHelperTextProperty); }
            set { SetValue(LBLCITYNAMEHelperTextProperty, value); }
        }

        public static readonly BindableProperty LBLCITYNAMEHelperTextProperty =
            BindableProperty.Create(nameof(LBLCITYNAMEHelperText), typeof(string), typeof(EnterpriseGeoVCTpl));

        public string LBLCITYNAMEErrorText
        {
            get { return (string)GetValue(LBLCITYNAMEErrorTextProperty); }
            set { SetValue(LBLCITYNAMEErrorTextProperty, value); }
        }

        public static readonly BindableProperty LBLCITYNAMEErrorTextProperty =
            BindableProperty.Create(nameof(LBLCITYNAMEErrorText), typeof(string), typeof(EnterpriseGeoVCTpl));

        public bool CITYNAMEHasError
        {
            get { return (bool)GetValue(CITYNAMEHasErrorProperty); }
            set { SetValue(CITYNAMEHasErrorProperty, value); }
        }

        public static readonly BindableProperty CITYNAMEHasErrorProperty =
            BindableProperty.Create(nameof(CITYNAMEHasError), typeof(bool), typeof(EnterpriseGeoVCTpl));

        public string CITYNAMEText
        {
            get { return (string)GetValue(CITYNAMETextProperty); }
            set { SetValue(CITYNAMETextProperty, value); }
        }

        public static BindableProperty CITYNAMETextProperty =
                      BindableProperty.Create(nameof(CITYNAMEText), typeof(string),
                      typeof(EnterpriseGeoVCTpl), defaultBindingMode: BindingMode.TwoWay);

        #endregion

        public EnterpriseGeoVCTpl()
        {
            InitializeComponent();
            _content.BindingContext = this;
        }

        public event EventHandler FindCep;
        private void txtZIPCODE_Completed(object sender, EventArgs e) =>
            FindCep?.Invoke(sender, e);

        public event EventHandler<SelectedItemChangedEventArgs> UFChoiceSelected;
        private void ceUF_ChoiceSelected(object sender, SelectedItemChangedEventArgs e) =>
            UFChoiceSelected?.Invoke(sender, e);
    }
}