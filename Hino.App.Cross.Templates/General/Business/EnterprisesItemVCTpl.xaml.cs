﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.App.Cross.Templates.General.Business
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EnterprisesItemVCTpl : ViewCell
    {
        public EnterprisesItemVCTpl()
        {
            InitializeComponent();
        }
        public event EventHandler EditItemClicked;
        private void MiEdit_Clicked(object sender, EventArgs e)
        {
            EditItemClicked?.Invoke(sender, e);
        }

        public event EventHandler RemoveItemClicked;
        private void MiRemove_Clicked(object sender, EventArgs e)
        {
            RemoveItemClicked?.Invoke(sender, e);
        }
    }
}