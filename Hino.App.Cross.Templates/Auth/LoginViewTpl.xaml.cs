﻿using Hino.App.Cross.Utils.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.App.Cross.Templates.Auth
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginViewTpl : ContentView
    {
        public bool IsNotBusy
        {
            get { return (bool)GetValue(IsNotBusyProperty); }
            set { SetValue(IsNotBusyProperty, value); }
        }

        public static readonly BindableProperty IsNotBusyProperty =
            BindableProperty.Create(nameof(IsNotBusy), typeof(bool), typeof(LoginViewTpl));

        #region Establishment Key
        public string LBLEstablishmentKeyPlaceholder
        {
            get { return (string)GetValue(LBLEstablishmentKeyPlaceholderProperty); }
            set { SetValue(LBLEstablishmentKeyPlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLEstablishmentKeyPlaceholderProperty =
            BindableProperty.Create(nameof(LBLEstablishmentKeyPlaceholder), typeof(string), typeof(LoginViewTpl));

        public string LBLEstablishmentKeyHelperText
        {
            get { return (string)GetValue(LBLEstablishmentKeyHelperTextProperty); }
            set { SetValue(LBLEstablishmentKeyHelperTextProperty, value); }
        }

        public static readonly BindableProperty LBLEstablishmentKeyHelperTextProperty =
            BindableProperty.Create(nameof(LBLEstablishmentKeyHelperText), typeof(string), typeof(LoginViewTpl));

        public string LBLEstablishmentKeyErrorText
        {
            get { return (string)GetValue(LBLEstablishmentKeyErrorTextProperty); }
            set { SetValue(LBLEstablishmentKeyErrorTextProperty, value); }
        }

        public static readonly BindableProperty LBLEstablishmentKeyErrorTextProperty =
            BindableProperty.Create(nameof(LBLEstablishmentKeyErrorText), typeof(string), typeof(LoginViewTpl));

        public bool EstablishmentKeyHasError
        {
            get { return (bool)GetValue(EstablishmentKeyHasErrorProperty); }
            set { SetValue(EstablishmentKeyHasErrorProperty, value); }
        }

        public static readonly BindableProperty EstablishmentKeyHasErrorProperty =
            BindableProperty.Create(nameof(EstablishmentKeyHasError), typeof(bool), typeof(LoginViewTpl));


        #endregion

        #region User Or Email
        public string LBLUserOrEmailPlaceholder
        {
            get { return (string)GetValue(LBLUserOrEmailPlaceholderProperty); }
            set { SetValue(LBLUserOrEmailPlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLUserOrEmailPlaceholderProperty =
            BindableProperty.Create(nameof(LBLUserOrEmailPlaceholder), typeof(string), typeof(LoginViewTpl));

        public string LBLUserOrEmailHelperText
        {
            get { return (string)GetValue(LBLUserOrEmailHelperTextProperty); }
            set { SetValue(LBLUserOrEmailHelperTextProperty, value); }
        }

        public static readonly BindableProperty LBLUserOrEmailHelperTextProperty =
            BindableProperty.Create(nameof(LBLUserOrEmailHelperText), typeof(string), typeof(LoginViewTpl));

        public string LBLUserOrEmailErrorText
        {
            get { return (string)GetValue(LBLUserOrEmailErrorTextProperty); }
            set { SetValue(LBLUserOrEmailErrorTextProperty, value); }
        }

        public static readonly BindableProperty LBLUserOrEmailErrorTextProperty =
            BindableProperty.Create(nameof(LBLUserOrEmailErrorText), typeof(string), typeof(LoginViewTpl));

        public bool UserOrEmailHasError
        {
            get { return (bool)GetValue(UserOrEmailHasErrorProperty); }
            set { SetValue(UserOrEmailHasErrorProperty, value); }
        }

        public static readonly BindableProperty UserOrEmailHasErrorProperty =
            BindableProperty.Create(nameof(UserOrEmailHasError), typeof(bool), typeof(LoginViewTpl));
        #endregion

        #region Password
        public string LBLPasswordPlaceholder
        {
            get { return (string)GetValue(LBLPasswordPlaceholderProperty); }
            set { SetValue(LBLPasswordPlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLPasswordPlaceholderProperty =
            BindableProperty.Create(nameof(LBLPasswordPlaceholder), typeof(string), typeof(LoginViewTpl));

        public string LBLPasswordHelperText
        {
            get { return (string)GetValue(LBLPasswordHelperTextProperty); }
            set { SetValue(LBLPasswordHelperTextProperty, value); }
        }

        public static readonly BindableProperty LBLPasswordHelperTextProperty =
            BindableProperty.Create(nameof(LBLPasswordHelperText), typeof(string), typeof(LoginViewTpl));

        public string LBLPasswordErrorText
        {
            get { return (string)GetValue(LBLPasswordErrorTextProperty); }
            set { SetValue(LBLPasswordErrorTextProperty, value); }
        }

        public static readonly BindableProperty LBLPasswordErrorTextProperty =
            BindableProperty.Create(nameof(LBLPasswordErrorText), typeof(string), typeof(LoginViewTpl));

        public bool PasswordHasError
        {
            get { return (bool)GetValue(PasswordHasErrorProperty); }
            set { SetValue(PasswordHasErrorProperty, value); }
        }

        public static readonly BindableProperty PasswordHasErrorProperty =
            BindableProperty.Create(nameof(PasswordHasError), typeof(bool), typeof(LoginViewTpl));
        #endregion

        public string UserOrEmailText
        {
            get { return (string)GetValue(UserOrEmailTextProperty); }
            set { SetValue(UserOrEmailTextProperty, value); }
        }

        public static BindableProperty UserOrEmailTextProperty =
                      BindableProperty.Create(nameof(UserOrEmailText), typeof(string),
                      typeof(LoginViewTpl), defaultBindingMode: BindingMode.TwoWay);


        public string PasswordText
        {
            get { return (string)GetValue(PasswordTextProperty); }
            set { SetValue(PasswordTextProperty, value); }
        }

        public static BindableProperty PasswordTextProperty =
                      BindableProperty.Create(nameof(PasswordText), typeof(string),
                      typeof(LoginViewTpl), defaultBindingMode: BindingMode.TwoWay);

        public string EstablishmentKeyText
        {
            get { return (string)GetValue(EstablishmentKeyTextProperty); }
            set { SetValue(EstablishmentKeyTextProperty, value); }
        }

        public static BindableProperty EstablishmentKeyTextProperty =
                      BindableProperty.Create(nameof(EstablishmentKeyText), typeof(string),
                      typeof(LoginViewTpl), defaultBindingMode: BindingMode.TwoWay);


        public LoginViewTpl()
        {
            InitializeComponent();
            _content.BindingContext = this;
        }

        public event EventHandler BtnRegisterClicked;
        private void BtnRegister_Clicked(object sender, EventArgs e) =>
            BtnRegisterClicked?.Invoke(sender, e);

        public event EventHandler BtnLoginClicked;
        private void BtnEnter_Clicked(object sender, EventArgs e) =>
            BtnLoginClicked?.Invoke(sender, e);        

        public event EventHandler EstablishmentKeyCompleted;
        private void TxtEstablishmentKey_Completed(object sender, EventArgs e)
        {
            EstablishmentKeyCompleted?.Invoke(sender, e);
        }

        public event EventHandler UserLoginCompleted;
        private void TxtUser_Completed(object sender, EventArgs e) =>
            UserLoginCompleted?.Invoke(sender, e);

        public event EventHandler PasswordCompleted;
        private void TxtPassword_Completed(object sender, EventArgs e) =>
            PasswordCompleted?.Invoke(sender, e);
        
    }
}