﻿using Hino.App.Cross.Models.Auth;
using Hino.App.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.App.Cross.Templates.Auth
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AccountViewTpl : ContentView
    {
        public ObservableCollection<AccountsModel> MenuItems
        {
            get { return (ObservableCollection<AccountsModel>)GetValue(MenuItemsProperty); }
            set { SetValue(MenuItemsProperty, value); }
        }

        public static readonly BindableProperty MenuItemsProperty =
            BindableProperty.Create(nameof(MenuItems), 
                typeof(ObservableCollection<AccountsModel>), 
                typeof(AccountViewTpl));


        public int LISTHEIGHT
        {
            get { return (int)GetValue(LISTHEIGHTProperty); }
            set { SetValue(LISTHEIGHTProperty, value); }
        }

        public static readonly BindableProperty LISTHEIGHTProperty =
            BindableProperty.Create(nameof(LISTHEIGHT),
                typeof(int),
                typeof(AccountViewTpl));


        public AccountViewTpl()
        {
            InitializeComponent();

            //MenuItems = new ObservableCollection<AccountsModel>();
            _content.BindingContext = this;
        }

        public event EventHandler BtnRegisterClicked;
        private void btnRegister_Clicked(object sender, EventArgs e) =>
            BtnRegisterClicked?.Invoke(sender, e);

        public event EventHandler ItemSelected;
        private void MenuItemsListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (!(e.SelectedItem is AccountsModel item))
                return;

            ItemSelected?.Invoke(item, e);
            MenuItemsListView.SelectedItem = null;
        }

        public event EventHandler BtnEnterClicked;
        private void btnEnter_Clicked(object sender, EventArgs e)
        {
            try
            {
                BtnEnterClicked?.Invoke(sender, e);
            }
            catch (Exception ex)
            {
                DebugErrorLog.Log(ex);

            }
        }

        public event EventHandler BtnMenuItemClicked;
        private void MenuItem_Clicked(object sender, EventArgs e)
        {
            var menuItem = ((MenuItem)sender);
            BtnMenuItemClicked?.Invoke(menuItem.CommandParameter, e);
        }
    }
}