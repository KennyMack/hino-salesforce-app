﻿using Hino.App.Cross.Utils.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.App.Cross.Templates.Auth
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegisterViewTpl : ContentView
    {
        public bool IsNotBusy
        {
            get { return (bool)GetValue(IsNotBusyProperty); }
            set { SetValue(IsNotBusyProperty, value); }
        }

        public static readonly BindableProperty IsNotBusyProperty =
            BindableProperty.Create(nameof(IsNotBusy), typeof(bool), typeof(LoginViewTpl));

        #region Your Data
        public string LBLYourData
        {
            get { return (string)GetValue(LBLYourDataProperty); }
            set { SetValue(LBLYourDataProperty, value); }
        }

        public static readonly BindableProperty LBLYourDataProperty =
            BindableProperty.Create(nameof(LBLYourData), typeof(string), typeof(LoginViewTpl));

        #endregion

        #region Confirm
        public string LBLConfirm
        {
            get { return (string)GetValue(LBLConfirmProperty); }
            set { SetValue(LBLConfirmProperty, value); }
        }

        public static readonly BindableProperty LBLConfirmProperty =
            BindableProperty.Create(nameof(LBLConfirm), typeof(string), typeof(LoginViewTpl));

        #endregion

        #region Establishment Key
        public string LBLEstablishmentKeyPlaceholder
        {
            get { return (string)GetValue(LBLEstablishmentKeyPlaceholderProperty); }
            set { SetValue(LBLEstablishmentKeyPlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLEstablishmentKeyPlaceholderProperty =
            BindableProperty.Create(nameof(LBLEstablishmentKeyPlaceholder), typeof(string), typeof(LoginViewTpl));

        public string LBLEstablishmentKeyHelperText
        {
            get { return (string)GetValue(LBLEstablishmentKeyHelperTextProperty); }
            set { SetValue(LBLEstablishmentKeyHelperTextProperty, value); }
        }

        public static readonly BindableProperty LBLEstablishmentKeyHelperTextProperty =
            BindableProperty.Create(nameof(LBLEstablishmentKeyHelperText), typeof(string), typeof(LoginViewTpl));

        public string LBLEstablishmentKeyErrorText
        {
            get { return (string)GetValue(LBLEstablishmentKeyErrorTextProperty); }
            set { SetValue(LBLEstablishmentKeyErrorTextProperty, value); }
        }

        public static readonly BindableProperty LBLEstablishmentKeyErrorTextProperty =
            BindableProperty.Create(nameof(LBLEstablishmentKeyErrorText), typeof(string), typeof(LoginViewTpl));

        public bool EstablishmentKeyHasError
        {
            get { return (bool)GetValue(EstablishmentKeyHasErrorProperty); }
            set { SetValue(EstablishmentKeyHasErrorProperty, value); }
        }

        public static readonly BindableProperty EstablishmentKeyHasErrorProperty =
            BindableProperty.Create(nameof(EstablishmentKeyHasError), typeof(bool), typeof(LoginViewTpl));
        #endregion

        #region User Key
        public string LBLUserKeyPlaceholder
        {
            get { return (string)GetValue(LBLUserKeyPlaceholderProperty); }
            set { SetValue(LBLUserKeyPlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLUserKeyPlaceholderProperty =
            BindableProperty.Create(nameof(LBLUserKeyPlaceholder), typeof(string), typeof(LoginViewTpl));

        public string LBLUserKeyHelperText
        {
            get { return (string)GetValue(LBLUserKeyHelperTextProperty); }
            set { SetValue(LBLUserKeyHelperTextProperty, value); }
        }

        public static readonly BindableProperty LBLUserKeyHelperTextProperty =
            BindableProperty.Create(nameof(LBLUserKeyHelperText), typeof(string), typeof(LoginViewTpl));

        public string LBLUserKeyErrorText
        {
            get { return (string)GetValue(LBLUserKeyErrorTextProperty); }
            set { SetValue(LBLUserKeyErrorTextProperty, value); }
        }

        public static readonly BindableProperty LBLUserKeyErrorTextProperty =
            BindableProperty.Create(nameof(LBLUserKeyErrorText), typeof(string), typeof(LoginViewTpl));

        public bool UserKeyHasError
        {
            get { return (bool)GetValue(UserKeyHasErrorProperty); }
            set { SetValue(UserKeyHasErrorProperty, value); }
        }

        public static readonly BindableProperty UserKeyHasErrorProperty =
            BindableProperty.Create(nameof(UserKeyHasError), typeof(bool), typeof(LoginViewTpl));
        #endregion

        #region UserName
        public string LBLUserNamePlaceholder
        {
            get { return (string)GetValue(LBLUserNamePlaceholderProperty); }
            set { SetValue(LBLUserNamePlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLUserNamePlaceholderProperty =
            BindableProperty.Create(nameof(LBLUserNamePlaceholder), typeof(string), typeof(LoginViewTpl));

        public string LBLUserNameHelperText
        {
            get { return (string)GetValue(LBLUserNameHelperTextProperty); }
            set { SetValue(LBLUserNameHelperTextProperty, value); }
        }

        public static readonly BindableProperty LBLUserNameHelperTextProperty =
            BindableProperty.Create(nameof(LBLUserNameHelperText), typeof(string), typeof(LoginViewTpl));

        public string LBLUserNameErrorText
        {
            get { return (string)GetValue(LBLUserNameErrorTextProperty); }
            set { SetValue(LBLUserNameErrorTextProperty, value); }
        }

        public static readonly BindableProperty LBLUserNameErrorTextProperty =
            BindableProperty.Create(nameof(LBLUserNameErrorText), typeof(string), typeof(LoginViewTpl));

        public bool UserNameHasError
        {
            get { return (bool)GetValue(UserNameHasErrorProperty); }
            set { SetValue(UserNameHasErrorProperty, value); }
        }

        public static readonly BindableProperty UserNameHasErrorProperty =
            BindableProperty.Create(nameof(UserNameHasError), typeof(bool), typeof(LoginViewTpl));
        #endregion

        #region Email
        public string LBLEmailPlaceholder
        {
            get { return (string)GetValue(LBLEmailPlaceholderProperty); }
            set { SetValue(LBLEmailPlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLEmailPlaceholderProperty =
            BindableProperty.Create(nameof(LBLEmailPlaceholder), typeof(string), typeof(LoginViewTpl));

        public string LBLEmailHelperText
        {
            get { return (string)GetValue(LBLEmailHelperTextProperty); }
            set { SetValue(LBLEmailHelperTextProperty, value); }
        }

        public static readonly BindableProperty LBLEmailHelperTextProperty =
            BindableProperty.Create(nameof(LBLEmailHelperText), typeof(string), typeof(LoginViewTpl));

        public string LBLEmailErrorText
        {
            get { return (string)GetValue(LBLEmailErrorTextProperty); }
            set { SetValue(LBLEmailErrorTextProperty, value); }
        }

        public static readonly BindableProperty LBLEmailErrorTextProperty =
            BindableProperty.Create(nameof(LBLEmailErrorText), typeof(string), typeof(LoginViewTpl));

        public bool EmailHasError
        {
            get { return (bool)GetValue(EmailHasErrorProperty); }
            set { SetValue(EmailHasErrorProperty, value); }
        }

        public static readonly BindableProperty EmailHasErrorProperty =
            BindableProperty.Create(nameof(EmailHasError), typeof(bool), typeof(LoginViewTpl));
        #endregion

        #region Password
        public string LBLPasswordPlaceholder
        {
            get { return (string)GetValue(LBLPasswordPlaceholderProperty); }
            set { SetValue(LBLPasswordPlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLPasswordPlaceholderProperty =
            BindableProperty.Create(nameof(LBLPasswordPlaceholder), typeof(string), typeof(LoginViewTpl));

        public string LBLPasswordHelperText
        {
            get { return (string)GetValue(LBLPasswordHelperTextProperty); }
            set { SetValue(LBLPasswordHelperTextProperty, value); }
        }

        public static readonly BindableProperty LBLPasswordHelperTextProperty =
            BindableProperty.Create(nameof(LBLPasswordHelperText), typeof(string), typeof(LoginViewTpl));

        public string LBLPasswordErrorText
        {
            get { return (string)GetValue(LBLPasswordErrorTextProperty); }
            set { SetValue(LBLPasswordErrorTextProperty, value); }
        }

        public static readonly BindableProperty LBLPasswordErrorTextProperty =
            BindableProperty.Create(nameof(LBLPasswordErrorText), typeof(string), typeof(LoginViewTpl));

        public bool PasswordHasError
        {
            get { return (bool)GetValue(PasswordHasErrorProperty); }
            set { SetValue(PasswordHasErrorProperty, value); }
        }

        public static readonly BindableProperty PasswordHasErrorProperty =
            BindableProperty.Create(nameof(PasswordHasError), typeof(bool), typeof(LoginViewTpl));
        #endregion

        #region PasswordConfirm
        public string LBLPasswordConfirmPlaceholder
        {
            get { return (string)GetValue(LBLPasswordConfirmPlaceholderProperty); }
            set { SetValue(LBLPasswordConfirmPlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLPasswordConfirmPlaceholderProperty =
            BindableProperty.Create(nameof(LBLPasswordConfirmPlaceholder), typeof(string), typeof(LoginViewTpl));

        public string LBLPasswordConfirmHelperText
        {
            get { return (string)GetValue(LBLPasswordConfirmHelperTextProperty); }
            set { SetValue(LBLPasswordConfirmHelperTextProperty, value); }
        }

        public static readonly BindableProperty LBLPasswordConfirmHelperTextProperty =
            BindableProperty.Create(nameof(LBLPasswordConfirmHelperText), typeof(string), typeof(LoginViewTpl));

        public string LBLPasswordConfirmErrorText
        {
            get { return (string)GetValue(LBLPasswordConfirmErrorTextProperty); }
            set { SetValue(LBLPasswordConfirmErrorTextProperty, value); }
        }

        public static readonly BindableProperty LBLPasswordConfirmErrorTextProperty =
            BindableProperty.Create(nameof(LBLPasswordConfirmErrorText), typeof(string), typeof(LoginViewTpl));

        public bool PasswordConfirmHasError
        {
            get { return (bool)GetValue(PasswordConfirmHasErrorProperty); }
            set { SetValue(PasswordConfirmHasErrorProperty, value); }
        }

        public static readonly BindableProperty PasswordConfirmHasErrorProperty =
            BindableProperty.Create(nameof(PasswordConfirmHasError), typeof(bool), typeof(LoginViewTpl));
        #endregion

        #region Properties
        public string EstablishmentKeyText
        {
            get { return (string)GetValue(EstablishmentKeyTextProperty); }
            set { SetValue(EstablishmentKeyTextProperty, value); }
        }

        public static BindableProperty EstablishmentKeyTextProperty =
                      BindableProperty.Create(nameof(EstablishmentKeyText), typeof(string),
                      typeof(LoginViewTpl), defaultBindingMode: BindingMode.TwoWay);

        public string UserKeyText
        {
            get { return (string)GetValue(UserKeyTextProperty); }
            set { SetValue(UserKeyTextProperty, value); }
        }

        public static BindableProperty UserKeyTextProperty =
                      BindableProperty.Create(nameof(UserKeyText), typeof(string),
                      typeof(LoginViewTpl), defaultBindingMode: BindingMode.TwoWay);

        public string UserNameText
        {
            get { return (string)GetValue(UserNameTextProperty); }
            set { SetValue(UserNameTextProperty, value); }
        }

        public static BindableProperty UserNameTextProperty =
                      BindableProperty.Create(nameof(UserNameText), typeof(string),
                      typeof(LoginViewTpl), defaultBindingMode: BindingMode.TwoWay);

        public string EmailText
        {
            get { return (string)GetValue(EmailTextProperty); }
            set { SetValue(EmailTextProperty, value); }
        }

        public static BindableProperty EmailTextProperty =
                      BindableProperty.Create(nameof(EmailText), typeof(string),
                      typeof(LoginViewTpl), defaultBindingMode: BindingMode.TwoWay);

        public string PasswordText
        {
            get { return (string)GetValue(PasswordTextProperty); }
            set { SetValue(PasswordTextProperty, value); }
        }

        public static BindableProperty PasswordTextProperty =
                      BindableProperty.Create(nameof(PasswordText), typeof(string),
                      typeof(LoginViewTpl), defaultBindingMode: BindingMode.TwoWay);

        public string PasswordConfirmText
        {
            get { return (string)GetValue(PasswordConfirmTextProperty); }
            set { SetValue(PasswordConfirmTextProperty, value); }
        }

        public static BindableProperty PasswordConfirmTextProperty =
                      BindableProperty.Create(nameof(PasswordConfirmText), typeof(string),
                      typeof(LoginViewTpl), defaultBindingMode: BindingMode.TwoWay);
        #endregion


        public RegisterViewTpl()
        {
            InitializeComponent();
            _content.BindingContext = this;
        }

        public event EventHandler EstablishmentKeyCompleted;
        private void TxtEstablishmentKey_Completed(object sender, EventArgs e) =>
            EstablishmentKeyCompleted?.Invoke(sender, e);

        public event EventHandler UserKeyCompleted;
        private void TxtUserKey_Completed(object sender, EventArgs e) =>
            UserKeyCompleted?.Invoke(sender, e);

        public event EventHandler UserNameCompleted;
        private void TxtUserName_Completed(object sender, EventArgs e) =>
            UserNameCompleted?.Invoke(sender, e);

        public event EventHandler EmailCompleted;
        private void TxtEmail_Completed(object sender, EventArgs e) =>
            EmailCompleted?.Invoke(sender, e);

        public event EventHandler PasswordCompleted;
        private void TxtPassword_Completed(object sender, EventArgs e) =>
            PasswordCompleted?.Invoke(sender, e);

        public event EventHandler PasswordConfirmCompleted;
        private void TxtPasswordConfirm_Completed(object sender, EventArgs e) =>
            PasswordConfirmCompleted?.Invoke(sender, e);

        public event EventHandler BtnRegisterClicked;
        private void BtnRegister_Clicked(object sender, EventArgs e) =>
            BtnRegisterClicked?.Invoke(sender, e);

    }
}