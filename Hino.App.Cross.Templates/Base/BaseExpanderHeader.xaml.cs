﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.App.Cross.Templates.Base
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BaseExpanderHeader : ContentView
    {
        public string LBLDescription
        {
            get { return (string)GetValue(LBLDescriptionProperty); }
            set { SetValue(LBLDescriptionProperty, value); }
        }

        public static readonly BindableProperty LBLDescriptionProperty =
            BindableProperty.Create(nameof(LBLDescription), typeof(string), typeof(BaseExpanderHeader));

        public BaseExpanderHeader()
        {
            InitializeComponent();
            _content.BindingContext = this;
        }
    }
}