﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.App.Cross.Templates.Base
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BaseTitleView : ContentView
    {
        public event EventHandler BtnClosePageClicked;
        public BaseTitleView()
        {
            InitializeComponent();

            lblClose.Tap((s, e) => BtnClosePageClicked?.Invoke(s, e));
        }
    }
}