﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace Hino.App.Cross.Templates.Base
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BaseContentPage : ContentPage
    {
        public string PageName { get; protected set; }
        public BaseContentPage()
        {
            InitializeComponent();
        }

        public override string ToString()
        {
            return PageName;
        }

        protected virtual void OnAfterClose(object sender, EventArgs e)
        {

        }


        // override object.Equals
        public override bool Equals(object obj)
        {
            //       
            // See the full list of guidelines at
            //   http://go.microsoft.com/fwlink/?LinkID=85237  
            // and also the guidance for operator== at
            //   http://go.microsoft.com/fwlink/?LinkId=85238
            //

            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            return this.ToString() == obj.ToString();
        }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            return PageName.GetHashCode();
        }
    }
}