﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace Hino.App.Cross.Templates.FloatingButton
{
    public class HIFloatingButton : View
    {
        //BindableProperty.Create(nameof(ButtonColor), typeof(Color), typeof(HIFABButton), Color.Accent);
        public static readonly BindableProperty SizeProperty = 
            BindableProperty.Create("Size", typeof(FabSize), typeof(HIFloatingButton), FabSize.Normal);
            //<HIFloatingButton, FabSize>(mn => mn.Size, FabSize.Normal);

        public static readonly BindableProperty NormalColorProperty = 
            BindableProperty.Create("NormalColor", typeof(Color), typeof(HIFloatingButton), Color.FromHex("#0d47a1"));
        //BindableProperty.Create<HIFloatingButton, Color>(mn => mn.NormalColor, Color.Blue);

        public static readonly BindableProperty RippleColorProperty = 
            BindableProperty.Create("RippleColor", typeof(Color), typeof(HIFloatingButton), Color.Gray);
        //BindableProperty.Create<HIFloatingButton, Color>(mn => mn.RippleColor, Color.Gray);

        public static readonly BindableProperty DisabledColorProperty =
            BindableProperty.Create("DisabledColor", typeof(Color), typeof(HIFloatingButton), Color.Gray);
        //BindableProperty.Create<HIFloatingButton, Color>(mn => mn.DisabledColor, Color.Gray);

        public static readonly BindableProperty HasShadowProperty = 
            BindableProperty.Create("HasShadow", typeof(bool), typeof(HIFloatingButton), true);
        // BindableProperty.Create<HIFloatingButton, bool>(mn => mn.HasShadow, true);

        public static readonly BindableProperty SourceProperty = 
            BindableProperty.Create("Source", typeof(ImageSource), typeof(HIFloatingButton));
        // BindableProperty.Create<HIFloatingButton, ImageSource>(mn => mn.Source, null);

        public static readonly BindableProperty CommandProperty = 
            BindableProperty.Create(nameof(Command), typeof(ICommand), typeof(HIFloatingButton), null);

        // BindableProperty.Create<HIFloatingButton, ICommand>(mn => mn.Command, null, propertyChanged: HandleCommandChanged);

        public static readonly BindableProperty CommandParameterProperty = 
            BindableProperty.Create(nameof(CommandParameter), typeof(ICommand), typeof(HIFloatingButton), null);
        // BindableProperty.Create<HIFloatingButton, object>(mn => mn.CommandParameter, null);

        public static readonly BindableProperty AnimateOnSelectionProperty = 
            BindableProperty.Create("AnimateOnSelection", typeof(bool), typeof(HIFloatingButton), true);
        // BindableProperty.Create<HIFloatingButton, bool>(mn => mn.AnimateOnSelection, true);

        public event EventHandler<EventArgs> Clicked;

        public FabSize Size
        {
            get { return (FabSize)this.GetValue(SizeProperty); }
            set { this.SetValue(SizeProperty, value); }
        }

        public Color NormalColor
        {
            get { return (Color)this.GetValue(NormalColorProperty); }
            set { this.SetValue(NormalColorProperty, value); }
        }

        public Color RippleColor
        {
            get { return (Color)this.GetValue(RippleColorProperty); }
            set { this.SetValue(RippleColorProperty, value); }
        }

        public Color DisabledColor
        {
            get { return (Color)this.GetValue(DisabledColorProperty); }
            set { this.SetValue(DisabledColorProperty, value); }
        }

        public bool HasShadow
        {
            get { return (bool)this.GetValue(HasShadowProperty); }
            set { this.SetValue(HasShadowProperty, value); }
        }

        [TypeConverter(typeof(ImageSourceConverter))]
        public ImageSource Source
        {
            get { return (ImageSource)this.GetValue(SourceProperty); }
            set { this.SetValue(SourceProperty, value); }
        }

        public ICommand Command
        {
            get { return (ICommand)this.GetValue(CommandProperty); }
            set { this.SetValue(CommandProperty, value); }
        }

        public object CommandParameter
        {
            get { return (object)this.GetValue(CommandParameterProperty); }
            set { this.SetValue(CommandParameterProperty, value); }
        }

        public bool AnimateOnSelection
        {
            get { return (bool)this.GetValue(AnimateOnSelectionProperty); }
            set { this.SetValue(AnimateOnSelectionProperty, value); }
        }

        public virtual void SendClicked()
        {
            var param = this.CommandParameter;

            if (this.Command != null && this.Command.CanExecute(param))
            {
                this.Command.Execute(param);
            }

            this.Clicked?.Invoke(this, EventArgs.Empty);
        }

        private void InternalHandleCommand(ICommand oldValue, ICommand newValue)
        {
            // TOOD: attach to CanExecuteChanged
        }

        private static void HandleCommandChanged(BindableObject bindable, ICommand oldValue, ICommand newValue)
        {
            (bindable as HIFloatingButton).InternalHandleCommand(oldValue, newValue);
        }
    }
}
