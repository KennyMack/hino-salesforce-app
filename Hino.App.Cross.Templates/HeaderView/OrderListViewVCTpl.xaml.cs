﻿using Hino.App.Cross.Templates.ListView.Interfaces;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XF.Material.Forms.UI.Dialogs;
using XF.Material.Forms.UI.Dialogs.Configurations;

namespace Hino.App.Cross.Templates.HeaderView
{
    public class FilterUpdatedArgs: EventArgs
    {
        public View Filters { get; private set; }
    }

    public class SortUpdatedArgs : EventArgs
    {
        public View Filters { get; private set; }
    }

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OrderListViewVCTpl : ContentView
    {
        public bool IsModal;

        MaterialAlertDialogConfiguration _config = new MaterialAlertDialogConfiguration
        {
            ButtonAllCaps = true
        };

        public static readonly BindableProperty SortViewProperty =
           BindableProperty.Create(nameof(SortView), typeof(View), typeof(OrderListViewVCTpl),
               defaultBindingMode: BindingMode.TwoWay,
               propertyChanged: (bindable, oldval, newval) =>
               {

               });

        public View SortView
        {
            get { return (View)GetValue(SortViewProperty); }
            set { SetValue(SortViewProperty, value); }
        }

        public static readonly BindableProperty FilterViewProperty =
           BindableProperty.Create(nameof(FilterView), typeof(View), typeof(OrderListViewVCTpl),
               defaultBindingMode: BindingMode.TwoWay,
               propertyChanged: (bindable, oldval, newval) =>
               {

               });

        public View FilterView
        {
            get { return (View)GetValue(FilterViewProperty); }
            set { SetValue(FilterViewProperty, value); }
        }

        public event EventHandler<FilterUpdatedArgs> FilterUpdated;
        public event EventHandler<SortUpdatedArgs> SortUpdated;
        public event EventHandler OnShown;

        public OrderListViewVCTpl()
        {
            InitializeComponent();
            slContainer.BindingContext = this;
            btnFilter.Tap(btnFilterTap);
            btnSort.Tap(btnSortTap);
            IsModal = false;
        }

        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
        }

        protected override void OnParentSet()
        {
            base.OnParentSet();
            PaintOptions();
            btnFilter.IsVisible = (FilterView != null);
            btnSort.IsVisible = (SortView != null);
            slContainer.HeightRequest = (FilterView == null) && (SortView == null) ? 0 : 40;
        }

        private Color DefaultColor = Color.FromHex("#3f3f3f");
        private Color LinkColor = Color.FromHex("#4784E6");

        private void PaintOptions()
        {
            
            var defaultColor = Color.FromHex("#3f3f3f");

            icoFilter.TextColor = defaultColor;
            lblFilter.TextColor = defaultColor;
            icoSort.TextColor = defaultColor;
            lblSort.TextColor = defaultColor;
        }

        async void btnFilterTap(object sender, EventArgs e)
        {
            if (FilterView != null && !IsModal)
            {
                IsModal = true;
                OnShown.Invoke(IsModal, new EventArgs());

                await ((IFilterContentView)FilterView).LoadAsync();
                bool? wasConfirmed = await MaterialDialog.Instance.ShowCustomContentAsync(FilterView, 
                    "",
                    title: Cross.Resources.Messages.Filter,
                    configuration: _config,
                    dismissiveText: Hino.App.Cross.Resources.Messages.ClearFilter,
                    confirmingText: Hino.App.Cross.Resources.Messages.Filter
                    );

                IsModal = false;
                OnShown.Invoke(IsModal, new EventArgs());

                var colorButton = (wasConfirmed ?? false) ? LinkColor : DefaultColor;

                if (wasConfirmed ?? false)
                    FilterUpdated.Invoke(((IFilterContentView)FilterView).GetFilterAsync(), new FilterUpdatedArgs());
                else
                {
                    ((IFilterContentView)FilterView).ClearFilters();
                    FilterUpdated.Invoke(null, new FilterUpdatedArgs());
                }
                icoFilter.TextColor = colorButton;
                lblFilter.TextColor = colorButton;
            }
        }
        async void btnSortTap(object sender, EventArgs e)
        {
            if (SortView != null && !IsModal)
            {
                IsModal = true;
                OnShown.Invoke(IsModal, new EventArgs());

                await ((ISortContentView)SortView).LoadAsync();
                bool? wasConfirmed = await MaterialDialog.Instance.ShowCustomContentAsync(SortView,
                    "",
                    title: Cross.Resources.Messages.Sort,
                    configuration: _config,
                    dismissiveText: Cross.Resources.Messages.ClearSort,
                    confirmingText: Cross.Resources.Messages.Sort);

                IsModal = false;
                OnShown.Invoke(IsModal, new EventArgs());

                var colorButton = (wasConfirmed ?? false) ? LinkColor : DefaultColor;

                if (wasConfirmed ?? false)
                    SortUpdated.Invoke(((ISortContentView)SortView).GetSortsAsync(), new SortUpdatedArgs());
                else
                {
                    ((ISortContentView)SortView).ClearSorts();
                    SortUpdated.Invoke(null, new SortUpdatedArgs());
                }
                icoSort.TextColor = colorButton;
                lblSort.TextColor = colorButton;
            }
        }
    }
}