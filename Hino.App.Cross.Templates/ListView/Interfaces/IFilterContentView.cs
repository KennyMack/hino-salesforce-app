﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.App.Cross.Templates.ListView.Interfaces
{
    public interface IFilterContentView
    {
        Task LoadAsync();
        void ClearFilters();
        object GetFilterAsync();
        Task LoadFiltersAsync();
    }
}
