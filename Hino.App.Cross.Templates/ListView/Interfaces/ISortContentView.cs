﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.App.Cross.Templates.ListView.Interfaces
{
    public interface ISortContentView
    {
        Task LoadAsync();
        void ClearSorts();
        object GetSortsAsync();
        Task LoadSortsAsync();
    }
}
