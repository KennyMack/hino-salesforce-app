﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Hino.App.Cross.Templates.ListView
{
    public class FilterItem
    {
        public string PropertyName { get; set; }

        public string Caption { get; set; }

        public object Value { get; set; }

        public string PropertyValue { get; set; }
        public View ViewObject { get; set; }

        public FilterItem(string pName, object pValue, string pPropertyValue, View pViewObject)
        {
            PropertyName = pName;
            Value = pValue;
            ViewObject = pViewObject;
            PropertyValue = pPropertyValue;
        }

        public FilterItem()
        {

        }
    }

    public class FilterListView
    {
        public List<FilterItem> Filters { get; set; }

        public FilterListView()
        {
            Filters = new List<FilterItem>();
        }

        public View CreateView()
        {
            var content = new ContentView();
            var stackLayout = new StackLayout();
            content.Content = stackLayout;

            foreach (var item in Filters)
            {
                var label = new Label();
                label.Text = item.Caption;
                stackLayout.Children.Add(label);
                stackLayout.Children.Add(item.ViewObject);
            }


            return content;
        }
    }
}
