﻿using Hino.App.Cross.Models.General.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Hino.App.Cross.Templates.ListView
{
    public class ListGroup<T> : ObservableCollection<T>
    {
        public string LongName { get; set; }
        public string ShortName { get; set; }
    }
}
