﻿using Autofac;
using Autofac.Extras.CommonServiceLocator;
using CommonServiceLocator;
using Hino.App.Core.Navigator.NavPage;
using Hino.App.Core.Navigator.Utilities;
using Hino.App.Cross.Services.Navigation;
using Hino.App.Cross.Services.ViewModels;
using Hino.App.Cross.Services.ViewModels.Auth;
using Hino.App.Cross.Services.ViewModels.General;
using Hino.App.Cross.Services.ViewModels.General.Business;
using Hino.App.Cross.Services.ViewModels.General.Products;
using Hino.App.Cross.Services.ViewModels.Sales;
using Hino.App.Cross.Services.ViewModels.Search;
using Hino.Salesforce.App.Views;
using Hino.Salesforce.App.Views.Auth;
using Hino.Salesforce.App.Views.General;
using Hino.Salesforce.App.Views.General.Business.Enterprises;
using Hino.Salesforce.App.Views.General.Business.Payment;
using Hino.Salesforce.App.Views.General.Products;
using Hino.Salesforce.App.Views.Preferences;
using Hino.Salesforce.App.Views.Sales;
using System;
using Xamarin.Forms;

namespace Hino.App.Core
{
    public abstract class AppContainer
    {
        public void Setup()
        {
            var containerBuilder = new ContainerBuilder();

            RegisterServices(containerBuilder);

            var container = containerBuilder.Build();

            ServiceLocator.SetLocatorProvider(() => new AutofacServiceLocator(container));

            container.BeginLifetimeScope();
        }

        protected virtual void RegisterServices(ContainerBuilder containerBuilder)
        {
            containerBuilder.RegisterType<Hino.Salesforce.App.App>().SingleInstance();

            containerBuilder.RegisterType<NavigationService>().As<INavigationService>().SingleInstance();
            // containerBuilder.RegisterType<JobDialogService>().As<IJobDialogService>().InstancePerDependency();

            containerBuilder.RegisterType<AccountsPage>().Named<Page>(ViewNames.Auth.AccountPage.ToString()).As<AccountsPage>().InstancePerDependency();
            containerBuilder.RegisterType<LoginPage>().Named<Page>(ViewNames.Auth.LoginPage.ToString()).As<LoginPage>().InstancePerDependency();
            containerBuilder.RegisterType<RegisterPage>().Named<Page>(ViewNames.Auth.RegisterPage.ToString()).As<RegisterPage>().InstancePerDependency();
            containerBuilder.RegisterType<CreateEnterprisePage>().Named<Page>(ViewNames.Enterprises.CreateEnterprisePage.ToString()).As<CreateEnterprisePage>().InstancePerDependency();
            containerBuilder.RegisterType<DetailEnterprisePage>().Named<Page>(ViewNames.Enterprises.DetailEnterprisePage.ToString()).As<DetailEnterprisePage>().InstancePerDependency();
            containerBuilder.RegisterType<ListEnterprisesPage>().Named<Page>(ViewNames.Enterprises.ListEnterprisesPage.ToString()).As<ListEnterprisesPage>().InstancePerDependency();
            containerBuilder.RegisterType<MainPaymentPage>().Named<Page>(ViewNames.Payment.MainPaymentPage.ToString()).As<MainPaymentPage>().InstancePerDependency();
            containerBuilder.RegisterType<ListPaymentTypePage>().Named<Page>(ViewNames.Payment.ListPaymentTypePage.ToString()).As<ListPaymentTypePage>().InstancePerDependency();
            containerBuilder.RegisterType<ListPaymentConditionPage>().Named<Page>(ViewNames.Payment.ListPaymentConditionPage.ToString()).As<ListPaymentConditionPage>().InstancePerDependency();
            containerBuilder.RegisterType<EstablishmentPage>().Named<Page>(ViewNames.General.EstablishmentPage.ToString()).As<EstablishmentPage>().InstancePerDependency();
            
            containerBuilder.RegisterType<ListProductsPage>().Named<Page>(ViewNames.Products.ListProductsPage.ToString()).As<ListProductsPage>().InstancePerDependency();
            containerBuilder.RegisterType<DetailProductsPage>().Named<Page>(ViewNames.Products.DetailProductsPage.ToString()).As<DetailProductsPage>().InstancePerDependency();

            containerBuilder.RegisterType<PreferencesPage>().Named<Page>(ViewNames.Preferences.PreferencesPage.ToString()).As<PreferencesPage>().InstancePerDependency();

            containerBuilder.RegisterType<CreateSalePage>().Named<Page>(ViewNames.Sales.CreateSalePage.ToString()).As<CreateSalePage>().InstancePerDependency();
            containerBuilder.RegisterType<MainSalesPage>().Named<Page>(ViewNames.Sales.MainSalesPage.ToString()).As<MainSalesPage>().InstancePerDependency();
            
            containerBuilder.RegisterType<PageTest>().Named<Page>(ViewNames.Test.TestPage.ToString()).As<PageTest>().InstancePerDependency();

            containerBuilder.RegisterType<AccountVM>().InstancePerDependency();
            containerBuilder.RegisterType<LoginVM>().InstancePerDependency();
            containerBuilder.RegisterType<RegisterVM>().InstancePerDependency();

            containerBuilder.RegisterType<ListProductsVM>().InstancePerDependency();
            containerBuilder.RegisterType<DetailProductsVM>().InstancePerDependency();

            containerBuilder.RegisterType<CreateEnterpriseVM>().InstancePerDependency();
            containerBuilder.RegisterType<DetailEnterpriseVM>().InstancePerDependency();
            containerBuilder.RegisterType<ListEnterprisesVM>().InstancePerDependency();
            containerBuilder.RegisterType<MainPaymentVM>().InstancePerDependency();
            containerBuilder.RegisterType<PaymentConditionVM>().InstancePerDependency();
            containerBuilder.RegisterType<PaymentTypeVM>().InstancePerDependency();
            containerBuilder.RegisterType<EstablishmentVM>().InstancePerDependency();
            containerBuilder.RegisterType<SearchSinglePageVM>().InstancePerDependency();
            containerBuilder.RegisterType<MainViewModel>().InstancePerDependency();
            containerBuilder.RegisterType<PreferencesVM>().InstancePerDependency();
            containerBuilder.RegisterType<CreateSalesVM>().InstancePerDependency();
            containerBuilder.RegisterType<MainSalesVM>().InstancePerDependency();
            containerBuilder.RegisterType<vm>().InstancePerDependency();

        }
    }
}
