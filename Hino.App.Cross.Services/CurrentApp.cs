﻿using Hino.App.Cross.Models.Auth;
using Hino.App.Cross.Models.General;
using Hino.App.Cross.Services.DB.General;
using Hino.App.Cross.Services.DB.Interfaces.Auth;
using Hino.App.Cross.Services.DB.Interfaces.General;
using Hino.App.Cross.Templates.Base;
using System;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Hino.App.Cross.Services
{
    public static class CurrentApp
    {
        // public static INavigation _INavigation { get => BaseNavigation._INavigation; set => BaseNavigation._INavigation = value; }
        // 
        public static bool IsModal { get; set; }
        public static bool IsFilterModal { get; set; }
        // 
        public static bool IsConnected { get; set; }
         
        // public static async Task ShowModalAsync(Page pPage) =>
        //     await BaseNavigation.ShowModalAsync(pPage);
        // 
        // public static async Task ShowAsync(Page pPage) =>
        //     await BaseNavigation.ShowAsync(pPage);
        // 
        // public static async Task ClosePageModalAsync() =>
        //     await BaseNavigation.ClosePageModalAsync();

        public static string EstablishmentKeyLogged 
        {
            get => Preferences.Get("EstablishmentKeyLogged", ""); 
            set
            {
                if (value == null)
                    Preferences.Remove("EstablishmentKeyLogged");
                else
                    Preferences.Set("EstablishmentKeyLogged", value);
            }
        }

        public static string UserKeyLogged
        {
            get => Preferences.Get("UserKeyLogged", "");
            set
            {
                if (value == null)
                    Preferences.Remove("UserKeyLogged");
                else
                    Preferences.Set("UserKeyLogged", value);
            }
        }

        #region Is Logged
        public static bool IsLogged()
        {
            //var _IUserLoggedDB = DInjection.GetIntance<IUserLoggedDB>();

            if (!string.IsNullOrEmpty(UserKeyLogged) && 
                !string.IsNullOrEmpty(EstablishmentKeyLogged))
                return true;

           /* var User = await _IUserLoggedDB.GetByUserKeyAsync(EstablishmentKeyLogged, UserKeyLogged);

            if (User != null)
            {
                var _IGEEstablishmentDB = DInjection.GetIntance<IGEEstablishmentDB>();
                ESTABLISHMENT = await _IGEEstablishmentDB.GetByEstablishmentKey(EstablishmentKeyLogged);
                USERLOGGED = User;
                TOKENDATA = new TokenData
                {
                    EstablishmentKey = User.EstablishmentKey,
                    RefreshToken = User.RefreshToken,
                    SessionId = User.SessionId,
                    UserKey = User.UserKey,
                    UserToken = User.UserToken
                };

                return true;
            }*/

            return false;

            /* var user = USERLOGGED;
             var token = TOKENDATA;
             return ((user != null) &&
                 (!string.IsNullOrEmpty(user.UserKey) &&
                 !string.IsNullOrEmpty(user.UserName) &&
                 !string.IsNullOrEmpty(user.UniqueKey)) &&
                 ((token != null) &&
                 !string.IsNullOrEmpty(token.RefreshToken) &&
                 !string.IsNullOrEmpty(token.UserToken)));*/
        }
        #endregion

        #region Base Url
        public static string BASEURL
        {
            get => "192.168.15.103/Hino.Salesforce.API/api";// "3.229.108.251:6081/api";
        }
        #endregion

        #region TokenData
        public static TokenData TOKENDATA { get; set; }
        /*{
            get => new TokenData
            {
                UserToken = Preferences.Get("tokendata_usertoken", ""),
                RefreshToken = Preferences.Get("tokendata_refreshtoken", ""),
                EstablishmentKey = Preferences.Get("tokendata_establishmentkey", "")
            };
            set
            {
                if (value == null)
                {
                    Preferences.Remove("tokendata_usertoken");
                    Preferences.Remove("tokendata_refreshtoken");
                    Preferences.Remove("tokendata_establishmentkey");
                }
                else
                {
                    Preferences.Set("tokendata_usertoken", value.UserToken);
                    Preferences.Set("tokendata_refreshtoken", value.RefreshToken);
                    Preferences.Set("tokendata_establishmentkey", value.EstablishmentKey);
                }
            }
        }*/
        #endregion

        #region UserLogged
        public static UserLoggedModel USERLOGGED { get; set; }
        /*public static UserLoggedModel USERLOGGED
        {
            get
            {
                try
                {
                    var user = new UserLoggedModel
                    {
                        Id = Convert.ToInt64(Preferences.Get("user_id", "0")),
                        EstablishmentKey = Preferences.Get("user_establishmentkey", ""),
                        UniqueKey = Preferences.Get("user_uniquekey", ""),
                        UserName = Preferences.Get("user_username", ""),
                        Email = Preferences.Get("user_email", ""),
                        LastLogin = Convert.ToDateTime(Preferences.Get("user_lastlogin", DateTime.Now.ToString())),
                        UserType = Convert.ToInt32(Preferences.Get("user_usertype", "0")),
                        PercDiscount = Convert.ToDecimal(Preferences.Get("user_percdiscount", "0")),
                        PercCommission = Convert.ToDecimal(Preferences.Get("user_perccommission", "0")),
                        UserKey = Preferences.Get("user_userkey", "")
                    };
                    return user;

                }
                catch (Exception)
                {
                    USERLOGGED = null;

                    return USERLOGGED;
                }
            }
            set
            {
                if (value == null)
                {
                    Preferences.Remove("user_id");
                    Preferences.Remove("user_establishmentkey");
                    Preferences.Remove("user_uniquekey");
                    Preferences.Remove("user_username");
                    Preferences.Remove("user_email");
                    Preferences.Remove("user_lastlogin");
                    Preferences.Remove("user_usertype");
                    Preferences.Remove("user_percdiscount");
                    Preferences.Remove("user_perccommission");
                    Preferences.Remove("user_userkey");
                }
                else
                {
                    Preferences.Set("user_id", value.Id.ToString());
                    Preferences.Set("user_establishmentkey", value.EstablishmentKey);
                    Preferences.Set("user_uniquekey", value.UniqueKey);
                    Preferences.Set("user_username", value.UserName);
                    Preferences.Set("user_email", value.Email);
                    Preferences.Set("user_lastlogin", value.LastLogin.ToString());
                    Preferences.Set("user_usertype", value.UserType.ToString());
                    Preferences.Set("user_percdiscount", value.PercDiscount.ToString());
                    Preferences.Set("user_perccommission", value.PercCommission.ToString());
                    Preferences.Set("user_userkey", value.UserKey);
                }
            }
        }*/
        #endregion

        #region Establishment
        public static GEEstablishmentsModel ESTABLISHMENT { get; set; }
        /*public static GEEstablishmentsModel ESTABLISHMENT
        {
            get
            {
                try
                {
                    var user = new GEEstablishmentsModel
                    {
                        Id = Convert.ToInt64(Preferences.Get("establishment_id", "0")),
                        EstablishmentKey = Convert.ToString(Preferences.Get("establishment_establishmentkey", "")),
                        UniqueKey = Convert.ToString(Preferences.Get("establishment_uniquekey", "")),
                        RazaoSocial = Convert.ToString(Preferences.Get("establishment_razaosocial", "")),
                        NomeFantasia = Convert.ToString(Preferences.Get("establishment_nomefantasia", "")),
                        Email = Convert.ToString(Preferences.Get("establishment_email", "")),
                        Phone = Convert.ToString(Preferences.Get("establishment_phone", "")),
                        CNPJCPF = Convert.ToString(Preferences.Get("establishment_cnpjcpf", "")),
                        Devices = Convert.ToInt32(Preferences.Get("establishment_devices", "0")),
                        PIS = Convert.ToDecimal(Preferences.Get("establishment_pis", "0")),
                        COFINS = Convert.ToDecimal(Preferences.Get("establishment_cofins", "0")),
                        AllowEnterprise = Convert.ToInt32(Preferences.Get("establishment_allowenterprise", "0")) == 1,
                        AllowPayment = Convert.ToInt32(Preferences.Get("establishment_allowpayment", "0")) == 1,
                        AllowChangePrice = Convert.ToInt32(Preferences.Get("establishment_allowchangeprice", "0")) == 1
                    };
                    return user;

                }
                catch (Exception)
                {
                    ESTABLISHMENT = null;

                    return ESTABLISHMENT;
                }
            }
            set
            {
                if (value == null)
                {
                    Preferences.Remove("establishment_id");
                    Preferences.Remove("establishment_establishmentkey");
                    Preferences.Remove("establishment_uniquekey");
                    Preferences.Remove("establishment_razaosocial");
                    Preferences.Remove("establishment_nomefantasia");
                    Preferences.Remove("establishment_email");
                    Preferences.Remove("establishment_phone");
                    Preferences.Remove("establishment_cnpjcpf");
                    Preferences.Remove("establishment_devices");
                    Preferences.Remove("establishment_pis");
                    Preferences.Remove("establishment_cofins");
                    Preferences.Remove("establishment_allowenterprise");
                    Preferences.Remove("establishment_allowpayment");
                    Preferences.Remove("establishment_allowchangeprice");
                }
                else
                {
                    Preferences.Set("establishment_id", value.Id.ToString());
                    Preferences.Set("establishment_establishmentkey", value.EstablishmentKey.ToString());
                    Preferences.Set("establishment_uniquekey", value.UniqueKey.ToString());
                    Preferences.Set("establishment_razaosocial", value.RazaoSocial.ToString());
                    Preferences.Set("establishment_nomefantasia", value.NomeFantasia.ToString());
                    Preferences.Set("establishment_email", value.Email.ToString());
                    Preferences.Set("establishment_phone", value.Phone.ToString());
                    Preferences.Set("establishment_cnpjcpf", value.CNPJCPF.ToString());
                    Preferences.Set("establishment_devices", value.Devices.ToString());
                    Preferences.Set("establishment_pis", value.PIS.ToString());
                    Preferences.Set("establishment_cofins", value.COFINS.ToString());
                    Preferences.Set("establishment_allowenterprise", (value.AllowEnterprise ? 1 : 0).ToString());
                    Preferences.Set("establishment_allowpayment", (value.AllowPayment ? 1 : 0).ToString());
                    Preferences.Set("establishment_allowchangeprice", (value.AllowChangePrice ? 1 : 0).ToString());
                }
            }
        }*/
        #endregion

    }
}
