﻿using Hino.App.Cross.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;
using MasterDetailPage = Xamarin.Forms.MasterDetailPage;

namespace Hino.App.Cross.Services.Navigation
{
    public abstract class BaseView<TViewModel> : ContentPage where TViewModel : BaseViewModel
    {
        public string PageName { get; protected set; }
        protected TViewModel ViewModel { get; }

        protected BaseView()
        {
            ViewModel = CommonServiceLocator.ServiceLocator.Current.GetInstance<TViewModel>();
            BindingContext = ViewModel;
            On<iOS>().SetUseSafeArea(true);
        }

        public override string ToString()
        {
            return PageName;
        }

        // override object.Equals
        public override bool Equals(object obj)
        {
            //       
            // See the full list of guidelines at
            //   http://go.microsoft.com/fwlink/?LinkID=85237  
            // and also the guidance for operator== at
            //   http://go.microsoft.com/fwlink/?LinkId=85238
            //

            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            return this.ToString() == obj.ToString();
        }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            return PageName.GetHashCode();
        }
    }

    public abstract class BaseModalView<TViewModel> : XF.Navigation.UI.Pages.ContentModalPage where TViewModel : BaseViewModel
    {
        public string PageName { get; protected set; }
        protected TViewModel ViewModel { get; }

        protected BaseModalView()
        {
            ViewModel = CommonServiceLocator.ServiceLocator.Current.GetInstance<TViewModel>();
            BindingContext = ViewModel;
            On<iOS>().SetUseSafeArea(true);
        }

        public override string ToString()
        {
            return PageName;
        }

        // override object.Equals
        public override bool Equals(object obj)
        {
            //       
            // See the full list of guidelines at
            //   http://go.microsoft.com/fwlink/?LinkID=85237  
            // and also the guidance for operator== at
            //   http://go.microsoft.com/fwlink/?LinkId=85238
            //

            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            return this.ToString() == obj.ToString();
        }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            return PageName.GetHashCode();
        }
    }

    public abstract class BaseMasterDetailView<TViewModel> : MasterDetailPage where TViewModel : BaseViewModel
    {
        public string PageName { get; protected set; }
        protected TViewModel ViewModel { get; }

        protected BaseMasterDetailView()
        {
            ViewModel = CommonServiceLocator.ServiceLocator.Current.GetInstance<TViewModel>();
            BindingContext = ViewModel;
            On<iOS>().SetUseSafeArea(true);
        }

        public override string ToString()
        {
            return PageName;
        }

        // override object.Equals
        public override bool Equals(object obj)
        {
            //       
            // See the full list of guidelines at
            //   http://go.microsoft.com/fwlink/?LinkID=85237  
            // and also the guidance for operator== at
            //   http://go.microsoft.com/fwlink/?LinkId=85238
            //

            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            return this.ToString() == obj.ToString();
        }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            return PageName.GetHashCode();
        }
    }

    public abstract class BaseTabbedView<TViewModel> : Xamarin.Forms.TabbedPage where TViewModel : BaseViewModel
    {
        public string PageName { get; protected set; }
        protected TViewModel ViewModel { get; }

        protected BaseTabbedView()
        {
            ViewModel = CommonServiceLocator.ServiceLocator.Current.GetInstance<TViewModel>();
            BindingContext = ViewModel;
            On<iOS>().SetUseSafeArea(true);
        }

        public override string ToString()
        {
            return PageName;
        }

        // override object.Equals
        public override bool Equals(object obj)
        {
            //       
            // See the full list of guidelines at
            //   http://go.microsoft.com/fwlink/?LinkID=85237  
            // and also the guidance for operator== at
            //   http://go.microsoft.com/fwlink/?LinkId=85238
            //

            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            return this.ToString() == obj.ToString();
        }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            return PageName.GetHashCode();
        }
    }
}
