﻿using Hino.App.Cross.Models.General;
using Hino.App.Cross.Services.API.Interfaces;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Hino.App.Cross.Utils.Exceptions;

namespace Hino.App.Cross.Services.API
{
    public class Request : IRequest
    {
        public readonly JsonSerializerSettings _JsonSerializerSettings;
        public readonly JsonSerializer _JsonSerializer;

        public string BASEURL = $"http://{CurrentApp.BASEURL}";

        public List<ModelException> Errors { get; set; }

        public Request()
        {
            _JsonSerializer = new JsonSerializer
            {
                Culture = new System.Globalization.CultureInfo("pt-BR"),
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                DateFormatString = "dd/MM/yyyy HH:mm:ss"
            };

            _JsonSerializerSettings = new JsonSerializerSettings
            {
                Culture = new System.Globalization.CultureInfo("pt-BR"),
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                DateFormatString = "dd/MM/yyyy HH:mm:ss"
            };
            Errors = new List<ModelException>();
        }

        #region Default Headers
        private void DefaultHeaders(HttpClient pClient, TimeSpan? timeout = null)
        {
            if (timeout == null)
                pClient.Timeout = new System.TimeSpan(0, 0, 1000);
            else
                pClient.Timeout = (TimeSpan)timeout;
            pClient.DefaultRequestHeaders.Accept.Add(
                new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json")
            );
        }
        #endregion

        #region Create Token Header
        private void CreateTokenHeader(HttpClient pClient, bool Token)
        {
            if (Token)
            {
                pClient.DefaultRequestHeaders.Authorization =
                    new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", CurrentApp.TOKENDATA.UserToken);
                pClient.DefaultRequestHeaders.Add("Refresh-Token", CurrentApp.TOKENDATA.RefreshToken);
            }
        }
        #endregion

        public async Task<JContainer> GetAsync(string path, bool Token, TimeSpan? timeout = null)
        {
            HttpResponseMessage respMessage = null;
            JContainer data = null;

            using (HttpClient client = new HttpClient())
            {
                DefaultHeaders(client);

                CreateTokenHeader(client, Token);
                try
                {
                    if (timeout == null)
                        client.Timeout = new System.TimeSpan(0, 0, 1000);
                    else
                        client.Timeout = (TimeSpan)timeout;
                    respMessage = await client.GetAsync($"{BASEURL}/{path}");

                    if (respMessage != null)
                    {
                        var json = respMessage.Content.ReadAsStringAsync().Result;
                        var dt = JObject.Parse(json);

                        var t = dt.GetValue("Message");
                        if (dt.GetValue("Message") != null)
                            data = (JContainer)JsonConvert.DeserializeObject(dt.GetValue("Message").ToString(), _JsonSerializerSettings);
                        else
                            data = (JContainer)JsonConvert.DeserializeObject(JObject.Parse(json).GetValue("Data").ToString(), _JsonSerializerSettings);
                    }

                }
                catch (Exception e)
                {
                    DebugErrorLog.Log(e);
                }

            }

            return data;
        }

        public async Task<JContainer> PostAsync(string path, bool Token, HttpContent content)
        {
            HttpResponseMessage respMessage = null;
            JContainer data = null;

            using (HttpClient client = new HttpClient())
            {
                DefaultHeaders(client);

                CreateTokenHeader(client, Token);

                try
                {
                    var cont= content.ReadAsStringAsync().Result;
                    var url = $"{BASEURL}/{path}";
                    respMessage = await client.PostAsync(url, content);

                    if (respMessage != null)
                    {
                        var json = respMessage.Content.ReadAsStringAsync().Result;
                        var dt = JObject.Parse(json);

                        var t = dt.GetValue("Message");
                        if (dt.GetValue("Message") != null)
                            data = (JContainer)JsonConvert.DeserializeObject(dt.GetValue("Message").ToString(), _JsonSerializerSettings);
                        else
                            data = (JContainer)JsonConvert.DeserializeObject(JObject.Parse(json).GetValue("Data").ToString(), _JsonSerializerSettings);

                    }
                }
                catch (System.Exception ex)
                {
                    DebugErrorLog.Log(ex);
                }
            }

            return data;
        }

        public async Task<JContainer> PutAsync(string path, bool Token, HttpContent content)
        {
            HttpResponseMessage respMessage = null;
            JContainer data = null;

            using (HttpClient client = new HttpClient())
            {
                DefaultHeaders(client);

                CreateTokenHeader(client, Token);

                try
                {
                    respMessage = await client.PutAsync($"{BASEURL}/{path}", content);

                    if (respMessage != null)
                    {
                        var json = respMessage.Content.ReadAsStringAsync().Result;
                        var dt = JObject.Parse(json);

                        var t = dt.GetValue("Message");
                        if (dt.GetValue("Message") != null)
                            data = (JContainer)JsonConvert.DeserializeObject(dt.GetValue("Message").ToString(), _JsonSerializerSettings);
                        else
                            data = (JContainer)JsonConvert.DeserializeObject(JObject.Parse(json).GetValue("Data").ToString(), _JsonSerializerSettings);

                    }
                }
                catch (System.Exception ex)
                {
                    DebugErrorLog.Log(ex);
                }
            }

            return data;
        }

        public async Task<JContainer> DeleteAsync(string path, bool Token)
        {
            HttpResponseMessage respMessage = null;
            JContainer data = null;

            using (HttpClient client = new HttpClient())
            {
                DefaultHeaders(client);

                CreateTokenHeader(client, Token);

                try
                {
                    respMessage = await client.DeleteAsync($"{BASEURL}/{path}");

                    if (respMessage != null)
                    {
                        var json = respMessage.Content.ReadAsStringAsync().Result;
                        var dt = JObject.Parse(json);

                        var t = dt.GetValue("Message");
                        if (dt.GetValue("Message") != null)
                            data = (JContainer)JsonConvert.DeserializeObject(dt.GetValue("Message").ToString(), _JsonSerializerSettings);
                        else
                            data = (JContainer)JsonConvert.DeserializeObject(JObject.Parse(json).GetValue("Data").ToString(), _JsonSerializerSettings);

                    }
                }
                catch (System.Exception ex)
                {
                    DebugErrorLog.Log(ex);
                }
            }

            return data;
        }
        public async Task<T> GetFullURLAsync<T>(string path, bool Token, TimeSpan? timeout = null)
        {
            HttpResponseMessage respMessage = null;
            T data = default;

            using (HttpClient client = new HttpClient())
            {
                DefaultHeaders(client);

                CreateTokenHeader(client, Token);
                try
                {
                    if (timeout == null)
                        client.Timeout = new System.TimeSpan(0, 0, 1000);
                    else
                        client.Timeout = (TimeSpan)timeout;
                    respMessage = await client.GetAsync(path);

                    if (respMessage != null)
                    {
                        var json = respMessage.Content.ReadAsStringAsync().Result;

                        data = JsonConvert.DeserializeObject<T>(json, _JsonSerializerSettings);
                    }

                }
                catch (Exception e)
                {
                    DebugErrorLog.Log(e);
                }
            }

            return data;
        }
    }
}
