﻿
using Hino.App.Cross.Services.API.Interfaces;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Hino.App.Cross.Models;
using Hino.App.Cross.Utils.Exceptions;
using Hino.App.Cross.Models.General;
using Hino.App.Cross.Models.Auth;
using Hino.App.Cross.Utils.Attributes;
using Hino.App.Cross.Utils.Paging;

namespace Hino.App.Cross.Services.API
{
    public class BaseAPI<T> : IBaseAPI<T> where T : BaseEntity
    {
        protected readonly Request _Request;
        public List<ModelException> Errors { get => _Request.Errors; set => _Request.Errors = value; }
        public JsonSerializerSettings _JsonSerializerSettings { get => _Request._JsonSerializerSettings; }
        public JsonSerializer _JsonSerializer { get => _Request._JsonSerializer; }

        public UserLoggedModel User;
        public readonly string EndPoint;
        public string BASEURL
        {
            get => _Request.BASEURL;
        }

        public BaseAPI()
        {
            _Request = new Request();
            User = CurrentApp.USERLOGGED;
            try
            {
                Type baseType = typeof(T);
                EndPoint =
                    ((EndPointAttribute)baseType.GetCustomAttributes(typeof(EndPointAttribute), false).FirstOrDefault()).EndPoint;
            }
            catch (Exception)
            {
                throw new NotImplementedException("FALTOU O EndPointAttribute BAHIA !!!");
            }
        }

        public async Task<PagedResult<T>> GetAllSyncAsync(DateTime pDate)
        {
            var result =
                await _Request.GetAsync(
                    $"{EndPoint.Replace("{pEstablishmentKey}", User.EstablishmentKey)}/sync/date/{pDate.ToString("ddMMyyyyHHmmss")}", true);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<PagedResult<T>>(this._JsonSerializer);
        }

        public async Task<PagedResult<T>> GetAllAsync()
        {
            var result =
                await _Request.GetAsync(
                    $"{EndPoint.Replace("{pEstablishmentKey}", User.EstablishmentKey)}/all", true);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<PagedResult<T>>(this._JsonSerializer);
        }

        public async Task<T> GetByIdAsync(string pUniqueKey, long pId)
        {
            var result =
            await _Request.GetAsync(
                $"{EndPoint.Replace("{pEstablishmentKey}", User.EstablishmentKey)}/key/{pUniqueKey}/id{pId}", true);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<T>(this._JsonSerializer);
        }

        public async Task<T> PostAsync(T model)
        {
            var content = new StringContent(
                JsonConvert.SerializeObject(model), 
                Encoding.UTF8, "application/json");

            var result =
               await _Request.PostAsync(
                   $"{EndPoint.Replace("{pEstablishmentKey}", User.EstablishmentKey)}/save", true, content);
            
            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<T>(this._JsonSerializer);
        }

        public async Task<T> PutAsync(T model, string pUniqueKey, long id)
        {
            var content = new StringContent(
                   JsonConvert.SerializeObject(model),
                   Encoding.UTF8, "application/json");

            var result =
               await _Request.PutAsync(
                   $"{EndPoint.Replace("{pEstablishmentKey}", User.EstablishmentKey)}/key/{pUniqueKey}/id/{id}/save", true, content);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<T>(this._JsonSerializer);
        }

        public async Task<T> DeleteAsync(string pUniqueKey, long id)
        {
            var result =
               await _Request.DeleteAsync(
                   $"{EndPoint.Replace("{pEstablishmentKey}", User.EstablishmentKey)}/key/{pUniqueKey}/id/{id}/delete", true);

            return null;
        }

        public async Task<bool> ConectionTest()
        {
            var result =
              await _Request.GetAsync(
                  $"Test", false, new TimeSpan(0, 0, 5));

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return false;
            }

            return true;
        }
    }
}
