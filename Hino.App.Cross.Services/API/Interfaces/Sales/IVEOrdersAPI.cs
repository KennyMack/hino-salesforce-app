﻿using Hino.App.Cross.Models.Sales;
using Hino.App.Cross.Utils.Paging;
using System;
using System.Threading.Tasks;

namespace Hino.App.Cross.Services.API.Interfaces.Sales
{
    public interface IVEOrdersAPI : IBaseAPI<VEOrdersModel>
    {
        Task<PagedResult<VEOrdersModel>> GetAllSyncUserAsync(long pUserId, DateTime pDate);
    }
}
