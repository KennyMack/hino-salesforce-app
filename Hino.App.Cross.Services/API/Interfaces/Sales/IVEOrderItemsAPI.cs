﻿using Hino.App.Cross.Models.Sales;

namespace Hino.App.Cross.Services.API.Interfaces.Sales
{
    public interface IVEOrderItemsAPI : IBaseAPI<VEOrderItemsModel>
    {
    }
}
