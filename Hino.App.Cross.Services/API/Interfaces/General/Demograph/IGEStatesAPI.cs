﻿using Hino.App.Cross.Models.General.Demograph;

namespace Hino.App.Cross.Services.API.Interfaces.General.Demograph
{
    public interface IGEStatesAPI : IBaseAPI<GEStatesModel>
    {
    }
}
