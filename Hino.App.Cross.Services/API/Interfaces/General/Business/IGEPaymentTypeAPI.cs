﻿using Hino.App.Cross.Models.General.Business;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.App.Cross.Services.API.Interfaces.General.Business
{
    public interface IGEPaymentTypeAPI : IBaseAPI<GEPaymentTypeModel>
    {
    }
}
