﻿using Hino.App.Cross.Models.General.Business;
using Hino.App.Cross.Utils.Paging;
using System;
using System.Threading.Tasks;

namespace Hino.App.Cross.Services.API.Interfaces.General.Business
{
    public interface IGEEnterprisesAPI : IBaseAPI<GEEnterprisesModel>
    {
        Task<PagedResult<GEEnterprisesModel>> GetAllSyncByUserIdAsync(DateTime pDate, long pUserId);
    }
}
