﻿using Hino.App.Cross.Models.General.Business;

namespace Hino.App.Cross.Services.API.Interfaces.General.Business
{
    public interface IGEPaymentConditionAPI : IBaseAPI<GEPaymentConditionModel>
    {
    }
}
