﻿using Hino.App.Cross.Models.General;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.App.Cross.Services.API.Interfaces.General
{
    public interface IGEEstablishmentsAPI : IBaseAPI<GEEstablishmentsModel>
    {
        Task<GEEstablishmentsModel> MyEstabAsync(string pEstablishment);
    }
}
