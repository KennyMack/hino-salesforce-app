﻿using Hino.App.Cross.Models;
using Hino.App.Cross.Models.Auth;
using Hino.App.Cross.Models.General;
using System.Threading.Tasks;

namespace Hino.App.Cross.Services.API.Interfaces.General
{
    public interface IUsersAPI: IBaseAPI<GEUsersModel>
    {
        Task<DefaultResultModel> CreateUserAsync(RegisterModel model);
    }
}
