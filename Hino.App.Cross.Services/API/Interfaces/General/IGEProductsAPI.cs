﻿using Hino.App.Cross.Models.General;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.App.Cross.Services.API.Interfaces.General
{
    public interface IGEProductsAPI : IBaseAPI<GEProductsModel>
    {
    }
}
