﻿using Hino.App.Cross.Models;
using Hino.App.Cross.Models.Auth;
using System.Threading.Tasks;

namespace Hino.App.Cross.Services.API.Interfaces.Auth
{
    public interface IAuthAPI: IBaseAPI<LoginModel>
    {
        Task<AuthResultModel> AuthenticateAsync(LoginModel pLogin);
        Task<AuthResultModel> RefreshTokenAsync();
        Task<DefaultResultModel> Me();
    }
}
