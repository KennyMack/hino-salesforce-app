﻿using Hino.App.Cross.Utils.Exceptions;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Hino.App.Cross.Services.API.Interfaces
{
    public interface IRequest
    {
        List<ModelException> Errors { get; set; }
        Task<JContainer> GetAsync(string path, bool Token, TimeSpan? timeout = null);
        Task<JContainer> PostAsync(string path, bool Token, HttpContent content);
        Task<JContainer> PutAsync(string path, bool Token, HttpContent content);
        Task<JContainer> DeleteAsync(string path, bool Token);
    }
}
