﻿using Hino.App.Cross.Models;
using Hino.App.Cross.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.App.Cross.Services.API.Interfaces
{
    public interface IBaseAPI<T> where T : BaseEntity
    {
        string BASEURL { get; }
        Task<PagedResult<T>> GetAllSyncAsync(DateTime pDate);
        Task<PagedResult<T>> GetAllAsync();
        Task<T> GetByIdAsync(string pUniqueKey, long pId);
        Task<T> PostAsync(T model);
        Task<T> PutAsync(T model, string pUniqueKey, long id);
        Task<T> DeleteAsync(string pUniqueKey, long id);
        Task<bool> ConectionTest();
    }
}
