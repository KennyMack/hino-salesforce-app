﻿using Hino.App.Cross.Models;
using Hino.App.Cross.Models.Auth;
using Hino.App.Cross.Resources;
using Hino.App.Cross.Services.API.Interfaces.Auth;
using Hino.App.Cross.Services.ViewModels.Auth;
using Hino.App.Cross.Utils.Exceptions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Hino.App.Cross.Services.API.Auth
{
    public class AuthAPI: BaseAPI<LoginModel>, IAuthAPI
    {
        //#region Authenticate
        //public async Task<JContainer> Authenticate(Usuarios usuario)
        //{
        //    HttpContent content = new FormUrlEncodedContent(new[]
        //    {
        //        new KeyValuePair<String, String>("codusuario", usuario.codusuario.ToUpper()),
        //        new KeyValuePair<String, String>("senha", usuario.senha)
        //    });
        //
        //
        //    return await Request.PostAsync(string.Format(UrlsAuth.POSTAUTHENTICATE, App.CodEstab), content);
        //
        //    /*var sucesso = (bool)data["success"];
        //    if (sucesso)
        //    {
        //        var result = data["data"]["codusuario"].ToString();
        //
        //       // usuario.codestab = 
        //    }
        //    
        //
        //    return usuario;*/
        //}
        //#endregion

        public async Task<AuthResultModel> AuthenticateAsync(LoginModel pLogin)
        {                       
            AuthResultModel result = new AuthResultModel();

            using (HttpClient client = new HttpClient())
            {
                HttpContent content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("username", pLogin.UserOrEmail.IndexOf("@") < 0 ? pLogin.UserOrEmail : ""),
                    new KeyValuePair<string, string>("email", pLogin.UserOrEmail.IndexOf("@") >= 0 ? pLogin.UserOrEmail : ""),
                    new KeyValuePair<string, string>("password", pLogin.Password),
                    new KeyValuePair<string, string>("grant_type", "password"),
                    new KeyValuePair<string, string>("establishmentkey", pLogin.EstablishmentKey),
                    new KeyValuePair<string, string>("origin_login", "MOBILE"),
                });

                client.Timeout = new System.TimeSpan(0, 0, 1000);
                client.DefaultRequestHeaders.Accept.Add(
                    new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded")
                );                

                try
                {
                    var cont = content.ReadAsStringAsync().Result;
                    var url = $"{base.BASEURL}/{base.EndPoint}";
                    var respMessage = await client.PostAsync(url, content);

                    if (respMessage != null)
                    {
                        var json = respMessage.Content.ReadAsStringAsync().Result;
                        result = JsonConvert.DeserializeObject<AuthResultModel>(json, base._JsonSerializerSettings);
                    }
                    else
                    {
                        result.error = "no-comunication";
                        result.error_description = Messages.NoComunication;
                    }
                }
                catch (System.Exception ex)
                {
                    DebugErrorLog.Log(ex);
                    result.error = "no-comunication";
                    result.error_description = ex.Message;
                }
            }

            return result;
        }

        public async Task<DefaultResultModel> Me()
        {
            try
            {
                var result = await _Request.GetAsync("Auth/Mordor/me", true);

                return result.ToObject<DefaultResultModel>(base._JsonSerializer);
            }
            catch(Exception ex)
            {
                DebugErrorLog.Log(ex);
                var errors = new List<ModelException>();
                errors.Add(new ModelException
                {
                    ErrorCode = 20001,
                    Field = "Me",
                    Messages = new string[] { "Sem conexão" },
                    Value = ""

                });

                return new DefaultResultModel
                {
                    data = null,
                    error = errors,
                    status = 400,
                    success = false
                };
            }
        }

        public async Task<AuthResultModel> RefreshTokenAsync()
        {
            AuthResultModel result = new AuthResultModel();

            using (HttpClient client = new HttpClient())
            {
                HttpContent content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("refresh_token", CurrentApp.TOKENDATA.RefreshToken),
                    new KeyValuePair<string, string>("establishmentkey", CurrentApp.TOKENDATA.EstablishmentKey),
                    new KeyValuePair<string, string>("grant_type", "refresh_token"),
                    new KeyValuePair<string, string>("origin_login", "MOBILE"),
                });

                client.Timeout = new System.TimeSpan(0, 0, 1000);
                client.DefaultRequestHeaders.Accept.Add(
                    new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded")
                );

                try
                {
                    var cont = content.ReadAsStringAsync().Result;
                    var url = $"{base.BASEURL}/{base.EndPoint}";
                    var respMessage = await client.PostAsync(url, content);

                    if (respMessage != null)
                    {
                        var json = respMessage.Content.ReadAsStringAsync().Result;
                        result = JsonConvert.DeserializeObject<AuthResultModel>(json, base._JsonSerializerSettings);
                    }
                    else
                    {
                        result.error = "no-comunication";
                        result.error_description = Messages.NoComunication;
                    }
                }
                catch (System.Exception ex)
                {
                    DebugErrorLog.Log(ex);
                    result.error = "no-comunication";
                    result.error_description = ex.Message;
                }
            }

            return result;
        }
    }
}
