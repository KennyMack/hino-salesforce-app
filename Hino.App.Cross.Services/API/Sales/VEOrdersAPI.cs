﻿using Hino.App.Cross.Models;
using Hino.App.Cross.Models.Auth;
using Hino.App.Cross.Models.Sales;
using Hino.App.Cross.Services.API.Interfaces.Sales;
using Hino.App.Cross.Utils.Paging;
using System;
using System.Threading.Tasks;

namespace Hino.App.Cross.Services.API.Sales
{
    public class VEOrdersAPI : BaseAPI<VEOrdersModel>, IVEOrdersAPI
    {
        public async Task<PagedResult<VEOrdersModel>> GetAllSyncUserAsync(long pUserId, DateTime pDate)
        {
            var result =
                await _Request.GetAsync(
                    $"{EndPoint.Replace("{pEstablishmentKey}", User.EstablishmentKey)}/user/{pUserId}/sync/date/{pDate.ToString("ddMMyyyyHHmmss")}", true);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<PagedResult<VEOrdersModel>>(this._JsonSerializer);
        }

    }
}
