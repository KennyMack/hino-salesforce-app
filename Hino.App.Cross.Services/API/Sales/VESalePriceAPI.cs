﻿using Hino.App.Cross.Models.Sales;
using Hino.App.Cross.Services.API.Interfaces.Sales;

namespace Hino.App.Cross.Services.API.Sales
{
    public class VESalePriceAPI : BaseAPI<VESalePriceModel>, IVESalePriceAPI
    {
    }
}
