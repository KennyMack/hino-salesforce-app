﻿using Hino.App.Cross.Models.General.Demograph;
using Hino.App.Cross.Services.API.Interfaces.General.Demograph;

namespace Hino.App.Cross.Services.API.General.Demograph
{
    public class GEStatesAPI : BaseAPI<GEStatesModel>, IGEStatesAPI
    {
    }
}
