﻿using Hino.App.Cross.Models;
using Hino.App.Cross.Models.General;
using Hino.App.Cross.Services.API.Interfaces.General;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.App.Cross.Services.API.General
{
    public class GEEstablishmentsAPI : BaseAPI<GEEstablishmentsModel>, IGEEstablishmentsAPI
    {
        public async Task<GEEstablishmentsModel> MyEstabAsync(string pEstablishment)
        {
            var result =
                await _Request.GetAsync(
                    $"{EndPoint.Replace("{pEstablishmentKey}", pEstablishment)}/my-estab", true);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<GEEstablishmentsModel>(this._JsonSerializer);
        }
    }
}
