﻿using Hino.App.Cross.Models;
using Hino.App.Cross.Models.Auth;
using Hino.App.Cross.Models.General;
using Hino.App.Cross.Services.API.Interfaces.General;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Hino.App.Cross.Services.API.General
{
    public class UsersAPI : BaseAPI<GEUsersModel>, IUsersAPI
    {
        public async Task<DefaultResultModel> CreateUserAsync(RegisterModel model)
        {
            var content = new StringContent(
                JsonConvert.SerializeObject(model),
                Encoding.UTF8, "application/json");

            var url = $"{base.EndPoint.Replace("{pEstablishmentKey}", model.EstablishmentKey)}/mobile/create";
            var ret = await _Request.PostAsync(url, false, content);  
            return ret.ToObject<DefaultResultModel>(base._JsonSerializer);
        }
    }
}
