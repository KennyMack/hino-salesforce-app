﻿using Hino.App.Cross.Models.General.Business;
using Hino.App.Cross.Services.API.Interfaces.General.Business;

namespace Hino.App.Cross.Services.API.General.Business
{
    public class GEPaymentTypeAPI : BaseAPI<GEPaymentTypeModel>, IGEPaymentTypeAPI
    {
    }
}
