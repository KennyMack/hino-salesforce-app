﻿using Hino.App.Cross.Models;
using Hino.App.Cross.Models.Auth;
using Hino.App.Cross.Models.General.Business;
using Hino.App.Cross.Services.API.Interfaces.General.Business;
using Hino.App.Cross.Utils.Paging;
using System;
using System.Threading.Tasks;

namespace Hino.App.Cross.Services.API.General.Business
{
    public class GEEnterprisesAPI : BaseAPI<GEEnterprisesModel>, IGEEnterprisesAPI
    {
        public async Task<PagedResult<GEEnterprisesModel>> GetAllSyncByUserIdAsync(DateTime pDate, long pUserId)
        {
            var result =
                await _Request.GetAsync(
                    $"{EndPoint.Replace("{pEstablishmentKey}", User.EstablishmentKey)}/sync/date/{pDate.ToString("ddMMyyyyHHmmss")}/user/{pUserId}", true);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<PagedResult<GEEnterprisesModel>>(this._JsonSerializer);
        }
    }
}
