﻿using Hino.App.Cross.Services.API;
using Hino.App.Cross.Utils.Demograph;
using Hino.App.Cross.Utils.Exceptions;
using Hino.App.Cross.Utils.Extensions;
using Hino.App.Cross.Utils.Interfaces;
using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xamarin.Forms;
using Hino.App.Cross.Utils;
using Hino.App.Cross.Models;

namespace Hino.App.Cross.Services.Application.General
{
    public class CEPApplication
    {
        public async Task<CEPModel> GetZipCodeAsync(string pZipCode)
        {
            var zipCode = pZipCode;
            var instanceMsg = DependencyService.Get<IMessage>();

            if (string.IsNullOrEmpty(zipCode) ||
                zipCode.Length != 9)
            {
                instanceMsg.ShowShortMessage(Hino.App.Cross.Resources.Validations.InvalidZipCode);
                return null;
            }

            if (!CurrentApp.IsConnected)
            {
                instanceMsg.ShowShortMessage(Hino.App.Cross.Resources.Messages.NoInternetConnection);
                return null;
            }

            try
            {
                var _Request = new Request();
                var result = await _Request.GetAsync($"Cep/{zipCode.OnlyNumbers()}", false);

                if (result == null)
                    throw new Exception("CEP NOT FOUND");

                var data = result.ToObject<DefaultResultModel>(_Request._JsonSerializer);

                if (!data.success)
                    throw new Exception("CEP NOT FOUND");

                return data.data.ToObject<CEPModel>(_Request._JsonSerializer);
            }
            catch (Exception ex)
            {
                DebugErrorLog.Log(ex);
                instanceMsg.ShowShortMessage(Hino.App.Cross.Resources.Messages.InvalidZipCode);
            }

            return null;
        }

        public async Task<CEPModel> OpenZipCodeInMapAsync(string pZipCode, string pNumber)
        {
            var instanceMsg = DependencyService.Get<IMessage>();
            var CepModel = await GetZipCodeAsync(pZipCode);

            if (CepModel != null)
            {
                CepModel.number = pNumber;
                var message = await UtilsExtensions.OpenMapAsync(CepModel);

                if (!string.IsNullOrEmpty(message))
                    instanceMsg.ShowShortMessage(message);
            }
            else
                instanceMsg.ShowShortMessage(Hino.App.Cross.Resources.Messages.InvalidZipCode);

            return CepModel;
        }
    }
}
