﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.App.Cross.Services.Application.Auth
{
    public class RegisterUserData
    {
        public string UserOrEmail { get; set; }
        public string EstablishmentKey { get; set; }
    }
}
