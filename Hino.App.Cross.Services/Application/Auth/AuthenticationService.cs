﻿using Hino.App.Cross.Models;
using Hino.App.Cross.Models.Auth;
using Hino.App.Cross.Models.General;
using Hino.App.Cross.Services.API.Interfaces.Auth;
using Hino.App.Cross.Services.API.Interfaces.General;
using Hino.App.Cross.Services.DB.Interfaces;
using Hino.App.Cross.Services.DB.Interfaces.Auth;
using Hino.App.Cross.Services.DB.Interfaces.General;
using Hino.App.Cross.Services.ViewModels.Auth;
using Hino.App.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.App.Cross.Services.Application.Auth
{
    public class AuthenticationService
    {
        public async Task<AuthResultModel> LoginAsync(LoginModel pLoginModel)
        {
            var _IAuthAPI = DInjection.GetIntance<IAuthAPI>();
            var ret = await _IAuthAPI.AuthenticateAsync(pLoginModel);

            if (!string.IsNullOrEmpty(ret.error))
            {
                return new AuthResultModel
                {
                    success = false,
                    error = ret.error,
                    error_description = ret.error_description
                };
            }

            ret.success = true;

            CurrentApp.TOKENDATA = new TokenData
            {
                EstablishmentKey = pLoginModel.EstablishmentKey,
                UserToken = ret.access_token,
                RefreshToken = ret.refresh_token,
                UserKey = ret.userkey,
                SessionId = ret.sessionid
            };

            CurrentApp.EstablishmentKeyLogged = CurrentApp.TOKENDATA.EstablishmentKey;
            CurrentApp.UserKeyLogged = CurrentApp.TOKENDATA.UserKey;

            return ret;
        }

        public async Task<AuthResultModel> RefreshLoginAsync()
        {
            var _IAuthAPI = DInjection.GetIntance<IAuthAPI>();
            var ret = await _IAuthAPI.RefreshTokenAsync();

            if (!string.IsNullOrEmpty(ret.error))
            {
                return new AuthResultModel
                {
                    success = false,
                    error = ret.error,
                    error_description = ret.error_description
                };
            }

            ret.success = true;

            CurrentApp.TOKENDATA = new TokenData
            {
                EstablishmentKey = ret.establishmentkey,
                UserToken = ret.access_token,
                RefreshToken = ret.refresh_token,
                UserKey = ret.userkey,
                SessionId = ret.sessionid
            };

            CurrentApp.EstablishmentKeyLogged = CurrentApp.TOKENDATA.EstablishmentKey;
            CurrentApp.UserKeyLogged = CurrentApp.TOKENDATA.UserKey;

            return ret;
        }

        public async Task<GEUsersModel> GetMe()
        {
            var _IAuthAPI = DInjection.GetIntance<IAuthAPI>();
            var me = await _IAuthAPI.Me();
            if (!me.success)
                return null;

            return me.data.ToObject<GEUsersModel>();
        }

        public async Task<string> SetUserLogged()
        {
            var _IUserLoggedDB = DInjection.GetIntance<IUserLoggedDB>();
            var _IAuthAPI = DInjection.GetIntance<IAuthAPI>();
            var me = await _IAuthAPI.Me();

            if (!me.success)
                return me.error[0].Messages.ToArray()[0];

            var UserServer = me.data.ToObject<GEUsersModel>();

            await _IUserLoggedDB.AddOrUpdateItemAsync(new UserLoggedModel
            {
                Id = UserServer.Id,
                EstablishmentKey = UserServer.EstablishmentKey,
                UniqueKey = UserServer.UniqueKey,
                UserName = UserServer.UserName,
                Email = UserServer.Email,
                SessionId = CurrentApp.TOKENDATA.SessionId,
                UserToken = CurrentApp.TOKENDATA.UserToken,
                RefreshToken = CurrentApp.TOKENDATA.RefreshToken,
                LastLogin = Convert.ToDateTime(UserServer.LastLogin),
                UserType = Convert.ToInt32(UserServer.UserType),
                PercDiscount = UserServer.PercDiscount,
                PercCommission = UserServer.PercCommission,
                UserKey = UserServer.UserKey
            });

            return "";
        }

        public async Task<bool> SetEstablishmentLoggedAsync(string pEstablishment)
        {
            try
            {
                var EstabDB = DInjection.GetIntance<IGEEstablishmentDB>();

                var _IGEEstablishmentsAPI = DInjection.GetIntance<IGEEstablishmentsAPI>();
                var estab = await _IGEEstablishmentsAPI.MyEstabAsync(pEstablishment);
                if (estab != null)
                {
                    await EstabDB.AddOrUpdateItemAsync(estab);

                    return true;
                }
            }
            catch (Exception ex)
            {
                DebugErrorLog.Log(ex);
            }
            return false;
        }

        public async Task<bool> SetPreferencesDefaultAsync()
        {
            var _IPreferencesDB = DInjection.GetIntance<IPreferencesDB>();
            var _ConfigModel = await _IPreferencesDB.GetItemAsync(1);

            if (_ConfigModel != null)
                return true;

            try
            {
                await _IPreferencesDB.AddOrUpdateItemAsync(new ConfigModel
                {
                    Id = 1,
                    UniqueKey = Guid.NewGuid().ToString(),
                    EstablishmentKey = Guid.NewGuid().ToString(),
                    SyncOnSave = true,
                    SyncOnStart = true,
                    SyncWIFI = false,
                    IntervalSync = 30,
                    FirstAccess = true,
                    FirstSyncOk = false,
                    UseSecurity = false,
                    LastSync = new DateTime(1900, 1, 1, 1, 1, 1)
                });
            }
            catch (Exception ex)
            {
                DebugErrorLog.Log(ex);
                return false;
            }

            return true;
        }


        #region Check and refresh token
        public async Task<bool> CheckAndRefreshToken()
        {
            var me = await GetMe();

            if (me == null)
            {
                var refresh = await RefreshLoginAsync();
                return refresh.success;
            }
            else
                return true;
        }
        #endregion

        #region Check connection with server
        public async Task<bool> CheckConnectionWithServer()
        {
            var _IAuthAPI = DInjection.GetIntance<IAuthAPI>();
            return await _IAuthAPI.ConectionTest();
        }
        #endregion


        public async Task<DefaultResultModel> CreateUserAsync(RegisterModel model)
        {
            var _UsersAPI = DInjection.GetIntance<IUsersAPI>();
            return await _UsersAPI.CreateUserAsync(model);
        }
    }
}
