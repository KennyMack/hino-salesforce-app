﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Plugin.Fingerprint.Abstractions;
using System.Threading.Tasks;

namespace Hino.App.Cross.Services.Application.Auth
{
    public class AuthFingerPrint
    {
        private CancellationTokenSource _cancel;
        public event EventHandler OnAuthResult;
        public AuthenticationType AuthType
        {
            get;
            private set;
        }

        public AuthFingerPrint()
        {
            AuthType = Task.Run(async () =>  await Plugin.Fingerprint.CrossFingerprint.Current.GetAuthenticationTypeAsync()).Result;
        }

        public async Task AuthenticateAsync(string cancel = null, string fallback = null, string tooFast = null)
        {
            _cancel = new CancellationTokenSource(TimeSpan.FromSeconds(10));

            var dialogConfig = new AuthenticationRequestConfiguration(Hino.App.Cross.Resources.PageTitle.FingerPrintTitle, "")
            { // all optional
                CancelTitle = cancel,
                FallbackTitle = fallback,
                
                AllowAlternativeAuthentication = false
            };

            // optional
            //dialogConfig.HelpTexts.MovedTooFast = tooFast;

            var result = await Plugin.Fingerprint.CrossFingerprint.Current.AuthenticateAsync(dialogConfig, _cancel.Token);

            OnAuthResult?.Invoke(result, new EventArgs());
        }
    }
}
