﻿using Hino.App.Cross.Models;
using Hino.App.Cross.Services.DB.Interfaces;
using System;
using System.Threading.Tasks;

namespace Hino.App.Cross.Services.DB
{
    public class PreferencesDB : BaseDataStore<ConfigModel>, IPreferencesDB
    {
        public override async Task<ConfigModel> GetItemAsync(long id)
        {
            var Preference = await base.GetItemAsync(id);

            if (Preference == null)
            {
                Preference = new ConfigModel
                {
                    Id = 1,
                    UniqueKey = Guid.NewGuid().ToString(),
                    EstablishmentKey = Guid.NewGuid().ToString(),
                    SyncOnSave = true,
                    SyncOnStart = true,
                    SyncWIFI = false,
                    IntervalSync = 30,
                    FirstAccess = true,
                    FirstSyncOk = false,
                    UseSecurity = false,
                    LastSync = new DateTime(1900, 1, 1, 1, 1, 1)
                };
            }

            return Preference;
        }

        public async Task<ConfigModel> UpdateLastSync(DateTime pDate)
        {
            var model = await GetItemAsync(1);
            model.LastSync = pDate;
            await UpdateItemAsync(model);

            return model;
        }
    }
}
