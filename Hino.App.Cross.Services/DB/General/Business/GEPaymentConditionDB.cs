﻿using Hino.App.Cross.Models.General.Business;
using Hino.App.Cross.Services.DB.Interfaces.General.Business;
using Hino.App.Cross.Utils.Database;
using Hino.App.Cross.Utils.Paging;
using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.App.Cross.Services.DB.General.Business
{
    public class GEPaymentConditionDB : BaseDataStore<GEPaymentConditionModel>, IGEPaymentConditionDB
    {
        public async override Task<PagedResult<GEPaymentConditionModel>> Search(string pSearchData, int page)
        {
            var _DataBase = _LiteDBConn.GetDataBase();
            var _Collection = _DataBase.GetCollection<GEPaymentConditionModel>();

            var Result = _Collection
                .Query()
                .Where(r => r.UserKey == CurrentApp.UserKeyLogged)
                .Include(x => x.GEPaymentType)
                .OrderBy(r => r.Id)
                .ToList();

            if (!string.IsNullOrEmpty(pSearchData))
            {
                var num = ConvertSearchToNumber(pSearchData);
                Result = Result.Where(r =>
                    r.UserKey == CurrentApp.UserKeyLogged &&
                    (
                    r.Description.ToLower().Contains(pSearchData.ToLower()) ||
                    r.GEPaymentType.Description.ToLower().Contains(pSearchData.ToLower()) ||
                    r.Id == num))
                    .OrderBy(r => r.Id)
                    .ToList();
            }

            if (page > 0)
                Result = Result.Skip((page - 1) * 50).Take(50).ToList();

                return await Task.Run(() => PaginateQuery(Result, page));
        }

        public override async Task<PagedResult<GEPaymentConditionModel>> Search(Expression<Func<GEPaymentConditionModel, bool>> predicate,
           SortResult sort, string pSearchData, int page) =>
            await Search(pSearchData, page);

        public async override Task<GEPaymentConditionModel> AddOrUpdateItemAsync(GEPaymentConditionModel item)
        {
            var _DataBase = _LiteDBConn.GetDataBase();
            var _Collection = _DataBase.GetCollection<GEPaymentConditionModel>();

            await Task.Run(() => _Collection.Upsert(item));

            return item;
        }

        public override async Task<GEPaymentConditionModel> AddItemAsync(GEPaymentConditionModel item)
        {
            var _DataBase = _LiteDBConn.GetDataBase();
            var _Collection = _DataBase.GetCollection<GEPaymentConditionModel>();
            if (item.Id < 0)
            {
                item.Created = DateTime.Now;
                item.Modified = DateTime.Now;
            }

            await Task.Run(() => _Collection.Insert(item));
            return item;
        }

        public override async Task<GEPaymentConditionModel> UpdateItemAsync(GEPaymentConditionModel item)
        {
            var _DataBase = _LiteDBConn.GetDataBase();
            var _Collection = _DataBase.GetCollection<GEPaymentConditionModel>();
            item.Modified = DateTime.Now;
            await Task.Run(() => _Collection.Update(item));

            return item;
        }

        public override async Task<GEPaymentConditionModel> GetItemAsync(long id)
        {
            var _DataBase = _LiteDBConn.GetDataBase();
            var _Collection = _DataBase.GetCollection<GEPaymentConditionModel>();
            return await Task.Run(() => (_Collection.Query().Where(r =>
                r.Id == id &&
                r.UserKey == CurrentApp.UserKeyLogged)).FirstOrDefault());
        }

        public override async Task AddFromPagedResult(PagedResult<GEPaymentConditionModel> result)
        {
            var _DataBase = _LiteDBConn.GetDataBase();
            var _Collection = _DataBase.GetCollection<GEPaymentConditionModel>();

            result.Results.All(r => { r.UserKey = CurrentApp.UserKeyLogged; return true; });

            await Task.Run(() => _Collection.Upsert(result.Results));
        }

        public override async Task<PagedResult<GEPaymentConditionModel>> GetItemsAsync(int page)
        {
            var _DataBase = _LiteDBConn.GetDataBase();
            var _Collection = _DataBase.GetCollection<GEPaymentConditionModel>();
            IList<GEPaymentConditionModel> Result = null;

            if (page > 0)
                Result = _Collection.Query()
                    .Where(r => r.UserKey == CurrentApp.UserKeyLogged)
                    .OrderByDescending(r => r.Id)
                    .ToList()
                    .Skip((page - 1) * 50)
                    .Take(50)
                    .ToList();
            else
                Result = _Collection.Query()
                    .Where(r => r.UserKey == CurrentApp.UserKeyLogged)
                    .OrderByDescending(r => r.Id)
                    .ToList();


            return await Task.Run(() => PaginateQuery(Result, page));
        }

        public override async Task<PagedResult<GEPaymentConditionModel>> Query(Expression<Func<GEPaymentConditionModel, bool>> predicate, int page)
        {
            var _DataBase = _LiteDBConn.GetDataBase();
            var _Collection = _DataBase.GetCollection<GEPaymentConditionModel>();
            IList<GEPaymentConditionModel> Result = null;

            if (page > 0)
                Result = _Collection.Query()
                    .Where(r => r.UserKey == CurrentApp.UserKeyLogged)
                    .Where(predicate).OrderByDescending(r => r.Id).ToList().Skip((page - 1) * 50).Take(50).ToList();
            else
                Result = _Collection.Query()
                    .Where(r => r.UserKey == CurrentApp.UserKeyLogged)
                    .Where(predicate).OrderByDescending(r => r.Id).ToList();

            return await Task.Run(() => PaginateQuery(Result, page));
        }
    }
}
