﻿using Hino.App.Cross.Models.General.Business;
using Hino.App.Cross.Services.DB.Interfaces.General.Business;
using Hino.App.Cross.Utils.Database;
using Hino.App.Cross.Utils.Paging;
using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms.Internals;

namespace Hino.App.Cross.Services.DB.General.Business
{
    public class GEEnterprisesDB : BaseDataStore<GEEnterprisesModel>, IGEEnterprisesDB
    {
        /*public override async Task<PagedResult<GEEnterprisesModel>> Query(Expression<Func<GEEnterprisesModel, bool>> predicate, int page)
        {
            if (page > 0)
                return PaginateQuery(
                    await _DataBase.Table<GEEnterprisesModel>().Where(predicate)
                    .OrderBy(r => r.StatusSinc)
                    .ThenByDescending(r => r.Id)
                    .Skip((page - 1) * 50)
                    .Take(50).ToListAsync(), page);

            return PaginateQuery(
                    await _DataBase.Table<GEEnterprisesModel>().Where(predicate)
                    .OrderBy(r => r.StatusSinc)
                    .ThenByDescending(r => r.Id)
                    .ToListAsync(), page);
        }

        public override async Task<PagedResult<GEEnterprisesModel>> GetItemsAsync(int page)
        {
            if (page > 0)
                return PaginateQuery(
                    await _DataBase.Table<GEEnterprisesModel>()
                        .OrderBy(r => r.StatusSinc)
                        .ThenByDescending(r => r.Id)
                        .Skip((page - 1) * 50)
                        .Take(50).ToListAsync(), page);

            return PaginateQuery(
                    await _DataBase.Table<GEEnterprisesModel>()
                        .OrderBy(r => r.StatusSinc)
                        .ThenByDescending(r => r.Id)
                        .ToListAsync(), page);
        }*/

        string GenerateInitials(GEEnterprisesModel item)
        {
            var name = item.RazaoSocial.Trim().Split(' ');
            var clName = string.Join("", name);

            var rand = new Random();
            var charA = (int)Math.Floor((decimal)rand.Next(0, clName.Length));

            return $"{item.RazaoSocial[0]}{clName[charA]}".ToUpper();

        }

        public override Task<GEEnterprisesModel> AddOrUpdateItemAsync(GEEnterprisesModel item)
        {
            if (string.IsNullOrEmpty(item.Initials) ||
                string.IsNullOrWhiteSpace(item.Initials))
                item.Initials = GenerateInitials(item);

            return base.AddOrUpdateItemAsync(item);
        }

        public override Task<GEEnterprisesModel> AddItemAsync(GEEnterprisesModel item)
        {
            if (string.IsNullOrEmpty(item.Initials) ||
                string.IsNullOrWhiteSpace(item.Initials))
                item.Initials = GenerateInitials(item);

            return base.AddItemAsync(item);
        }

        /*public override async Task<GEEnterprisesModel> UpdateItemAsync(GEEnterprisesModel item)
        {
            item.Modified = DateTime.Now;

            if (string.IsNullOrEmpty(item.Initials) ||
                string.IsNullOrWhiteSpace(item.Initials))
                item.Initials = GenerateInitials(item);

            await _DataBase.UpdateAsync(item);

            return item;
        }*/

        public override async Task<PagedResult<GEEnterprisesModel>> GetItemsAsync(int page)
        {
            var _DataBase = _LiteDBConn.GetDataBase();
            var _Collection = _DataBase.GetCollection<GEEnterprisesModel>();
            IList<GEEnterprisesModel> Result = null;

            if (page > 0)
                Result = _Collection
                    .Include(x => x.GEEnterpriseGeo)
                    .Query()
                    .Where(r => r.UserKey == CurrentApp.UserKeyLogged)
                    .OrderBy(r => r.RazaoSocial)
                    .ToList()
                    .Skip((page - 1) * 50)
                    .Take(50)
                    .ToList();
            else
                Result = _Collection
                    .Include(x => x.GEEnterpriseGeo)
                    .Query()
                    .Where(r => r.UserKey == CurrentApp.UserKeyLogged)
                    .OrderBy(r => r.RazaoSocial)
                    .ToList();


            return await Task.Run(() => PaginateQuery(Result, page));

        }

        public override async Task<PagedResult<GEEnterprisesModel>> Query(Expression<Func<GEEnterprisesModel, bool>> predicate, int page)
        {
            var _DataBase = _LiteDBConn.GetDataBase();
            var _Collection = _DataBase.GetCollection<GEEnterprisesModel>();
            IList<GEEnterprisesModel> Result = null;

            if (page > 0)
                Result = _Collection
                    .Include(x => x.GEEnterpriseGeo)
                    .Query()
                    .Where(r => r.UserKey == CurrentApp.UserKeyLogged)
                    .Where(predicate).OrderBy(r => r.RazaoSocial).ToList().Skip((page - 1) * 50).Take(50).ToList();
            else
                Result = _Collection
                    .Include(x => x.GEEnterpriseGeo)
                    .Query()
                    .Where(r => r.UserKey == CurrentApp.UserKeyLogged)
                    .Where(predicate).OrderBy(r => r.Id).ToList();

            return await Task.Run(() => PaginateQuery(Result, page));
        }

        public override async Task<PagedResult<GEEnterprisesModel>> Search(string pSearchData, int page)
        {
            if (!string.IsNullOrEmpty(pSearchData))
            {
                var num = ConvertSearchToNumber(pSearchData);
                return await Query(r =>
                    r.RazaoSocial.ToLower().Contains(pSearchData.ToLower()) ||
                    r.Id == num ||
                    r.Initials.ToLower().Contains(pSearchData.ToLower()) ||
                    r.CNPJCPF.ToLower().Contains(pSearchData.ToLower()) ||
                    r.NomeFantasia.ToLower().Contains(pSearchData.ToLower()), page);
            }
            else
                return await GetItemsAsync(page);
        }

        public override async Task<PagedResult<GEEnterprisesModel>> Search(Expression<Func<GEEnterprisesModel, bool>> predicate,
            SortResult sort, string pSearchData, int page)
        {
            var _DataBase = _LiteDBConn.GetDataBase();
            var _Collection = _DataBase.GetCollection<GEEnterprisesModel>("GEEnterprisesModel");

            IList<GEEnterprisesModel> Result = null;

            var qry = _Collection
                .Query()
                .Where(r => r.UserKey == CurrentApp.UserKeyLogged);

            if (!string.IsNullOrEmpty(pSearchData))
            {
                var num = ConvertSearchToNumber(pSearchData);
                qry = qry.Where(r =>
                        r.RazaoSocial.ToLower().Contains(pSearchData.ToLower()) ||
                        r.Id == num ||
                        r.Initials.ToLower().Contains(pSearchData.ToLower()) ||
                        r.CNPJCPF.ToLower().Contains(pSearchData.ToLower()) ||
                        r.NomeFantasia.ToLower().Contains(pSearchData.ToLower()));
            }

            if (predicate != null)
                qry = qry.Where(predicate);

            Result = qry.ToList();

            var Descending = false;
            Func<GEEnterprisesModel, object> exp = x => x.RazaoSocial;
            if (sort != null)
            {
                Descending = sort.Descending;
                exp = (Func<GEEnterprisesModel, object>)sort.Expression;
            }

            if (Descending)
                Result = Result.OrderByDescending(exp).ToList();
            else
                Result = Result.OrderBy(exp).ToList();

            if (page > 0)
                Result = Result.Skip((page - 1) * 50).Take(50).ToList();

            return await Task.Run(() => PaginateQuery(Result, page));
            /*if (predicate != null || !string.IsNullOrEmpty(pSearchData))
            {
                var num = ConvertSearchToNumber(pSearchData);
                var _DataBase = _LiteDBConn.GetDataBase();
                var _Collection = _DataBase.GetCollection<GEEnterprisesModel>();
                IList<GEEnterprisesModel> Result = null;

                var qry = _Collection.Query()
                        .Where(r => r.UserKey == CurrentApp.UserKeyLogged);

                if (!string.IsNullOrEmpty(pSearchData))
                    qry = qry.Where(r =>
                    r.RazaoSocial.ToLower().Contains(pSearchData.ToLower()) ||
                    r.Id == num ||
                    r.Initials.ToLower().Contains(pSearchData.ToLower()) ||
                    r.CNPJCPF.ToLower().Contains(pSearchData.ToLower()) ||
                    r.NomeFantasia.ToLower().Contains(pSearchData.ToLower()));

                if (predicate != null)
                    qry = qry.Where(predicate);

                Expression<Func<GEEnterprisesModel, object>> exp = (Expression<Func<GEEnterprisesModel, object>>)sort.Expression;

                if (sort.Descending)
                    Result = qry.ToList().OrderByDescending(ordering => exp.Body).ToList();
                else
                    Result = qry.ToList().OrderBy(ordering => exp.Body).ToList();

                if (page > 0)
                    Result = Result.Skip((page - 1) * 50).Take(50).ToList();

                return await Task.Run(() => PaginateQuery(Result, page));
            }
            else
                return await GetItemsAsync(page);*/
        }


        public async Task<PagedResult<GEEnterprisesModel>> GetAllNew()
        {
            return await Query(r => r.Id < 0, -1);
        }
        
        public override async Task AddFromPagedResult(PagedResult<GEEnterprisesModel> result)
        {
            foreach (var item in result.Results)
            {
                if (string.IsNullOrEmpty(item.Initials) ||
                    string.IsNullOrWhiteSpace(item.Initials))
                    item.Initials = GenerateInitials(item);
            }

            await base.AddFromPagedResult(result);
        }
    }
}
