﻿using Hino.App.Cross.Models.General.Business;
using Hino.App.Cross.Services.DB.Interfaces.General.Business;
using Hino.App.Cross.Utils.Database;
using Hino.App.Cross.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Hino.App.Cross.Services.DB.General.Business
{
    public class GEPaymentTypeDB : BaseDataStore<GEPaymentTypeModel>, IGEPaymentTypeDB
    {
        public async override Task<GEPaymentTypeModel> AddOrUpdateItemAsync(GEPaymentTypeModel item)
        {
            var _DataBase = _LiteDBConn.GetDataBase();
            var _Collection = _DataBase.GetCollection<GEPaymentTypeModel>();

            await Task.Run(() => _Collection.Upsert(item));

            return item;
        }

        public override async Task<GEPaymentTypeModel> AddItemAsync(GEPaymentTypeModel item)
        {
            var _DataBase = _LiteDBConn.GetDataBase();
            var _Collection = _DataBase.GetCollection<GEPaymentTypeModel>();
            if (item.Id < 0)
            {
                item.Created = DateTime.Now;
                item.Modified = DateTime.Now;
            }

            await Task.Run(() => _Collection.Insert(item));
            return item;
        }

        public override async Task<GEPaymentTypeModel> UpdateItemAsync(GEPaymentTypeModel item)
        {
            var _DataBase = _LiteDBConn.GetDataBase();
            var _Collection = _DataBase.GetCollection<GEPaymentTypeModel>();
            item.Modified = DateTime.Now;
            await Task.Run(() => _Collection.Update(item));

            return item;
        }

        public override async Task<GEPaymentTypeModel> GetItemAsync(long id)
        {
            var _DataBase = _LiteDBConn.GetDataBase();
            var _Collection = _DataBase.GetCollection<GEPaymentTypeModel>();
            return await Task.Run(() => (_Collection.Query().Where(r =>
                r.Id == id &&
                r.UserKey == CurrentApp.UserKeyLogged)).FirstOrDefault());
        }

        public override async Task AddFromPagedResult(PagedResult<GEPaymentTypeModel> result)
        {
            var _DataBase = _LiteDBConn.GetDataBase();
            var _Collection = _DataBase.GetCollection<GEPaymentTypeModel>();

            result.Results.All(r => { r.UserKey = CurrentApp.UserKeyLogged; return true; });

            await Task.Run(() => _Collection.Upsert(result.Results));
        }

        public override async Task<PagedResult<GEPaymentTypeModel>> GetItemsAsync(int page)
        {
            var _DataBase = _LiteDBConn.GetDataBase();
            var _Collection = _DataBase.GetCollection<GEPaymentTypeModel>();
            IList<GEPaymentTypeModel> Result = null;

            if (page > 0)
                Result = _Collection.Query()
                    .Where(r => r.UserKey == CurrentApp.UserKeyLogged)
                    .OrderByDescending(r => r.Id)
                    .ToList()
                    .Skip((page - 1) * 50)
                    .Take(50)
                    .ToList();
            else
                Result = _Collection.Query()
                    .Where(r => r.UserKey == CurrentApp.UserKeyLogged)
                    .OrderByDescending(r => r.Id)
                    .ToList();


            return await Task.Run(() => PaginateQuery(Result, page));
        }

        public override async Task<PagedResult<GEPaymentTypeModel>> Query(Expression<Func<GEPaymentTypeModel, bool>> predicate, int page)
        {
            var _DataBase = _LiteDBConn.GetDataBase();
            var _Collection = _DataBase.GetCollection<GEPaymentTypeModel>();
            IList<GEPaymentTypeModel> Result = null;

            if (page > 0)
                Result = _Collection.Query()
                    .Where(r => r.UserKey == CurrentApp.UserKeyLogged)
                    .Where(predicate).OrderByDescending(r => r.Id).ToList().Skip((page - 1) * 50).Take(50).ToList();
            else
                Result = _Collection.Query()
                    .Where(r => r.UserKey == CurrentApp.UserKeyLogged)
                    .Where(predicate).OrderByDescending(r => r.Id).ToList();

            return await Task.Run(() => PaginateQuery(Result, page));
        }

        public override async Task<PagedResult<GEPaymentTypeModel>> Search(string pSearchData, int page)
        {
            if (!string.IsNullOrEmpty(pSearchData))
            {
                var num = ConvertSearchToNumber(pSearchData);
                return await Query(r =>
                    r.Id == num && r.EstablishmentKey == CurrentApp.EstablishmentKeyLogged, page);
            }
            else
                return await GetItemsAsync(page);
        }

        public override async Task<PagedResult<GEPaymentTypeModel>> Search(Expression<Func<GEPaymentTypeModel, bool>> predicate,
            SortResult sort, string pSearchData, int page) =>
            await Search(pSearchData, page);
    }
}
