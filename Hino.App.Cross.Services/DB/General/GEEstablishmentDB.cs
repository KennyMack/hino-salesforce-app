﻿using Hino.App.Cross.Models.General;
using Hino.App.Cross.Services.DB.Interfaces.General;
using Hino.App.Cross.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Hino.App.Cross.Services.DB.General
{
    public class GEEstablishmentDB : BaseDataStore<GEEstablishmentsModel>, IGEEstablishmentDB
    {
        public async Task<GEEstablishmentsModel> GetByEstablishmentKey(string pEstablishmentKey)
        {
            var result = (await Query(r => r.EstablishmentKey == pEstablishmentKey, -1)).Results;

            if (result.Count > 0)
                return result[0];

            return null;
        }

        public override async Task<PagedResult<GEEstablishmentsModel>> Query(Expression<Func<GEEstablishmentsModel, bool>> predicate, int page)
        {
            var _DataBase = _LiteDBConn.GetDataBase();
            var _Collection = _DataBase.GetCollection<GEEstablishmentsModel>();
            IList<GEEstablishmentsModel> Result = null;

            if (page > 0)
                Result = _Collection.Query()
                    .Where(predicate).OrderByDescending(r => r.Id).ToList().Skip((page - 1) * 50).Take(50).ToList();
            else
                Result = _Collection.Query()
                    .Where(predicate).OrderByDescending(r => r.Id).ToList();

            return await Task.Run(() => PaginateQuery(Result, page));
        }

        public override async Task<GEEstablishmentsModel> AddOrUpdateItemAsync(GEEstablishmentsModel item)
        {
            var _DataBase = _LiteDBConn.GetDataBase();
            var _Collection = _DataBase.GetCollection<GEEstablishmentsModel>();

            await Task.Run(() => _Collection.Upsert(item));

            return item;
        }

        public override async Task<PagedResult<GEEstablishmentsModel>> GetItemsAsync(int page)
        {
            var _DataBase = _LiteDBConn.GetDataBase();
            var _Collection = _DataBase.GetCollection<GEEstablishmentsModel>();

            IList<GEEstablishmentsModel> Result = null;

            if (page > 0)
                Result = _Collection.FindAll().OrderByDescending(r => r.Id).Skip((page - 1) * 50).Take(50).ToList();
            else
                Result = _Collection.FindAll().OrderByDescending(r => r.Id).ToList();


            return await Task.Run(() => PaginateQuery(Result, page));
        }
    }
}
