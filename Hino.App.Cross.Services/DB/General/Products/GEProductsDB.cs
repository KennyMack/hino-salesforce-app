﻿using Hino.App.Cross.Models.General;
using Hino.App.Cross.Services.DB.Interfaces.General.Products;
using Hino.App.Cross.Utils.Database;
using Hino.App.Cross.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.App.Cross.Services.DB.General.Products
{
    public class GEProductsDB : BaseDataStore<GEProductsModel>, IGEProductsDB
    {
        public override async Task<PagedResult<GEProductsModel>> Query(Expression<Func<GEProductsModel, bool>> predicate, int page)
        {
            var _DataBase = _LiteDBConn.GetDataBase();
            var _Collection = _DataBase.GetCollection<GEProductsModel>();

            IList<GEProductsModel> Result = _Collection.Query()
                    .Where(r => r.UserKey == CurrentApp.UserKeyLogged)
                    .Where(predicate).OrderByDescending(r => r.ProductKey).ToList();

            if (page > 0)
                Result = Result.Skip((page - 1) * 50).Take(50).ToList();

            return await Task.Run(() => PaginateQuery(Result, page));
        }

        public override async Task<PagedResult<GEProductsModel>> Search(string pSearchData, int page)
        {
            if (!string.IsNullOrEmpty(pSearchData))
            {
                return await Query(r =>
                    r.Name.ToLower().Contains(pSearchData) ||
                    r.ProductKey.ToLower().Contains(pSearchData) ||
                    r.NCM.ToLower().Contains(pSearchData), page);
            }
            else
                return await GetItemsAsync(page);
        }

        public override async Task<PagedResult<GEProductsModel>> Search(Expression<Func<GEProductsModel, bool>> predicate,
            SortResult sort, string pSearchData, int page)
        {
            var _DataBase = _LiteDBConn.GetDataBase();
            var _Collection = _DataBase.GetCollection<GEProductsModel>();

            IList<GEProductsModel> Result = null;

            var qry = _Collection.Query()
                    .Where(r => r.UserKey == CurrentApp.UserKeyLogged);

            if (!string.IsNullOrEmpty(pSearchData))
                qry = qry.Where(r =>
                        r.Name.ToLower().Contains(pSearchData) ||
                        r.ProductKey.ToLower().Contains(pSearchData) ||
                        r.NCM.ToLower().Contains(pSearchData));

            if (predicate != null)
                qry = qry.Where(predicate);

            Result = qry.ToList();

            var Descending = true;
            Func<GEProductsModel, object> exp = x => x.ProductKey;
            if (sort != null)
            {
                Descending = sort.Descending;
                exp = (Func<GEProductsModel, object>)sort.Expression;
            }

            if (Descending)
                Result = Result.OrderByDescending(exp).ToList();
            else
                Result = Result.OrderBy(exp).ToList();

            if (page > 0)
                Result = Result.Skip((page - 1) * 50).Take(50).ToList();


            return await Task.Run(() => PaginateQuery(Result, page));
            /*

            if (predicate != null || !string.IsNullOrEmpty(pSearchData))
            {
                var _DataBase = _LiteDBConn.GetDataBase();
                var _Collection = _DataBase.GetCollection<GEProductsModel>();
                IList<GEProductsModel> Result = null;

                var qry = _Collection.Query()
                        .Where(r => r.UserKey == CurrentApp.UserKeyLogged);

                if (!string.IsNullOrEmpty(pSearchData))
                    qry = qry.Where(r =>
                        r.Name.ToLower().Contains(pSearchData) ||
                        r.ProductKey.ToLower().Contains(pSearchData) ||
                        r.NCM.ToLower().Contains(pSearchData));

                if (predicate != null)
                    qry = qry.Where(predicate);

                Func<GEProductsModel, object> exp = (Func<GEProductsModel, object>)sort.Expression;

                if (sort.Descending)
                    Result = qry.ToList().OrderByDescending(exp).ToList();
                else
                    Result = qry.ToList().OrderBy(exp).ToList();

                if (page > 0)
                    Result = Result.Skip((page - 1) * 50).Take(50).ToList();

                return await Task.Run(() => PaginateQuery(Result, page));
            }
            else
                return await GetItemsAsync(page);*/
        }
    }
}
