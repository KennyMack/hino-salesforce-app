﻿using LiteDB;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Xamarin.Essentials;

namespace Hino.App.Cross.Services.DB
{
    public class LiteDBConn
    {
        public string PathLiteDataBase { get; private set; }
        private LiteDatabase _DataBase = null;

        public static LiteDBConn Instance = null;

        public static LiteDBConn GetInstance()
        {
            if (Instance == null)
                Instance = new LiteDBConn();

            return Instance;
        }

        public LiteDBConn()
        {
            PathLiteDataBase = Path.Combine(FileSystem.AppDataDirectory, "HINOERPDB.db");
        }

        public LiteDatabase GetDataBase()
        {
            if (_DataBase == null)
                _DataBase = new LiteDatabase(PathLiteDataBase);

            return _DataBase;
        }

        public void FreeInstance()
        {

        }
    }
}
