﻿using Hino.App.Cross.Models.General.Business;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.App.Cross.Services.DB.Interfaces.General.Business
{
    public interface IGEEnterpriseContactsDB : IBaseDataStore<GEEnterpriseContactsModel>
    {
    }
}
