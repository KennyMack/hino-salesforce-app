﻿using Hino.App.Cross.Models.General;

namespace Hino.App.Cross.Services.DB.Interfaces.General.Products
{
    public interface IGEProductsDB : IBaseDataStore<GEProductsModel>
    {
    }
}
