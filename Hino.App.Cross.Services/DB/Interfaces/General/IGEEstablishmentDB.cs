﻿using Hino.App.Cross.Models.General;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.App.Cross.Services.DB.Interfaces.General
{
    public interface IGEEstablishmentDB : IBaseDataStore<GEEstablishmentsModel>
    {
        Task<GEEstablishmentsModel> GetByEstablishmentKey(string pEstablishmentKey);
    }
}
