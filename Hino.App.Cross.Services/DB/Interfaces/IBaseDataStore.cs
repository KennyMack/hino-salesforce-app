﻿using Hino.App.Cross.Models;
using Hino.App.Cross.Utils.Database;
using Hino.App.Cross.Utils.Paging;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.App.Cross.Services.DB.Interfaces
{
    public interface IBaseDataStore<T> where T : BaseEntity, new()
    {
        Task<long> NextSequence();
        Task AddFromPagedResult(PagedResult<T> result);
        Task<T> AddOrUpdateItemAsync(T item);
        Task<T> AddItemAsync(T item);
        Task<T> UpdateItemAsync(T item);
        Task<bool> DeleteItemAsync(long id);
        Task<T> GetItemAsync(long id);
        Task<PagedResult<T>> GetItemsAsync(int page);
        Task<PagedResult<T>> Query(Expression<Func<T, bool>> predicate, int page);
        Task<PagedResult<T>> Search(string pSearchData, int page);
        Task<PagedResult<T>> Search(Expression<Func<T, bool>> predicate, SortResult sort, string pSearchData, int page);
        void ClearTable();
    }
}
