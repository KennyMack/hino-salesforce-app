﻿using Hino.App.Cross.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.App.Cross.Services.DB.Interfaces
{
    public interface IPreferencesDB : IBaseDataStore<ConfigModel>
    {
        Task<ConfigModel> UpdateLastSync(DateTime pDate);
    }
}
