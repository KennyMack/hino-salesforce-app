﻿
using Hino.App.Cross.Models.Auth;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.App.Cross.Services.DB.Interfaces.Auth
{
    public interface IUserLoggedDB : IBaseDataStore<UserLoggedModel>
    {
        Task<UserLoggedModel> GetByUserKeyAsync(string pEstablishmentKey, string pUserKey);
    }
}
