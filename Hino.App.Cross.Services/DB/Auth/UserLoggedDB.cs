﻿using Hino.App.Cross.Models.Auth;
using Hino.App.Cross.Services.DB.Interfaces.Auth;
using Hino.App.Cross.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Hino.App.Cross.Services.DB.Auth
{
    public class UserLoggedDB : BaseDataStore<UserLoggedModel>, IUserLoggedDB
    {
        public async Task<UserLoggedModel> GetByUserKeyAsync(string pEstablishmentKey, string pUserKey)
        {
            var User = (await Query(r => r.UserKey == pUserKey && r.EstablishmentKey == pEstablishmentKey, -1)).Results;

            if (User.Count > 0)
                return User[0];

            return null;
        }

        public override async Task<PagedResult<UserLoggedModel>> Query(Expression<Func<UserLoggedModel, bool>> predicate, int page)
        {
            var _DataBase = _LiteDBConn.GetDataBase();
            var _Collection = _DataBase.GetCollection<UserLoggedModel>();
            IList<UserLoggedModel> Result = null;

            if (page > 0)
                Result = _Collection.Query()
                    .Where(predicate).OrderByDescending(r => r.Id).ToList().Skip((page - 1) * 50).Take(50).ToList();
            else
                Result = _Collection.Query()
                    .Where(predicate).OrderByDescending(r => r.Id).ToList();

            return await Task.Run(() => PaginateQuery(Result, page));
        }


        public override async Task<UserLoggedModel> AddOrUpdateItemAsync(UserLoggedModel item)
        {
            var _DataBase = _LiteDBConn.GetDataBase();
            var _Collection = _DataBase.GetCollection<UserLoggedModel>();

            await Task.Run(() => _Collection.Upsert(item));

            return item;
        }

        public override async Task<PagedResult<UserLoggedModel>> GetItemsAsync(int page)
        {
            var _DataBase = _LiteDBConn.GetDataBase();
            var _Collection = _DataBase.GetCollection<UserLoggedModel>();

            IList<UserLoggedModel> Result = null;

            if (page > 0)
                Result = _Collection.FindAll().OrderByDescending(r => r.Id).Skip((page - 1) * 50).Take(50).ToList();
            else
                Result = _Collection.FindAll().OrderByDescending(r => r.Id).ToList();


            return await Task.Run(() => PaginateQuery(Result, page));
        }
    }
}
