﻿using Hino.App.Cross.Models;
using Hino.App.Cross.Services.DB.Interfaces;
using Hino.App.Cross.Utils.Database;
using Hino.App.Cross.Utils.Exceptions;
using Hino.App.Cross.Utils.Paging;
using LiteDB;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace Hino.App.Cross.Services.DB
{
    public class BaseDataStore<T> : IBaseDataStore<T> where T : BaseEntity, new()
    {
        protected string PathLiteDataBase { get; private set; }
        protected LiteDBConn _LiteDBConn;
        public BaseDataStore()
        {
            PathLiteDataBase = Path.Combine(FileSystem.AppDataDirectory, "HINOERPDB.db");
            _LiteDBConn = LiteDBConn.GetInstance();
        }

        public async virtual Task<T> AddOrUpdateItemAsync(T item)
        {
            var _DataBase = _LiteDBConn.GetDataBase();
            var _Collection = _DataBase.GetCollection<T>();
            item.UserKey = CurrentApp.UserKeyLogged;

            await Task.Run(() => _Collection.Upsert(item));

            return item;
        }

        public virtual async Task<T> AddItemAsync(T item)
        {
            var _DataBase = _LiteDBConn.GetDataBase();
            var _Collection = _DataBase.GetCollection<T>();
            item.UserKey = CurrentApp.UserKeyLogged;
            if (item.Id < 0)
            {
                item.Created = DateTime.Now;
                item.Modified = DateTime.Now;
            }
            await Task.Run(() => _Collection.Insert(item));
            return item;
        }

        public virtual async Task<T> UpdateItemAsync(T item)
        {
            var _DataBase = _LiteDBConn.GetDataBase();
            var _Collection = _DataBase.GetCollection<T>();
            item.UserKey = CurrentApp.UserKeyLogged;
            item.Modified = DateTime.Now;
            await Task.Run(() => _Collection.Update(item));

            return item;
        }

        public virtual async Task<bool> DeleteItemAsync(long id)
        {
            var _DataBase = _LiteDBConn.GetDataBase();
            var _Collection = _DataBase.GetCollection<T>();
            return await Task.Run(() => _Collection.Delete(id));
        }

        public virtual async Task<T> GetItemAsync(long id)
        {
            var _DataBase = _LiteDBConn.GetDataBase();
            var _Collection = _DataBase.GetCollection<T>();
            return await Task.Run(() => (_Collection.Query().Where(r =>
                r.Id == id &&
                r.UserKey == CurrentApp.UserKeyLogged)).FirstOrDefault());
        }

        public virtual async Task AddFromPagedResult(PagedResult<T> result)
        {
            var _DataBase = _LiteDBConn.GetDataBase();
            var _Collection = _DataBase.GetCollection<T>();

            result.Results.All(r => { r.UserKey = CurrentApp.UserKeyLogged; return true; });

            await Task.Run(() => _Collection.Upsert(result.Results));
        }

        public PagedResult<T> PaginateQuery(IList<T> query, int page)
        {
            var result = new PagedResult<T>
            {
                CurrentPage = page,
                PageSize = 50,
                RowCount = query.Count(),
                Results = query
            };

            var pageCount = (double)result.RowCount / 50;
            result.PageCount = (int)Math.Ceiling(pageCount);

            return result;
        }

        public virtual async Task<PagedResult<T>> GetItemsAsync(int page)
        {
            var _DataBase = _LiteDBConn.GetDataBase();
            var _Collection = _DataBase.GetCollection<T>();
            IList<T> Result = null;

            if (page > 0)
                Result = _Collection.Query()
                    .Where(r => r.UserKey == CurrentApp.UserKeyLogged)
                    .OrderByDescending(r => r.Id)
                    .ToList()
                    .Skip((page - 1) * 50)
                    .Take(50)
                    .ToList();
            else
                Result = _Collection.Query()
                    .Where(r => r.UserKey == CurrentApp.UserKeyLogged)
                    .OrderByDescending(r => r.Id)
                    .ToList();


            return await Task.Run(() => PaginateQuery(Result, page));
        }

        public virtual async Task<PagedResult<T>> Query(Expression<Func<T, bool>> predicate, int page)
        {
            var _DataBase = _LiteDBConn.GetDataBase();
            var _Collection = _DataBase.GetCollection<T>();
            IList<T> Result = null;

            if (page > 0)
                Result = _Collection.Query()
                    .Where(r => r.UserKey == CurrentApp.UserKeyLogged)
                    .Where(predicate).OrderByDescending(r => r.Id).ToList().Skip((page - 1) * 50).Take(50).ToList();
            else
                Result = _Collection.Query()
                    .Where(r => r.UserKey == CurrentApp.UserKeyLogged)
                    .Where(predicate).OrderByDescending(r => r.Id).ToList();

            return await Task.Run(() => PaginateQuery(Result, page));
        }

        public async Task<long> NextSequence()
        {
            Random rd = new Random();
            var id = 0;
            while (true)
            {
                id = rd.Next(-999999999, -1);
                if (await GetItemAsync(id) == null)
                    break;
            }

            return id;
        }

        #region Convert Search Data to number
        protected int ConvertSearchToNumber(string pSearch)
        {
            if (int.TryParse(pSearch, out int ret))
                return ret;

            return 0;
        }
        #endregion

        public virtual async Task<PagedResult<T>> Search(string pSearchData, int page)
        {
            if (!string.IsNullOrEmpty(pSearchData))
            {
                var num = ConvertSearchToNumber(pSearchData);
                return await Query(r =>
                    r.Id == num && r.UserKey == CurrentApp.UserKeyLogged, page);
            }
            else
                return await GetItemsAsync(page);
        }

        public virtual async Task<PagedResult<T>> Search(Expression<Func<T, bool>> predicate,
            SortResult sort, string pSearchData, int page)
        {
            var _DataBase = _LiteDBConn.GetDataBase();
            var _Collection = _DataBase.GetCollection<T>();

            IList<T> Result = null;

            var qry = _Collection.Query()
                    .Where(r => r.UserKey == CurrentApp.UserKeyLogged);

            if (!string.IsNullOrEmpty(pSearchData))
            {
                var num = ConvertSearchToNumber(pSearchData);
                qry = qry.Where(r => r.Id == num);
            }

            if (predicate != null)
                qry = qry.Where(predicate);

            Result = qry.ToList();

            var Descending = true;
            Func<T, object> exp = x => x.Id;
            if (sort != null)
            {
                Descending = sort.Descending;
                exp = (Func<T, object>)sort.Expression;
            }

            if (Descending)
                Result = Result.OrderByDescending(exp).ToList();
            else
                Result = Result.OrderBy(exp).ToList();

            if (page > 0)
                Result = Result.Skip((page - 1) * 50).Take(50).ToList();


            return await Task.Run(() => PaginateQuery(Result, page));


            /*if (predicate != null || !string.IsNullOrEmpty(pSearchData))
            {
                var num = ConvertSearchToNumber(pSearchData);
                var _DataBase = _LiteDBConn.GetDataBase();
                var _Collection = _DataBase.GetCollection<T>();
                IList<T> Result = null;

                var qry = _Collection.Query()
                        .Where(r => r.UserKey == CurrentApp.UserKeyLogged);

                if (!string.IsNullOrEmpty(pSearchData))
                    qry = qry.Where(r => r.Id == num);

                if (predicate != null)
                    qry = qry.Where(predicate);

                Expression<Func<T, object>> exp = (Expression<Func<T, object>>)sort.Expression;

                if (sort.Descending)
                    Result = qry.ToList().OrderByDescending(ordering => exp.Body).ToList();
                else
                    Result = qry.ToList().OrderBy(ordering => exp.Body).ToList();

                if (page > 0)
                    Result = Result.Skip((page - 1) * 50).Take(50).ToList();

                return await Task.Run(() => PaginateQuery(Result, page));
            }
            else
                return await GetItemsAsync(page);*/
        }

        public virtual void ClearTable()
        {
            var _DataBase = _LiteDBConn.GetDataBase();
            try
            {
                var _Collection = _DataBase.GetCollection<T>();
                _DataBase.DropCollection(_Collection.Name);
            }
            catch (Exception ex)
            {
                DebugErrorLog.Log(ex);
            }
        }

        
    }
}
