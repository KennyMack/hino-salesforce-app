﻿using Hino.App.Cross.Models.Auth;
using Hino.App.Cross.Services.API.Interfaces.Auth;
using Hino.App.Cross.Services.Application.Auth;
using Hino.App.Cross.Services.DB.Interfaces.Auth;
using Hino.App.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using XF.Material.Forms.UI.Dialogs;

namespace Hino.App.Cross.Services.ViewModels.Auth
{
    public class LoginVM: BaseViewModel
    {
        #region Properties
        private LoginModel _LoginModel;
        public event EventHandler GoToMainPage;
        public event EventHandler GoToRegister;
        public IUserLoggedDB _IUserLoggedDB;
        public IAuthAPI _IAuthAPI;
        public ICommand CmdLoginClick { protected set; get; }
        public ICommand CmdRegisterClick { protected set; get; }
        #endregion

        #region Labels
        public string LBLUSEROREMAILPLACEHOLDER { get => App.Cross.Resources.Fields.UserIdentifier; }
        public string LBLUSEROREMAILHELPERTEXT { get => App.Cross.Resources.Fields.UserIdentifier; }
        public string LBLUSEROREMAILERRORTEXT { get => App.Cross.Resources.Messages.InformUser; }

        public string LBLPASSWORDPLACEHOLDER { get => App.Cross.Resources.Fields.Password; }
        public string LBLPASSWORDHELPERTEXT { get => App.Cross.Resources.Fields.Password; }
        public string LBLPASSWORDERRORTEXT { get => App.Cross.Resources.Messages.InformPassword; }

        public string LBLESTABLISHMENTKEYPLACEHOLDER { get => App.Cross.Resources.Fields.EstablishmentKey; }
        public string LBLESTABLISHMENTKEYHELPERTEXT { get => App.Cross.Resources.Fields.EstablishmentKey; }
        public string LBLESTABLISHMENTKEYERRORTEXT { get => App.Cross.Resources.Messages.InformEstablishmentKey; }
        #endregion

        #region Fields
        public bool _HasUserError = false;
        public bool HASUSERERROR
        {
            get
            {
                return _HasUserError;
            }
            set
            {
                SetProperty(ref _HasUserError, value);
            }
        }
        public bool _HasPasswordError = false;
        public bool HASPASSWORDERROR
        {
            get
            {
                return _HasPasswordError;
            }
            set
            {
                SetProperty(ref _HasPasswordError, value);
            }
        }
        public bool _HasEstablishmentKeyError = false;
        public bool HASESTABLISHMENTKEYERROR
        {
            get
            {
                return _HasEstablishmentKeyError;
            }
            set
            {
                SetProperty(ref _HasEstablishmentKeyError, value);
            }
        }

        public string _UserOrEmail = "";
        public string USEROREMAIL
        {
            get
            {
                return _UserOrEmail;
            }
            set
            {
                SetProperty(ref _UserOrEmail, value);
            }
        }

        public string _Password = "";
        public string PASSWORD
        {
            get
            {
                return _Password;
            }
            set
            {
                SetProperty(ref _Password, value);
            }
        }

        public string _EstablishmentKey = "";
        public string ESTABLISHMENTKEY
        {
            get
            {
                return _EstablishmentKey;
            }
            set
            {
                SetProperty(ref _EstablishmentKey, value);
            }
        }
        #endregion

        #region Initialize
        private void Initialize()
        {
            Title = "Hino Sistemas";
            _LoginModel = new LoginModel();
            CmdLoginClick = new Command(OnLoginClick);
            CmdRegisterClick = new Command(OnRegisterClick);
            _IUserLoggedDB = DInjection.GetIntance<IUserLoggedDB>();
            _IAuthAPI = DInjection.GetIntance<IAuthAPI>();
        }
        #endregion

        #region Construtor
        public LoginVM(string pEmail)
        {
            USEROREMAIL = pEmail;
            Initialize();
        }

        public LoginVM()
        {
            Initialize();
        }
        #endregion

        public void ClearData()
        {
            IsBusy = false;
        }

        #region Register Click
        public void OnRegisterClick()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            GoToRegister?.Invoke(new RegisterUserData
            {
                EstablishmentKey = ESTABLISHMENTKEY,
                UserOrEmail = USEROREMAIL
            }, null);
        }
        #endregion

        #region Login Click
        public async void OnLoginClick()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            HASUSERERROR = false;
            HASPASSWORDERROR = false;
            HASESTABLISHMENTKEYERROR = false;

            _LoginModel.UserOrEmail = _UserOrEmail;
            _LoginModel.Password = _Password;
            _LoginModel.EstablishmentKey = _EstablishmentKey;

            HASUSERERROR = (string.IsNullOrEmpty(_LoginModel.UserOrEmail));
            HASPASSWORDERROR = (string.IsNullOrEmpty(_LoginModel.Password));
            HASESTABLISHMENTKEYERROR = (string.IsNullOrEmpty(_LoginModel.EstablishmentKey));

            if (!HASUSERERROR &&
                !HASPASSWORDERROR &&
                !HASESTABLISHMENTKEYERROR)
            {
                using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.Messages.LoadProcessing))
                {
                    IsBusy = true;
                    try
                    {
                        if (!CurrentApp.IsConnected)
                        {
                            ShowShortMessage(Resources.Messages.NoInternetConnection);
                            return;
                        }

                        var AuthService = new AuthenticationService();
                        var ret = await AuthService.LoginAsync(_LoginModel);

                        if (!ret.success)
                        {
                            IsBusy = false;
                            await ShowSnackBarAsync(ret.error_description);
                            return;
                        }

                        var message = await AuthService.SetUserLogged();
                        if (message != "")
                        {
                            IsBusy = false;
                            await ShowSnackBarAsync(message);
                            return;
                        }

                        await AuthService.SetEstablishmentLoggedAsync(_LoginModel.EstablishmentKey);

                        await AuthService.SetPreferencesDefaultAsync();

                        await Task.Delay(200);
                        ShowShortMessage(Resources.Messages.LoginSucceeded);

                        GoToMainPage?.Invoke(this, null);
                    }
                    catch (Exception ex)
                    {
                        DebugErrorLog.Log(ex);
                        ShowShortMessage(Resources.Messages.ComunicationFail);
                    }
                    finally
                    {
                        IsBusy = false;
                    }
                }
            }
            else
                IsBusy = false;
        }
        #endregion
    }
}
