﻿using Hino.App.Cross.Models.Auth;
using Hino.App.Cross.Services.DB.Interfaces.Auth;
using Hino.App.Cross.Services.DB.Interfaces.General;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using XF.Material.Forms.UI.Dialogs;
using Hino.App.Cross.Utils.Exceptions;
using System.Runtime.CompilerServices;

namespace Hino.App.Cross.Services.ViewModels.Auth
{
    public class AccountVM : BaseViewModel
    {     
        public ObservableCollection<AccountsModel> Items { get; set; }
        public IUserLoggedDB _IUserLoggedDB;
        public IGEEstablishmentDB _IGEEstablishmentDB;

        private int _ListHeight = 60;
        public int LISTHEIGHT
        {
            get
            {
                return _ListHeight;
            }
            set
            {
                SetProperty(ref _ListHeight, value);
            }
        }

        public AccountVM()
        {
            Title = "Hino Sistemas";
            _IUserLoggedDB = DInjection.GetIntance<IUserLoggedDB>();
            _IGEEstablishmentDB = DInjection.GetIntance<IGEEstablishmentDB>();
            Items = new ObservableCollection<AccountsModel>();
            Items.CollectionChanged += Items_CollectionChanged;
        }

        private void Items_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            LISTHEIGHT = (Items.Count * 70 + 2);
        }

        public async Task LoadItems()
        {
            var Users = await _IUserLoggedDB.GetItemsAsync(-1);
            var Establishments = (await _IGEEstablishmentDB.GetItemsAsync(-1)).Results;

            foreach (var item in Users.Results)
            {
                var Establishment = Establishments.FirstOrDefault(r => r.EstablishmentKey == item.EstablishmentKey);
                if (Establishment != null)
                {
                    Items.Add(new AccountsModel
                    {
                        Email = item.Email,
                        EstablishmentKey = item.EstablishmentKey,
                        NomeFantasia = Establishment.NomeFantasia,
                        RazaoSocial = Establishment.RazaoSocial,
                        UserKey = item.UserKey,
                        UserName = item.UserName,
                        UserToken = item.UserToken,
                        RefreshToken = item.RefreshToken,
                        SessionId = item.SessionId,
                        Estab = Establishment,
                        User = item
                    });
                }
            }
        }

        protected async override Task RefreshData()
        {
            await base.RefreshData();

            IsBusy = true;
            var Title = base.Title;
            base.Title = Resources.Messages.Loading;
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.Messages.LoadProcessing))
            {
                try
                {
                    Items.Clear();
                    await LoadItems();

                    IsBusy = false;
                }
                catch (Exception e)
                {
                    DebugErrorLog.Log(e);
                    ShowShortMessage(e.Message);
                    base.Title = Title;

                    IsBusy = false;
                }
                finally
                {
                    IsBusy = false;
                    base.Title = Title;
                }
            }
        }

        public async Task RemoveAccount(AccountsModel pAccount)
        {
            if (IsBusy)
                return;

            IsBusy = true;
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.Messages.LoadProcessing))
            {
                try
                {
                    Items.Clear();

                    await _IUserLoggedDB.DeleteItemAsync(pAccount.User.Id);

                    ShowShortMessage(Resources.Messages.RemoveSuccess);

                    await LoadItems();

                    IsBusy = false;
                }
                catch (Exception e)
                {
                    DebugErrorLog.Log(e);
                    ShowShortMessage(e.Message);
                    base.Title = Title;

                    IsBusy = false;
                }
                finally
                {
                    IsBusy = false;
                    base.Title = Title;
                }


            }
        }

    }
}
