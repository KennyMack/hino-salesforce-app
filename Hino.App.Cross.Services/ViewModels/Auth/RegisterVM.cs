﻿using Hino.App.Cross.Models;
using Hino.App.Cross.Models.Auth;
using Hino.App.Cross.Models.Validation.Auth;
using Hino.App.Cross.Services.API.Interfaces.Auth;
using Hino.App.Cross.Services.API.Interfaces.General;
using Hino.App.Cross.Services.Application.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using XF.Material.Forms.UI.Dialogs;

namespace Hino.App.Cross.Services.ViewModels.Auth
{
    public class RegisterVM : BaseViewModel
    {
        #region Properties
        public ICommand CmdRegisterClick { protected set; get; }
        public IAuthAPI _IAuthAPI;
        public IUsersAPI _IUsersAPI;
        private RegisterModel _RegisterModel;
        public event EventHandler GoToMainPage;
        #endregion

        #region Labels
        public string LBLCONFIRM { get => App.Cross.Resources.Fields.Confirm; }
        public string LBLYOURDATA { get => App.Cross.Resources.Fields.YourData; }
        public string LBLESTABLISHMENTKEYPLACEHOLDER { get => App.Cross.Resources.Fields.EstablishmentKey; }
        public string LBLESTABLISHMENTKEYHELPERTEXT { get => App.Cross.Resources.Fields.EstablishmentKey; }
        public string LBLESTABLISHMENTKEYERRORTEXT { get => string.Format(Resources.Messages.InformGeneric, App.Cross.Resources.Fields.EstablishmentKey); }
        public string LBLUSERKEYPLACEHOLDER { get => App.Cross.Resources.Fields.UserKey; }
        public string LBLUSERKEYHELPERTEXT { get => App.Cross.Resources.Fields.UserKey; }
        public string LBLUSERKEYERRORTEXT { get => string.Format(Resources.Messages.InformGeneric, App.Cross.Resources.Fields.UserKey); }
        public string LBLUSERNAMEPLACEHOLDER { get => App.Cross.Resources.Fields.UserName; }
        public string LBLUSERNAMEHELPERTEXT { get => App.Cross.Resources.Fields.UserName; }
        public string LBLUSERNAMEERRORTEXT { get => string.Format(Resources.Messages.InformGeneric, App.Cross.Resources.Fields.UserName); }
        public string LBLEMAILPLACEHOLDER { get => App.Cross.Resources.Fields.Email; }
        public string LBLEMAILHELPERTEXT { get => App.Cross.Resources.Fields.Email; }
        public string LBLEMAILERRORTEXT { get => string.Format(Resources.Messages.InformGeneric, App.Cross.Resources.Fields.Email); }
        public string LBLPASSWORDPLACEHOLDER { get => App.Cross.Resources.Fields.Password; }
        public string LBLPASSWORDHELPERTEXT { get => App.Cross.Resources.Fields.Password; }
        public string LBLPASSWORDERRORTEXT { get => string.Format(Resources.Messages.InformGeneric, App.Cross.Resources.Fields.Password); }
        public string LBLPASSWORDCONFIRMPLACEHOLDER { get => App.Cross.Resources.Fields.PasswordConfirm; }
        public string LBLPASSWORDCONFIRMHELPERTEXT { get => App.Cross.Resources.Fields.PasswordConfirm; }
        public string LBLPASSWORDCONFIRMERRORTEXT { get => string.Format(Resources.Messages.InformGeneric, App.Cross.Resources.Fields.PasswordConfirm); }
        #endregion

        #region Fields
        private string _EstablishmentKey;
        private string _UserKey;
        private string _UserName;
        private string _Email;
        private string _Password;
        private string _PasswordConfirm;

        public string ESTABLISHMENTKEY { get => _EstablishmentKey; set => SetProperty(ref _EstablishmentKey, value); }

        public string USERKEY { get => _UserKey; set => SetProperty(ref _UserKey, value); }

        public string USERNAME { get => _UserName; set => SetProperty(ref _UserName, value); }

        public string EMAIL { get => _Email; set => SetProperty(ref _Email, value); }

        public string PASSWORD { get => _Password; set => SetProperty(ref _Password, value); }

        public string PASSWORDCONFIRM { get => _PasswordConfirm; set => SetProperty(ref _PasswordConfirm, value); }
        #endregion

        #region Errors
        private bool _HasEstablishmentKeyError;
        private bool _HasUserKeyError;
        private bool _HasUserNameError;
        private bool _HasEmailError;
        private bool _HasPasswordError;
        private bool _HasPasswordConfirmError;

        public bool HASESTABLISHMENTKEYERROR { get => _HasEstablishmentKeyError; set => SetProperty(ref _HasEstablishmentKeyError, value); }

        public bool HASUSERKEYERROR { get => _HasUserKeyError; set => SetProperty(ref _HasUserKeyError, value); }

        public bool HASUSERNAMEERROR { get => _HasUserNameError; set => SetProperty(ref _HasUserNameError, value); }

        public bool HASEMAILERROR { get => _HasEmailError; set => SetProperty(ref _HasEmailError, value); }

        public bool HASPASSWORDERROR { get => _HasPasswordError; set => SetProperty(ref _HasPasswordError, value); }

        public bool HASPASSWORDCONFIRMERROR { get => _HasPasswordConfirmError; set => SetProperty(ref _HasPasswordConfirmError, value); }
        #endregion

        public RegisterVM()
        {
            Title = LBLYOURDATA;
            _RegisterModel = new RegisterModel();
            CmdRegisterClick = new Command(OnRegisterClick);
            _Validator = new RegisterValidator();

            _IAuthAPI = DInjection.GetIntance<IAuthAPI>();
            _IUsersAPI = DInjection.GetIntance<IUsersAPI>();
        }

        public async void OnRegisterClick()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            HASESTABLISHMENTKEYERROR = false;
            HASUSERKEYERROR = false;
            HASUSERNAMEERROR = false;
            HASEMAILERROR = false;
            HASPASSWORDERROR = false;
            HASPASSWORDCONFIRMERROR = false;

            _RegisterModel.EstablishmentKey = _EstablishmentKey;
            _RegisterModel.UserKey = _UserKey;
            _RegisterModel.UserName = _UserName;
            _RegisterModel.Email = _Email;
            _RegisterModel.Password = _Password;
            _RegisterModel.PasswordConfirm = _PasswordConfirm;
            _RegisterModel.OriginCreate = "MOBILE";

            HASESTABLISHMENTKEYERROR = (string.IsNullOrEmpty(_RegisterModel.EstablishmentKey));
            HASUSERKEYERROR = (string.IsNullOrEmpty(_RegisterModel.UserKey));
            HASUSERNAMEERROR = (string.IsNullOrEmpty(_RegisterModel.UserName));
            HASEMAILERROR = (string.IsNullOrEmpty(_RegisterModel.Email));
            HASPASSWORDERROR = (string.IsNullOrEmpty(_RegisterModel.Password));
            HASPASSWORDCONFIRMERROR = (string.IsNullOrEmpty(_RegisterModel.PasswordConfirm));
            if (!HASESTABLISHMENTKEYERROR &&
                !HASUSERKEYERROR &&
                !HASUSERNAMEERROR &&
                !HASEMAILERROR &&
                !HASPASSWORDERROR &&
                !HASPASSWORDCONFIRMERROR)
            {
                if (await ValidateModelAsync(_RegisterModel))
                {
                    using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.Messages.LoadProcessing))
                    {
                        IsBusy = true;

                        await Task.Delay(2200);

                        try
                        {
                            var AuthService = new AuthenticationService();

                            var ret = await AuthService.CreateUserAsync(_RegisterModel);

                            if (!ret.success)
                            {
                                await Task.Delay(200);
                                IsBusy = false;
                                await ShowSnackBarAsync(ret.error[0].Messages.ToArray()[0]);
                                // ShowShortMessage(ret.error[0].Messages.ToArray()[0]);
                                return;
                            }

                            await Task.Delay(200);

                            var loginModel = new LoginModel
                            {
                                UserOrEmail = _RegisterModel.Email,
                                Password = _RegisterModel.Password,
                                EstablishmentKey = _RegisterModel.EstablishmentKey
                            };

                            var retLogin = await AuthService.LoginAsync(loginModel);

                            if (!retLogin.success)
                            {
                                IsBusy = false;
                                await ShowSnackBarAsync(retLogin.error_description);
                                return;
                            }

                            await Task.Delay(200);

                            var message = await AuthService.SetUserLogged();
                            if (message != "")
                            {
                                IsBusy = false;
                                await ShowSnackBarAsync(message);
                                return;
                            }

                            await AuthService.SetEstablishmentLoggedAsync(loginModel.EstablishmentKey);

                            await AuthService.SetPreferencesDefaultAsync();

                            ShowShortMessage(Resources.Messages.LoginSucceeded);

                            await Task.Delay(200);
                            GoToMainPage?.Invoke(this, null);
                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Debug.WriteLine(ex);
                            ShowShortMessage(Resources.Messages.ComunicationFail);
                        }
                        finally
                        {
                            IsBusy = false;
                        }
                    }
                }
                else
                {
                    DisplayValidationModelErrors(true);
                    IsBusy = false;
                }
            }
            else
                IsBusy = false;
        }
    }
}
