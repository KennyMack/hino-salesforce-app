﻿using Hino.App.Cross.Models;
using Hino.App.Cross.Models.Validation.Auth;
using Hino.App.Cross.Services.API.Interfaces.Auth;
using Hino.App.Cross.Services.API.Interfaces.General;
using Hino.App.Cross.Services.Application.Auth;
using Hino.App.Cross.Services.DB.Interfaces;
using Hino.App.Cross.Services.Utils;
using Hino.App.Cross.Utils.Exceptions;
using Hino.App.Cross.Utils.Messages;
using Plugin.Fingerprint.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using XF.Material.Forms.UI.Dialogs;

namespace Hino.App.Cross.Services.ViewModels
{
    public class PreferencesVM: BaseViewModel
    {
        #region Properties
        private AuthFingerPrint _AuthFingerPrint;
        public ICommand CmdSaveClick { protected set; get; }
        #endregion

        #region Labels
        public string LBLSYNCWIFIPLACEHOLDER { get => App.Cross.Resources.Fields.SyncWIFI; }

        public string LBLSYNCONSTARTPLACEHOLDER { get => App.Cross.Resources.Fields.SyncOnStart; }

        public string LBLSYNCONSAVEPLACEHOLDER { get => App.Cross.Resources.Fields.SyncOnSave; }

        public string LBLINTERVALSYNCPLACEHOLDER { get => App.Cross.Resources.Fields.IntervalSync; }

        public string LBLLASTSYNCPLACEHOLDER { get => App.Cross.Resources.Fields.LastSync; }
        public string LBLLASTSYNCHELPERTEXT { get => App.Cross.Resources.Fields.LastSync; }

        public string LBLCONFIRMBTN { get => App.Cross.Resources.Messages.Save; }
        public string LBLCANCELBTN { get => App.Cross.Resources.Messages.Cancel; }

        public string LBLCLEARALL { get => Resources.Messages.ClearAllData; }
        public string LBLSYNCALL { get => Resources.Messages.SyncAllData; }

        public string LBLUSESECURITYPLACEHOLDER { get => Resources.Fields.UseSecurity; }
        #endregion

        #region Fields
        private bool _SyncWIFI;
        private bool _SyncOnStart;
        private bool _SyncOnSave;
        private bool _CanUseSecurity;
        private bool _UseSecurity;
        private bool _PreviousUseSecurity;
        private int _IntervalSync;
        private DateTime _LastSync;

        public bool SYNCWIFI { get => _SyncWIFI; set => SetProperty(ref _SyncWIFI, value); }

        public bool SYNCONSTART { get => _SyncOnStart; set => SetProperty(ref _SyncOnStart, value); }

        public bool SYNCONSAVE { get => _SyncOnSave; set => SetProperty(ref _SyncOnSave, value); }

        public bool USESECURITY { get => _UseSecurity; set => SetProperty(ref _UseSecurity, value); }

        public bool CANUSESECURITY { get => _CanUseSecurity; set => SetProperty(ref _CanUseSecurity, value); }

        public int INTERVALSYNC { get => _IntervalSync; set => SetProperty(ref _IntervalSync, value); }

        public DateTime LASTSYNC { get => _LastSync; set => SetProperty(ref _LastSync, value); }
        #endregion

        public PreferencesVM()
        {
            Title = Resources.PageTitle.Preferences;
            _AuthFingerPrint = new AuthFingerPrint();
            _AuthFingerPrint.OnAuthResult += _AuthFingerPrint_OnAuthResult;
            CANUSESECURITY = _AuthFingerPrint.AuthType != Plugin.Fingerprint.Abstractions.AuthenticationType.None;
            CmdSaveClick = new Command(OnSaveClick);
        }

        private async void _AuthFingerPrint_OnAuthResult(object sender, EventArgs e)
        {
            await Task.Delay(300);
            try
            {
                var result = ((FingerprintAuthenticationResult)sender);
                if (result.Authenticated)
                {
                    IsBusy = true;
                    await SaveChanges();
                    IsBusy = false;
                    await Navigation.PopAsync();
                }
                else
                {
                    await LoadData();
                    ShowShortMessage(result.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                DebugErrorLog.Log(ex);
                IsBusy = false;
            }
        }

        public async Task LoadData()
        {
            var Preference = await _IPreferencesDB.GetItemAsync(1);

            if (Preference != null)
            {
                SYNCONSAVE = Preference.SyncOnSave;
                SYNCWIFI = Preference.SyncWIFI;
                SYNCONSTART = Preference.SyncOnStart;
                INTERVALSYNC = Preference.IntervalSync;
                LASTSYNC = Preference.LastSync;
                USESECURITY = Preference.UseSecurity;
                _PreviousUseSecurity = Preference.UseSecurity;
            }
        }

        public async Task SaveChanges()
        {
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.Messages.LoadProcessing))
            {
                var Preference = await _IPreferencesDB.GetItemAsync(1);

                if (Preference == null)
                {
                    Preference = new ConfigModel();
                    Preference.FirstAccess = false;
                    Preference.FirstSyncOk = true;
                    Preference.UseSecurity = false;
                    Preference.LastSync = new DateTime(1900, 1, 1, 1, 1, 1);
                }

                Preference.Id = 1;
                Preference.EstablishmentKey = Guid.NewGuid().ToString();
                Preference.SyncOnSave = SYNCONSAVE;
                Preference.SyncWIFI = SYNCWIFI;
                Preference.SyncOnStart = SYNCONSTART;
                Preference.IntervalSync = INTERVALSYNC;
                Preference.LastSync = LASTSYNC;
                Preference.UseSecurity = USESECURITY;

                await _IPreferencesDB.AddOrUpdateItemAsync(Preference);
                ShowShortMessage(Resources.Messages.SaveSuccess);
            }
        }

        public async void OnSaveClick()
        {
            if (IsBusy)
                return;

            try
            {
                IsBusy = true;

                var Continue = true;

                if (CANUSESECURITY)
                {
                    if (Continue &&
                        _PreviousUseSecurity != USESECURITY)
                    {
                        Continue = false;
                        await _AuthFingerPrint.AuthenticateAsync(Resources.Fields.Cancel);
                    }
                }

                if (Continue)
                {
                    await SaveChanges();
                    IsBusy = false;
                    await Navigation.PopAsync();
                }
                else
                    IsBusy = false;
            }
            catch (Exception ex)
            {
                DebugErrorLog.Log(ex);
                IsBusy = false;

            }
        }

        #region Clear Data Clicked
        public void OnClearDataClick()
        {
            if (IsBusy)
                return;

            OnMessageConfirmRaise(new MessageRaiseEvent(Resources.Messages.Confirm, Resources.Messages.ConfirmClearDatabase));
        }

        public async void ClearAllData()
        {
            if (IsBusy)
                return;
            bool Ok = false;
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.Messages.LoadProcessing))
            {
                IsBusy = true;
                try
                {
                    new DataBaseCleaner().ClearDataBase();

                    CurrentApp.EstablishmentKeyLogged = null;
                    CurrentApp.UserKeyLogged = null;
                    CurrentApp.TOKENDATA = null;
                    CurrentApp.ESTABLISHMENT = null;
                    CurrentApp.USERLOGGED = null;

                    IsBusy = false;

                    ShowShortMessage(Resources.Messages.ClearAllDataComplete);
                    Ok = true;
                }
                catch (Exception ex)
                {
                    DebugErrorLog.Log(ex);
                    IsBusy = false;
                }
                finally
                {
                    IsBusy = false;
                }
            }

            if (Ok)
                MessagingCenter.Send(this, "PREFERENCES_CLEARCOMPLETE", "COMPLETE");
        }

        #endregion
    }
}
