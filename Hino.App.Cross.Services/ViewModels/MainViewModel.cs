﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Hino.App.Cross.Services.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        public async Task ChangeCurrentPage(string pPage) =>
            await Navigation.PushAsync(pPage);
    }
}
