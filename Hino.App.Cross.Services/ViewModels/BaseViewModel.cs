﻿using CommonServiceLocator;
using FluentValidation;
using FluentValidation.Results;
using Hino.App.Cross.Models;
using Hino.App.Cross.Services.DB.Interfaces;
using Hino.App.Cross.Services.Navigation;
using Hino.App.Cross.Utils.Database;
using Hino.App.Cross.Utils.Exceptions;
using Hino.App.Cross.Utils.Extensions;
using Hino.App.Cross.Utils.Interfaces;
using Hino.App.Cross.Utils.Paging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using XF.Material.Forms.UI.Dialogs;

namespace Hino.App.Cross.Services.ViewModels
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        public INavigationService Navigation { get; }
        public IServiceLocator ServiceLocator { get; }
        public ICommand RefreshDataCommand { get; }
        public ICommand LoadMoreCommand { get; }
        public ICommand CloseCommand { get; }

        public IValidator _Validator;
        public ValidationResult _ValidationResult;
        protected readonly IPreferencesDB _IPreferencesDB;

        public void ShowShortMessage(string message)
        {
            var instance = DependencyService.Get<IMessage>();
            instance.ShowShortMessage(message, true);
        }

        public void ShowLongMessage(string message)
        {
            var instance = DependencyService.Get<IMessage>();
            instance.ShowShortMessage(message, false);
        }

        public async Task ShowSnackBarAsync(string message, int Duration = 2750) =>
            await MaterialDialog.Instance.SnackbarAsync(message, msDuration: Duration);

        public async Task<bool> ShowSnackBarActionAsync(string message, string pActionButton = "", int Duration = 2750) =>
             await MaterialDialog.Instance.SnackbarAsync(message, pActionButton, msDuration: Duration);

        // public IDBSQLite GetDBInstance() => DependencyService.Get<IDBSQLite>();

        public BaseViewModel()
        {
            RefreshDataCommand = new Command(async () => await RefreshData());
            LoadMoreCommand = new Command(async () => await LoadMoreData());
            CloseCommand = new Command(() => Navigation.PopModalAsync());

            ServiceLocator = CommonServiceLocator.ServiceLocator.Current;
            Navigation = ServiceLocator.GetInstance<INavigationService>();
            _IPreferencesDB = DInjection.GetIntance<IPreferencesDB>();
        }

        private bool _ShowSearch = false;
        public bool ShowSearch
        {
            get => _ShowSearch;
            set
            {
                SetProperty(ref _ShowSearch, value);
            }
        }

        int _pageNum = 1;
        public int PageNum
        {
            get { return _pageNum; }
            set
            {
                SetProperty(ref _pageNum, value);
            }
        }

        bool _isBusy = false;
        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                SetProperty(ref _isBusy, value);
                IsNotBusy = !value;
            }
        }

        bool _isNotBusy = true;
        public bool IsNotBusy
        {
            get { return _isNotBusy; }
            set { SetProperty(ref _isNotBusy, value); }
        }

        string _title = string.Empty;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private bool _ShowBarCode = false;
        public bool ShowBarCode
        {
            get => _ShowBarCode;
            set
            {
                SetProperty(ref _ShowBarCode, value);
            }
        }

        protected bool SetProperty<T>(ref T backingStore, T value,
            [CallerMemberName]string propertyName = "",
            Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(backingStore, value))
                return false;

            backingStore = value;
            onChanged?.Invoke();
            OnPropertyChanged(propertyName);
            return true;
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var changed = PropertyChanged;
            if (changed == null)
                return;

            changed.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        protected virtual async Task RefreshData()
        {
            await Task.Delay(10);
            if (IsBusy)
                return;
            this.PageNum = 1;
        }

        protected virtual Task LoadMoreData()
        {
            return Task.FromResult(1);
        }

        public virtual int NextPageNum() =>
            PageNum++;

        #region Message Information
        public event EventHandler<IMessageRaise> MessageRaise;
        protected void OnMessageRaise(IMessageRaise e) =>
            MessageRaise?.Invoke(this, e);
        #endregion

        #region Message Confirmation
        public event EventHandler<IMessageRaise> MessageConfirmRaise;
        protected void OnMessageConfirmRaise(IMessageRaise e) =>
            MessageConfirmRaise?.Invoke(this, e);
        #endregion

        #region Load Complete
        public event EventHandler<ILoadComplete> OnLoadComplete;
        protected void OnLoadCompleteRaise(ILoadComplete e)
        {
            this.IsBusy = false;
            OnLoadComplete?.Invoke(this, e);
        }
        #endregion

        #region Display Validation Model Errors
        public void DisplayValidationModelErrors(bool first = false)
        {
            if (_ValidationResult != null)
            {
                try
                {
                    if (_ValidationResult.Errors.Any())
                        UtilsExtensions.ErrrorVibrate();

                    foreach (var error in _ValidationResult.Errors)
                    {
                        var propertyName = $"HAS{error.PropertyName.ToUpper()}ERROR";

                        this.GetType().GetProperty(propertyName)
                            .SetValue(this, true);

                        propertyName = $"LBL{error.PropertyName.ToUpper()}ERRORTEXT";

                        var PlaceHolderValue = "";
                        if (error.FormattedMessagePlaceholderValues.TryGetValue("PropertyName", out object value))
                            PlaceHolderValue = value.ToString();

                        this.GetType().GetProperty(propertyName)
                            .SetValue(this, string.Format(error.ErrorMessage, PlaceHolderValue));
                    }
                }
                catch (Exception e)
                {
                    DebugErrorLog.Log(e);
                }
            }

            /*if (_ValidationResult != null)
            {
                foreach (var error in _ValidationResult.Errors)
                {
                    ShowShortMessage(error.ErrorMessage);
                    if (first)
                        break;
                }
            }*/
        }
        #endregion

        #region Validate Model
        public async Task<bool> ValidateModelAsync<T>(T model)
        {
            try
            {
                var properties = this.GetType().GetProperties();

                var propErrors = properties.Where(r => 
                    r.CanWrite &&
                    r.CanRead &&
                    r.PropertyType == typeof(bool) &&
                    r.Name.StartsWith("HAS") &&
                    r.Name.EndsWith("ERROR"));

                foreach (var item in propErrors)
                    item.SetValue(this, false);

                propErrors = properties.Where(r =>
                    r.CanWrite &&
                    r.CanRead &&
                    r.PropertyType == typeof(string) &&
                    r.Name.StartsWith("LBL") &&
                    r.Name.EndsWith("ERRORTEXT"));

                foreach (var item in propErrors)
                    item.SetValue(this, "");

            }
            catch (Exception ex)
            {
                DebugErrorLog.Log(ex);
            }

            try
            {
                _ValidationResult = await _Validator?.ValidateAsync(model);

                return _ValidationResult.IsValid;
            }
            catch (Exception ex)
            {
                DebugErrorLog.Log(ex);
                throw new NotImplementedException("Tá sem a instância Bahiano!!!!");
            }
        }
        #endregion

        //#region Change Page
        //public async Task ChangePage(Page page) =>
        //    await App.Current.MainPage.Navigation.PushAsync(page);
        //#endregion

        #region Infinity List View
        public void InfiniteListView(ListView pList)
        {
            pList.ItemAppearing -= null;
            pList.ItemAppearing += InfiniteListView_ItemAppearing;
        }

        void InfiniteListView_ItemAppearing(object sender, ItemVisibilityEventArgs e)
        {
            var lv = ((ListView)sender);
            var items = lv.ItemsSource as IList;

            if (IsBusy || items.Count == 0 || CurrentApp.IsModal || CurrentApp.IsFilterModal)
                return;

            if (items != null && e.Item == items[items.Count - 1])
            {
                if (LoadMoreCommand != null && LoadMoreCommand.CanExecute(null))
                {
                    NextPageNum();
                    LoadMoreCommand.Execute(null);
                }
            }
        }
        #endregion

        #region SearchData

        protected string _SearchData;
        public string SEARCHDATA
        {
            get
            {
                return _SearchData;
            }
            set
            {
                SetProperty(ref _SearchData, value);
            }
        }

        public void SearchEvent(SearchBar sbText)
        {
            sbText.TextChanged -= null;
            sbText.TextChanged += SbText_TextChanged;
            sbText.SearchButtonPressed -= null;
            sbText.SearchButtonPressed += SbText_SearchButtonPressed;
        }

        private async void SbText_SearchButtonPressed(object sender, EventArgs e)
        {
            await SearchData_Changed();
        }

        private async void SbText_TextChanged(object sender, TextChangedEventArgs e)
        {
            await SearchData_Changed();
        }

        public virtual Task SearchData_Changed()
        {
            return Task.FromResult<object>(null);
        }
        #endregion

        /// <summary>
        /// When overriden, allow to add additional logic to this view model when the view where it was attached was pushed using <see cref="INavigationService.PushAsync(string, object)"/>.
        /// </summary>
        /// <param name="navigationParameter">The navigation parameter to pass after the view was pushed.</param>
        public virtual void OnViewPushed(object navigationParameter = null) { }

        /// <summary>
        /// When overriden, allow to add additional logic to this view model when the view where it was attached was popped using <see cref="INavigationService.PopAsync"/>.
        /// </summary>
        public virtual void OnViewPopped()
        {
            CleanUp();
        }

        public virtual void CleanUp() { }

        protected virtual SortResult CreateDefaultSort<T>() where T : BaseEntity
        {
            Func<T, object> orderByProperty = ordering => ordering.Id;
            return new SortResult
            {
                Descending = true,
                Expression = orderByProperty
            };

        }

        public virtual async Task SetFilterExpression(object pFilter)
        {
            await RefreshData();
        }

        public virtual Task SetSortExpression(object pSort)
        {
            return Task.FromResult<object>(null);
        }

        public virtual void OnFilterModalShow(bool isModal)
        {
            CurrentApp.IsFilterModal = isModal;
        }

        // #region FAB Button
        // public void FABButton(HIFloatingButton pfabBtn)
        // {
        //     pfabBtn.Clicked += OnFABButtonTapped;
        // }
        // 
        // public virtual void OnFABButtonTapped(object sender, EventArgs e)
        // {
        // 
        // }
        // #endregion
    }

    public static class ObservableCollectionEx
    {
        #region Copy From List
        public static void CopyFromList<T>(this ObservableCollection<T> to, IEnumerable<T> from, bool pClearList = true)
        {
            if (pClearList)
                to.Clear();

            foreach (var item in from)
            {
                to.Add(item);
            }
        }
        #endregion

        #region Copy From Paged Result
        public static void CopyFromPagedResult<T>(this ObservableCollection<T> to, PagedResult<T> from, bool pClearList = true) where T : class
        {
            if (pClearList)
                to.Clear();

            foreach (var item in from.Results)
            {
                to.Add(item);
            }
        }
        #endregion

        /*#region Copy From Paged Result
        public static void CopyFromPagedResult<T>(this MultiSelectObservableCollection<T> to, PagedResult<T> from, bool pClearList = true) where T : class
        {
            if (pClearList)
                to.Clear();

            foreach (var item in from.Results)
            {
                to.Add(item);
            }
        }
        #endregion*/

    }
}
