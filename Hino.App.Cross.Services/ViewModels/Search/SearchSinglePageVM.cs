﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Hino.App.Cross.Utils.Paging;
using Hino.App.Cross.Services.Search.Interfaces;
using System.Windows.Input;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using Hino.App.Cross.Services.Search;
using Rg.Plugins.Popup.Services;
using Hino.App.Cross.Utils.Exceptions;

namespace Hino.App.Cross.Services.ViewModels.Search
{
    public class SearchSinglePageVM : BaseViewModel
    {
        readonly string KeyName, Description, Details;

        public ObservableCollection<SearchItemList> Items { get; set; }

        SearchItemList _SelectedItem;
        public SearchItemList SelectedItem
        {
            get => _SelectedItem;
            set
            {
                SetProperty(ref _SelectedItem, value);
            }
        }

        public ICommand CmdOkClick { protected set; get; }
        public ICommand CmdBackClick { protected set; get; }

        readonly ISearchDataResult _IRepositorySearch;
        public SearchSinglePageVM(ISearchDataResult pIRepositorySearch)
        {
            CmdOkClick = new Command(OnOkClick);
            CmdBackClick = new Command(OnBackClick);
            Items = new ObservableCollection<SearchItemList>();

            _IRepositorySearch = pIRepositorySearch;
            KeyName = _IRepositorySearch.KeyName;
            Description = _IRepositorySearch.Description;
            Details = _IRepositorySearch.Details;

        }

        #region Get Data From Source
        private async Task<PagedResult<SearchItemList>> GetDataFromSourceAsync()
        {
            return await _IRepositorySearch.GetDataFromSourceAsync(_SearchData, base.PageNum);
        }
        #endregion

        #region Search Data
        public async override Task SearchData_Changed()
        {
            base.PageNum = 1;

            Items.CopyFromPagedResult(await GetDataFromSourceAsync());
        }
        #endregion

        protected async override Task LoadMoreData()
        {
            IsBusy = true;

            try
            {
                Items.CopyFromPagedResult(await GetDataFromSourceAsync(), false);
                IsBusy = false;
            }
            catch (Exception e)
            {
                DebugErrorLog.Log(e);
                ShowShortMessage(e.Message);

                IsBusy = false;
            }
            finally
            {
                IsBusy = false;
            }
        }

        protected async override Task RefreshData()
        {
            await base.RefreshData();

            IsBusy = true;

            base.PageNum = 1;
            Items.Clear();

            await LoadMoreData();
        }

        private async void OnBackClick(object obj)
        {
            MessagingCenter.Send(this, "ModalClosed", "a");
            await PopupNavigation.Instance.PopAsync();
        }

        private async void OnOkClick(object obj)
        {
            if (_SelectedItem == null)
            {
                ShowShortMessage("Selecione o registro");
                return;
            }

            MessagingCenter.Send(this, "ItemSelected", SelectedItem);
            await PopupNavigation.Instance.PopAsync();
        }
    }

    
}
