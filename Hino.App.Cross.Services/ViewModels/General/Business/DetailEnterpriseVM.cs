﻿using Hino.App.Cross.Models.General.Business;
using Hino.App.Cross.Services.Application.General;
using Hino.App.Cross.Services.DB.Interfaces.General.Business;
using Hino.App.Cross.Services.Utils;
using Hino.App.Cross.Utils.Enums;
using Hino.App.Cross.Utils.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Hino.App.Cross.Services.ViewModels.General.Business
{
    public class DetailEnterpriseVM : BaseViewModel
    {
        private readonly IGEEnterprisesDB _IGEEnterprisesDB;
        public GEEnterprisesModel _GEEnterprisesModel;

        #region Labels
        public string LBLADDRESS => Resources.Fields.CommercialAddress;
        public string LBLDELIVERYADDRESS => Resources.Fields.DeliveryAddress;
        public string LBLLEVYADDRESS => Resources.Fields.LevyAddress;
        #endregion

        #region Fields
        string _statussinccolor;
        public string STATUSSINCCOLOR
        {
            get => _statussinccolor; set => SetProperty(ref _statussinccolor, value);
        }
        string _displayname;
        public string DISPLAYNAME
        {
            get => _displayname; set => SetProperty(ref _displayname, value);
        }
        string _initials;
        public string INITIALS
        {
            get => _initials; set => SetProperty(ref _initials, value);
        }
        string _razaosocial;
        public string RAZAOSOCIAL
        {
            get => _razaosocial; set => SetProperty(ref _razaosocial, value);
        }
        string _nomefantasia;
        public string NOMEFANTASIA
        {
            get => _nomefantasia; set => SetProperty(ref _nomefantasia, value);
        }
        string _cnpjcpf;
        public string CNPJCPF
        {
            get => _cnpjcpf; set => SetProperty(ref _cnpjcpf, value);
        }
        string _cellphone;
        public string CELLPHONE
        {
            get => _cellphone; set => SetProperty(ref _cellphone, value);
        }
        string _phone;
        public string PHONE
        {
            get => _phone; set => SetProperty(ref _phone, value);
        }
        string _email;
        public string EMAIL
        {
            get => _email; set => SetProperty(ref _email, value);
        }
        string _site;
        public string SITE
        {
            get => _site; set => SetProperty(ref _site, value);
        }
        string _zipcode;
        public string ZIPCODE
        {
            get => _zipcode; set => SetProperty(ref _zipcode, value);
        }
        string _address;
        public string ADDRESS
        {
            get => _address; set => SetProperty(ref _address, value);
        }
        string _district;
        public string DISTRICT
        {
            get => _district; set => SetProperty(ref _district, value);
        }
        string _num;
        public string NUM
        {
            get => _num; set => SetProperty(ref _num, value);
        }
        string _complement;
        public string COMPLEMENT
        {
            get => _complement; set => SetProperty(ref _complement, value);
        }
        string _uf;
        public string UF
        {
            get => _uf; set => SetProperty(ref _uf, value);
        }
        string _cityName;
        public string CITYNAME
        {
            get => _cityName; set => SetProperty(ref _cityName, value);
        }

        bool _showcellphone;
        public bool SHOWCELLPHONE
        {
            get => _showcellphone; set => SetProperty(ref _showcellphone, value);
        }
        bool _showphone;
        public bool SHOWPHONE
        {
            get => _showphone; set => SetProperty(ref _showphone, value);
        }
        bool _showemail;
        public bool SHOWEMAIL
        {
            get => _showemail; set => SetProperty(ref _showemail, value);
        }
        bool _showsite;
        public bool SHOWSITE
        {
            get => _showsite; set => SetProperty(ref _showsite, value);
        }
        bool _showzipcode;
        public bool SHOWZIPCODE
        {
            get => _showzipcode; set => SetProperty(ref _showzipcode, value);
        }
        bool _showaddress;
        public bool SHOWADDRESS
        {
            get => _showaddress; set => SetProperty(ref _showaddress, value);
        }
        bool _showdistrict;
        public bool SHOWDISTRICT
        {
            get => _showdistrict; set => SetProperty(ref _showdistrict, value);
        }
        bool _shownum;
        public bool SHOWNUM
        {
            get => _shownum; set => SetProperty(ref _shownum, value);
        }
        bool _showcomplement;
        public bool SHOWCOMPLEMENT
        {
            get => _showcomplement; set => SetProperty(ref _showcomplement, value);
        }
        bool _showuf;
        public bool SHOWUF
        {
            get => _showuf; set => SetProperty(ref _showuf, value);
        }
        bool _showcityName;
        public bool SHOWCITYNAME
        {
            get => _showcityName; set => SetProperty(ref _showcityName, value);
        }

        string _deliverycellphone;
        public string DELIVERYCELLPHONE
        {
            get => _deliverycellphone; set => SetProperty(ref _deliverycellphone, value);
        }
        string _deliveryphone;
        public string DELIVERYPHONE
        {
            get => _deliveryphone; set => SetProperty(ref _deliveryphone, value);
        }
        string _deliveryemail;
        public string DELIVERYEMAIL
        {
            get => _deliveryemail; set => SetProperty(ref _deliveryemail, value);
        }
        string _deliverysite;
        public string DELIVERYSITE
        {
            get => _deliverysite; set => SetProperty(ref _deliverysite, value);
        }
        string _deliveryzipcode;
        public string DELIVERYZIPCODE
        {
            get => _deliveryzipcode; set => SetProperty(ref _deliveryzipcode, value);
        }
        string _deliveryaddress;
        public string DELIVERYADDRESS
        {
            get => _deliveryaddress; set => SetProperty(ref _deliveryaddress, value);
        }
        string _deliverydistrict;
        public string DELIVERYDISTRICT
        {
            get => _deliverydistrict; set => SetProperty(ref _deliverydistrict, value);
        }
        string _deliverynum;
        public string DELIVERYNUM
        {
            get => _deliverynum; set => SetProperty(ref _deliverynum, value);
        }
        string _deliverycomplement;
        public string DELIVERYCOMPLEMENT
        {
            get => _deliverycomplement; set => SetProperty(ref _deliverycomplement, value);
        }
        string _deliveryuf;
        public string DELIVERYUF
        {
            get => _deliveryuf; set => SetProperty(ref _deliveryuf, value);
        }
        string _deliverycityName;
        public string DELIVERYCITYNAME
        {
            get => _deliverycityName; set => SetProperty(ref _deliverycityName, value);
        }

        bool _deliveryshowcellphone;
        public bool DELIVERYSHOWCELLPHONE
        {
            get => _deliveryshowcellphone; set => SetProperty(ref _deliveryshowcellphone, value);
        }
        bool _deliveryshowphone;
        public bool DELIVERYSHOWPHONE
        {
            get => _deliveryshowphone; set => SetProperty(ref _deliveryshowphone, value);
        }
        bool _deliveryshowemail;
        public bool DELIVERYSHOWEMAIL
        {
            get => _deliveryshowemail; set => SetProperty(ref _deliveryshowemail, value);
        }
        bool _deliveryshowsite;
        public bool DELIVERYSHOWSITE
        {
            get => _deliveryshowsite; set => SetProperty(ref _deliveryshowsite, value);
        }
        bool _deliveryshowzipcode;
        public bool DELIVERYSHOWZIPCODE
        {
            get => _deliveryshowzipcode; set => SetProperty(ref _deliveryshowzipcode, value);
        }
        bool _deliveryshowaddress;
        public bool DELIVERYSHOWADDRESS
        {
            get => _deliveryshowaddress; set => SetProperty(ref _deliveryshowaddress, value);
        }
        bool _deliveryshowdistrict;
        public bool DELIVERYSHOWDISTRICT
        {
            get => _deliveryshowdistrict; set => SetProperty(ref _deliveryshowdistrict, value);
        }
        bool _deliveryshownum;
        public bool DELIVERYSHOWNUM
        {
            get => _deliveryshownum; set => SetProperty(ref _deliveryshownum, value);
        }
        bool _deliveryshowcomplement;
        public bool DELIVERYSHOWCOMPLEMENT
        {
            get => _deliveryshowcomplement; set => SetProperty(ref _deliveryshowcomplement, value);
        }
        bool _deliveryshowuf;
        public bool DELIVERYSHOWUF
        {
            get => _deliveryshowuf; set => SetProperty(ref _deliveryshowuf, value);
        }
        bool _deliveryshowcityName;
        public bool DELIVERYSHOWCITYNAME
        {
            get => _deliveryshowcityName; set => SetProperty(ref _deliveryshowcityName, value);
        }


        string _levycellphone;
        public string LEVYCELLPHONE
        {
            get => _levycellphone; set => SetProperty(ref _levycellphone, value);
        }
        string _levyphone;
        public string LEVYPHONE
        {
            get => _levyphone; set => SetProperty(ref _levyphone, value);
        }
        string _levyemail;
        public string LEVYEMAIL
        {
            get => _levyemail; set => SetProperty(ref _levyemail, value);
        }
        string _levysite;
        public string LEVYSITE
        {
            get => _levysite; set => SetProperty(ref _levysite, value);
        }
        string _levyzipcode;
        public string LEVYZIPCODE
        {
            get => _levyzipcode; set => SetProperty(ref _levyzipcode, value);
        }
        string _levyaddress;
        public string LEVYADDRESS
        {
            get => _levyaddress; set => SetProperty(ref _levyaddress, value);
        }
        string _levydistrict;
        public string LEVYDISTRICT
        {
            get => _levydistrict; set => SetProperty(ref _levydistrict, value);
        }
        string _levynum;
        public string LEVYNUM
        {
            get => _levynum; set => SetProperty(ref _levynum, value);
        }
        string _levycomplement;
        public string LEVYCOMPLEMENT
        {
            get => _levycomplement; set => SetProperty(ref _levycomplement, value);
        }
        string _levyuf;
        public string LEVYUF
        {
            get => _levyuf; set => SetProperty(ref _levyuf, value);
        }
        string _levycityName;
        public string LEVYCITYNAME
        {
            get => _levycityName; set => SetProperty(ref _levycityName, value);
        }

        bool _levyshowcellphone;
        public bool LEVYSHOWCELLPHONE
        {
            get => _levyshowcellphone; set => SetProperty(ref _levyshowcellphone, value);
        }
        bool _levyshowphone;
        public bool LEVYSHOWPHONE
        {
            get => _levyshowphone; set => SetProperty(ref _levyshowphone, value);
        }
        bool _levyshowemail;
        public bool LEVYSHOWEMAIL
        {
            get => _levyshowemail; set => SetProperty(ref _levyshowemail, value);
        }
        bool _levyshowsite;
        public bool LEVYSHOWSITE
        {
            get => _levyshowsite; set => SetProperty(ref _levyshowsite, value);
        }
        bool _levyshowzipcode;
        public bool LEVYSHOWZIPCODE
        {
            get => _levyshowzipcode; set => SetProperty(ref _levyshowzipcode, value);
        }
        bool _levyshowaddress;
        public bool LEVYSHOWADDRESS
        {
            get => _levyshowaddress; set => SetProperty(ref _levyshowaddress, value);
        }
        bool _levyshowdistrict;
        public bool LEVYSHOWDISTRICT
        {
            get => _levyshowdistrict; set => SetProperty(ref _levyshowdistrict, value);
        }
        bool _levyshownum;
        public bool LEVYSHOWNUM
        {
            get => _levyshownum; set => SetProperty(ref _levyshownum, value);
        }
        bool _levyshowcomplement;
        public bool LEVYSHOWCOMPLEMENT
        {
            get => _levyshowcomplement; set => SetProperty(ref _levyshowcomplement, value);
        }
        bool _levyshowuf;
        public bool LEVYSHOWUF
        {
            get => _levyshowuf; set => SetProperty(ref _levyshowuf, value);
        }
        bool _levyshowcityName;
        public bool LEVYSHOWCITYNAME
        {
            get => _levyshowcityName; set => SetProperty(ref _levyshowcityName, value);
        }

        #endregion

        public DetailEnterpriseVM()
        {
            _GEEnterprisesModel = new GEEnterprisesModel();
            _IGEEnterprisesDB = DInjection.GetIntance<IGEEnterprisesDB>();
        }

        public async override void OnViewPushed(object navigationParameter = null)
        {
            base.OnViewPushed(navigationParameter);
            _GEEnterprisesModel = (GEEnterprisesModel)navigationParameter;
            await LoadRegister();
        }

        #region Load Register
        public async Task LoadRegister()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            if (_GEEnterprisesModel != null)
            {
                var Enterprise = await _IGEEnterprisesDB.GetItemAsync(_GEEnterprisesModel.Id);

                if (Enterprise != null)
                {
                    _GEEnterprisesModel = Enterprise;
                    RAZAOSOCIAL = _GEEnterprisesModel.RazaoSocial;
                    NOMEFANTASIA = _GEEnterprisesModel.NomeFantasia;
                    CNPJCPF = _GEEnterprisesModel.CNPJCPFType;
                    INITIALS = _GEEnterprisesModel.Initials;

                    PHONE = _GEEnterprisesModel.CommercialAddress_Phone ?? "";
                    CELLPHONE = _GEEnterprisesModel.CommercialAddress_CellPhone ?? "";
                    EMAIL = _GEEnterprisesModel.CommercialAddress_Email ?? "";
                    SITE = _GEEnterprisesModel.CommercialAddress_Site ?? "";
                    ZIPCODE = _GEEnterprisesModel.CommercialAddress_ZipCode ?? "";
                    ADDRESS = $"{_GEEnterprisesModel.CommercialAddress_Address}, {_GEEnterprisesModel.CommercialAddress_Num}"; 
                    DISTRICT = _GEEnterprisesModel.CommercialAddress_District ?? "";
                    NUM = _GEEnterprisesModel.CommercialAddress_Num ?? "";
                    COMPLEMENT = _GEEnterprisesModel.CommercialAddress_Complement ?? "";
                    CITYNAME = _GEEnterprisesModel.CommercialAddress_CityName ?? "";
                    DISPLAYNAME = _GEEnterprisesModel.DisplayName.SubStrTitle();
                    STATUSSINCCOLOR = _GEEnterprisesModel.StatusSincColor;

                    DELIVERYPHONE = _GEEnterprisesModel.DeliveryAddress_Phone ?? "";
                    DELIVERYCELLPHONE = _GEEnterprisesModel.DeliveryAddress_CellPhone ?? "";
                    DELIVERYEMAIL = _GEEnterprisesModel.DeliveryAddress_Email ?? "";
                    DELIVERYSITE = _GEEnterprisesModel.DeliveryAddress_Site ?? "";
                    DELIVERYZIPCODE = _GEEnterprisesModel.DeliveryAddress_ZipCode ?? "";
                    DELIVERYADDRESS = $"{_GEEnterprisesModel.DeliveryAddress_Address}, {_GEEnterprisesModel.DeliveryAddress_Num}";
                    DELIVERYDISTRICT = _GEEnterprisesModel.DeliveryAddress_District ?? "";
                    DELIVERYNUM = _GEEnterprisesModel.DeliveryAddress_Num ?? "";
                    DELIVERYCOMPLEMENT = _GEEnterprisesModel.DeliveryAddress_Complement ?? "";
                    DELIVERYCITYNAME = _GEEnterprisesModel.DeliveryAddress_CityName ?? "";

                    LEVYPHONE = _GEEnterprisesModel.LevyAddress_Phone ?? "";
                    LEVYCELLPHONE = _GEEnterprisesModel.LevyAddress_CellPhone ?? "";
                    LEVYEMAIL = _GEEnterprisesModel.LevyAddress_Email ?? "";
                    LEVYSITE = _GEEnterprisesModel.LevyAddress_Site ?? "";
                    LEVYZIPCODE = _GEEnterprisesModel.LevyAddress_ZipCode ?? "";
                    LEVYADDRESS = $"{_GEEnterprisesModel.LevyAddress_Address}, {_GEEnterprisesModel.LevyAddress_Num}";
                    LEVYDISTRICT = _GEEnterprisesModel.LevyAddress_District ?? "";
                    LEVYNUM = _GEEnterprisesModel.LevyAddress_Num ?? "";
                    LEVYCOMPLEMENT = _GEEnterprisesModel.LevyAddress_Complement ?? "";
                    LEVYCITYNAME = _GEEnterprisesModel.LevyAddress_CityName ?? "";

                    SHOWPHONE = !string.IsNullOrEmpty(_GEEnterprisesModel.CommercialAddress_Phone);
                    SHOWCELLPHONE = !string.IsNullOrEmpty(_GEEnterprisesModel.CommercialAddress_CellPhone);
                    SHOWEMAIL = !string.IsNullOrEmpty(_GEEnterprisesModel.CommercialAddress_Email);
                    SHOWSITE = !string.IsNullOrEmpty(_GEEnterprisesModel.CommercialAddress_Site);
                    SHOWZIPCODE = !string.IsNullOrEmpty(_GEEnterprisesModel.CommercialAddress_ZipCode);
                    SHOWADDRESS = !string.IsNullOrEmpty(_GEEnterprisesModel.CommercialAddress_Address);
                    SHOWDISTRICT = !string.IsNullOrEmpty(_GEEnterprisesModel.CommercialAddress_District);
                    SHOWNUM = !string.IsNullOrEmpty(_GEEnterprisesModel.CommercialAddress_Num);
                    SHOWCOMPLEMENT = !string.IsNullOrEmpty(_GEEnterprisesModel.CommercialAddress_Complement);
                    SHOWCITYNAME = !string.IsNullOrEmpty(_GEEnterprisesModel.CommercialAddress_CityName);

                    DELIVERYSHOWPHONE = !string.IsNullOrEmpty(_GEEnterprisesModel.DeliveryAddress_Phone);
                    DELIVERYSHOWCELLPHONE = !string.IsNullOrEmpty(_GEEnterprisesModel.DeliveryAddress_CellPhone);
                    DELIVERYSHOWEMAIL = !string.IsNullOrEmpty(_GEEnterprisesModel.DeliveryAddress_Email);
                    DELIVERYSHOWSITE = !string.IsNullOrEmpty(_GEEnterprisesModel.DeliveryAddress_Site);
                    DELIVERYSHOWZIPCODE = !string.IsNullOrEmpty(_GEEnterprisesModel.DeliveryAddress_ZipCode);
                    DELIVERYSHOWADDRESS = !string.IsNullOrEmpty(_GEEnterprisesModel.DeliveryAddress_Address);
                    DELIVERYSHOWDISTRICT = !string.IsNullOrEmpty(_GEEnterprisesModel.DeliveryAddress_District);
                    DELIVERYSHOWNUM = !string.IsNullOrEmpty(_GEEnterprisesModel.DeliveryAddress_Num);
                    DELIVERYSHOWCOMPLEMENT = !string.IsNullOrEmpty(_GEEnterprisesModel.DeliveryAddress_Complement);
                    DELIVERYSHOWCITYNAME = !string.IsNullOrEmpty(_GEEnterprisesModel.DeliveryAddress_CityName);

                    LEVYSHOWPHONE = !string.IsNullOrEmpty(_GEEnterprisesModel.LevyAddress_Phone);
                    LEVYSHOWCELLPHONE = !string.IsNullOrEmpty(_GEEnterprisesModel.LevyAddress_CellPhone);
                    LEVYSHOWEMAIL = !string.IsNullOrEmpty(_GEEnterprisesModel.LevyAddress_Email);
                    LEVYSHOWSITE = !string.IsNullOrEmpty(_GEEnterprisesModel.LevyAddress_Site);
                    LEVYSHOWZIPCODE = !string.IsNullOrEmpty(_GEEnterprisesModel.LevyAddress_ZipCode);
                    LEVYSHOWADDRESS = !string.IsNullOrEmpty(_GEEnterprisesModel.LevyAddress_Address);
                    LEVYSHOWDISTRICT = !string.IsNullOrEmpty(_GEEnterprisesModel.LevyAddress_District);
                    LEVYSHOWNUM = !string.IsNullOrEmpty(_GEEnterprisesModel.LevyAddress_Num);
                    LEVYSHOWCOMPLEMENT = !string.IsNullOrEmpty(_GEEnterprisesModel.LevyAddress_Complement);
                    LEVYSHOWCITYNAME = !string.IsNullOrEmpty(_GEEnterprisesModel.LevyAddress_CityName);
                }
            }

            IsBusy = false;
        }
        #endregion

        public void CallCellPhoneAsync(AddressType pType)
        {
            if (IsBusy)
                return;

            string CellPhone;
            switch (pType)
            {
                case AddressType.CommercialAddress:
                    CellPhone = CELLPHONE;
                    break;
                case AddressType.DeliveryAddress:
                    CellPhone = DELIVERYCELLPHONE;
                    break;
                case AddressType.LevyAddress:
                    CellPhone = LEVYCELLPHONE;
                    break;
                default:
                    IsBusy = false;
                    return;
            }

            if (CellPhone.IsEmpty())
                return;

            IsBusy = true;
            var message = UtilsExtensions.PhoneCall(CellPhone);

            if (!string.IsNullOrEmpty(message))
                ShowShortMessage(message);
            IsBusy = false;
        }

        public void CallPhoneAsync(AddressType pType)
        {
            if (IsBusy)
                return;

            string Phone;
            switch (pType)
            {
                case AddressType.CommercialAddress:
                    Phone = PHONE;
                    break;
                case AddressType.DeliveryAddress:
                    Phone = DELIVERYPHONE;
                    break;
                case AddressType.LevyAddress:
                    Phone = LEVYPHONE;
                    break;
                default:
                    IsBusy = false;
                    return;
            }

            if (Phone.IsEmpty())
                return;

            IsBusy = true;
            var message = UtilsExtensions.PhoneCall(Phone);

            if (!string.IsNullOrEmpty(message))
                ShowShortMessage(message);
            IsBusy = false;
        }

        public async Task SendEmailAsync(AddressType pType)
        {
            if (IsBusy)
                return;

            string Email;
            switch (pType)
            {
                case AddressType.CommercialAddress:
                    Email = EMAIL;
                    break;
                case AddressType.DeliveryAddress:
                    Email = DELIVERYEMAIL;
                    break;
                case AddressType.LevyAddress:
                    Email = LEVYEMAIL;
                    break;
                default:
                    IsBusy = false;
                    return;
            }

            if (Email.IsEmpty())
                return;

            IsBusy = true;
            var message = await UtilsExtensions.SendEmailAsync("Contato", "", new List<string> { Email });

            if (!string.IsNullOrEmpty(message))
                ShowShortMessage(message);
            IsBusy = false;
        }

        public async Task BrowserAsync(AddressType pType)
        {
            if (IsBusy)
                return;

            string Site;
            switch (pType)
            {
                case AddressType.CommercialAddress:
                    Site = SITE;
                    break;
                case AddressType.DeliveryAddress:
                    Site = DELIVERYSITE;
                    break;
                case AddressType.LevyAddress:
                    Site = LEVYSITE;
                    break;
                default:
                    IsBusy = false;
                    return;
            }

            if (Site.IsEmpty())
                return;

            IsBusy = true;
            var message = await UtilsExtensions.OpenBrowserAsync(Site);

            if (!string.IsNullOrEmpty(message))
                ShowShortMessage(message);
            IsBusy = false;
        }

        public async Task GeoAsync(AddressType pType)
        {
            if (IsBusy)
                return;

            IsBusy = true;

            string Number;
            string ZipCode;
            switch (pType)
            {
                case AddressType.CommercialAddress:
                    ZipCode = ZIPCODE;
                    Number = NUM;
                    break;
                case AddressType.DeliveryAddress:
                    ZipCode = DELIVERYZIPCODE;
                    Number = DELIVERYNUM;
                    break;
                case AddressType.LevyAddress:
                    ZipCode = LEVYZIPCODE;
                    Number = LEVYNUM;
                    break;
                default:
                    IsBusy = false;
                    return;
            }

            var cepApplication = new CEPApplication();
            await cepApplication.OpenZipCodeInMapAsync(ZipCode, Number);

           IsBusy = false;
        }

        public async Task SendSMSAsync(AddressType pType)
        {
            if (IsBusy)
                return;

            string CellPhone;
            switch (pType)
            {
                case AddressType.CommercialAddress:
                    CellPhone = CELLPHONE;
                    break;
                case AddressType.DeliveryAddress:
                    CellPhone = DELIVERYCELLPHONE;
                    break;
                case AddressType.LevyAddress:
                    CellPhone = LEVYCELLPHONE;
                    break;
                default:
                    IsBusy = false;
                    return;
            }

            if (CellPhone.IsEmpty())
                return;

            IsBusy = true;

            var message = await UtilsExtensions.SendSmsAsync(CellPhone);

            if (!string.IsNullOrEmpty(message))
                ShowShortMessage(message);
            IsBusy = false;


            IsBusy = false;
        }

        public async override void OnViewPopped()
        {
            base.OnViewPopped();
            await LoadRegister();
        }
    }
}
