﻿using Hino.App.Cross.Utils.Extensions;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace Hino.App.Cross.Services.ViewModels.General.Business
{
    public class EnterpriseGeoVM: BaseViewModel
    {
        #region Labels
        public string LBLZIPCODEPLACEHOLDER { get => App.Cross.Resources.Fields.ZipCode; }
        public string LBLZIPCODEHELPERTEXT { get => App.Cross.Resources.Fields.ZipCode; }
        string _lblzipcodeerrortext = string.Format(Resources.Messages.InformGeneric, App.Cross.Resources.Fields.ZipCode);
        public string LBLZIPCODEERRORTEXT { get => _lblzipcodeerrortext; set => SetProperty(ref _lblzipcodeerrortext, value); }

        public string LBLADDRESSPLACEHOLDER { get => App.Cross.Resources.Fields.Address; }
        public string LBLADDRESSHELPERTEXT { get => App.Cross.Resources.Fields.Address; }
        string _lbladdresserrortext = string.Format(Resources.Messages.InformGeneric, App.Cross.Resources.Fields.Address);
        public string LBLADDRESSERRORTEXT { get => _lbladdresserrortext; set => SetProperty(ref _lbladdresserrortext, value); }

        public string LBLDISTRICTPLACEHOLDER { get => App.Cross.Resources.Fields.District; }
        public string LBLDISTRICTHELPERTEXT { get => App.Cross.Resources.Fields.District; }
        string _lbldistricterrortext = string.Format(Resources.Messages.InformGeneric, App.Cross.Resources.Fields.District);
        public string LBLDISTRICTERRORTEXT { get => _lbldistricterrortext; set => SetProperty(ref _lbldistricterrortext, value); }

        public string LBLNUMPLACEHOLDER { get => App.Cross.Resources.Fields.Num; }
        public string LBLNUMHELPERTEXT { get => App.Cross.Resources.Fields.Num; }
        string _lblnumerrortext = string.Format(Resources.Messages.InformGeneric, App.Cross.Resources.Fields.Num);
        public string LBLNUMERRORTEXT { get => _lblnumerrortext; set => SetProperty(ref _lblnumerrortext, value); }

        public string LBLCOMPLEMENTPLACEHOLDER { get => App.Cross.Resources.Fields.Complement; }
        public string LBLCOMPLEMENTHELPERTEXT { get => App.Cross.Resources.Fields.Complement; }
        string _lblcomplementerrortext = string.Format(Resources.Messages.InformGeneric, App.Cross.Resources.Fields.Complement);
        public string LBLCOMPLEMENTERRORTEXT { get => _lblcomplementerrortext; set => SetProperty(ref _lblcomplementerrortext, value); }

        public string LBLUFPLACEHOLDER { get => App.Cross.Resources.Fields.UF; }
        public string LBLUFHELPERTEXT { get => App.Cross.Resources.Fields.UF; }
        string _lbluferrortext = string.Format(Resources.Messages.InformGeneric, App.Cross.Resources.Fields.UF);
        public string LBLUFERRORTEXT { get => _lbluferrortext; set => SetProperty(ref _lbluferrortext, value); }

        public string LBLCITYNAMEPLACEHOLDER { get => App.Cross.Resources.Fields.CityName; }
        public string LBLCITYNAMEHELPERTEXT { get => App.Cross.Resources.Fields.CityName; }
        string _lblcitynameerrortext = string.Format(Resources.Messages.InformGeneric, App.Cross.Resources.Fields.CityName);
        public string LBLCITYNAMEERRORTEXT { get => _lblcitynameerrortext; set => SetProperty(ref _lblcitynameerrortext, value); }

        public string LBLEMAILPLACEHOLDER { get => App.Cross.Resources.Fields.Email; }
        public string LBLEMAILHELPERTEXT { get => App.Cross.Resources.Fields.Email; }
        string _lblemailerrortext = string.Format(Resources.Messages.InformGeneric, App.Cross.Resources.Fields.Email);
        public string LBLEMAILERRORTEXT { get => _lblemailerrortext; set => SetProperty(ref _lblemailerrortext, value); }

        public string LBLSITEPLACEHOLDER { get => App.Cross.Resources.Fields.Site; }
        public string LBLSITEHELPERTEXT { get => App.Cross.Resources.Fields.Site; }
        string _lblsiteerrortext = string.Format(Resources.Messages.InformGeneric, App.Cross.Resources.Fields.Site);
        public string LBLSITEERRORTEXT { get => _lblsiteerrortext; set => SetProperty(ref _lblsiteerrortext, value); }

        public string LBLCELLPHONEPLACEHOLDER { get => App.Cross.Resources.Fields.CellPhone; }
        public string LBLCELLPHONEHELPERTEXT { get => App.Cross.Resources.Fields.CellPhone; }
        string _lblcellphoneerrortext = string.Format(Resources.Messages.InformGeneric, App.Cross.Resources.Fields.CellPhone);
        public string LBLCELLPHONEERRORTEXT { get => _lblcellphoneerrortext; set => SetProperty(ref _lblcellphoneerrortext, value); }

        public string LBLPHONEPLACEHOLDER { get => App.Cross.Resources.Fields.Phone; }
        public string LBLPHONEHELPERTEXT { get => App.Cross.Resources.Fields.Phone; }
        string _lblphoneerrortext = string.Format(Resources.Messages.InformGeneric, App.Cross.Resources.Fields.Phone);
        public string LBLPHONEERRORTEXT { get => _lblphoneerrortext; set => SetProperty(ref _lblphoneerrortext, value); }
        #endregion

        #region Fields
        #region Errors
        bool _haszipcodeerror = false;
        public bool HASZIPCODEERROR { get => _haszipcodeerror; set => SetProperty(ref _haszipcodeerror, value); }
        bool _hasaddresserror = false;
        public bool HASADDRESSERROR { get => _hasaddresserror; set => SetProperty(ref _hasaddresserror, value); }
        bool _hasdistricterror = false;
        public bool HASDISTRICTERROR { get => _hasdistricterror; set => SetProperty(ref _hasdistricterror, value); }
        bool _hasnumerror = false;
        public bool HASNUMERROR { get => _hasnumerror; set => SetProperty(ref _hasnumerror, value); }
        bool _hascomplementerror = false;
        public bool HASCOMPLEMENTERROR { get => _hascomplementerror; set => SetProperty(ref _hascomplementerror, value); }
        bool _hasuferror = false;
        public bool HASUFERROR { get => _hasuferror; set => SetProperty(ref _hasuferror, value); }
        bool _hassiteerror = false;
        public bool HASSITEERROR { get => _hassiteerror; set => SetProperty(ref _hassiteerror, value); }
        bool _hasemailerror = false;
        public bool HASEMAILERROR { get => _hasemailerror; set => SetProperty(ref _hasemailerror, value); }

        bool _hascellphoneerror = false;
        public bool HASCELLPHONEERROR { get => _hascellphoneerror; set => SetProperty(ref _hascellphoneerror, value); }
        bool _hasphoneerror = false;
        public bool HASPHONEERROR { get => _hasphoneerror; set => SetProperty(ref _hasphoneerror, value); }
        bool _hascitynameerror = false;
        public bool HASCITYNAMEERROR { get => _hascitynameerror; set => SetProperty(ref _hascitynameerror, value); }
        #endregion

        string _zipcode;
        public string ZIPCODE
        {
            get => _zipcode; set => SetProperty(ref _zipcode, value);
        }
        string _address;
        public string ADDRESS
        {
            get => _address; set => SetProperty(ref _address, value);
        }
        string _district;
        public string DISTRICT
        {
            get => _district; set => SetProperty(ref _district, value);
        }
        string _num;
        public string NUM
        {
            get => _num; set => SetProperty(ref _num, value);
        }
        string _complement;
        public string COMPLEMENT
        {
            get => _complement; set => SetProperty(ref _complement, value);
        }
        string _uf;
        public string UF
        {
            get => _uf; set => SetProperty(ref _uf, value);
        }
        string _cityName;
        public string CITYNAME
        {
            get => _cityName; set => SetProperty(ref _cityName, value);
        }
        string _email;
        public string EMAIL
        {
            get => _email; set => SetProperty(ref _email, value);
        }
        string _site;
        public string SITE
        {
            get => _site; set => SetProperty(ref _site, value);
        }
        string _cellphone;
        public string CELLPHONE
        {
            get => _cellphone; set => SetProperty(ref _cellphone, value);
        }
        string _phone;
        public string PHONE
        {
            get => _phone; set => SetProperty(ref _phone, value);
        }
        public string IBGE { get; set; }
        public string CountryIni { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string StateName { get; set; }
        #endregion

        public bool HasAnyChanged() => 
            !ZIPCODE.IsEmpty() ||
            !ADDRESS.IsEmpty() ||
            !DISTRICT.IsEmpty() ||
            !NUM.IsEmpty() ||
            !COMPLEMENT.IsEmpty() ||
            !UF.IsEmpty() ||
            !CITYNAME.IsEmpty() ||
            !EMAIL.IsEmpty() ||
            !SITE.IsEmpty() ||
            !CELLPHONE.IsEmpty() ||
            !PHONE.IsEmpty();

    }
}
