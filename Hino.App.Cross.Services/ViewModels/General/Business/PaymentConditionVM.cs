﻿using Hino.App.Cross.Models.General.Business;
using Hino.App.Cross.Services.DB.Interfaces.General.Business;
using Hino.App.Cross.Utils.Exceptions;
using Hino.App.Cross.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace Hino.App.Cross.Services.ViewModels.General.Business
{
    public class PaymentConditionVM: BaseViewModel
    {
        private readonly IGEPaymentConditionDB _IGEPaymentConditionDB;
        public ObservableCollection<GEPaymentConditionModel> Items { get; set; }

        public PaymentConditionVM()
        {
            Title = Resources.PageTitle.CondPaymentPage;

            Items = new ObservableCollection<GEPaymentConditionModel>();
            _IGEPaymentConditionDB = DInjection.GetIntance<IGEPaymentConditionDB>();
        }

        #region Get Data From Source
        private async Task<PagedResult<GEPaymentConditionModel>> GetDataFromSourceAsync()
        {
            return await _IGEPaymentConditionDB.Search(_SearchData, base.PageNum);
        }
        #endregion

        protected async override Task LoadMoreData()
        {
            IsBusy = true;
            var Title = base.Title;
            base.Title = Resources.Messages.Loading;

            try
            {
                Items.CopyFromPagedResult(await GetDataFromSourceAsync(), false);
                IsBusy = false;
            }
            catch (Exception e)
            {
                DebugErrorLog.Log(e);
                ShowShortMessage(e.Message);
                base.Title = Title;

                IsBusy = false;
            }
            finally
            {
                IsBusy = false;
            }
        }

        protected async override Task RefreshData()
        {
            await base.RefreshData();

            IsBusy = true;

            base.PageNum = 1;
            Items.Clear();

            await LoadMoreData();
        }
    }
}
