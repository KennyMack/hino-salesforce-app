﻿using Hino.App.Cross.Services.DB.Interfaces;
using Hino.App.Cross.Services.Sync.General.Business;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using XF.Material.Forms.UI.Dialogs;

namespace Hino.App.Cross.Services.ViewModels.General.Business
{
    public class MainPaymentVM: BaseViewModel
    {

        #region Labels
        public string LBLSYNCBTN { get => Resources.Messages.Syncronize; }
        #endregion

        public MainPaymentVM()
        {
            Title = Hino.App.Cross.Resources.PageTitle.ListPaymentsTypePage;
        }

        #region On Syncronize
        public async Task<bool> Syncronize()
        {
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.Messages.Syncronizing))
            {
                var Preference = await _IPreferencesDB.GetItemAsync(1);

                var _GEPaymentTypeSync = new GEPaymentTypeSync();
                var _GEPaymentConditionSync = new GEPaymentConditionSync();

                var res1 = await _GEPaymentTypeSync.Download(Preference.LastSync);
                var res2 = await _GEPaymentConditionSync.Download(Preference.LastSync);

                return !(!res1 || !res2);
            }
        }
        #endregion
    }
}
