﻿using EntryAutoComplete;
using Hino.App.Cross.Models.General.Business;
using Hino.App.Cross.Models.Validation.General.Business;
using Hino.App.Cross.Services.Application.General;
using Hino.App.Cross.Services.DB.Interfaces.General.Business;
using Hino.App.Cross.Templates;
using Hino.App.Cross.Utils.Demograph;
using Hino.App.Cross.Utils.Enums;
using Hino.App.Cross.Utils.Exceptions;
using Hino.App.Cross.Utils.Extensions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using XF.Material.Forms.UI.Dialogs;

namespace Hino.App.Cross.Services.ViewModels.General.Business
{
    public class CreateEnterpriseVM : BaseViewModel
    {
        public bool IsEditing = false;
        private GEEnterprisesModel _GEEnterprisesModel;
        private GEEnterpriseGeoModel _GEEnterpriseGeoModel;
        private IGEEnterprisesDB _IGEEnterprisesDB;

        public ICommand SaveCommand;

        public event EventHandler UFChanged;
        public event EventHandler TypeChanged;

        #region Labels
        public string LBLRAZAOSOCIALPLACEHOLDER { get => App.Cross.Resources.Fields.RazaoSocial; }
        public string LBLRAZAOSOCIALHELPERTEXT { get => App.Cross.Resources.Fields.RazaoSocial; }
        string _lblrazaosocialerrortext = string.Format(Resources.Messages.InformGeneric, App.Cross.Resources.Fields.RazaoSocial);
        public string LBLRAZAOSOCIALERRORTEXT { get => _lblrazaosocialerrortext; set => SetProperty(ref _lblrazaosocialerrortext, value); }

        public string LBLNOMEFANTASIAPLACEHOLDER { get => App.Cross.Resources.Fields.NomeFantasia; }
        public string LBLNOMEFANTASIAHELPERTEXT { get => App.Cross.Resources.Fields.NomeFantasia; }
        string _lblnomefantasiaerrortext = string.Format(Resources.Messages.InformGeneric, App.Cross.Resources.Fields.NomeFantasia);
        public string LBLNOMEFANTASIAERRORTEXT { get => _lblnomefantasiaerrortext; set => SetProperty(ref _lblnomefantasiaerrortext, value); }

        public string LBLCNPJCPFPLACEHOLDER { get => App.Cross.Resources.Fields.CNPJCPF; }
        public string LBLCNPJCPFHELPERTEXT { get => App.Cross.Resources.Fields.CNPJCPF; }
        string _lblcnpjcpferrortext = string.Format(Resources.Messages.InformGeneric, App.Cross.Resources.Fields.CNPJCPF);
        public string LBLCNPJCPFERRORTEXT { get => _lblcnpjcpferrortext; set => SetProperty(ref _lblcnpjcpferrortext, value); }

        public string LBLTYPEPLACEHOLDER { get => App.Cross.Resources.Fields.Type; }
        public string LBLTYPEHELPERTEXT { get => App.Cross.Resources.Fields.Type; }
        string _lbltypeerrortext = string.Format(Resources.Messages.InformGeneric, App.Cross.Resources.Fields.Type);
        public string LBLTYPEERRORTEXT { get => _lbltypeerrortext; set => SetProperty(ref _lbltypeerrortext, value); }

        public string LBLCONFIRMBTN { get => App.Cross.Resources.Messages.Save; }
        public string LBLCANCELBTN { get => App.Cross.Resources.Messages.Cancel; }
        #endregion

        #region Fields
        #region Errors
        bool _hasrazaosocialerror = false;
        public bool HASRAZAOSOCIALERROR { get => _hasrazaosocialerror; set => SetProperty(ref _hasrazaosocialerror, value); }
        bool _hasnomefantasiaerror = false;
        public bool HASNOMEFANTASIAERROR { get => _hasnomefantasiaerror; set => SetProperty(ref _hasnomefantasiaerror, value); }
        bool _hascnpjcpferror = false;
        public bool HASCNPJCPFERROR { get => _hascnpjcpferror; set => SetProperty(ref _hascnpjcpferror, value); }
        bool _hastypeerror = false;
        public bool HASTYPEERROR { get => _hastypeerror; set => SetProperty(ref _hastypeerror, value); }
        bool _hascellphoneerror = false;
        public bool HASCELLPHONEERROR { get => _hascellphoneerror; set => SetProperty(ref _hascellphoneerror, value); }
        bool _hasphoneerror = false;
        public bool HASPHONEERROR { get => _hasphoneerror; set => SetProperty(ref _hasphoneerror, value); }
        bool _hasemailerror = false;
        public bool HASEMAILERROR { get => _hasemailerror; set => SetProperty(ref _hasemailerror, value); }
        bool _hassiteerror = false;
        public bool HASSITEERROR { get => _hassiteerror; set => SetProperty(ref _hassiteerror, value); }
        #endregion

        string _razaosocial;
        public string RAZAOSOCIAL
        {
            get => _razaosocial; set => SetProperty(ref _razaosocial, value);
        }
        string _nomefantasia;
        public string NOMEFANTASIA
        {
            get => _nomefantasia; set => SetProperty(ref _nomefantasia, value);
        }
        string _cnpjcpf;
        public string CNPJCPF
        {
            get => _cnpjcpf; set => SetProperty(ref _cnpjcpf, value);
        }
        string _typeenterprise;
        public string TYPEENTERPRISE
        {
            get => _typeenterprise; set => SetProperty(ref _typeenterprise, value);
        }

        EnterpriseGeoVM _CommercialAddress;
        public EnterpriseGeoVM CommercialAddress
        {
            get => _CommercialAddress; set => SetProperty(ref _CommercialAddress, value);
        }
        EnterpriseGeoVM _DeliveryAddress;
        public EnterpriseGeoVM DeliveryAddress
        {
            get => _DeliveryAddress; set => SetProperty(ref _DeliveryAddress, value);
        }
        EnterpriseGeoVM _LevyAddress;
        public EnterpriseGeoVM LevyAddress
        {
            get => _LevyAddress; set => SetProperty(ref _LevyAddress, value);
        }
        #endregion

        public IList<string> TYPECHOICES => new List<string>
        {
            "Pessoa Física",
            "Pessoa Jurídica"
        };
        public int GetTypeIndex() =>
            TYPEENTERPRISE == "Pessoa Jurídica" ? 1 : 0;

        public CreateEnterpriseVM()
        {
            IsEditing = false;
            Title = Resources.PageTitle.NewEnterprisePage;
            CommercialAddress = new EnterpriseGeoVM();
            DeliveryAddress = new EnterpriseGeoVM();
            LevyAddress = new EnterpriseGeoVM();

            TYPEENTERPRISE = "Pessoa Física";
            SaveCommand = new Command(async () => await OnSaveClick());
            _IGEEnterprisesDB = DInjection.GetIntance<IGEEnterprisesDB>();
            _Validator = new CreateEnterpriseValidator();
            _GEEnterprisesModel = new GEEnterprisesModel();
            _GEEnterpriseGeoModel = new GEEnterpriseGeoModel();

            CommercialAddress._Validator = new CreateEnterpriseGeoValidator();
            DeliveryAddress._Validator = new CreateEnterpriseGeoValidator();
            LevyAddress._Validator = new CreateEnterpriseGeoValidator();
        }

        #region Load Register
        public async Task LoadRegister()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            var Enterprise = await _IGEEnterprisesDB.GetItemAsync(_GEEnterprisesModel.Id);

            if (Enterprise != null)
            {
                _GEEnterprisesModel = Enterprise;
                // this.CopyProperties(Enterprise);
                RAZAOSOCIAL = Enterprise.RazaoSocial;
                NOMEFANTASIA = Enterprise.NomeFantasia;
                CNPJCPF = Enterprise.CNPJCPF;
                TYPEENTERPRISE = Enterprise.Type.ToString();

                CommercialAddress.CopyProperties(Enterprise.CommercialAddress);
                DeliveryAddress.CopyProperties(Enterprise.DeliveryAddress);
                LevyAddress.CopyProperties(Enterprise.LevyAddress);

                TypeChanged?.Invoke(Enterprise, new EventArgs());

                if (!(Enterprise.CommercialAddress?.UF ?? "").IsEmpty())
                    UFChanged?.Invoke(Enterprise.CommercialAddress.UF, new EventArgs());
            }

            IsBusy = false;
        }
        #endregion

        public async override void OnViewPushed(object navigationParameter = null)
        {
            base.OnViewPushed(navigationParameter);

            if (navigationParameter != null)
            {
                IsEditing = true;
                _GEEnterprisesModel = (GEEnterprisesModel)navigationParameter;

                await LoadRegister();
            }
        }

        #region GetCepAsync
        public async Task GetCepAsync(AddressType pAddress)
        {
            if (IsBusy)
                return;

            IsBusy = true;

            var AdrressSel = this.CommercialAddress;

            switch (pAddress)
            {
                case AddressType.CommercialAddress:
                    AdrressSel = this.CommercialAddress;
                    break;
                case AddressType.DeliveryAddress:
                    AdrressSel = this.DeliveryAddress;
                    break;
                case AddressType.LevyAddress:
                    AdrressSel = this.LevyAddress;
                    break;
            }

            AdrressSel.ADDRESS = "";
            AdrressSel.DISTRICT = "";
            AdrressSel.COMPLEMENT = "";
            AdrressSel.UF = "";
            AdrressSel.CITYNAME = "";
            AdrressSel.IBGE = "";
            AdrressSel.CountryIni = "";
            AdrressSel.CountryCode = "";
            AdrressSel.CountryName = "";
            AdrressSel.StateName = "";

            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.Messages.LoadProcessing))
            {
                var cepApplication = new CEPApplication();
                var CepModel = await cepApplication.GetZipCodeAsync(AdrressSel.ZIPCODE);

                if (CepModel != null)
                {
                    AdrressSel.ADDRESS = CepModel.logradouro;
                    AdrressSel.DISTRICT = CepModel.bairro;
                    AdrressSel.COMPLEMENT = CepModel.complemento;
                    AdrressSel.UF = CepModel.uf;
                    AdrressSel.CITYNAME = CepModel.localidade;
                    AdrressSel.IBGE = CepModel.ibge;
                    AdrressSel.CountryIni = CepModel.Country.CIOC;
                    AdrressSel.CountryCode = CepModel.Country.NumericCode;
                    AdrressSel.CountryName = CepModel.Country.Name;
                    AdrressSel.StateName = CepModel.GEStates.Name ?? CepModel.uf;
                }
            }

            IsBusy = false;
        }
        #endregion

        #region On Create Click
        public async Task<bool> OnSaveClick()
        {
            if (IsBusy)
                return false;

            try
            {
                IsBusy = true;
                using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.Messages.LoadProcessing))
                {
                    _GEEnterprisesModel.Id = IsEditing ? _GEEnterprisesModel.Id : await _IGEEnterprisesDB.NextSequence();
                    _GEEnterprisesModel.Type = GetTypeIndex();
                    _GEEnterprisesModel.CopyProperties(this);

                    if (!await ValidateModelAsync(_GEEnterprisesModel))
                    {
                        DisplayValidationModelErrors(true);
                        IsBusy = false;
                        return false;
                    }

                    _GEEnterpriseGeoModel.CopyProperties(this.CommercialAddress);
                    _GEEnterpriseGeoModel.CNPJCPF = _GEEnterprisesModel.CNPJCPF;
                    _GEEnterpriseGeoModel.EnterpriseId = _GEEnterprisesModel.Id;
                    _GEEnterpriseGeoModel.Type = 1;

                    if (!await CommercialAddress.ValidateModelAsync(_GEEnterpriseGeoModel))
                    {
                        CommercialAddress.DisplayValidationModelErrors(true);
                        IsBusy = false;
                        return false;
                    }

                    if (DeliveryAddress.HasAnyChanged())
                    {
                        _GEEnterpriseGeoModel.CopyProperties(this.DeliveryAddress);
                        _GEEnterpriseGeoModel.CNPJCPF = _GEEnterprisesModel.CNPJCPF;
                        _GEEnterpriseGeoModel.EnterpriseId = _GEEnterprisesModel.Id;
                        _GEEnterpriseGeoModel.Type = 1;

                        if (!await DeliveryAddress.ValidateModelAsync(_GEEnterpriseGeoModel))
                        {
                            DeliveryAddress.DisplayValidationModelErrors(true);
                            IsBusy = false;
                            return false;
                        }
                    }

                    if (LevyAddress.HasAnyChanged())
                    {
                        _GEEnterpriseGeoModel.CopyProperties(this.LevyAddress);
                        _GEEnterpriseGeoModel.CNPJCPF = _GEEnterprisesModel.CNPJCPF;
                        _GEEnterpriseGeoModel.EnterpriseId = _GEEnterprisesModel.Id;
                        _GEEnterpriseGeoModel.Type = 2;

                        if (!await LevyAddress.ValidateModelAsync(_GEEnterpriseGeoModel))
                        {
                            CommercialAddress.DisplayValidationModelErrors(true);
                            IsBusy = false;
                            return false;
                        }
                    }

                    _GEEnterprisesModel.CopyProperties(this.CommercialAddress, "CommercialAddress_");
                    _GEEnterprisesModel.CopyProperties(this.DeliveryAddress, "DeliveryAddress_");
                    _GEEnterprisesModel.CopyProperties(this.LevyAddress, "LevyAddress_");

                    _GEEnterprisesModel.EstablishmentKey = CurrentApp.ESTABLISHMENT.EstablishmentKey;
                    _GEEnterprisesModel.UniqueKey = IsEditing ? _GEEnterprisesModel.UniqueKey : Guid.NewGuid().ToString();
                    _GEEnterprisesModel.Initials = IsEditing ? _GEEnterprisesModel.Initials : "";
                    _GEEnterprisesModel.Status = IsEditing ? _GEEnterprisesModel.StatusSinc : 0;
                    _GEEnterprisesModel.StatusSinc = IsEditing ? _GEEnterprisesModel.StatusSinc : 0;
                    _GEEnterprisesModel.IsActive = true;
                    _GEEnterprisesModel.Created = IsEditing ? _GEEnterprisesModel.Created : DateTime.Now;
                    // var EmpBase = (await _IGEEnterprisesDB.Query(r => r.Id > 0, 1)).Results.FirstOrDefault();
                    _GEEnterprisesModel.RegionId = 0;//EmpBase.RegionId;
                    _GEEnterprisesModel.UserId = CurrentApp.USERLOGGED.Id;
                    _GEEnterprisesModel.Modified = DateTime.Now;
                    _GEEnterprisesModel.Classification = 0;

                    await _IGEEnterprisesDB.AddOrUpdateItemAsync(_GEEnterprisesModel);
                    //TODO: CORRIGIR SINCRONIZAÇÃO
                    //var Config = await _IConfigDB.GetItemAsync(1);

                    //    if (Config.SyncOnSave)
                    //    {
                    //        using (SyncData sync = new SyncData())
                    //        {
                    //            if (sync.IsConnected() &&
                    //                await sync.ConnectionTypeOK())
                    //                await sync.SendChanges();
                    //        }
                    //    }
                    ShowShortMessage(Resources.Messages.SaveSuccess);
                    IsBusy = false;
                }

                await Task.Delay(400);
                await Navigation.PopModalAsync();
            }
            catch (Exception ex)
            {
                DebugErrorLog.Log(ex);
                ShowShortMessage(Resources.Messages.ComunicationFail);
                IsBusy = false;
                return false;
            }
            finally
            {
                IsBusy = false;
            }

            return true;
        }
        #endregion
    }
}
