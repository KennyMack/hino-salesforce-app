﻿using Hino.App.Cross.Models.General.Business;
using Hino.App.Cross.Services.DB.Interfaces;
using Hino.App.Cross.Services.DB.Interfaces.General.Business;
using Hino.App.Cross.Services.Sync.General.Business;
using Hino.App.Cross.Templates.Base;
using Hino.App.Cross.Templates.ListView;
using Hino.App.Cross.Utils.Database;
using Hino.App.Cross.Utils.Exceptions;
using Hino.App.Cross.Utils.Messages;
using Hino.App.Cross.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms.Internals;
using XF.Material.Forms.UI.Dialogs;

namespace Hino.App.Cross.Services.ViewModels.General.Business
{
    public class ListEnterprisesVM: BaseViewModel
    {
        private readonly IGEEnterprisesDB _IGEEnterprisesDB;
        public GEEnterprisesModel SelectedItem;
        public ObservableCollection<GEEnterprisesModel> Items { get; set; }
        public Expression<Func<GEEnterprisesModel, bool>> FilterExpression { get; set; }
        public SortResult SortExpression { get; set; }

        public ListEnterprisesVM()
        {
            Title = Resources.PageTitle.ListEnterprisePage;

            Items = new ObservableCollection<GEEnterprisesModel>();
            _IGEEnterprisesDB = DInjection.GetIntance<IGEEnterprisesDB>();
        }

        protected override SortResult CreateDefaultSort<T>()
        {
            Func<GEEnterprisesModel, object> orderByProperty = ordering => ordering.RazaoSocial;
            return new SortResult
            {
                Descending = false,
                Expression = orderByProperty
            };
        }

        #region Get Data From Source
        private async Task<PagedResult<GEEnterprisesModel>> GetDataFromSourceAsync()
        {
            return await _IGEEnterprisesDB.Search(FilterExpression, SortExpression, _SearchData, base.PageNum);
        }
        #endregion

        #region Search Data
        public async override Task SearchData_Changed()
        {
            base.PageNum = 1;

            Items.CopyFromPagedResult(await GetDataFromSourceAsync());
        }
        #endregion

        #region Load More
        protected async override Task LoadMoreData()
        {
            IsBusy = true;
            var Title = base.Title;
            base.Title = Resources.Messages.Loading;

            try
            {
                /*var results = await GetDataFromSourceAsync();

                var groups = results.Results.Select(r => r.RazaoSocial.Substring(0, 1)).GroupBy(r => r);

                foreach (var item in groups)
                {
                    var group = new ListGroup<GEEnterprisesModel>();
                    group.ShortName = item.Key;
                    group.LongName = item.Key;
                    if (!Items.Any(r => r.LongName == group.LongName))
                        Items.Add(group);
                }

                foreach (var item in results.Results)
                {
                    var groupAdd = Items.First(r => r.ShortName == item.RazaoSocial.Substring(0, 1));

                    if (groupAdd != null)
                    {
                        groupAdd.Add(item);
                    }
                }*/

                Items.CopyFromPagedResult(await GetDataFromSourceAsync(), false);
                IsBusy = false;
            }
            catch (Exception e)
            {
                DebugErrorLog.Log(e);
                ShowShortMessage(e.Message);
                base.Title = Title;

                IsBusy = false;
            }
            finally
            {
                IsBusy = false;
                base.Title = Title;
            }
        }
        #endregion

        #region Refresh Data
        protected async override Task RefreshData()
        {
            await base.RefreshData();

            IsBusy = true;

            base.PageNum = 1;
            Items.Clear();

            await LoadMoreData();
        }
        #endregion

        #region On Syncronize
        public async Task<bool> Syncronize()
        {
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.Messages.Syncronizing))
            {
                var Preference = await _IPreferencesDB.GetItemAsync(1);

                var _GEEnterprisesSync = new GEEnterprisesSync();

                await _GEEnterprisesSync.Download(Preference.LastSync);

                await RefreshData();


                return true;
            }
        }
        #endregion

        #region Delete Click
        public void DeleteClick()
        {
            if (SelectedItem != null)
            {
                if (SelectedItem.StatusSinc == 0)
                {
                    OnMessageConfirmRaise(new MessageRaiseEvent(Resources.Messages.Confirm, Resources.Messages.ConfirmRemove, SelectedItem));
                }
                else
                    ShowShortMessage(Resources.Messages.EnterpriseWasSyncronized);
            }
            else
                ShowShortMessage(Resources.Messages.EnterpriseNotSelected);
        }
        #endregion

        #region Delete Enterprise
        public async Task DeleteEnterprise(GEEnterprisesModel pModel)
        {
            var id = Convert.ToInt64(pModel.Id);

            if (await _IGEEnterprisesDB.DeleteItemAsync(id))
            {
                Items.Remove(SelectedItem);
                ShowShortMessage(Resources.Messages.RemoveSuccess);
            }
        }
        #endregion

        public override void OnViewPopped()
        {
            base.OnViewPopped();
            if (!CurrentApp.IsFilterModal)
                RefreshDataCommand.Execute(null);
        }

        public async override Task SetFilterExpression(object pFilter)
        {
            if (pFilter == null)
                return;

            FilterExpression = (!(pFilter is Expression<Func<GEEnterprisesModel, bool>>)) ? null : ((Expression<Func<GEEnterprisesModel, bool>>)pFilter);
            await RefreshData();
        }

        public async override Task SetSortExpression(object pSort)
        {
            if (pSort == null || ((SortResult)pSort).Expression == null)
                return;

            SortExpression = (!(pSort is SortResult)) ? CreateDefaultSort<GEEnterprisesModel>() : ((SortResult)pSort);
            await RefreshData();
        }
    }
}
