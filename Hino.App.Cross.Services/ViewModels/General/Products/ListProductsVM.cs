﻿using Hino.App.Cross.Models.General;
using Hino.App.Cross.Services.DB.Interfaces;
using Hino.App.Cross.Services.DB.Interfaces.General.Products;
using Hino.App.Cross.Templates.ListView;
using Hino.App.Cross.Utils.Database;
using Hino.App.Cross.Utils.Exceptions;
using Hino.App.Cross.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using XF.Material.Forms.UI.Dialogs;

namespace Hino.App.Cross.Services.ViewModels.General.Products
{
    public class ListProductsVM: BaseViewModel
    {
        private readonly IGEProductsDB _IGEProductsDB;
        public GEProductsModel SelectedItem;
        public ObservableCollection<GEProductsModel> Items { get; set; }
        public Expression<Func<GEProductsModel, bool>> FilterExpression { get; set; }
        public SortResult SortExpression { get; set; }

        public ListProductsVM()
        {
            Title = Resources.PageTitle.ListProductsPage;

            Func<GEProductsModel, object> orderByProperty = ordering => ordering.ProductKey;

            SortExpression = CreateDefaultSort<GEProductsModel>();

            Items = new ObservableCollection<GEProductsModel>();
            _IGEProductsDB = DInjection.GetIntance<IGEProductsDB>();
        }

        protected override SortResult CreateDefaultSort<T>()
        {
            Func<GEProductsModel, object> orderByProperty = ordering => ordering.ProductKey;
            return new SortResult
            {
                Descending = true,
                Expression = orderByProperty
            };
        }

        #region On Syncronize
        public async Task<bool> Syncronize()
        {
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.Messages.Syncronizing))
            {/*
                var Preference = await _IPreferencesDB.GetItemAsync(1);

                var _GEEnterprisesSync = new GEEnterprisesSync();

                await _GEEnterprisesSync.Download(Preference.LastSync);
                */

                _IGEProductsDB.ClearTable();
                await _IGEProductsDB.AddItemAsync(new GEProductsModel()
                {
                    ProductKey = "700.00001",
                    Image = "https://picsum.photos/200/300",
                    Name = "PRODUTO 1",
                    Description = "PRODUTO 1",
                    Type = "COMERCIALIZAÇÃO",
                    Family = "COMERCIO",
                    PercIPI = 14,
                    PercMaxDiscount = 0,
                    Value = 52,
                    StockBalance = 2,
                    Status = 3,
                    NCM = "100255",
                    Unit = "UN",
                    Weight =1,
                    PackageQTD = 2
                });


                await _IGEProductsDB.AddItemAsync(new GEProductsModel()
                {
                    ProductKey = "700.00002",
                    Image = "https://picsum.photos/200/300",
                    Name = "PRODUTO 2",
                    Description = "PRODUTO 2",
                    Type = "COMERCIALIZAÇÃO",
                    Family = "COMERCIO",
                    PercIPI = 14,
                    PercMaxDiscount = 0,
                    Value = 51,
                    StockBalance = 12,
                    Status = 3,
                    NCM = "100255",
                    Unit = "UN",
                    Weight = 5,
                    PackageQTD = 2
                });

                await _IGEProductsDB.AddItemAsync( new GEProductsModel()
                {
                    ProductKey = "700.00003",
                    Image = "https://picsum.photos/200/300",
                    Name = "PRODUTO 3",
                    Description = "PRODUTO 3",
                    Type = "COMERCIALIZAÇÃO",
                    Family = "COMERCIO",
                    PercIPI = 12,
                    PercMaxDiscount = 0,
                    Value = 2,
                    StockBalance = 13,
                    Status = 3,
                    NCM = "100255",
                    Unit = "KG",
                    Weight = 5,
                    PackageQTD = 2
                });

                await _IGEProductsDB.AddItemAsync( new GEProductsModel()
                {
                    ProductKey = "700.00004",
                    Image = "https://picsum.photos/200/300",
                    Name = "SERVICO 1",
                    Description = "SERVIÇO 1",
                    Type = "SERVIÇOS",
                    Family = "SERVIÇOS",
                    PercIPI = 2,
                    PercMaxDiscount = 0,
                    Value = 10,
                    StockBalance = 12,
                    Status = 3,
                    NCM = "100255",
                    Unit = "KG",
                    Weight = 0,
                    PackageQTD = 0
                });

                await _IGEProductsDB.AddItemAsync( new GEProductsModel()
                {
                    ProductKey = "700.00005",
                    Image = "https://picsum.photos/200/300",
                    Name = "PRODUTO 5",
                    Description = "PRODUTO 5",
                    Type = "INDUSTRIALIZAÇÃO",
                    Family = "INDUSTRIA",
                    PercIPI = 10,
                    PercMaxDiscount = 0,
                    Value = 5,
                    StockBalance = 20,
                    Status = 3,
                    NCM = "100255",
                    Unit = "PC",
                    Weight = 10,
                    PackageQTD = 5
                });


                await RefreshData();


                return true;
            }
        }
        #endregion

        #region Get Data From Source
        private async Task<PagedResult<GEProductsModel>> GetDataFromSourceAsync()
        {
            return await _IGEProductsDB.Search(FilterExpression, SortExpression, _SearchData, base.PageNum);
        }
        #endregion

        #region Search Data
        public async override Task SearchData_Changed()
        {
            base.PageNum = 1;

            Items.CopyFromPagedResult(await GetDataFromSourceAsync());
        }
        #endregion

        #region Load More
        protected async override Task LoadMoreData()
        {
            IsBusy = true;
            var Title = base.Title;
            base.Title = Resources.Messages.Loading;

            try
            {
                Items.CopyFromPagedResult(await GetDataFromSourceAsync(), false);
                IsBusy = false;
            }
            catch (Exception e)
            {
                DebugErrorLog.Log(e);
                ShowShortMessage(e.Message);
                base.Title = Title;

                IsBusy = false;
            }
            finally
            {
                IsBusy = false;
                base.Title = Title;
            }
        }
        #endregion

        #region Refresh Data
        protected async override Task RefreshData()
        {
            await base.RefreshData();

            IsBusy = true;

            base.PageNum = 1;
            Items.Clear();

            await LoadMoreData();
        }
        #endregion

        public override void OnViewPopped()
        {
            base.OnViewPopped();
            if (!CurrentApp.IsFilterModal)
                RefreshDataCommand.Execute(null);
        }

        public async override Task SetFilterExpression(object pFilter)
        {
            FilterExpression = (!(pFilter is Expression<Func<GEProductsModel, bool>>)) ? null : ((Expression<Func<GEProductsModel, bool>>)pFilter);
            await RefreshData();
        }
        public async override Task SetSortExpression(object pSort)
        {
            SortExpression = (!(pSort is SortResult)) ? CreateDefaultSort<GEProductsModel>() : ((SortResult)pSort);
            await RefreshData();
        }
    }
}
