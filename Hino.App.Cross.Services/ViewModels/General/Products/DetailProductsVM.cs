﻿using Hino.App.Cross.Models.General;
using Hino.App.Cross.Services.DB.Interfaces.General.Products;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.App.Cross.Services.ViewModels.General.Products
{
    public class DetailProductsVM : BaseViewModel
    {
        private readonly IGEProductsDB _IGEProductsDB;
        public GEProductsModel _GEProductsModel;

        public DetailProductsVM()
        {
            _GEProductsModel = new GEProductsModel();
            _IGEProductsDB = DInjection.GetIntance<IGEProductsDB>();
        }

        #region Load Register
        public async Task LoadRegister()
        {
            if (IsBusy)
                return;
            await Task.Delay(1);

            if (_GEProductsModel != null)
            {
                var Product = await _IGEProductsDB.GetItemAsync(_GEProductsModel.Id);

                if (Product != null)
                {

                }
            }

            IsBusy = false;
        }
        #endregion

        public override void OnViewPushed(object navigationParameter = null)
        {
            base.OnViewPushed(navigationParameter);
            _GEProductsModel = (GEProductsModel)navigationParameter;
        }

        public async override void OnViewPopped()
        {
            base.OnViewPopped();
            await LoadRegister();
        }
    }
}
