﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.App.Cross.Services.ViewModels.General
{
    public class EstablishmentVM : BaseViewModel
    {
        #region Labels
        public string LBLRAZAOSOCIALPLACEHOLDER { get => Resources.Fields.RazaoSocial; }
        public string LBLRAZAOSOCIALHELPERTEXT { get => Resources.Fields.RazaoSocial; }
        public string LBLNOMEFANTASIAPLACEHOLDER { get => Resources.Fields.NomeFantasia; }
        public string LBLNOMEFANTASIAHELPERTEXT { get => Resources.Fields.NomeFantasia; }
        public string LBLEMAILPLACEHOLDER { get => Resources.Fields.Email; }
        public string LBLEMAILHELPERTEXT { get => Resources.Fields.Email; }
        public string LBLPHONEPLACEHOLDER { get => Resources.Fields.Phone; }
        public string LBLPHONEHELPERTEXT { get => Resources.Fields.Phone; }
        public string LBLCNPJCPFPLACEHOLDER { get => Resources.Fields.CNPJCPF; }
        public string LBLCNPJCPFHELPERTEXT { get => Resources.Fields.CNPJCPF; }
        public string LBLPISPLACEHOLDER { get => Resources.Fields.PIS_; }
        public string LBLPISHELPERTEXT { get => Resources.Fields.PIS_; }
        public string LBLCOFINSPLACEHOLDER { get => Resources.Fields.COFINS_; }
        public string LBLCOFINSHELPERTEXT { get => Resources.Fields.COFINS_; }
        #endregion

        #region Fields
        private string _RazaoSocial;
        private string _NomeFantasia;
        private string _Email;
        private string _Phone;
        private string _CNPJCPF;
        private string _PIS;
        private string _COFINS;

        public string RAZAOSOCIAL { get => _RazaoSocial; set => SetProperty(ref _RazaoSocial, value); }
        public string NOMEFANTASIA { get => _NomeFantasia; set => SetProperty(ref _NomeFantasia, value); }
        public string EMAIL { get => _Email; set => SetProperty(ref _Email, value); }
        public string PHONE { get => _Phone; set => SetProperty(ref _Phone, value); }
        public string CNPJCPF { get => _CNPJCPF; set => SetProperty(ref _CNPJCPF, value); }
        public string PIS { get => _PIS; set => SetProperty(ref _PIS, value); }
        public string COFINS { get => _COFINS; set => SetProperty(ref _COFINS, value); }
        #endregion

        public EstablishmentVM()
        {
            Title = Resources.PageTitle.Establishment;
        }

        #region Load Data
        public void LoadData()
        {
            var Estab = CurrentApp.ESTABLISHMENT;

            RAZAOSOCIAL = Estab.RazaoSocial;
            NOMEFANTASIA = Estab.NomeFantasia;
            EMAIL = Estab.Email;
            PHONE = Estab.Phone;
            CNPJCPF = Estab.CNPJCPF;
            PIS = Estab.PIS.ToString("n4");
            COFINS = Estab.COFINS.ToString("n4");
        }
        #endregion
    }
}
