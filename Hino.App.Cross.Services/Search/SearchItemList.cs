﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.App.Cross.Services.Search
{
    public class SearchItemList
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public string Details { get; set; }
        public string Title { get; set; }
    }
}
