﻿using Hino.App.Cross.Services.ViewModels.Search;
using Hino.App.Cross.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.App.Cross.Services.Search.Interfaces
{
    public interface ISearchDataResult
    {
        string KeyName { get; set; }
        string Description { get; set; }
        string Details { get; set; }
        string Title { get; set; }
        Task<PagedResult<SearchItemList>> GetDataFromSourceAsync(string pSearchData, int pPage);
    }
}
