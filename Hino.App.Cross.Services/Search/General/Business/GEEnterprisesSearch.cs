﻿using Hino.App.Cross.Models.Interfaces;
using Hino.App.Cross.Services.DB.Interfaces.General.Business;
using Hino.App.Cross.Services.Search.Interfaces;
using Hino.App.Cross.Utils.Extensions;
using Hino.App.Cross.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.App.Cross.Services.Search.General.Business
{
    public class GEEnterprisesSearch : ISearchDataResult
    {
        private IGEEnterprisesDB _IGEEnterprisesDB;
        public GEEnterprisesSearch()
        {
            _IGEEnterprisesDB = DInjection.GetIntance<IGEEnterprisesDB>();
        }

        public string KeyName { get; set; }
        public string Description { get; set; }
        public string Details { get; set; }
        public string Title { get; set; }

        public async Task<PagedResult<SearchItemList>> GetDataFromSourceAsync(string pSearchData, int pPage)
        {
            var result = await _IGEEnterprisesDB.Search(pSearchData, pPage);

            var lstSearchResult = new List<SearchItemList>();
            foreach (IBaseEntity item in result.Results)
            {
                var ret = item.GetProperties();

                var sil = new SearchItemList();

                if (!string.IsNullOrEmpty(KeyName))
                    sil.Id = Convert.ToString(ret[KeyName]);
                if (!string.IsNullOrEmpty(Description))
                    sil.Description = Convert.ToString(ret[Description]);
                if (!string.IsNullOrEmpty(Details))
                    sil.Details = Convert.ToString(ret[Details]);
                sil.Title = Title;
                lstSearchResult.Add(sil);
            }

            return new PagedResult<SearchItemList>
            {
                CurrentPage = result.CurrentPage,
                PageCount = result.PageCount,
                PageSize = result.PageSize,
                RowCount = result.RowCount,
                Results = lstSearchResult
            };

        }
    }
}
