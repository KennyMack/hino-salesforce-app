﻿using Hino.App.Cross.Models;
using Hino.App.Cross.Services.DB;
using Hino.App.Cross.Services.DB.Interfaces;
using Hino.App.Cross.Services.Sync.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.App.Cross.Services.Sync
{
    public class BaseSync: IBaseSync
    {
        public BaseSync()
        {

        }

        public virtual Task<bool> Download(DateTime pLastSync)
        {
            return Task.FromResult(true);
        }

        public virtual Task<bool> TestConnection()
        {
            throw new NotImplementedException();
        }

        public virtual Task<bool> TestSession()
        {
            throw new NotImplementedException();
        }

        public virtual Task<bool> Upload()
        {
            return Task.FromResult(true);
        }
    }
}
