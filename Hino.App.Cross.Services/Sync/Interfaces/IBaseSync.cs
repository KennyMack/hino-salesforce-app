﻿using Hino.App.Cross.Models;
using Hino.App.Cross.Services.API;
using Hino.App.Cross.Services.DB;
using Hino.App.Cross.Services.DB.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.App.Cross.Services.Sync.Interfaces
{
    public interface IBaseSync
    {
        Task<bool> TestConnection();
        Task<bool> TestSession();
        Task<bool> Upload();
        Task<bool> Download(DateTime pLastSync);
    }
}
