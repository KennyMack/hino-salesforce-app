﻿using Hino.App.Cross.Services.API.Interfaces.General.Business;
using Hino.App.Cross.Services.DB.Interfaces.General.Business;
using Hino.App.Cross.Utils.Exceptions;
using System;
using System.Threading.Tasks;

namespace Hino.App.Cross.Services.Sync.General.Business
{
    public class GEEnterprisesSync : BaseSync
    {
        private readonly IGEEnterprisesAPI _IGEEnterprisesAPI;
        private readonly IGEEnterprisesDB _IGEEnterprisesDB;
        public GEEnterprisesSync()
        {
            _IGEEnterprisesAPI = DInjection.GetIntance<IGEEnterprisesAPI>();
            _IGEEnterprisesDB = DInjection.GetIntance<IGEEnterprisesDB>();
        }

        public override async Task<bool> Download(DateTime pLastSync)
        {
            try
            {
                var result = await _IGEEnterprisesAPI.GetAllSyncByUserIdAsync(pLastSync, CurrentApp.USERLOGGED.Id);

                await _IGEEnterprisesDB.AddFromPagedResult(result);
            }
            catch (Exception ex)
            {
                DebugErrorLog.Log(ex);
                return false;
            }

            return true;
        }
    }
}
