﻿using Hino.App.Cross.Services.API.Interfaces.General.Business;
using Hino.App.Cross.Services.DB.Interfaces.General.Business;
using Hino.App.Cross.Utils.Exceptions;
using System;
using System.Threading.Tasks;

namespace Hino.App.Cross.Services.Sync.General.Business
{
    public class GEPaymentConditionSync:  BaseSync
    {
        private readonly IGEPaymentConditionAPI _IGEPaymentConditionAPI;
        private readonly IGEPaymentConditionDB _IGEPaymentConditionDB;
        public GEPaymentConditionSync()
        {
            _IGEPaymentConditionDB = DInjection.GetIntance<IGEPaymentConditionDB>();
            _IGEPaymentConditionAPI = DInjection.GetIntance<IGEPaymentConditionAPI>();
        }

        public override async Task<bool> Download(DateTime pLastSync)
        {
            try
            {
                var result = await _IGEPaymentConditionAPI.GetAllSyncAsync(pLastSync);

                await _IGEPaymentConditionDB.AddFromPagedResult(result);
            }
            catch (Exception ex)
            {
                DebugErrorLog.Log(ex);
                return false;
            }

            return true;
        }
    }
}
