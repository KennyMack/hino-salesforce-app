﻿using Hino.App.Cross.Services.API.Interfaces.General.Business;
using Hino.App.Cross.Services.DB.Interfaces.General.Business;
using Hino.App.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.App.Cross.Services.Sync.General.Business
{
    public class GEPaymentTypeSync: BaseSync
    {
        private readonly IGEPaymentTypeAPI _IGEPaymentTypeAPI;
        private readonly IGEPaymentTypeDB _IGEPaymentTypeDB;
        public GEPaymentTypeSync()
        {
            _IGEPaymentTypeDB = DInjection.GetIntance<IGEPaymentTypeDB>();
            _IGEPaymentTypeAPI = DInjection.GetIntance<IGEPaymentTypeAPI>();
        }

        public override async Task<bool> Download(DateTime pLastSync)
        {
            try
            {
                var result = await _IGEPaymentTypeAPI.GetAllSyncAsync(pLastSync);

                await _IGEPaymentTypeDB.AddFromPagedResult(result);
            }
            catch (Exception ex)
            {
                DebugErrorLog.Log(ex);
                return false;
            }

            return true;
        }
    }
}
