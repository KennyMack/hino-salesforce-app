﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Hino.App.Cross.Services.Utils
{
    public class Messaging : IDisposable
    {
        private readonly string Message;
        public Messaging(string pMessage)
        {
            Message = pMessage;
        }

        #region Send Message
        public void SendMessage(string text)
        {
            MessagingCenter.Send(this, Message, text);
        }
        #endregion

        #region Unsubscribe
        public void Unsubscribe()
        {
            MessagingCenter.Unsubscribe<Messaging, string>(this, Message);
        }
        #endregion

        public void Dispose()
        {
            MessagingCenter.Unsubscribe<Messaging, string>(this, Message);
        }
    }
}
