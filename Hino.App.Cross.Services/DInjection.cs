﻿using System;
using System.Collections.Generic;
using Autofac;
using System.Text;
using Hino.App.Cross.Services.API.Auth;
using Hino.App.Cross.Services.API.General.Business;
using Hino.App.Cross.Services.API.General;
using Hino.App.Cross.Services.API.Interfaces.Auth;
using Hino.App.Cross.Services.API.Interfaces.General;
using Hino.App.Cross.Services.API.Interfaces.General.Business;
using Hino.App.Cross.Services.API.Interfaces.General.Demograph;
using Hino.App.Cross.Services.API.Interfaces.Sales;
using Hino.App.Cross.Services.API.Sales;
using Hino.App.Cross.Services.API.General.Demograph;
using Hino.App.Cross.Services.DB.Interfaces;
using Hino.App.Cross.Services.DB;
using Hino.App.Cross.Services.DB.General.Business;
using Hino.App.Cross.Services.DB.Interfaces.General.Business;
using Hino.App.Cross.Services.DB.Interfaces.Auth;
using Hino.App.Cross.Services.DB.Auth;
using Hino.App.Cross.Services.DB.Interfaces.General;
using Hino.App.Cross.Services.DB.General;
using Hino.App.Cross.Services.DB.General.Products;
using Hino.App.Cross.Services.DB.Interfaces.General.Products;

namespace Hino.App.Cross.Services
{
    public static class DInjection
    {
        private static IContainer _container;

        public static void Initialize()
        {
            if (_container == null)
            {
                var builder = new ContainerBuilder();
                builder.RegisterType<AuthAPI>().As<IAuthAPI>();
                builder.RegisterType<UsersAPI>().As<IUsersAPI>();
                builder.RegisterType<GEProductsAPI>().As<IGEProductsAPI>();

                builder.RegisterType<GEEstablishmentsAPI>().As<IGEEstablishmentsAPI>();
                builder.RegisterType<GEEnterprisesAPI>().As<IGEEnterprisesAPI>();
                builder.RegisterType<GEPaymentConditionAPI>().As<IGEPaymentConditionAPI>();
                builder.RegisterType<GEPaymentTypeAPI>().As<IGEPaymentTypeAPI>();
                builder.RegisterType<GECitiesAPI>().As<IGECitiesAPI>();
                builder.RegisterType<GECountriesAPI>().As<IGECountriesAPI>();
                builder.RegisterType<GEStatesAPI>().As<IGEStatesAPI>();
                builder.RegisterType<VEOrderItemsAPI>().As<IVEOrderItemsAPI>();
                builder.RegisterType<VEOrdersAPI>().As<IVEOrdersAPI>();
                builder.RegisterType<VESalePriceAPI>().As<IVESalePriceAPI>();
                builder.RegisterType<VERegionSaleAPI>().As<IVERegionSaleAPI>();

                builder.RegisterType<UserLoggedDB>().As<IUserLoggedDB>();
                builder.RegisterType<GEEstablishmentDB>().As<IGEEstablishmentDB>();
                builder.RegisterType<PreferencesDB>().As<IPreferencesDB>();
                builder.RegisterType<GEPaymentConditionDB>().As<IGEPaymentConditionDB>();
                builder.RegisterType<GEPaymentTypeDB>().As<IGEPaymentTypeDB>();
                builder.RegisterType<GEEnterprisesDB>().As<IGEEnterprisesDB>();
                builder.RegisterType<GEEnterpriseGeoDB>().As<IGEEnterpriseGeoDB>();
                builder.RegisterType<GEProductsDB>().As<IGEProductsDB>();
                builder.RegisterType<GEEnterpriseContactsDB>().As<IGEEnterpriseContactsDB>();


                _container = builder.Build();
            }
        }

        public static T GetIntance<T>() =>
            _container.Resolve<T>();
    }
}
