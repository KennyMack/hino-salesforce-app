﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.App.Core.Navigation.Utilities
{
    public static class ViewNames
    {
        public const string MainPage = nameof(Hino.Salesforce.App.Views.MainPage);

        public const string AuthAccountPage = nameof(Hino.Salesforce.App.Views.Auth.AccountsPage);

        public const string AuthLoginPage = nameof(Hino.Salesforce.App.Views.Auth.LoginPage);

        public const string AuthRegisterPage = nameof(Hino.Salesforce.App.Views.Auth.RegisterPage);

        public const string GeneralBusinessEnterprisesCreateEnterprisePage = nameof(Hino.Salesforce.App.Views.General.Business.Enterprises.CreateEnterprisePage);

        public const string GeneralBusinessEnterprisesDetailEnterprisePage = nameof(Hino.Salesforce.App.Views.General.Business.Enterprises.DetailEnterprisePage);

        public const string GeneralBusinessEnterprisesListEnterprisesPage = nameof(Hino.Salesforce.App.Views.General.Business.Enterprises.ListEnterprisesPage);

        public const string GeneralBusinessPaymentMainPaymentPage = nameof(Hino.Salesforce.App.Views.General.Business.Payment.MainPaymentPage);

        public const string GeneralBusinessPaymentListPaymentConditionPage = nameof(Hino.Salesforce.App.Views.General.Business.Payment.ListPaymentConditionPage);

        public const string GeneralBusinessPaymentListPaymentTypePage = nameof(Hino.Salesforce.App.Views.General.Business.Payment.ListPaymentTypePage);

        public const string GeneralEstablishmentPage = nameof(Hino.Salesforce.App.Views.General.EstablishmentPage);

        public const string PreferencesPreferencesPage = nameof(Hino.Salesforce.App.Views.Preferences.PreferencesPage);

        public const string SalesCreateSalePage = nameof(Hino.Salesforce.App.Views.Sales.CreateSalePage);

        public const string SalesMainSalesPage = nameof(Hino.Salesforce.App.Views.Sales.MainSalesPage);
    }
}
