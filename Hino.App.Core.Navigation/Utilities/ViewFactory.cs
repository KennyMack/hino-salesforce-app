﻿using CommonServiceLocator;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Hino.App.Core.Navigation.Utilities
{
    internal static class ViewFactory
    {
        internal static Page GetView(string viewName)
        {
            switch (viewName)
            {
                case "MainView":
                    return new MainView();
                case "MainPage":
                    return new MainPage();
                case "ItemsPage":
                    return new ItemsPage();
                case "AboutPage":
                    return new AboutPage();
                case "PrincipalPage":
                    return new PrincipalPage();
                default:
                    return new MainView();
            }
        }

        internal static TView GetView<TView>() where TView : ContentPage
        {
            return ServiceLocator.Current.GetInstance<TView>();
        }
    }
}
