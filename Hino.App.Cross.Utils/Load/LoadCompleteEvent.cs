﻿using Hino.App.Cross.Utils.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.App.Cross.Utils.Load
{
    public class LoadCompleteEvent : EventArgs, ILoadComplete
    {
        public object Sender { get; private set; }
        public LoadCompleteEvent(object pSender)
        {
            this.Sender = pSender;
        }
    }
}
