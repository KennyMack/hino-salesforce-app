﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.App.Cross.Utils.Load.Enums
{
    public enum LoadStatus
    {
        Iniciando = 0,
        Concluido = 1
    }

    public enum LoadType
    {
        ShowLoad = 0,
        SyncStatus = 1,
        RefreshLogin = 2
    }
}
