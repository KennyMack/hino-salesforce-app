﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.App.Cross.Utils.Interfaces
{
    public interface IMessageRaise
    {
        string Title { get; }
        string Text { get; }

        object Tag { get; }
    }
}
