﻿using System;

namespace Hino.App.Cross.Utils.Interfaces
{
    public interface IMessage
    {
        void ShowShortMessage(String text, bool Short = true);
    }
}
