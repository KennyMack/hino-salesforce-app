﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.App.Cross.Utils.Interfaces
{
    public interface ILoadComplete
    {
        object Sender { get; }
    }
}
