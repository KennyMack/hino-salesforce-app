﻿using Hino.App.Cross.Utils.Interfaces;
using System;

namespace Hino.App.Cross.Utils.Messages
{
    public class MessageRaiseEvent: EventArgs, IMessageRaise
    {
        public MessageRaiseEvent(string title, string text)
        {
            Text = text;
            Title = title;
        }
        public MessageRaiseEvent(string title, string text, object tag)
        {
            Text = text;
            Title = title;
            Tag = tag;
        }

        public string Text { get; private set; }
        public string Title { get; private set; }
        public object Tag { get; private set; }
    }
}
