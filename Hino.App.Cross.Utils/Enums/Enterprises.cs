﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.App.Cross.Utils.Enums
{
    public enum AddressType
    {
        CommercialAddress = 0,
        DeliveryAddress = 1,
        LevyAddress = 2
    }
}
