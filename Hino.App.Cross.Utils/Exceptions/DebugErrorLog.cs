﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.App.Cross.Utils.Exceptions
{
    public static class DebugErrorLog
    {
        public static void Log(object ex)
        {
            System.Diagnostics.Debug.WriteLine(ex);
        }
    }
}
