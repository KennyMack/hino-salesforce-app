﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.App.Cross.Utils.Database
{
    public class SortResult
    {
        public bool Descending { get; set; }
        public object Expression { get; set; }
    }
}
