﻿using System;
using System.Linq;

namespace Hino.App.Cross.Utils.Extensions
{
    public static class InReferencia
    {
        public static bool In<T>(this T item, params T[] items)
        {
            if (items == null)
                throw new ArgumentNullException("itens");

            return items.Contains(item);
        }
    }
}
