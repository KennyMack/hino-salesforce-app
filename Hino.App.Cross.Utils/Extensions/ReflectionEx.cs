﻿using Hino.App.Cross.Utils.Exceptions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Hino.App.Cross.Utils.Extensions
{
    public static class ReflectionEx
    {
        public static void CopyProperties<TDestino, TOrigem>(this TDestino objDest, TOrigem objOrig, string prevNameDest = "") where TDestino : class where TOrigem : class
        {
            var isNullable = false;
            Type PropType;
            foreach (var attr in objOrig.GetType().GetProperties().Where(p => p.CanRead))
            {
                var propertyInfo = objDest.GetType().GetProperty($"{prevNameDest}{attr.Name}", BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                try
                {
                    if (propertyInfo != null && propertyInfo.CanWrite)
                    {
                        PropType = propertyInfo.PropertyType;
                        isNullable = false;
                        if (propertyInfo.PropertyType.Name.IndexOf("Nullable`", StringComparison.Ordinal) > -1)
                        {
                            PropType = Nullable.GetUnderlyingType(PropType);
                            isNullable = true;
                        }

                        var attrVal = attr.GetValue(objOrig, null);

                        if (isNullable && attrVal == null)
                        {
                            propertyInfo.SetValue(objDest, null, null);
                        }
                        else
                        {
                            switch (PropType.Name)
                            {
                                case "Int16":
                                    propertyInfo.SetValue(objDest, Convert.ToInt16(attrVal), null);
                                    break;
                                case "Int32":
                                    propertyInfo.SetValue(objDest, Convert.ToInt32(attrVal), null);
                                    break;
                                case "Int64":
                                    propertyInfo.SetValue(objDest, Convert.ToInt64(attrVal), null);
                                    break;
                                case "SByte":
                                    propertyInfo.SetValue(objDest, Convert.ToSByte(attrVal), null);
                                    break;
                                case "DateTime":
                                    propertyInfo.SetValue(objDest, Convert.ToDateTime(attrVal, new CultureInfo("pt-BR")), null);
                                    break;
                                case "Boolean":
                                    if (attrVal == null)
                                    {
                                        propertyInfo.SetValue(objDest, false, null);
                                        break;
                                    }

                                    if (int.TryParse(attrVal.ToString(), out int val))
                                    {
                                        propertyInfo.SetValue(objDest, val > 0, null);
                                        break;
                                    }

                                    if (bool.TryParse(attrVal.ToString(), out bool valb))
                                    {
                                        propertyInfo.SetValue(objDest, valb, null);
                                        break;
                                    }

                                    propertyInfo.SetValue(objDest, false, null);
                                    break;
                                default:
                                    propertyInfo.SetValue(objDest, attrVal, null);
                                    break;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    DebugErrorLog.Log(e);

                    throw new Exception($"Não foi possível converter a property {attr.Name} para {propertyInfo.Name} valor '{attr.GetValue(objOrig, null)}' motivo: {e.Message}");
                }
            }

        }

        public static Dictionary<string, object> GetProperties<T>(this T objOrig) where T : class
        {
            var valueDict = new Dictionary<string, object>();

            foreach (var attr in objOrig.GetType().GetProperties())
            {
                var value = attr.GetValue(objOrig, null);
                valueDict.Add(attr.Name, value);
            }

            return valueDict;
        }

        /// <summary>
        /// Compares the properties of two objects of the same type and returns if all properties are equal.
        /// </summary>
        /// <param name="objectA">The first object to compare.</param>
        /// <param name="objectB">The second object to compre.</param>
        /// <param name="ignoreList">A list of property names to ignore from the comparison.</param>
        /// <returns><c>true</c> if all property values are equal, otherwise <c>false</c>.</returns>
        public static bool EqualTo(this object objectA, object objectB, params string[] ignoreList)
        {
            bool result;

            if (objectA != null && objectB != null)
            {
                Type objectType;

                objectType = objectA.GetType();

                result = true; // assume by default they are equal

                foreach (PropertyInfo propertyInfo in objectType.GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(p => p.CanRead && !ignoreList.Contains(p.Name)))
                {
                    object valueA;
                    object valueB;

                    valueA = propertyInfo.GetValue(objectA, null);
                    valueB = propertyInfo.GetValue(objectB, null);

                    // if it is a primative type, value type or implements IComparable, just directly try and compare the value
                    if (CanDirectlyCompare(propertyInfo.PropertyType))
                    {
                        if (!AreValuesEqual(valueA, valueB))
                        {
                            Console.WriteLine("Mismatch with property '{0}.{1}' found.", objectType.FullName, propertyInfo.Name);
                            result = false;
                        }
                    }
                    // if it implements IEnumerable, then scan any items
                    else if (typeof(IEnumerable).IsAssignableFrom(propertyInfo.PropertyType))
                    {
                        IEnumerable<object> collectionItems1;
                        IEnumerable<object> collectionItems2;
                        int collectionItemsCount1;
                        int collectionItemsCount2;

                        // null check
                        if (valueA == null && valueB != null || valueA != null && valueB == null)
                        {
                            Console.WriteLine("Mismatch with property '{0}.{1}' found.", objectType.FullName, propertyInfo.Name);
                            result = false;
                        }
                        else if (valueA != null && valueB != null)
                        {
                            collectionItems1 = ((IEnumerable)valueA).Cast<object>();
                            collectionItems2 = ((IEnumerable)valueB).Cast<object>();
                            collectionItemsCount1 = collectionItems1.Count();
                            collectionItemsCount2 = collectionItems2.Count();

                            // check the counts to ensure they match
                            if (collectionItemsCount1 != collectionItemsCount2)
                            {
                                Console.WriteLine("Collection counts for property '{0}.{1}' do not match.", objectType.FullName, propertyInfo.Name);
                                result = false;
                            }
                            // and if they do, compare each item... this assumes both collections have the same order
                            else
                            {
                                for (int i = 0; i < collectionItemsCount1; i++)
                                {
                                    object collectionItem1;
                                    object collectionItem2;
                                    Type collectionItemType;

                                    collectionItem1 = collectionItems1.ElementAt(i);
                                    collectionItem2 = collectionItems2.ElementAt(i);
                                    collectionItemType = collectionItem1.GetType();

                                    if (CanDirectlyCompare(collectionItemType))
                                    {
                                        if (!AreValuesEqual(collectionItem1, collectionItem2))
                                        {
                                            Console.WriteLine("Item {0} in property collection '{1}.{2}' does not match.", i, objectType.FullName, propertyInfo.Name);
                                            result = false;
                                        }
                                    }
                                    else if (!EqualTo(collectionItem1, collectionItem2, ignoreList))
                                    {
                                        Console.WriteLine("Item {0} in property collection '{1}.{2}' does not match.", i, objectType.FullName, propertyInfo.Name);
                                        result = false;
                                    }
                                }
                            }
                        }
                    }
                    else if (propertyInfo.PropertyType.IsClass)
                    {
                        if (!EqualTo(propertyInfo.GetValue(objectA, null), propertyInfo.GetValue(objectB, null), ignoreList))
                        {
                            Console.WriteLine("Mismatch with property '{0}.{1}' found.", objectType.FullName, propertyInfo.Name);
                            result = false;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Cannot compare property '{0}.{1}'.", objectType.FullName, propertyInfo.Name);
                        result = false;
                    }
                }
            }
            else
                result = object.Equals(objectA, objectB);

            return result;
        }

        /// <summary>
        /// Determines whether value instances of the specified type can be directly compared.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>
        /// 	<c>true</c> if this value instances of the specified type can be directly compared; otherwise, <c>false</c>.
        /// </returns>
        private static bool CanDirectlyCompare(Type type)
        {
            return typeof(IComparable).IsAssignableFrom(type) || type.IsPrimitive || type.IsValueType;
        }

        /// <summary>
        /// Compares two values and returns if they are the same.
        /// </summary>
        /// <param name="valueA">The first value to compare.</param>
        /// <param name="valueB">The second value to compare.</param>
        /// <returns><c>true</c> if both values match, otherwise <c>false</c>.</returns>
        private static bool AreValuesEqual(this object valueA, object valueB)
        {
            bool result;
            IComparable selfValueComparer;

            selfValueComparer = valueA as IComparable;

            if (valueA == null && valueB != null || valueA != null && valueB == null)
                result = false; // one of the values is null
            else if (selfValueComparer != null && selfValueComparer.CompareTo(valueB) != 0)
                result = false; // the comparison using IComparable failed
            else if (!object.Equals(valueA, valueB))
                result = false; // the comparison using Equals failed
            else
                result = true; // match

            return result;
        }


    }

}
