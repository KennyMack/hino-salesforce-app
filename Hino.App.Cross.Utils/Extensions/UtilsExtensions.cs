﻿
using Hino.App.Cross.Utils.Demograph;
using Hino.App.Cross.Utils.Exceptions;
using Hino.App.Cross.Utils.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Hino.App.Cross.Utils.Extensions
{
    public static class UtilsExtensions
    {
        #region Copy to clipboard
        public static async Task CopyToClipboard(string pText) =>
            await Clipboard.SetTextAsync(pText);

        public static async Task CopyValueToClipboard(View pInput)
        {
            try
            {
                var properties = pInput.GetType().GetProperties();

                var propText = properties.FirstOrDefault(r => r.Name == "Text");

                if (propText != null)
                {
                    await CopyToClipboard(propText.GetValue(pInput).ToString());
                    DependencyService.Get<IMessage>().ShowShortMessage(
                            "Texto copiado"
                            , true);
                }
            }
            catch (Exception ex)
            {
                DebugErrorLog.Log(ex);
            }
        }
        #endregion
        public static async Task<string> OpenBrowserAsync(string pUrl)
        {
            var retVal = "";
            try
            {
                var url = pUrl;
                if (!url.StartsWith("http"))
                    url = $"http://{url}";

                await Browser.OpenAsync(new Uri(url));
            }
            catch (Exception ex)
            {
                // Other error has occurred.
                DebugErrorLog.Log(ex);
                retVal = Hino.App.Cross.Resources.Messages.CannotOpenBrowser;
            }

            return retVal;
        }

        public async static Task<string> OpenMapAsync(CEPModel pCepModel)
        {
            var retVal = "";
            try
            {
                var placemark = new Placemark
                {
                    CountryName = "Brazil",
                    AdminArea = pCepModel.uf,
                    Thoroughfare = $"{pCepModel.logradouro} {pCepModel.number}",
                    Locality = pCepModel.localidade,
                    PostalCode = pCepModel.cep
                };

                await Map.OpenAsync(placemark);
            }
            catch (Exception ex)
            {
                // Other error has occurred.
                DebugErrorLog.Log(ex);
                retVal = Hino.App.Cross.Resources.Messages.CannotOpenMaps;
            }

            return retVal;
        }

        public static string ClearPhoneNumber(string pNumber)
        {
            var reg = new Regex(@"\d+");

            var matches = reg.Matches(pNumber);

            var number = "";
            foreach (var item in matches)
                number += item.ToString();

            return number;
        }

        public static string PhoneCall(string pNumber)
        {
            var retVal = "";
            try
            {
                PhoneDialer.Open(ClearPhoneNumber(pNumber));
            }
            catch (ArgumentNullException anEx)
            {
                // Number was null or white space
                DebugErrorLog.Log(anEx);
                retVal = Hino.App.Cross.Resources.Messages.InvalidPhoneNumber;
            }
            catch (FeatureNotSupportedException ex)
            {
                // Phone Dialer is not supported on this device.
                DebugErrorLog.Log(ex);
                retVal = Hino.App.Cross.Resources.Messages.CannotCallPhoneNumber;
            }
            catch (Exception ex)
            {
                // Other error has occurred.
                DebugErrorLog.Log(ex);
                retVal = Hino.App.Cross.Resources.Messages.CannotCallPhoneNumber;
            }

            return retVal;
        }

        public static async Task<string> SendSmsAsync(string pNumber)
        {
            var retVal = "";
            try
            {
                var message = new SmsMessage("Olá, ", new[] { ClearPhoneNumber(pNumber) });
                await Sms.ComposeAsync(message);
            }
            catch (FeatureNotSupportedException ex)
            {
                // Sms is not supported on this device.
                DebugErrorLog.Log(ex);
                retVal = Hino.App.Cross.Resources.Messages.CannotSendMessage;
            }
            catch (Exception ex)
            {
                // Other error has occurred.
                DebugErrorLog.Log(ex);
                retVal = Hino.App.Cross.Resources.Messages.CannotSendMessage;
            }
            return retVal;
        }

        public static async Task<string> SendEmailAsync(string subject, string body, List<string> recipients)
        {
            var retVal = "";
            try
            {
                var message = new EmailMessage
                {
                    Subject = subject,
                    Body = body,
                    To = recipients,
                    //Cc = ccRecipients,
                    //Bcc = bccRecipients
                };

                await Email.ComposeAsync(message);
            }
            catch (Exception ex)
            {
                // Some other exception occurred
                DebugErrorLog.Log(ex);
                retVal = Hino.App.Cross.Resources.Messages.CannotSendEmail;
            }
            return retVal;
        }

        public static void ShortVibrate()
        {
            try
            {
                // Use default vibration length
                Vibration.Vibrate(5);
            }
            catch (FeatureNotSupportedException ex)
            {
                // Feature not supported on device
                DebugErrorLog.Log(ex);
            }
            catch (Exception ex)
            {
                // Other error has occurred.
                DebugErrorLog.Log(ex);
            }
        }

        public static void ErrrorVibrate()
        {
            try
            {
                // Use default vibration length
                Vibration.Vibrate(250);
            }
            catch (FeatureNotSupportedException ex)
            {
                // Feature not supported on device
                DebugErrorLog.Log(ex);
            }
            catch (Exception ex)
            {
                // Other error has occurred.
                DebugErrorLog.Log(ex);
            }
        }
    }
}
