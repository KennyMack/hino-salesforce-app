﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.App.Cross.Utils.Extensions
{
    public static class ConvertEx
    {
        public static int? ToInt32(this object value)
        {
            if (value == null || value == DBNull.Value)
                return null;

            var val = 0;
            if (int.TryParse(value.ToString(), out val))
                return val;

            return null;
        }

        public static short? ToInt16(this object value)
        {
            if (value == null || value == DBNull.Value)
                return null;

            short val = 0;
            if (short.TryParse(value.ToString(), out val))
                return val;

            return null;
        }

        public static long? ToInt64(this object value)
        {
            if (value == null || value == DBNull.Value)
                return null;

            long val = 0;
            if (long.TryParse(value.ToString(), out val))
                return val;

            return null;
        }

        public static decimal? ToDecimal(this object value)
        {
            if (value == null || value == DBNull.Value)
                return null;

            decimal val = 0;
            if (decimal.TryParse(value.ToString(), out val))
                return val;

            return null;
        }

        public static sbyte? ToSByte(this object value)
        {
            if (value == null || value == DBNull.Value)
                return null;

            sbyte val = 0;
            if (sbyte.TryParse(value.ToString(), out val))
                return val;

            return null;
        }

        public static DateTime? ToDateTime(this object value)
        {
            if (value == null || value == DBNull.Value)
                return null;

            DateTime val = DateTime.Now;
            if (DateTime.TryParse(value.ToString(), out val))
                return val;

            return null;
        }
    }
}
