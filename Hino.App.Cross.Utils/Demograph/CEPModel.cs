﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.App.Cross.Utils.Demograph
{
    public class CEPModel
    {
        public string cep { get; set; }
        public string logradouro { get; set; }
        public string complemento { get; set; }
        public string bairro { get; set; }
        public string localidade { get; set; }
        public string uf { get; set; }
        public string unidade { get; set; }
        public string ibge { get; set; }
        public string gia { get; set; }
        public string number { get; set; }

        public GECEPAddress CEPAddress
        {
            get
            {
                if (HereAddress != null)
                    return new GECEPAddress
                    {
                        Label = HereAddress.Address.Label,
                        Country = this.Country != null ? this.Country.CIOC : HereAddress.Address.Country,
                        StateName = HereAddress.Address.StateName ?? uf,
                        State = HereAddress.Address.State ?? uf,
                        City = HereAddress.Address.City ?? localidade,
                        District = HereAddress.Address.District ?? bairro,
                        Street = HereAddress.Address.Street ?? logradouro,
                        HouseNumber = HereAddress.Address.HouseNumber,
                        PostalCode = HereAddress.Address.PostalCode ?? cep,
                        Complement = complemento
                    };

                return new GECEPAddress
                {
                    Label = $"{logradouro}, {bairro}, {localidade} - {uf}, {cep}, Brasil",
                    Country = "BRA",
                    StateName = uf,
                    State = uf,
                    City = localidade,
                    District = bairro,
                    Street = logradouro,
                    HouseNumber = "",
                    PostalCode = cep,
                    Complement = complemento
                };
            }
        }

        public CEPStates GEStates
        {
            get
            {
                if (HereAddress != null)
                    return new CEPStates
                    {
                        Initials = HereAddress.Address.State.ToUpper(),
                        Name = HereAddress.Address.StateName
                    };

                return new CEPStates
                {
                    Name = null,
                    Initials = uf.ToUpper()
                };
            }
        }
        public CEPCities GECities
        {
            get
            {
                if (HereAddress != null)
                    return new CEPCities
                    {
                        Name = HereAddress.Address.City,
                        IBGE = ibge
                    };

                return new CEPCities
                {
                    IBGE = ibge,
                    Name = localidade
                };
            }
        }

        public CEPCountries Country { get; set; }
        public CEPMapBoxAddress GeoAddress { get; set; }
        public CEPHereLocation HereAddress { get; set; }
    }

    public class GECEPAddress
    {
        public string Label { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string District { get; set; }
        public string Street { get; set; }
        public string HouseNumber { get; set; }
        public string StateName { get; set; }
        public string PostalCode { get; set; }
        public string Complement { get; set; }
    }
}
