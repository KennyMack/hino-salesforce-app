﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hino.App.Cross.Utils.Demograph
{
    public class CEPHereLocation
    {
        public CEPHereLocation()
        {
            NavigationPosition = new List<HereGeoItemVM>();
        }

        public string LocationId { get; set; }
        public string LocationType { get; set; }
        public HereGeoItemVM DisplayPosition { get; set; }
        public List<HereGeoItemVM> NavigationPosition { get; set; }
        public HereMapViewVM MapView { get; set; }
        public HereAddressVM Address { get; set; }
        public HereAddressDetailsVM AddressDetails { get; set; }
        public HereMapReferenceVM MapReference { get; set; }
        public HereLinkInfoVM LinkInfo { get; set; }
        public HereAdminInfoVM AdminInfo { get; set; }
    }

    public class HereAddressVM
    {
        public HereAddressVM()
        {
            AdditionalData = new List<HereAditionalDataVM>();
        }

        public string Label { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string District { get; set; }
        public string Street { get; set; }
        public string HouseNumber { get; set; }
        public string PostalCode { get; set; }
        public string StateName
        {
            get => AdditionalData.Where(r => r.key == "StateName").FirstOrDefault()?.value ?? "";
        }

        public List<HereAditionalDataVM> AdditionalData { get; set; }
    }

    public class HereAddressDetailsVM
    {
        public string CountryCode { get; set; }
        public HereDetailVM Country { get; set; }
        public HereDetailVM State { get; set; }
        public HereDetailVM City { get; set; }
        public HereDetailVM District { get; set; }
        public HereDetailVM Street { get; set; }
        public HereStreetDetailVM StreetDetails { get; set; }
        public HereDetailVM HouseNumber { get; set; }
        public string PostalCode { get; set; }
    }

    public class HereMachQualityVM
    {
        public HereMachQualityVM()
        {
            Street = new List<decimal>();
        }
        public decimal PostalCode { get; set; }
        public decimal HouseNumber { get; set; }
        public decimal City { get; set; }
        public decimal District { get; set; }
        public List<decimal> Street { get; set; }
    }

    public class HereMapReferenceVM
    {
        public string ReferenceId { get; set; }
        public string Spot { get; set; }
        public string SideOfStreet { get; set; }
        public string CountryId { get; set; }
        public string StateId { get; set; }
        public string CityId { get; set; }
        public string DistrictId { get; set; }
        public string AddressId { get; set; }
        public string RoadLinkId { get; set; }
    }

    public class HereLinkInfoVM
    {
        public HereLinkInfoVM()
        {
            TravelDirection = new List<string>();
            LinkFlags = new List<string>();
            AccessFlags = new List<string>();
        }

        public int FunctionalClass { get; set; }
        public List<string> TravelDirection { get; set; }
        public string SpeedCategory { get; set; }
        public List<string> LinkFlags { get; set; }
        public List<string> AccessFlags { get; set; }
    }

    public class HereAdminInfoVM
    {
        public string TimeZoneOffset { get; set; }
        public DateTime LocalTime { get; set; }
        public string Currency { get; set; }
        public string DrivingSide { get; set; }
        public string SystemOfMeasure { get; set; }
    }

    public class HereDetailVM
    {
        public string value { get; set; }
        public string Language { get; set; }
    }

    public class HereStreetDetailVM
    {
        public string BaseName { get; set; }
        public string StreetType { get; set; }
        public string StreetTypeBefore { get; set; }
        public string StreetTypeAttached { get; set; }
        public string Language { get; set; }
    }

    public class HereMapViewVM
    {
        public HereGeoItemVM TopLeft { get; set; }
        public HereGeoItemVM BottomRight { get; set; }
    }

    public class HereGeoItemVM
    {
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
    }

    public class HereAditionalDataVM
    {
        public string value { get; set; }
        public string key { get; set; }
    }
}
