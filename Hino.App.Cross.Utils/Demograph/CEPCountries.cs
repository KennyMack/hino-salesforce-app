﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.App.Cross.Utils.Demograph
{
    public class CEPCountries
    {
        public string Initials { get; set; }
        public string Name { get; set; }
        public string BACEN { get; set; }
        public string DDI { get; set; }
        public string LocalName { get; set; }
        public string CIOC { get; set; }
        public string NumericCode { get; set; }
        public decimal Lat { get; set; }
        public decimal Lng { get; set; }
        public string flag { get; set; }

        public long IdERP { get; set; }
    }
}
