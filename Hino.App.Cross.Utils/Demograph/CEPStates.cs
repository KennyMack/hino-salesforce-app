﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.App.Cross.Utils.Demograph
{
    public class CEPStates
    {
        public string Name { get; set; }
        public string Initials { get; set; }
        public string IBGE { get; set; }
        public string CodeFIPS { get; set; }
        public decimal Lat { get; set; }
        public decimal Lng { get; set; }
        public long CountryID { get; set; }
    }
}
