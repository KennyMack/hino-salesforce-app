﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hino.App.Cross.Utils.Demograph
{
    public class CEPMapBoxAddress
    {
        public string formatedAddress =>
               this.ToString();
        public string zipcode { get; set; }
        public string district { get; set; }
        public string id { get; set; }
        public string type { get; set; }
        public List<string> place_type { get; set; }
        public decimal relevance { get; set; }
        public MapBoxPropertiesVM properties { get; set; }
        public string text { get; set; }
        public string place_name { get; set; }
        public string matching_text { get; set; }
        public string matching_place_name { get; set; }
        public List<decimal> center { get; set; }
        public MapBoxGeometryVM geometry { get; set; }
        public string address { get; set; }
        public List<MapBoxContextVM> context { get; set; }
        public decimal lat
        {
            get
            {
                try
                {
                    return center[0];
                }
                catch (Exception)
                {
                    return 0M;
                }
            }
        }

        public decimal lng
        {
            get
            {
                try
                {
                    return center[1];
                }
                catch (Exception)
                {
                    return 0M;
                }
            }
        }

        public void SetNumber(string pNumber)
        {
            if (address == null)
                address = pNumber;
        }

        public CEPMapBoxAddress()
        {
            place_type = new List<string>();
            properties = new MapBoxPropertiesVM();
            center = new List<decimal>();
            geometry = new MapBoxGeometryVM();
            context = new List<MapBoxContextVM>();
        }

        public override string ToString()
        {
            var city = context.FirstOrDefault(r => r.id.StartsWith("place."))?.text ?? "";
            var state = context.FirstOrDefault(r => r.id.StartsWith("region."))?.text ?? "";

            var addressNumber = address;

            if (!string.IsNullOrEmpty(addressNumber))
                addressNumber = $", {addressNumber} -";
            else
                addressNumber = " -";

            return $"{text}{addressNumber} {district}, {city} - {state}, {zipcode}";
        }
    }
    public class MapBoxPropertiesVM
    {
        public string accuracy { get; set; }
    }

    public class MapBoxGeometryVM
    {
        public string type { get; set; }
        public List<decimal> coordinates { get; set; }

        public MapBoxGeometryVM()
        {
            coordinates = new List<decimal>();
        }
    }

    public class MapBoxContextVM
    {
        public string id { get; set; }
        public string text { get; set; }
        public string wikidata { get; set; }
        public string short_code { get; set; }
    }
}
