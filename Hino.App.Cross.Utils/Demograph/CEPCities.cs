﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.App.Cross.Utils.Demograph
{
    public class CEPCities
    {
        public string Name { get; set; }
        public string IBGE { get; set; }
        public string DDD { get; set; }
        public long StateID { get; set; }
    }
}
