﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.App.Cross.Utils.Attributes
{
    [System.AttributeUsage(System.AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public class EndPointAttribute :  Attribute
    {
        // This is a positional argument
        public EndPointAttribute(string pEndPoint)
        {
            this.EndPoint = pEndPoint;
        }

        public string EndPoint
        {
            get;
            private set;
        }
    }
}
