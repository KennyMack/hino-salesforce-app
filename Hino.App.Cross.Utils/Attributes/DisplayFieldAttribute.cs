﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Hino.App.Cross.Utils.Attributes
{
    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class DisplayFieldAttribute : DisplayNameAttribute
    {
        public DisplayFieldAttribute([CallerMemberName]string propertyName = "")
        {
            var a = new System.Resources.ResourceManager(typeof(Cross.Resources.Fields))
            {
                IgnoreCase = true
            };
            DisplayNameValue = a.GetString(propertyName);
        }
    }
}
