﻿using System.Collections.Generic;

namespace Hino.App.Cross.Utils.Paging
{
    public class PagedResult<T>: BasePagedResult where T : class
    {
        public IList<T> Results { get; set; }

        public PagedResult()
        {
            Results = new List<T>();
        }
    }
}
