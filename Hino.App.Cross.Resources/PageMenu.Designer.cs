﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Hino.App.Cross.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class PageMenu {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal PageMenu() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Hino.App.Cross.Resources.PageMenu", typeof(PageMenu).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Conectado.
        /// </summary>
        public static string Conected {
            get {
                return ResourceManager.GetString("Conected", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Empresas.
        /// </summary>
        public static string Enterprises {
            get {
                return ResourceManager.GetString("Enterprises", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Estabelecimento.
        /// </summary>
        public static string Establishment {
            get {
                return ResourceManager.GetString("Establishment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sair.
        /// </summary>
        public static string Exit {
            get {
                return ResourceManager.GetString("Exit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hino Sistemas.
        /// </summary>
        public static string HeaderTitle {
            get {
                return ResourceManager.GetString("HeaderTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Não Conectado.
        /// </summary>
        public static string NotConected {
            get {
                return ResourceManager.GetString("NotConected", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Forma/Cond. Pagto..
        /// </summary>
        public static string Payments {
            get {
                return ResourceManager.GetString("Payments", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Configurações.
        /// </summary>
        public static string Preferences {
            get {
                return ResourceManager.GetString("Preferences", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Produtos.
        /// </summary>
        public static string Products {
            get {
                return ResourceManager.GetString("Products", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Meus Pedidos.
        /// </summary>
        public static string Sales {
            get {
                return ResourceManager.GetString("Sales", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sincronizar.
        /// </summary>
        public static string Sincronize {
            get {
                return ResourceManager.GetString("Sincronize", resourceCulture);
            }
        }
    }
}
