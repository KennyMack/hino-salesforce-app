﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Hino.App.Cross.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class PageTitle {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal PageTitle() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Hino.App.Cross.Resources.PageTitle", typeof(PageTitle).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cond. Pagto..
        /// </summary>
        public static string CondPaymentPage {
            get {
                return ResourceManager.GetString("CondPaymentPage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Empresa {0}.
        /// </summary>
        public static string DetailEnterprisePage {
            get {
                return ResourceManager.GetString("DetailEnterprisePage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Produto {0}.
        /// </summary>
        public static string DetailProductPage {
            get {
                return ResourceManager.GetString("DetailProductPage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Empresa x Vendas.
        /// </summary>
        public static string DetailSaleEnterprisePage {
            get {
                return ResourceManager.GetString("DetailSaleEnterprisePage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Produto x Vendas.
        /// </summary>
        public static string DetailSaleProductPage {
            get {
                return ResourceManager.GetString("DetailSaleProductPage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Empresa {0}.
        /// </summary>
        public static string EditEnterprisePage {
            get {
                return ResourceManager.GetString("EditEnterprisePage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pedido {0}.
        /// </summary>
        public static string EditSalePage {
            get {
                return ResourceManager.GetString("EditSalePage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Estabelecimento.
        /// </summary>
        public static string Establishment {
            get {
                return ResourceManager.GetString("Establishment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Desbloquear com digital.
        /// </summary>
        public static string FingerPrintTitle {
            get {
                return ResourceManager.GetString("FingerPrintTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Empresas.
        /// </summary>
        public static string ListEnterprisePage {
            get {
                return ResourceManager.GetString("ListEnterprisePage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Forma/Cond. Pagto..
        /// </summary>
        public static string ListPaymentsTypePage {
            get {
                return ResourceManager.GetString("ListPaymentsTypePage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Produtos.
        /// </summary>
        public static string ListProductsPage {
            get {
                return ResourceManager.GetString("ListProductsPage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Meus Pedidos.
        /// </summary>
        public static string ListSalesPage {
            get {
                return ResourceManager.GetString("ListSalesPage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nova Empresa.
        /// </summary>
        public static string NewEnterprisePage {
            get {
                return ResourceManager.GetString("NewEnterprisePage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Novo Pedido.
        /// </summary>
        public static string NewSalePage {
            get {
                return ResourceManager.GetString("NewSalePage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Forma Pagto..
        /// </summary>
        public static string PaymentPage {
            get {
                return ResourceManager.GetString("PaymentPage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Configurações.
        /// </summary>
        public static string Preferences {
            get {
                return ResourceManager.GetString("Preferences", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Intervalo de sincronia.
        /// </summary>
        public static string SyncInterval {
            get {
                return ResourceManager.GetString("SyncInterval", resourceCulture);
            }
        }
    }
}
