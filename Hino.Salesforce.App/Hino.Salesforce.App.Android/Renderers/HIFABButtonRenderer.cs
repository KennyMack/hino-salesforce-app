﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using FAB = Android.Support.Design.Widget.FloatingActionButton;
using Xamarin.Forms;
using Widget = Android.Support.Design.Widget;
using Xamarin.Forms.Platform.Android;
using Android.Content.Res;
using System.ComponentModel;
using Hino.Salesforce.App.Droid.Renderers;
using Hino.App.Cross.Templates.FloatingButton;
using System.Threading.Tasks;
using Android.Graphics;
using Android.Support.V4.View;

[assembly: ExportRenderer(typeof(HIFloatingButton), typeof(HIFABButtonRenderer))]
namespace Hino.Salesforce.App.Droid.Renderers
{
    public class HIFABButtonRenderer : Xamarin.Forms.Platform.Android.AppCompat.ViewRenderer<HIFloatingButton, FAB>
    {
        public HIFABButtonRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<HIFloatingButton> e)
        {
            base.OnElementChanged(e);

            if (this.Control == null)
            {
                this.ViewGroup.SetClipChildren(false);
                this.ViewGroup.SetClipToPadding(false);
                this.UpdateControlForSize();

                this.UpdateStyle();
            }

            if (e.NewElement != null)
            {
                this.Control.Click += Fab_Click;
            }
            else if (e.OldElement != null)
            {
                this.Control.Click -= Fab_Click;
            }
        }

        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == HIFloatingButton.SizeProperty.PropertyName)
            {
                this.UpdateControlForSize();
            }
            else if (e.PropertyName == HIFloatingButton.NormalColorProperty.PropertyName ||
                     e.PropertyName == HIFloatingButton.RippleColorProperty.PropertyName ||
                     e.PropertyName == HIFloatingButton.DisabledColorProperty.PropertyName)
            {
                this.SetBackgroundColors();
            }
            else if (e.PropertyName == HIFloatingButton.HasShadowProperty.PropertyName)
            {
                this.SetHasShadow();
            }
            else if (e.PropertyName == HIFloatingButton.SourceProperty.PropertyName)
            {
                this.SetImage();
            }
            else if (e.PropertyName == HIFloatingButton.IsEnabledProperty.PropertyName)
            {
                this.UpdateEnabled();
            }
            else
            {
                base.OnElementPropertyChanged(sender, e);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.Control.Click -= this.Fab_Click;
            }

            base.Dispose(disposing);
        }

        private void UpdateControlForSize()
        {
            LayoutInflater inflater = (LayoutInflater)this.Context.GetSystemService(Android.Content.Context.LayoutInflaterService);

            Widget.FloatingActionButton fab = null;

            if (this.Element.Size == FabSize.Mini)
            {
                fab = (Widget.FloatingActionButton)inflater.Inflate(Resource.Layout.mini_fab, null);
            }
            else // then normal
            {
                fab = (Widget.FloatingActionButton)inflater.Inflate(Resource.Layout.normal_fab, null);
            }

            this.SetNativeControl(fab);
            this.UpdateStyle();
        }

        private void UpdateStyle()
        {
            this.SetBackgroundColors();

            this.SetHasShadow();

            this.SetImage();

            this.UpdateEnabled();
        }

        private void SetBackgroundColors()
        {
            this.Control.BackgroundTintList = ColorStateList.ValueOf(this.Element.NormalColor.ToAndroid());
            try
            {
                this.Control.RippleColor = this.Element.RippleColor.ToAndroid();
            }
            catch (MissingMethodException)
            {
                // ignore
            }
        }

        private void SetHasShadow()
        {
            try
            {
                if (this.Element.HasShadow)
                {
                    Android.Support.V4.View.ViewCompat.SetElevation(this.Control, 20);
                }
                else
                {
                    Android.Support.V4.View.ViewCompat.SetElevation(this.Control, 0);
                }
            }
            catch { }
        }

        private void SetImage()
        {
            Task.Run(async () =>
            {
                var bitmap = await this.GetBitmapAsync(this.Element.Source);

                (this.Context as Activity).RunOnUiThread(() =>
                {
                    this.Control?.SetImageBitmap(bitmap);
                });
            });
        }

        private void UpdateEnabled()
        {
            this.Control.Enabled = this.Element.IsEnabled;

            if (this.Control.Enabled == false)
            {
                this.Control.BackgroundTintList = ColorStateList.ValueOf(this.Element.DisabledColor.ToAndroid());
            }
            else
            {
                this.UpdateBackgroundColor();
            }
        }

        private async Task<Bitmap> GetBitmapAsync(ImageSource source)
        {
            var handler = GetHandler(source);
            var returnValue = (Bitmap)null;

            returnValue = await handler.LoadImageAsync(source, this.Context);

            return returnValue;
        }

        private void Fab_Click(object sender, EventArgs e)
        {
            this.Element.SendClicked();
        }

        private static IImageSourceHandler GetHandler(ImageSource source)
        {
            IImageSourceHandler returnValue = null;
            if (source is UriImageSource)
            {
                returnValue = new ImageLoaderSourceHandler();
            }
            else if (source is FileImageSource)
            {
                returnValue = new FileImageSourceHandler();
            }
            else if (source is StreamImageSource)
            {
                returnValue = new StreamImagesourceHandler();
            }
            return returnValue;
        }
    }
}