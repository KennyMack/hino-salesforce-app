﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Hino.App.Cross.Utils.Interfaces;

namespace Hino.Salesforce.App.Droid
{
    public class Message : IMessage
    {
        public void ShowShortMessage(string text, bool pShort = false)
        {
            Toast.MakeText(Android.App.Application.Context, text, pShort ? ToastLength.Short : ToastLength.Long).Show();
        }
    }
}