﻿using Autofac;
using Hino.App.Core;

namespace Hino.Salesforce.App.Droid.Core
{
    public class PlatformContainer : AppContainer
    {
        protected override void RegisterServices(ContainerBuilder containerBuilder)
        {
            base.RegisterServices(containerBuilder);

            //Register platform-specific services
        }
    }
}