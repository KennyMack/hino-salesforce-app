﻿using Hino.App.Cross.Utils.Icons;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Salesforce.App.Models
{
    public enum MenuItemType
    {
        Orders,
        Products,
        Enterprises,
        PaymentType,
        Config,
        Establishment,
        Exit,
        Browse,
        About
    }
    public class HomeMenuItem
    {
        public MenuItemType Id { get; set; }

        public string Title { get; set; }

        public string Icon { get; set; }
    }
}
