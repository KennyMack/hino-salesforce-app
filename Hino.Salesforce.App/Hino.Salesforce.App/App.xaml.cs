﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Hino.Salesforce.App.Views;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System.Globalization;
using Xamarin.Essentials;
using Hino.App.Cross.Utils.Interfaces;
using Hino.App.Cross.Services;
using Hino.Salesforce.App.Views.Auth;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;
using Hino.App.Cross.Utils.Exceptions;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Hino.App.Cross.Services.Navigation;
using Hino.App.Core.Navigator.Utilities;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Hino.Salesforce.App
{
    public partial class App : Xamarin.Forms.Application
    {
        private readonly INavigationService _NavigationService;
        public App(INavigationService navigationService)
        {
            _NavigationService = navigationService;
            Xamarin.Forms.Application.Current.On<Xamarin.Forms.PlatformConfiguration.Android>().UseWindowSoftInputModeAdjust(WindowSoftInputModeAdjust.Resize);
            InitializeComponent();
            Device.SetFlags(new string[] { "Expander_Experimental" });

            CultureInfo.DefaultThreadCurrentCulture = new CultureInfo("pt-BR");
            
            XF.Material.Forms.Material.Init(this, "Material.Configuration");
            XF.Navigation.Forms.Navigator.Init(this, "NavBarConfig.Style");

            DInjection.Initialize();
        }

        private void Connectivity_ConnectivityChanged(object sender, ConnectivityChangedEventArgs e)
        {
            CurrentApp.IsConnected = e.NetworkAccess == NetworkAccess.Internet;
            var instance = DependencyService.Get<IMessage>();
            instance.ShowShortMessage(!CurrentApp.IsConnected ?
                Hino.App.Cross.Resources.Messages.NoInternetConnection
                : Hino.App.Cross.Resources.Messages.InternetConection);
        }

        private async Task AskPermissions()
        {
            #region Permissions
            try
            {
                var status = await CrossPermissions.Current.CheckPermissionStatusAsync<CameraPermission>();
                if (status != Plugin.Permissions.Abstractions.PermissionStatus.Granted)
                {
                    if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Camera))
                        DependencyService.Get<IMessage>().ShowShortMessage("Permissão para a câmera é obrigatória.");
                }

                status = await CrossPermissions.Current.CheckPermissionStatusAsync<StoragePermission>();
                if (status != Plugin.Permissions.Abstractions.PermissionStatus.Granted)
                {
                    if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Storage))
                        DependencyService.Get<IMessage>().ShowShortMessage("Permissão para acesso ao armazenamento é obrigatória.");
                }

                status = await CrossPermissions.Current.CheckPermissionStatusAsync<LocationPermission>();
                if (status != Plugin.Permissions.Abstractions.PermissionStatus.Granted)
                {
                    if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Location))
                        DependencyService.Get<IMessage>().ShowShortMessage("Permissão para acesso a sua localização.");
                }

                status = await CrossPermissions.Current.CheckPermissionStatusAsync<CalendarPermission>();
                if (status != Plugin.Permissions.Abstractions.PermissionStatus.Granted)
                {
                    if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Calendar))
                        DependencyService.Get<IMessage>().ShowShortMessage("Permissão para acesso ao seu calendário.");
                }
            }
            catch (Exception ex)
            {
                DebugErrorLog.Log(ex);
            }
            #endregion

        }

        protected async override void OnStart()
        {
            // Handle when your app starts
            CultureInfo.DefaultThreadCurrentCulture = new CultureInfo("pt-BR");
            Connectivity.ConnectivityChanged += Connectivity_ConnectivityChanged;
            CurrentApp.IsConnected = Connectivity.NetworkAccess == NetworkAccess.Internet;

            if (CurrentApp.IsLogged())
                _NavigationService.SetRootView(ViewNames.Sales.MainSalesPage.ToString());
            //App.Current.MainPage = new MainPage();
            else
                App.Current.MainPage = new NavigationPage(new AccountsPage());

            await AskPermissions();

        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
            Connectivity.ConnectivityChanged -= Connectivity_ConnectivityChanged;
        }

        protected async override void OnResume()
        {
            base.OnResume();
            // Handle when your app resumes
            CultureInfo.DefaultThreadCurrentCulture = new CultureInfo("pt-BR");
            Connectivity.ConnectivityChanged += Connectivity_ConnectivityChanged;
            CurrentApp.IsConnected = Connectivity.NetworkAccess == NetworkAccess.Internet;

            await AskPermissions();
        }
    }
}
