﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;

using Xamarin.Forms;

using Hino.Salesforce.App.Models;
using Hino.Salesforce.App.Views;
using Hino.App.Cross.Utils.Exceptions;

namespace Hino.Salesforce.App.ViewModels
{
    public class VeggieModel
    {
        public string Name { get; set; }
        public string Comment { get; set; }
        public bool IsReallyAVeggie { get; set; }
        public string Image { get; set; }
        public VeggieModel()
        {
        }
    }

    public class GroupedVeggieModel : ObservableCollection<VeggieModel>
    {
        public string LongName { get; set; }
        public string ShortName { get; set; }
    }
    public class ItemsViewModel : BaseViewModel
    {
        public ObservableCollection<GroupedVeggieModel> Items { get; set; }
        public Command LoadItemsCommand { get; set; }

        public ItemsViewModel()
        {
            Title = "Browse";
            Items = new ObservableCollection<GroupedVeggieModel>();
            LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());

            MessagingCenter.Subscribe<NewItemPage, Item>(this, "AddItem", async (obj, item) =>
            {
                var newItem = item as Item;
                //Items.Add(newItem);
                await DataStore.AddItemAsync(newItem);
            });
        }

        async Task ExecuteLoadItemsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Items.Clear();

                var veggieGroup = new GroupedVeggieModel() { LongName = "vegetables", ShortName = "v" };
                var fruitGroup = new GroupedVeggieModel() { LongName = "fruit", ShortName = "f" };
                var fruitGroup2 = new GroupedVeggieModel() { LongName = "carne", ShortName = "c" };
                veggieGroup.Add(new VeggieModel() { Name = "celery", IsReallyAVeggie = true, Comment = "try ants on a log" });
                veggieGroup.Add(new VeggieModel() { Name = "tomato", IsReallyAVeggie = false, Comment = "pairs well with basil" });
                veggieGroup.Add(new VeggieModel() { Name = "zucchini", IsReallyAVeggie = true, Comment = "zucchini bread > bannana bread" });
                veggieGroup.Add(new VeggieModel() { Name = "peas", IsReallyAVeggie = true, Comment = "like peas in a pod" });
                fruitGroup.Add(new VeggieModel() { Name = "banana", IsReallyAVeggie = false, Comment = "available in chip form factor" });
                fruitGroup.Add(new VeggieModel() { Name = "strawberry", IsReallyAVeggie = false, Comment = "spring plant" });
                fruitGroup.Add(new VeggieModel() { Name = "cherry", IsReallyAVeggie = false, Comment = "topper for icecream" });

                fruitGroup2.Add(new VeggieModel() { Name = "bife", IsReallyAVeggie = false, Comment = "bife" });
                fruitGroup2.Add(new VeggieModel() { Name = "porco", IsReallyAVeggie = false, Comment = "porco" });
                Items.Add(veggieGroup); 
                Items.Add(fruitGroup);
                Items.Add(fruitGroup2);
                //lstView.ItemsSource = grouped;



                var items = await DataStore.GetItemsAsync(true);
                /*foreach (var item in items)
                {
                    Items.Add(item);
                }*/
            }
            catch (Exception ex)
            {
                DebugErrorLog.Log(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}