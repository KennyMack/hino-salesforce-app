﻿using Hino.App.Cross.Services.Navigation;
using Hino.App.Cross.Services.ViewModels.Sales;
using Hino.App.Cross.Templates.Base;
using Hino.Salesforce.App.Models;
using Plugin.Fingerprint.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.Sales
{
    public abstract class BaseCreateSalePage : BaseModalView<CreateSalesVM> { }
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CreateSalePage : BaseCreateSalePage
    {
        private CancellationTokenSource _cancel;
        private bool _initialized;

        public CreateSalePage()
        {
            PageName = "Hino.Salesforce.App.Views.Sales.CreateSalePage";
            InitializeComponent();
        }

        public CreateSalePage (Item pItem)
        {
            InitializeComponent ();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            if (!_initialized)
            {
                _initialized = true;
                lblAuthenticationType.Text = "Auth Type: " + await Plugin.Fingerprint.CrossFingerprint.Current.GetAuthenticationTypeAsync();
            }
        }


        private async void OnAuthenticate(object sender, EventArgs e)
        {
            await AuthenticateAsync("Prove you have fingers!");
        }

        private async void OnAuthenticateLocalized(object sender, EventArgs e)
        {
            await AuthenticateAsync("Beweise, dass du Finger hast!", "Abbrechen", "Anders!", "Viel zu schnell!");
        }

        private async Task AuthenticateAsync(string reason, string cancel = null, string fallback = null, string tooFast = null)
        {
            _cancel = swAutoCancel.IsToggled ? new CancellationTokenSource(TimeSpan.FromSeconds(10)) : new CancellationTokenSource();
            lblStatus.Text = "";

            var dialogConfig = new AuthenticationRequestConfiguration("My app", reason)
            { // all optional
                CancelTitle = cancel,
                FallbackTitle = fallback,
                AllowAlternativeAuthentication = swAllowAlternative.IsToggled
            };

            // optional
            dialogConfig.HelpTexts.MovedTooFast = tooFast;

            var result = await Plugin.Fingerprint.CrossFingerprint.Current.AuthenticateAsync(dialogConfig, _cancel.Token);

            await SetResultAsync(result);
        }

        private async Task SetResultAsync(FingerprintAuthenticationResult result)
        {
            if (result.Authenticated)
            {
                await DisplayAlert("Tudo ok", "Tudo ok", "OK");
                //await Navigation.PushAsync(new SecretView());
            }
            else
            {
                lblStatus.Text = $"{result.Status}: {result.ErrorMessage}";
            }
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            await ViewModel.Navigation.PopModalAsync();
        }
    }
}