﻿using Hino.App.Core.Navigator.Utilities;
using Hino.App.Cross.Services.Navigation;
using Hino.App.Cross.Services.ViewModels.Sales;
using Hino.App.Cross.Templates.Base;
using Hino.Salesforce.App.Models;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.Sales
{
    public abstract class BaseMainSalesPage : BaseView<MainSalesVM> { }
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainSalesPage : BaseMainSalesPage
    {
        public ObservableCollection<Item> Items { get; set; }

        public MainSalesPage()
        {
            PageName = "Hino.Salesforce.App.Views.Sales.MainSalesPage";
            InitializeComponent();

            Items = new ObservableCollection<Item>
            {
                new Item() { Id = "1", Text = "Item 1", Description = "Item 1" },
                new Item() { Id = "2", Text = "Item 2", Description = "Item 2" },
                new Item() { Id = "3", Text = "Item 3", Description = "Item 3" },
            };

            lvSales.ItemsSource = Items;
        }

        async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var item = args.SelectedItem as Item;
            if (item == null)
                return;

            await Navigation.PushModalAsync(new CreateSalePage(item));

            // Manually deselect item.
            lvSales.SelectedItem = null;
        }

        private async void AddItem_Clicked(object sender, EventArgs e) =>
            await ViewModel.Navigation.PushModalAsync(ViewNames.Sales.CreateSalePage.ToString());

        private void ToolbarItem_Clicked(object sender, EventArgs e)
        {

        }

        private void SearchItem_Clicked(object sender, EventArgs e)
        {

        }

        private void SyncItem_Clicked(object sender, EventArgs e)
        {

        }

        private void OrderByDateDESC_Clicked(object sender, EventArgs e)
        {

        }

        private void OrderByDateASC_Clicked(object sender, EventArgs e)
        {

        }
    }
}
