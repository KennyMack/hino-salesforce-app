﻿using Hino.App.Core.Navigator.Utilities;
using Hino.App.Cross.Models.Auth;
using Hino.App.Cross.Services;
using Hino.App.Cross.Services.Application.Auth;
using Hino.App.Cross.Services.Navigation;
using Hino.App.Cross.Services.ViewModels.Auth;
using Hino.App.Cross.Templates.Base;
using Hino.App.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.Auth
{
    public abstract class BaseAccountsPage : BaseView<AccountVM> { }

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AccountsPage : BaseAccountsPage
    {
        public AccountsPage()
        {
            PageName = "Hino.Salesforce.App.Views.Auth.AccountsPage";
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ViewModel.RefreshDataCommand.Execute(null);
        }

        private void AccountViewTpl_ItemSelected(object sender, EventArgs e)
        {
            if (!(sender is AccountsModel item))
                return;

            CurrentApp.TOKENDATA = new TokenData
            {
                EstablishmentKey = item.EstablishmentKey,
                UserToken = item.UserToken,
                RefreshToken = item.RefreshToken,
                UserKey = item.UserKey,
                SessionId = item.SessionId
            };

            CurrentApp.EstablishmentKeyLogged = CurrentApp.TOKENDATA.EstablishmentKey;
            CurrentApp.UserKeyLogged = CurrentApp.TOKENDATA.UserKey;

            CurrentApp.ESTABLISHMENT = item.Estab;
            CurrentApp.USERLOGGED = item.User;

            Device.BeginInvokeOnMainThread(() =>
            {
                try
                {
                    ViewModel.Navigation.SetRootView(ViewNames.Sales.MainSalesPage.ToString());
                }
                catch (Exception ex)
                {
                    DebugErrorLog.Log(ex);
                }
            });
        }

        private void AccountViewTpl_BtnRegisterClicked(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                try
                {
                    await Navigation.PushAsync(ViewFactory.GetView(ViewNames.Auth.RegisterPage.ToString()));
                }
                catch (Exception ex)
                {
                    DebugErrorLog.Log(ex);
                }
            });
        }

        private void AccountViewTpl_BtnEnterClicked(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                try
                {

                    await Navigation.PushAsync(ViewFactory.GetView(ViewNames.Auth.LoginPage.ToString()));
                }
                catch (Exception ex)
                {
                    DebugErrorLog.Log(ex);
                }
            });
        }

        private async void AccountViewTpl_BtnMenuItemClicked(object sender, EventArgs e)
        {
            if (await App.Current.MainPage.DisplayAlert(
                Hino.App.Cross.Resources.Messages.Confirm, 
                Hino.App.Cross.Resources.Messages.ConfirmRemove, 
                Hino.App.Cross.Resources.Messages.Yes, 
                Hino.App.Cross.Resources.Messages.No))
                await ViewModel.RemoveAccount(((AccountsModel)sender));
        }
    }
}