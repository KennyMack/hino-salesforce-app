﻿using Hino.App.Core.Navigator.Utilities;
using Hino.App.Cross.Services.Application.Auth;
using Hino.App.Cross.Services.Navigation;
using Hino.App.Cross.Services.ViewModels.Auth;
using Hino.App.Cross.Templates.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.Auth
{
    public abstract class BaseRegisterPage : BaseView<RegisterVM> { }

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegisterPage : BaseRegisterPage
    {
        public RegisterPage(RegisterUserData pRegisterUserData)
        {
            PageName = "Hino.Salesforce.App.Views.Auth.RegisterPage";
            InitializeComponent();
            ViewModel.ESTABLISHMENTKEY = pRegisterUserData.EstablishmentKey;
            ViewModel.EMAIL = pRegisterUserData.UserOrEmail.IndexOf("@") > -1 ? pRegisterUserData.UserOrEmail : "";
            ViewModel.USERNAME = pRegisterUserData.UserOrEmail.IndexOf("@") <= -1 ? pRegisterUserData.UserOrEmail : "";

            ViewModel.GoToMainPage += RegisterVM_GoToMainPage;
        }

        private void RegisterVM_GoToMainPage(object sender, EventArgs e) =>
            ViewModel.Navigation.SetRootView(ViewNames.Sales.MainSalesPage.ToString());

        public RegisterPage ()
        {
            InitializeComponent ();
            ViewModel.GoToMainPage += RegisterVM_GoToMainPage;
        }

        private void RegisterViewTpl_PasswordConfirmCompleted(object sender, EventArgs e) =>
            ViewModel.CmdRegisterClick.Execute(null);

        private void RegisterViewTpl_BtnRegisterClicked(object sender, EventArgs e) =>
            ViewModel.CmdRegisterClick.Execute(null);

        protected override void OnDisappearing() =>
            ViewModel.GoToMainPage -= RegisterVM_GoToMainPage;
    }
}