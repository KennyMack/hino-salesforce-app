﻿using Hino.App.Core.Navigator.Utilities;
using Hino.App.Cross.Services;
using Hino.App.Cross.Services.Application.Auth;
using Hino.App.Cross.Services.Navigation;
using Hino.App.Cross.Services.ViewModels.Auth;
using Hino.App.Cross.Templates.Base;
using Hino.App.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.Auth
{
    public abstract class BaseLoginPage : BaseView<LoginVM> { }
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPage : BaseLoginPage
    {
        public LoginPage ()
        {
            PageName = "Hino.Salesforce.App.Views.Auth.LoginPage";
            InitializeComponent ();


            ViewModel.GoToMainPage += _LoginVM_GoToMainPage;
            ViewModel.GoToRegister += _LoginVM_GoToRegister;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ViewModel.ClearData();
        }

        private void _LoginVM_GoToRegister(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                try
                {
                    await Navigation.PushAsync(new RegisterPage((RegisterUserData)sender));
                }
                catch (Exception ex)
                {
                    DebugErrorLog.Log(ex);
                }
            });
        }

        private void _LoginVM_GoToMainPage(object sender, EventArgs e) =>
            ViewModel.Navigation.SetRootView(ViewNames.Sales.MainSalesPage.ToString());

        private void BtnRegister_Clicked(object sender, EventArgs e) =>
            ViewModel.CmdRegisterClick.Execute(null);

        private void BtnEnter_Clicked(object sender, EventArgs e) =>
            ViewModel.CmdLoginClick.Execute(null);

        private void UserLogin_Completed(object sender, EventArgs e) =>
            ViewModel.CmdLoginClick.Execute(null);
    }
}