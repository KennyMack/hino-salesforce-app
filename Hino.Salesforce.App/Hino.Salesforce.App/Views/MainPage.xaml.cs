﻿using Hino.App.Core.Navigator.Utilities;
using Hino.App.Cross.Models.Auth;
using Hino.App.Cross.Services;
using Hino.App.Cross.Services.Application.Auth;
using Hino.App.Cross.Services.DB.Interfaces.Auth;
using Hino.App.Cross.Services.DB.Interfaces.General;
using Hino.App.Cross.Services.Navigation;
using Hino.App.Cross.Services.ViewModels;
using Hino.App.Cross.Templates.Base;
using Hino.App.Cross.Utils.Exceptions;
using Hino.App.Cross.Utils.Interfaces;
using Hino.Salesforce.App.Models;
using Hino.Salesforce.App.Views.Auth;
using Hino.Salesforce.App.Views.General;
using Hino.Salesforce.App.Views.General.Business.Enterprises;
using Hino.Salesforce.App.Views.General.Business.Payment;
using Hino.Salesforce.App.Views.Preferences;
using Hino.Salesforce.App.Views.Sales;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XF.Material.Forms.UI.Dialogs;

namespace Hino.Salesforce.App.Views
{
    public abstract class BaseMainPage : BaseMasterDetailView<MainViewModel> { }

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : BaseMainPage
    {
        bool Changing;
        bool FirstTime;

        Dictionary<MenuItemType, string> MenuPages = new Dictionary<MenuItemType, string>();

        public MainPage()
        {
            InitializeComponent();
            FirstTime = true;
            Changing = false;
            MasterBehavior = MasterBehavior.SplitOnPortrait;

            // MenuPages.Add(MenuItemType.Orders, ((NavigationPage)Detail).CurrentPage);

            // CurrentApp._INavigation = Detail.Navigation;
            this.IsPresentedChanged += MainPage_IsPresentedChanged;
        }

        private void MainPage_IsPresentedChanged(object sender, EventArgs e) =>
            MenuPageData.Update();

        private async Task AskPermissions()
        {
            #region Permissions
            try
            {
                var status = await CrossPermissions.Current.CheckPermissionStatusAsync<CameraPermission>();
                if (status != Plugin.Permissions.Abstractions.PermissionStatus.Granted)
                {
                    if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Camera))
                        DependencyService.Get<IMessage>().ShowShortMessage("Permissão para a câmera é obrigatória.");
                }

                status = await CrossPermissions.Current.CheckPermissionStatusAsync<StoragePermission>();
                if (status != Plugin.Permissions.Abstractions.PermissionStatus.Granted)
                {
                    if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Storage))
                        DependencyService.Get<IMessage>().ShowShortMessage("Permissão para acesso ao armazenamento é obrigatória.");
                }

                status = await CrossPermissions.Current.CheckPermissionStatusAsync<LocationPermission>();
                if (status != Plugin.Permissions.Abstractions.PermissionStatus.Granted)
                {
                    if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Location))
                        DependencyService.Get<IMessage>().ShowShortMessage("Permissão para acesso a sua localização.");
                }

                status = await CrossPermissions.Current.CheckPermissionStatusAsync<CalendarPermission>();
                if (status != Plugin.Permissions.Abstractions.PermissionStatus.Granted)
                {
                    if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Calendar))
                        DependencyService.Get<IMessage>().ShowShortMessage("Permissão para acesso ao seu calendário.");
                }
            }
            catch (Exception ex)
            {
                DebugErrorLog.Log(ex);
            }
            #endregion

        }

        protected async override void OnAppearing()
        {
            XF.Material.Forms.Material.PlatformConfiguration.ChangeStatusBarColor(Color.FromHex("#0d47a1"));

            if (FirstTime)
            {
                FirstTime = false;
                await AskPermissions();
                using (await MaterialDialog.Instance.LoadingDialogAsync(message: Hino.App.Cross.Resources.Messages.LoadProcessing))
                {
                    base.OnAppearing();
                    if (CurrentApp.ESTABLISHMENT == null)
                    {
                        try
                        {
                            var _IGEEstablishmentDB = DInjection.GetIntance<IGEEstablishmentDB>();
                            var _IUserLoggedDB = DInjection.GetIntance<IUserLoggedDB>();
                            var User = await _IUserLoggedDB.GetByUserKeyAsync(CurrentApp.EstablishmentKeyLogged, CurrentApp.UserKeyLogged);

                            CurrentApp.TOKENDATA = new TokenData
                            {
                                EstablishmentKey = User.EstablishmentKey,
                                UserToken = User.UserToken,
                                RefreshToken = User.RefreshToken,
                                UserKey = User.UserKey,
                                SessionId = User.SessionId
                            };

                            CurrentApp.ESTABLISHMENT = await _IGEEstablishmentDB.GetByEstablishmentKey(CurrentApp.EstablishmentKeyLogged);
                            CurrentApp.USERLOGGED = User;
                        }
                        catch (Exception ex)
                        {
                            DebugErrorLog.Log(ex);
                        }

                        if (CurrentApp.IsConnected)
                        {
                            try
                            {
                                var AuthService = new AuthenticationService();
                                if (!await AuthService.CheckAndRefreshToken())
                                {
                                    var instanceMsg = DependencyService.Get<IMessage>();
                                    instanceMsg.ShowShortMessage(Hino.App.Cross.Resources.Validations.InvalidZipCode);
                                }
                            }
                            catch (Exception ex)
                            {
                                DebugErrorLog.Log(ex);
                            }
                        }
                    }
                    MenuPageData.Update();
                }
            }
        }

        public void NavigateFromMenu(MenuItemType id)
        {
            if (Changing)
                return;

            Changing = true;

            if (!MenuPages.ContainsKey(id))
            {
                switch (id)
                {
                    case MenuItemType.Orders:
                        MenuPages.Add(id, ViewNames.Sales.MainSalesPage.ToString());
                        break;
                    case MenuItemType.Products:
                        MenuPages.Add(id, ViewNames.Products.ListProductsPage.ToString());
                        break;
                    case MenuItemType.Enterprises:
                        MenuPages.Add(id, ViewNames.Enterprises.ListEnterprisesPage.ToString());
                        break;
                    case MenuItemType.PaymentType:
                        MenuPages.Add(id, ViewNames.Payment.MainPaymentPage.ToString());
                        break;
                    case MenuItemType.Establishment:
                        MenuPages.Add(id, ViewNames.General.EstablishmentPage.ToString());
                        break;
                    case MenuItemType.Config:
                        MenuPages.Add(id, ViewNames.Preferences.PreferencesPage.ToString());
                        break;
                    case MenuItemType.Exit:
                        CurrentApp.EstablishmentKeyLogged = null;
                        CurrentApp.UserKeyLogged = null;
                        CurrentApp.TOKENDATA = null;
                        CurrentApp.ESTABLISHMENT = null;
                        CurrentApp.USERLOGGED = null;
                        break;
                    case MenuItemType.Browse:
                        MenuPages.Add(id, "ItemsPage");
                        break;
                    case MenuItemType.About:
                        MenuPages.Add(id, "ItemsPage");
                        break;
                    default:
                        MenuPages.Add(id, ViewNames.Sales.MainSalesPage.ToString());
                        break;
                }
            }

            var newPage = MenuPages[id];

            if (newPage != null /*&& !((NavigationPage)Detail).CurrentPage.Equals(newPage)*/)
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    try
                    {
                        await ViewModel.ChangeCurrentPage(newPage);
                    }
                    catch (Exception e)
                    {
                        DebugErrorLog.Log(e);
                    }

                    if (Device.RuntimePlatform == Device.Android)
                        await Task.Delay(100);
                });
                IsPresented = false;
            }
            else
                IsPresented = false;


            Changing = false;
        }
    }
}