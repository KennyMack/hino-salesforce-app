﻿using Hino.App.Cross.Services;
using Hino.App.Cross.Services.DB.Interfaces;
using Hino.App.Cross.Utils.Extensions;
using Hino.App.Cross.Utils.Icons;
using Hino.Salesforce.App.Models;
using Hino.Salesforce.App.Views.Auth;
using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuPage : ContentPage
    {
        MainPage RootPage { get => Application.Current.MainPage as MainPage; }
        List<HomeMenuItem> menuItems;
        public MenuPage()
        {
            InitializeComponent();

            ListViewMenu.ItemSelected += (sender, e) =>
            {
                if (e.SelectedItem == null)
                    return;

                var id = ((HomeMenuItem)e.SelectedItem).Id;
                if (id == MenuItemType.Exit)
                {
                    CurrentApp.EstablishmentKeyLogged = null;
                    CurrentApp.UserKeyLogged = null;
                    CurrentApp.TOKENDATA = null;
                    CurrentApp.ESTABLISHMENT = null;
                    CurrentApp.USERLOGGED = null;

                    App.Current.MainPage = new NavigationPage(new AccountsPage());
                }
                else
                    RootPage.NavigateFromMenu(id);

                ListViewMenu.SelectedItem = null;
            };
        }

        public void Update()
        {
            txtHeaderName.Text = Hino.App.Cross.Resources.PageMenu.HeaderTitle;
            txtEstablishmentName.Text = CurrentApp.ESTABLISHMENT?.NomeFantasia?.SubStrTitle();
            txtUserName.Text = CurrentApp.USERLOGGED?.UserName?.SubStrTitle();

            menuItems = new List<HomeMenuItem>();

            var Estab = CurrentApp.ESTABLISHMENT;

            menuItems.Add(new HomeMenuItem { Id = MenuItemType.Orders, Title = Hino.App.Cross.Resources.PageMenu.Sales, Icon = FontAwesomeIcons.ChartArea });
            menuItems.Add(new HomeMenuItem { Id = MenuItemType.Products, Title = Hino.App.Cross.Resources.PageMenu.Products, Icon = FontAwesomeIcons.Archive });
            menuItems.Add(new HomeMenuItem { Id = MenuItemType.Enterprises, Title = Hino.App.Cross.Resources.PageMenu.Enterprises, Icon = FontAwesomeIcons.Table });
            //if (Estab.AllowPayment)
            menuItems.Add(new HomeMenuItem { Id = MenuItemType.PaymentType, Title = Hino.App.Cross.Resources.PageMenu.Payments, Icon = FontAwesomeIcons.Tag });
            menuItems.Add(new HomeMenuItem { Id = MenuItemType.Establishment, Title = Hino.App.Cross.Resources.PageMenu.Establishment, Icon = FontAwesomeIcons.Hotel });
            menuItems.Add(new HomeMenuItem { Id = MenuItemType.Config, Title = Hino.App.Cross.Resources.PageMenu.Preferences, Icon = FontAwesomeIcons.Cogs });
            menuItems.Add(new HomeMenuItem { Id = MenuItemType.Exit, Title = Hino.App.Cross.Resources.PageMenu.Exit, Icon = FontAwesomeIcons.Times });

            ListViewMenu.ItemsSource = menuItems;
        }

    }
}