﻿using Hino.App.Cross.Services.Application.Auth;
using Hino.App.Cross.Services.Navigation;
using Hino.App.Cross.Services.Utils;
using Hino.App.Cross.Services.ViewModels;
using Hino.App.Cross.Templates.Base;
using Hino.Salesforce.App.Views.Auth;
using Plugin.Fingerprint.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.Preferences
{
    public abstract class BasePreferencesPage : BaseView<PreferencesVM> { }
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PreferencesPage : BasePreferencesPage
    {
        private bool _initialized;

        public PreferencesPage()
        {
            PageName = "Hino.Salesforce.App.Views.Preferences.PreferencesPage";
            InitializeComponent(); 
            ViewModel.MessageConfirmRaise += _PreferencesVM_MessageConfirmRaise;

            MessagingCenter.Subscribe<PreferencesVM, string>(this, "PREFERENCES_CLEARCOMPLETE", (obj, item) =>
            {
                App.Current.MainPage = new NavigationPage(new AccountsPage());
            });
        }

        private async void _PreferencesVM_MessageConfirmRaise(object sender, Hino.App.Cross.Utils.Interfaces.IMessageRaise e)
        {
            if (await DisplayAlert(e.Title, e.Text, Hino.App.Cross.Resources.Messages.Yes, Hino.App.Cross.Resources.Messages.No))
                ViewModel.ClearAllData();
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            if (!_initialized)
            {
                await ViewModel.LoadData();
                _initialized = true;
            }
        }

        private void ConfirmItem_Clicked(object sender, EventArgs e) =>
            ViewModel.CmdSaveClick.Execute(null);

        private async void CancelItem_Clicked(object sender, EventArgs e) =>
            await ViewModel.Navigation.PopAsync();

        private void PreferencesViewTpl_ClearAll(object sender, EventArgs e) =>
            ViewModel.OnClearDataClick();

        private void PreferencesViewTpl_SyncAll(object sender, EventArgs e)
        {

        }

        protected override void OnDisappearing()
        {
            MessagingCenter.Unsubscribe<PreferencesVM, string>(this, "PREFERENCES_CLEARCOMPLETE");

            base.OnDisappearing();
            GC.Collect();
        }

    }
}