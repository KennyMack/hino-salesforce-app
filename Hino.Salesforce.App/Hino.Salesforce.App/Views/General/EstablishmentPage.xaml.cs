﻿using Hino.App.Cross.Services.Navigation;
using Hino.App.Cross.Services.ViewModels.General;
using Hino.App.Cross.Templates.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.General
{
    public abstract class BaseEstablishmentPage : BaseView<EstablishmentVM> { }
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EstablishmentPage : BaseEstablishmentPage
    {
        public EstablishmentPage()
        {
            PageName = "Hino.Salesforce.App.Views.General.EstablishmentPage";
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            ViewModel.LoadData();
            base.OnAppearing();
        }
    }
}