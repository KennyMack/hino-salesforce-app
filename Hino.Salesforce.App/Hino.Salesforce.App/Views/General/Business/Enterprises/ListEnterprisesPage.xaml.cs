﻿using Hino.App.Core.Navigator.Utilities;
using Hino.App.Cross.Models.General.Business;
using Hino.App.Cross.Services;
using Hino.App.Cross.Services.Navigation;
using Hino.App.Cross.Services.ViewModels.General.Business;
using Hino.App.Cross.Templates.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.General.Business.Enterprises
{
    public abstract class BaseListEnterprisesPage : BaseView<ListEnterprisesVM> { }
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListEnterprisesPage : BaseListEnterprisesPage
    {
        public ListEnterprisesPage()
        {
            PageName = "Hino.Salesforce.App.Views.General.Business.Enterprises.ListEnterprisesPage";
            InitializeComponent();
            ViewModel.MessageConfirmRaise += ViewModel_MessageConfirmRaise;
        }

        private async void ViewModel_MessageConfirmRaise(object sender, Hino.App.Cross.Utils.Interfaces.IMessageRaise e)
        {
            if (e.Tag == null)
                return;

            if (await DisplayAlert(e.Title, e.Text, Hino.App.Cross.Resources.Messages.Remove, Hino.App.Cross.Resources.Messages.Cancel))
                await ViewModel.DeleteEnterprise((GEEnterprisesModel)e.Tag);

            ViewModel.SelectedItem = null;
            lvEnterprises.SelectedItem = null;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ViewModel.InfiniteListView(lvEnterprises);
            ViewModel.SearchEvent(txtEnterprisesSearchBar);

            if (ViewModel.Items.Count == 0)
                ViewModel.RefreshDataCommand.Execute(null);
        }

        async void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (!(e.SelectedItem is GEEnterprisesModel item))
                return;

            await ViewModel.Navigation.PushAsync(ViewNames.Enterprises.DetailEnterprisePage.ToString(), item);

            ViewModel.SelectedItem = null;
            lvEnterprises.SelectedItem = null;
        }

        private void FabBtn_Clicked(object sender, EventArgs e) =>
            Device.BeginInvokeOnMainThread(async () =>
            {
                await ViewModel.Navigation.PushModalAsync(ViewNames.Enterprises.CreateEnterprisePage.ToString());
            });

        private async void SyncItem_Clicked(object sender, EventArgs e) =>
            await ViewModel.Syncronize();

        private void SearchItem_Clicked(object sender, EventArgs e)
        {
            ViewModel.ShowSearch = !ViewModel.ShowSearch;

            if (ViewModel.ShowSearch)
                txtEnterprisesSearchBar.Focus();
        }

        private async void EnterprisesItemVCTpl_EditItemClicked(object sender, EventArgs e)
        {
            if (!(sender is MenuItem item))
                return;

            if (!(item.CommandParameter is GEEnterprisesModel itemSelected))
                return;

            await ViewModel.Navigation.PushModalAsync(ViewNames.Enterprises.CreateEnterprisePage.ToString(), itemSelected);
            ViewModel.SelectedItem = null;
            lvEnterprises.SelectedItem = null;
        }

        private void EnterprisesItemVCTpl_RemoveItemClicked(object sender, EventArgs e)
        {
            if (!(sender is MenuItem item))
                return;

            if (!(item.CommandParameter is GEEnterprisesModel itemSelected))
                return;

            ViewModel.SelectedItem = itemSelected;
            ViewModel.DeleteClick();
        }

        private async void OrderListViewVCTpl_FilterUpdated(object sender, Hino.App.Cross.Templates.HeaderView.FilterUpdatedArgs e) =>
            await ViewModel.SetFilterExpression(sender);

        private void OrderListViewVCTpl_OnShown(object sender, EventArgs e) =>
            ViewModel.OnFilterModalShow((bool)sender);

        private async void OrderListViewVCTpl_SortUpdated(object sender, Hino.App.Cross.Templates.HeaderView.SortUpdatedArgs e) =>
            await ViewModel.SetSortExpression(sender);
    }
}