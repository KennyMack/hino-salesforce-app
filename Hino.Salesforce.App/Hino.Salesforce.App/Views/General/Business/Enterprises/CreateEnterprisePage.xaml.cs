﻿using Hino.App.Cross.Models.General.Business;
using Hino.App.Cross.Services;
using Hino.App.Cross.Services.DB.Interfaces.General.Business;
using Hino.App.Cross.Services.Navigation;
using Hino.App.Cross.Services.Search.General.Business;
using Hino.App.Cross.Services.ViewModels.General.Business;
using Hino.App.Cross.Templates;
using Hino.App.Cross.Templates.Base;
using Hino.App.Cross.Utils.Enums;
using Hino.Salesforce.App.Views.Search;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XF.Material.Forms.UI.Dialogs;

namespace Hino.Salesforce.App.Views.General.Business.Enterprises
{
    public abstract class BaseCreateEnterprisePage : BaseModalView<CreateEnterpriseVM> { }

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CreateEnterprisePage : BaseCreateEnterprisePage
    {
        public CreateEnterprisePage()
        {
            PageName = "Hino.Salesforce.App.Views.General.Business.Enterprises.CreateEnterprisePage";
            InitializeComponent();

            ceType.SelectedChoice = "Pessoa Física";
            ceType.Text = "Pessoa Física";

            ViewModel.TypeChanged += (s, e) =>
            {
                var choice = ((GEEnterprisesModel)s).Type == 0 ? "Pessoa Física" : "Pessoa Jurídica";

                ceType.SelectedChoice = choice;
                ceType.Text = choice;
                txtCNPJCPF.Text = ((GEEnterprisesModel)s).CNPJCPF;
            };
        }

        private void ConfirmItem_Clicked(object sender, EventArgs e) =>
            ViewModel.SaveCommand.Execute(null);

        private void MaterialTextField_ChoiceSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var SelectedValue = (e.SelectedItem ?? "").ToString();
            
            if (ViewModel != null && !string.IsNullOrEmpty(SelectedValue))
                ViewModel.TYPEENTERPRISE = SelectedValue;
            
            var Behavior = new MaterialTextMaskedBehavior();
            
            if (SelectedValue == "Pessoa Física" || string.IsNullOrEmpty(SelectedValue))
                Behavior.Mask = "XXX.XXX.XXX-XX";
            else
                Behavior.Mask = "XX.XXX.XXX/XXXX-XX";
            
            txtCNPJCPF.Text = "";
            txtCNPJCPF.Behaviors.Clear();
            txtCNPJCPF.Behaviors.Add(Behavior);
        }

        private async void EnterpriseGeoVCTpl_CommercialAddress_FindCep(object sender, EventArgs e) =>
            await ViewModel.GetCepAsync(AddressType.CommercialAddress);

        private async void EnterpriseGeoVCTpl_DeliveryAddress_FindCep(object sender, EventArgs e) =>
            await ViewModel.GetCepAsync(AddressType.DeliveryAddress);

        private async void EnterpriseGeoVCTpl_LevyAddress_FindCep(object sender, EventArgs e) =>
            await ViewModel.GetCepAsync(AddressType.LevyAddress);


        private void EnterpriseGeoVCTpl_CommercialAddress_UFChoiceSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var SelectedValue = (e.SelectedItem ?? "").ToString();
            if (ViewModel != null && !string.IsNullOrEmpty(SelectedValue))
                ViewModel.CommercialAddress.UF = SelectedValue;
        }

        private void EnterpriseGeoVCTpl_DeliveryAddress_UFChoiceSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var SelectedValue = (e.SelectedItem ?? "").ToString();
            if (ViewModel != null && !string.IsNullOrEmpty(SelectedValue))
                ViewModel.DeliveryAddress.UF = SelectedValue;
        }

        private void EnterpriseGeoVCTpl_LevyAddress_UFChoiceSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var SelectedValue = (e.SelectedItem ?? "").ToString();
            if (ViewModel != null && !string.IsNullOrEmpty(SelectedValue))
                ViewModel.LevyAddress.UF = SelectedValue;
        }
    }
}