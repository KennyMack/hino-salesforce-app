﻿using Hino.App.Core.Navigator.Utilities;
using Hino.App.Cross.Models.General.Business;
using Hino.App.Cross.Services.Application.General;
using Hino.App.Cross.Services.Navigation;
using Hino.App.Cross.Services.Utils;
using Hino.App.Cross.Services.ViewModels.General.Business;
using Hino.App.Cross.Templates;
using Hino.App.Cross.Templates.Base;
using Hino.App.Cross.Utils.Enums;
using Hino.App.Cross.Utils.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.General.Business.Enterprises
{
    public abstract class BaseDetailEnterprisePage : BaseView<DetailEnterpriseVM> { }

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetailEnterprisePage : BaseDetailEnterprisePage
    {
        public DetailEnterprisePage()
        {
            PageName = "Hino.Salesforce.App.Views.General.Business.Enterprises.DetailEnterprisePage";
            InitializeComponent();

            lblNomeFantasia.LongPressToCopy();
            lblRazaoSocial.LongPressToCopy();
            lblCNPJCPF.LongPressToCopy();
            lblAddress.LongPressToCopy();
            lblDistrict.LongPressToCopy();
            lblZipCode.LongPressToCopy();
            lblComplement.LongPressToCopy();

            lblDeliveryAddress.LongPressToCopy();
            lblDeliveryDistrict.LongPressToCopy();
            lblDeliveryZipCode.LongPressToCopy();
            lblDeliveryComplement.LongPressToCopy();

            lblLevyAddress.LongPressToCopy();
            lblLevyDistrict.LongPressToCopy();
            lblLevyZipCode.LongPressToCopy();
            lblLevyComplement.LongPressToCopy();

            lblCellPhone.LongPressPhoneOptions(
                (s, e) => ViewModel.CallCellPhoneAsync(AddressType.CommercialAddress),
                async (s, e) => await ViewModel.SendSMSAsync(AddressType.CommercialAddress),
                async (s, e) => await UtilsExtensions.CopyValueToClipboard(lblCellPhone),
                null);

            lblPhone.LongPressPhoneOptions(
                (s, e) => ViewModel.CallPhoneAsync(AddressType.CommercialAddress),
                null,
                async (s, e) => await UtilsExtensions.CopyValueToClipboard(lblPhone),
                null);

            lblEmail.LongPressEmailOptions(
                async (s, e) => await ViewModel.SendEmailAsync(AddressType.CommercialAddress),
                async (s, e) => await UtilsExtensions.CopyValueToClipboard(lblEmail),
                null);

            lblSiteLink.LongPressBrowserOptions(
                async (s, e) => await ViewModel.BrowserAsync(AddressType.CommercialAddress),
                async (s, e) => await UtilsExtensions.CopyValueToClipboard(lblSiteLink),
                null);


            lblDeliveryCellPhone.LongPressPhoneOptions(
                (s, e) => ViewModel.CallCellPhoneAsync(AddressType.DeliveryAddress),
                async (s, e) => await ViewModel.SendSMSAsync(AddressType.DeliveryAddress),
                async (s, e) => await UtilsExtensions.CopyValueToClipboard(lblDeliveryCellPhone),
                null);

            lblDeliveryPhone.LongPressPhoneOptions(
                (s, e) => ViewModel.CallPhoneAsync(AddressType.DeliveryAddress),
                null,
                async (s, e) => await UtilsExtensions.CopyValueToClipboard(lblDeliveryPhone),
                null);

            lblDeliveryEmail.LongPressEmailOptions(
                async (s, e) => await ViewModel.SendEmailAsync(AddressType.DeliveryAddress),
                async (s, e) => await UtilsExtensions.CopyValueToClipboard(lblDeliveryEmail),
                null);

            lblDeliverySiteLink.LongPressBrowserOptions(
                async (s, e) => await ViewModel.BrowserAsync(AddressType.DeliveryAddress),
                async (s, e) => await UtilsExtensions.CopyValueToClipboard(lblDeliverySiteLink),
                null);



            lblLevyCellPhone.LongPressPhoneOptions(
                (s, e) => ViewModel.CallCellPhoneAsync(AddressType.LevyAddress),
                async (s, e) => await ViewModel.SendSMSAsync(AddressType.LevyAddress),
                async (s, e) => await UtilsExtensions.CopyValueToClipboard(lblLevyCellPhone),
                null);

            lblLevyPhone.LongPressPhoneOptions(
                (s, e) => ViewModel.CallPhoneAsync(AddressType.LevyAddress),
                null,
                async (s, e) => await UtilsExtensions.CopyValueToClipboard(lblLevyPhone),
                null);

            lblLevyEmail.LongPressEmailOptions(
                async (s, e) => await ViewModel.SendEmailAsync(AddressType.LevyAddress),
                async (s, e) => await UtilsExtensions.CopyValueToClipboard(lblLevyEmail),
                null);

            lblLevySiteLink.LongPressBrowserOptions(
                async (s, e) => await ViewModel.BrowserAsync(AddressType.LevyAddress),
                async (s, e) => await UtilsExtensions.CopyValueToClipboard(lblLevySiteLink),
                null);
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            await ViewModel.LoadRegister();
        }

        private async void btnOpenInMap_Clicked(object sender, EventArgs e) =>
            await ViewModel.GeoAsync(AddressType.CommercialAddress);

        private async void btnOpenInMapDeliveryAddress_Clicked(object sender, EventArgs e) =>
            await ViewModel.GeoAsync(AddressType.DeliveryAddress);

        private async void btnOpenInMapLevy_Clicked(object sender, EventArgs e) =>
            await ViewModel.GeoAsync(AddressType.LevyAddress);

        private void btnDelete_Clicked(object sender, EventArgs e)
        {

        }

        private async void btnEdit_Clicked(object sender, EventArgs e) =>
            await ViewModel.Navigation.PushModalAsync(ViewNames.Enterprises.CreateEnterprisePage.ToString(), ViewModel._GEEnterprisesModel);

    }
}