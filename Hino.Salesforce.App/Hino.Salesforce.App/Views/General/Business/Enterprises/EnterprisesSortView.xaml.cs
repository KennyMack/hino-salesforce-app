﻿using Hino.App.Cross.Models.General.Business;
using Hino.App.Cross.Services.ViewModels;
using Hino.App.Cross.Templates;
using Hino.App.Cross.Templates.ListView.Interfaces;
using Hino.App.Cross.Utils.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static Hino.Salesforce.App.Views.General.Business.Enterprises.EnterprisesSortViewVM;

namespace Hino.Salesforce.App.Views.General.Business.Enterprises
{
    class EnterprisesSortViewVM : BaseFilterViewModel
    {
        public enum SortType
        {
            Clear,
            SortRazaoSocialASC,
            SortRazaoSocialDESC,
            SortFantasiaASC,
            SortFantasiaDESC,
            SortIdASC,
            SortIdDESC
        }

        #region Labels
        public string LBLID { get => Hino.App.Cross.Resources.Fields.Id; }
        public string LBLRAZAOSOCIAL { get => Hino.App.Cross.Resources.Fields.RazaoSocial; }
        public string LBLNOMEFANTASIA { get => Hino.App.Cross.Resources.Fields.NomeFantasia; }
        #endregion

        SortResult _sort;
        public SortResult sort { get => _sort; set => SetProperty(ref _sort, value); }

        SortType _sortActual;
        public SortType SortActual { get => _sortActual; set => SetProperty(ref _sortActual, value); }

        public EnterprisesSortViewVM()
        {
            SetSelectedSort(SortType.Clear);
        }

        public void SetSelectedSort(SortType pSort)
        {
            var Descending = false;
            Func<GEEnterprisesModel, object> expr = null;

            SortActual = pSort;

            switch (pSort)
            {
                case SortType.Clear:
                    break;
                case SortType.SortRazaoSocialASC:
                    Descending = false;
                    expr = x => x.RazaoSocial;
                    break;
                case SortType.SortRazaoSocialDESC:
                    Descending = true;
                    expr = x => x.RazaoSocial;
                    break;
                case SortType.SortFantasiaASC:
                    Descending = false;
                    expr = x => x.NomeFantasia;
                    break;
                case SortType.SortFantasiaDESC:
                    Descending = true;
                    expr = x => x.NomeFantasia;
                    break;
                case SortType.SortIdASC:
                    Descending = false;
                    expr = x => x.Id;
                    break;
                case SortType.SortIdDESC:
                    Descending = true;
                    expr = x => x.Id;
                    break;
                default:
                    Descending = false;
                    expr = x => x.RazaoSocial;
                    break;
            }

            sort = new SortResult
            {
                Descending = Descending,
                Expression = expr
            };
        }
    }

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EnterprisesSortView : ContentView, ISortContentView
    {
        private EnterprisesSortViewVM _EnterprisesSortViewVM;
        public EnterprisesSortView()
        {
            InitializeComponent();

            this.BindingContext = _EnterprisesSortViewVM = new EnterprisesSortViewVM();

            SortIdASC.Tap((s, e) => { _EnterprisesSortViewVM.SetSelectedSort(SortType.SortIdASC); PaintOptions(); });
            SortIdDESC.Tap((s, e) => { _EnterprisesSortViewVM.SetSelectedSort(SortType.SortIdDESC); PaintOptions(); });
            SortRazaoSocialASC.Tap((s, e) => { _EnterprisesSortViewVM.SetSelectedSort(SortType.SortRazaoSocialASC); PaintOptions(); });
            SortRazaoSocialDESC.Tap((s, e) => { _EnterprisesSortViewVM.SetSelectedSort(SortType.SortRazaoSocialDESC); PaintOptions(); });
            SortNomeFantasiaASC.Tap((s, e) => { _EnterprisesSortViewVM.SetSelectedSort(SortType.SortFantasiaASC); PaintOptions(); });
            SortNomeFantasiaDESC.Tap((s, e) => { _EnterprisesSortViewVM.SetSelectedSort(SortType.SortFantasiaDESC); PaintOptions(); });
        }

        public void ClearSorts() =>
            _EnterprisesSortViewVM.SetSelectedSort(SortType.Clear);

        public object GetSortsAsync() =>
            _EnterprisesSortViewVM.sort;

        private void PaintOptions()
        {
            var linkColor = Color.FromHex("#4784E6");
            var defaultColor = Color.FromHex("#3f3f3f");

            icoIdASC.TextColor = defaultColor;
            lblIdASC.TextColor = defaultColor;
            icoIdDESC.TextColor = defaultColor;
            lblIdDESC.TextColor = defaultColor;
            icoRazaoSocialASC.TextColor = defaultColor;
            lblRazaoSocialASC.TextColor = defaultColor;
            icoRazaoSocialDESC.TextColor = defaultColor;
            lblRazaoSocialDESC.TextColor = defaultColor;
            icoNomeFantasiaASC.TextColor = defaultColor;
            lblNomeFantasiaASC.TextColor = defaultColor;
            icoNomeFantasiaDESC.TextColor = defaultColor;
            lblNomeFantasiaDESC.TextColor = defaultColor;

            switch (_EnterprisesSortViewVM.SortActual)
            {
                case SortType.SortIdASC:
                    icoIdASC.TextColor = linkColor;
                    lblIdASC.TextColor = linkColor;
                    break;
                case SortType.SortIdDESC:
                    icoIdDESC.TextColor = linkColor;
                    lblIdDESC.TextColor = linkColor;
                    break;
                case SortType.SortRazaoSocialASC:
                    icoRazaoSocialASC.TextColor = linkColor;
                    lblRazaoSocialASC.TextColor = linkColor;
                    break;
                case SortType.SortRazaoSocialDESC:
                    icoRazaoSocialDESC.TextColor = linkColor;
                    lblRazaoSocialDESC.TextColor = linkColor;
                    break;
                case SortType.SortFantasiaASC:
                    icoNomeFantasiaASC.TextColor = linkColor;
                    lblNomeFantasiaASC.TextColor = linkColor;
                    break;
                case SortType.SortFantasiaDESC:
                    icoNomeFantasiaDESC.TextColor = linkColor;
                    lblNomeFantasiaDESC.TextColor = linkColor;
                    break;
            }
        }

        public async Task LoadAsync()
        {
            await Task.Delay(1);
            PaintOptions();
        }

        public Task LoadSortsAsync() =>
            throw new NotImplementedException();
    }
}