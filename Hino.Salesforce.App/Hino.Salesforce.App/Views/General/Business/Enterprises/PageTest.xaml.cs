﻿using Hino.App.Cross.Services.Navigation;
using Hino.App.Cross.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.General.Business.Enterprises
{
    public class vm: BaseViewModel
    {

    }


    public abstract class BasePageTest : BaseModalView<vm> { }

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PageTest : BasePageTest
    {
        public PageTest()
        {
            PageName = "Hino.Salesforce.App.Views.General.Business.Enterprises.PageTest";
            InitializeComponent();
        }
    }
}