﻿using Hino.App.Cross.Models.General.Business;
using Hino.App.Cross.Services;
using Hino.App.Cross.Services.API.Interfaces.General.Business;
using Hino.App.Cross.Services.Navigation;
using Hino.App.Cross.Services.ViewModels.General.Business;
using Hino.App.Cross.Templates.Base;
using LiteDB;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.General.Business.Payment
{
    public abstract class BaseListPaymentConditionPage : BaseView<PaymentConditionVM> { }

    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ListPaymentConditionPage : BaseListPaymentConditionPage
    {
        public ListPaymentConditionPage ()
        {
            PageName = "Hino.Salesforce.App.Views.General.Business.Payment.ListPaymentConditionPage";
            InitializeComponent ();

            ViewModel.InfiniteListView(lvListPaymentCondition);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (ViewModel.Items.Count == 0)
                ViewModel.RefreshDataCommand.Execute(null);
        }
    }
}