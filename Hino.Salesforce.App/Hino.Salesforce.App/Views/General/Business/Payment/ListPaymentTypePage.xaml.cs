﻿using Hino.App.Cross.Services.Navigation;
using Hino.App.Cross.Services.ViewModels.General.Business;
using Hino.App.Cross.Templates.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.General.Business.Payment
{
    public abstract class BaseListPaymentTypePage : BaseView<PaymentTypeVM> { }
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListPaymentTypePage : BaseListPaymentTypePage
    {
        public ListPaymentTypePage()
        {
            PageName = "Hino.Salesforce.App.Views.General.Business.Payment.ListPaymentTypePage";
            InitializeComponent();

            ViewModel.InfiniteListView(lvListPaymentType);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (ViewModel.Items.Count == 0)
                ViewModel.RefreshDataCommand.Execute(null);
        }
    }
}