﻿using Hino.App.Cross.Services.Navigation;
using Hino.App.Cross.Services.ViewModels.General.Business;
using Hino.App.Cross.Templates.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.General.Business.Payment
{
    public abstract class BaseMainPaymentPage : BaseTabbedView<MainPaymentVM> { }
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPaymentPage : BaseMainPaymentPage
    {
        public MainPaymentPage ()
        {
            PageName = "Hino.Salesforce.App.Views.General.Business.Payment.MainPaymentPage";
            InitializeComponent();

            var ListPaymentTypePage = new ListPaymentTypePage
            {
                Title = "Forma Pagto."
            };

            var ListPaymentConditionPage = new ListPaymentConditionPage
            {
                Title = "Cond. Pagto."
            };

            this.Children.Add(ListPaymentTypePage);
            this.Children.Add(ListPaymentConditionPage);
        }

        private async void SyncItem_Clicked(object sender, EventArgs e) =>
            await ViewModel.Syncronize();
    }
}