﻿using Hino.App.Core.Navigator.Utilities;
using Hino.App.Cross.Models.General;
using Hino.App.Cross.Services;
using Hino.App.Cross.Services.Navigation;
using Hino.App.Cross.Services.ViewModels.General.Products;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.General.Products
{
    public abstract class BaseListProductsPage : BaseView<ListProductsVM> { }
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListProductsPage : BaseListProductsPage
    {
        public ListProductsPage()
        {
            base.PageName = "Hino.Salesforce.App.Views.General.Products.ListProductsPage";
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ViewModel.InfiniteListView(lvProducts);
            ViewModel.SearchEvent(txtProductsSearchBar);

            if (ViewModel.Items.Count == 0)
                ViewModel.RefreshDataCommand.Execute(null);
        }

        private async void SyncItem_Clicked(object sender, EventArgs e) =>
            await ViewModel.Syncronize();

        private void SearchItem_Clicked(object sender, EventArgs e)
        {
            ViewModel.ShowSearch = !ViewModel.ShowSearch;

            if (ViewModel.ShowSearch)
                txtProductsSearchBar.Focus();
        }

        async void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (!(e.SelectedItem is GEProductsModel item))
                return;
            
            await ViewModel.Navigation.PushModalAsync(ViewNames.Products.DetailProductsPage.ToString(), item);

            ViewModel.SelectedItem = null;
            lvProducts.SelectedItem = null;
        }

        private async void OrderListViewVCTpl_FilterUpdated(object sender, Hino.App.Cross.Templates.HeaderView.FilterUpdatedArgs e) =>
            await ViewModel.SetFilterExpression(sender);

        private void OrderListViewVCTpl_OnShown(object sender, EventArgs e) =>
            ViewModel.OnFilterModalShow((bool)sender);

        private async void OrderListViewVCTpl_SortUpdated(object sender, Hino.App.Cross.Templates.HeaderView.SortUpdatedArgs e) =>
            await ViewModel.SetSortExpression(sender);
    }
}