﻿using Hino.App.Cross.Services.Navigation;
using Hino.App.Cross.Services.ViewModels.General.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.General.Products
{
    public abstract class BaseDetailProductsPage : BaseModalView<DetailProductsVM> { }
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetailProductsPage : BaseDetailProductsPage
    {
        public DetailProductsPage()
        {
            this.PageName = "Hino.Salesforce.App.Views.General.Products.DetailProductsPage";
            InitializeComponent();
        }
    }
}