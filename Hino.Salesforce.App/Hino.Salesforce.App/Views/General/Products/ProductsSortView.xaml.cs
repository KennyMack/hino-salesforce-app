﻿using Hino.App.Cross.Models.General;
using Hino.App.Cross.Services;
using Hino.App.Cross.Services.DB.Interfaces.General.Products;
using Hino.App.Cross.Services.ViewModels;
using Hino.App.Cross.Templates;
using Hino.App.Cross.Templates.ListView.Interfaces;
using Hino.App.Cross.Utils.Database;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static Hino.Salesforce.App.Views.General.Products.ProductsSortViewVM;

namespace Hino.Salesforce.App.Views.General.Products
{
    class ProductsSortViewVM : BaseFilterViewModel
    {
        public enum SortType
        {
            Clear,
            SortProductKeyASC,
            SortProductKeyDESC,
            SortProductNameASC,
            SortProductNameDESC
        }

        #region Labels
        public string LBLPRODUCTNAME { get => Hino.App.Cross.Resources.Fields.ProductName; }
        public string LBLPRODUCTKEY { get => Hino.App.Cross.Resources.Fields.ProductKey; }
        #endregion

        SortResult _sort;
        public SortResult sort { get => _sort; set => SetProperty(ref _sort, value); }

        SortType _sortActual;
        public SortType SortActual { get => _sortActual; set => SetProperty(ref _sortActual, value); }

        public ProductsSortViewVM()
        {
            SetSelectedSort(SortType.Clear);
        }

        public void SetSelectedSort(SortType pSort)
        {
            var Descending = false;
            Func<GEProductsModel, object> expr = null;

            SortActual = pSort;

            switch (pSort)
            {
                case SortType.SortProductKeyASC:
                    Descending = false;
                    expr = x => x.ProductKey;
                    break;
                case SortType.SortProductKeyDESC:
                    Descending = true;
                    expr = x => x.ProductKey;
                    break;
                case SortType.SortProductNameASC:
                    Descending = false;
                    expr = x => x.Name;
                    break;
                case SortType.SortProductNameDESC:
                    Descending = true;
                    expr = x => x.Name;
                    break;
                default:
                    Descending = true;
                    expr = x => x.Id;
                    break;
            }
            sort = new SortResult
            {
                Descending = Descending,
                Expression = expr
            };
        }
    }

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProductsSortView : ContentView, ISortContentView
    {
        private ProductsSortViewVM _ProductsSortViewVM;
        public ProductsSortView()
        {
            InitializeComponent();
            

            this.BindingContext = _ProductsSortViewVM = new ProductsSortViewVM();
            SortProductKeyASC.Tap((s, e) => { _ProductsSortViewVM.SetSelectedSort(SortType.SortProductKeyASC); PaintOptions(); });
            SortProductKeyDESC.Tap((s, e) => { _ProductsSortViewVM.SetSelectedSort(SortType.SortProductKeyDESC); PaintOptions(); });
            SortProductNameASC.Tap((s, e) => { _ProductsSortViewVM.SetSelectedSort(SortType.SortProductNameASC); PaintOptions(); });
            SortProductNameDESC.Tap((s, e) => { _ProductsSortViewVM.SetSelectedSort(SortType.SortProductNameDESC); PaintOptions(); });


        }

        public void ClearSorts() =>
            _ProductsSortViewVM.SetSelectedSort(SortType.Clear);

        public object GetSortsAsync() => 
            _ProductsSortViewVM.sort;

        private void PaintOptions()
        {
            var linkColor = Color.FromHex("#4784E6");
            var defaultColor = Color.FromHex("#3f3f3f");

            icoSortProductKeyASC.TextColor = defaultColor;
            lblSortProductKeyASC.TextColor = defaultColor;
            icoSortProductKeyDESC.TextColor = defaultColor;
            lblSortProductKeyDESC.TextColor = defaultColor;
            icoSortProductNameASC.TextColor = defaultColor;
            lblSortProductNameASC.TextColor = defaultColor;
            icoSortProductNameDESC.TextColor = defaultColor;
            lblSortProductNameDESC.TextColor = defaultColor;

            switch (_ProductsSortViewVM.SortActual)
            {
                case SortType.SortProductKeyASC:
                    icoSortProductKeyASC.TextColor = linkColor;
                    lblSortProductKeyASC.TextColor = linkColor;
                    break;
                case SortType.SortProductKeyDESC:
                    icoSortProductKeyDESC.TextColor = linkColor;
                    lblSortProductKeyDESC.TextColor = linkColor;
                    break;
                case SortType.SortProductNameASC:
                    icoSortProductNameASC.TextColor = linkColor;
                    lblSortProductNameASC.TextColor = linkColor;
                    break;
                case SortType.SortProductNameDESC:
                    icoSortProductNameDESC.TextColor = linkColor;
                    lblSortProductNameDESC.TextColor = linkColor;
                    break;
            }
        }

        public async Task LoadAsync()
        {
            await Task.Delay(1);
            PaintOptions();
        }

        public Task LoadSortsAsync() =>
            throw new NotImplementedException();
    }
}