﻿using Hino.App.Cross.Models.General;
using Hino.App.Cross.Services;
using Hino.App.Cross.Services.DB.Interfaces.General.Products;
using Hino.App.Cross.Services.ViewModels;
using Hino.App.Cross.Templates.ListView.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.General.Products
{
    class ProdutctsFilterViewVM: BaseFilterViewModel
    {
        public ObservableCollection<string> UnitItems;
        private IGEProductsDB _IGEProductsDB;

        #region Labels
        public string LBLUNIT { get => Hino.App.Cross.Resources.Fields.Unit; }
        #endregion

        #region Fields
        string _unit;
        public string UNIT
        {
            get => _unit; set => SetProperty(ref _unit, value);
        }
        bool _showunit;
        public bool SHOWUNIT
        {
            get => _showunit; set => SetProperty(ref _showunit, value);
        }
        #endregion

        public ProdutctsFilterViewVM()
        {
            UnitItems = new ObservableCollection<string>();
        }

        public async Task LoadAsync()
        {
            await Task.Delay(1);
            _IGEProductsDB = DInjection.GetIntance<IGEProductsDB>();
        }

        public async Task<IEnumerable<string>> GetUnit()
        {
            var Products = await _IGEProductsDB.GetItemsAsync(-1);
            var Units = Products.Results.Select(r => r.Unit.ToUpper()).Distinct();
            UnitItems.Clear();
            foreach (var item in Units)
                UnitItems.Add(item);
            SHOWUNIT = UnitItems.Count > 0;
            return UnitItems;
        }
    }
    
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProdutctsFilterView : ContentView, IFilterContentView
    {
        private ProdutctsFilterViewVM _ProdutctsFilterViewVM;
        public ProdutctsFilterView()
        {
            InitializeComponent();
            this.BindingContext = _ProdutctsFilterViewVM = new ProdutctsFilterViewVM();
        }

        public void ClearFilters()
        {
            _ProdutctsFilterViewVM.UNIT = null;
            ceUNIT.SelectedChoice = null;
        }

        public object GetFilterAsync()
        {
            ParameterExpression argParam = Expression.Parameter(typeof(GEProductsModel), "Unit");

            Expression unityProperty = Expression.Property(argParam, "Unit");

            var unit = Expression.Constant(_ProdutctsFilterViewVM.UNIT);

            Expression e1 = Expression.Equal(unityProperty, unit);

            return Expression.Lambda<Func<GEProductsModel, bool>>(e1, argParam);
        }

        public async Task LoadAsync()
        {
            await LoadFiltersAsync();

            ceUNIT.Choices = _ProdutctsFilterViewVM.UnitItems;
        }

        public async Task LoadFiltersAsync()
        {
            await _ProdutctsFilterViewVM.LoadAsync();
            await _ProdutctsFilterViewVM.GetUnit();
        }
    }
}