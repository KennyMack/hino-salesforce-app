﻿using Hino.App.Cross.Services.Search;
using Hino.App.Cross.Services.Search.Interfaces;
using Hino.App.Cross.Services.ViewModels.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.Search
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SearchSinglePage : Rg.Plugins.Popup.Pages.PopupPage
    {
        private SearchSinglePageVM _SearchSinglePageVM;

        public SearchSinglePage(ISearchDataResult pIRepositorySearch)
        {
            InitializeComponent();
            BindingContext = _SearchSinglePageVM = new SearchSinglePageVM(pIRepositorySearch);
            _SearchSinglePageVM.Title = pIRepositorySearch.Title;
            _SearchSinglePageVM.InfiniteListView(lvResultSearch);
            _SearchSinglePageVM.SearchEvent(txtSearchBar);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (_SearchSinglePageVM.Items.Count == 0)
                _SearchSinglePageVM.RefreshDataCommand.Execute(null);
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var item = args.SelectedItem as SearchItemList;
            if (item == null)
                return;

            _SearchSinglePageVM.CmdOkClick.Execute(null);

            lvResultSearch.SelectedItem = null;
        }

        protected override bool OnBackButtonPressed()
        {
            _SearchSinglePageVM.CmdBackClick.Execute(null);
            return base.OnBackButtonPressed();
        }
    }
}