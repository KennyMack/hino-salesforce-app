﻿using Hino.App.Cross.Utils.Attributes;

namespace Hino.App.Cross.Models.Fiscal.Taxes
{
    [EndPoint("Fiscal/Aliquota/{pEstablishmentKey}")]
    public class FSICMSSTAliqUF : BaseEntity
    {
        [RequiredField]
        [DisplayField]
        public string UF { get; set; }

        [RequiredField]
        [DisplayField]
        public string NCM { get; set; }

        [RequiredField]
        [DisplayField]
        public decimal ALIQICMSST { get; set; }

        [RequiredField]
        [DisplayField]
        public decimal ALIQICMS { get; set; }

        [RequiredField]
        [DisplayField]
        public decimal MVA { get; set; }
    }
}
