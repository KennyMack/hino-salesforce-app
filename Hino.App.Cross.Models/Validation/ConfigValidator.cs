﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.App.Cross.Models.Validation
{
    public class ConfigValidator: AbstractValidator<ConfigModel>
    {
        public ConfigValidator()
        {
            RuleFor(x => x.IntervalSync)
               .NotNull()
               .WithMessage(Resources.Validations.RequiredDefault);
            RuleFor(x => x.IntervalSync)
                .Must(r => int.TryParse(r.ToString(), out int o))
                .WithMessage(Resources.Validations.MustBeNumber);
            RuleFor(x => x.IntervalSync)
                .Must(r =>
                {
                    if (int.TryParse(r.ToString(), out int o))
                        return o > 0;
                    return false;
                })
                .WithMessage(Resources.Validations.GreaterThanZero);
        }
    }
}
