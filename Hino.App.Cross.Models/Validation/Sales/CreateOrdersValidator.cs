﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using Hino.App.Cross.Models.Sales;

namespace Hino.App.Cross.Models.Validation.Sales
{
    public class CreateOrdersValidator : AbstractValidator<VEOrdersModel>
    {
        public CreateOrdersValidator()
        {
            RuleFor(x => x.EnterpriseID)
                .NotNull()
                .WithMessage(
                string.Format(
                    Resources.Validations.RequiredDefault,
                    Resources.Fields.RazaoSocial)
                );
            /*RuleFor(x => x.EnterpriseID)
                .Must(r =>
                {

                    if (int.TryParse(r.ToString(), out int o))
                        return o > 0;
                    return false;
                })
                .WithMessage(
                string.Format(
                    Resources.Validations.RequiredDefault,
                    Resources.Fields.RazaoSocial));*/

            RuleFor(x => x.UserID)
                .NotNull()
                .WithMessage(
                string.Format(
                    Resources.Validations.RequiredDefault,
                    Resources.Fields.UserName));

            RuleFor(x => x.UserID)
                .Must(r =>
                {
                    if (int.TryParse(r.ToString(), out int o))
                        return o > 0;
                    return false;
                })
                .WithMessage(
                string.Format(
                    Resources.Validations.RequiredDefault,
                    Resources.Fields.UserName));

            RuleFor(x => x.TypePaymentID)
                .NotNull()
                .WithMessage(
                string.Format(
                    Resources.Validations.RequiredDefault,
                    Resources.Fields.PaymentType));

            RuleFor(x => x.TypePaymentID)
                .Must(r =>
                {
                    if (int.TryParse(r.ToString(), out int o))
                        return o > 0;
                    return false;
                })
                .WithMessage(
                string.Format(
                    Resources.Validations.RequiredDefault,
                    Resources.Fields.PaymentType));

            RuleFor(x => x.PayConditionID)
                .NotNull()
                .WithMessage(
                string.Format(
                    Resources.Validations.RequiredDefault,
                    Resources.Fields.PaymentCondition));

            RuleFor(x => x.PayConditionID)
                .Must(r =>
                {
                    if (int.TryParse(r.ToString(), out int o))
                        return o > 0;
                    return false;
                })
                .WithMessage(
                string.Format(
                    Resources.Validations.RequiredDefault,
                    Resources.Fields.PaymentCondition));

            RuleFor(x => x.DeliveryDate)
                .NotNull()
                .WithMessage(
                string.Format(
                    Resources.Validations.RequiredDefault,
                    Resources.Fields.DeliveryDate));

            RuleFor(x => x.DeliveryDate)
                .Must(r =>
                {
                    if (DateTime.TryParse(r.ToString(), out DateTime o))
                        return o.Date >= DateTime.Now.Date;
                    return false;
                })
                .WithMessage(Resources.Validations.InvalidDeliveryDate);
        }
    }
}
