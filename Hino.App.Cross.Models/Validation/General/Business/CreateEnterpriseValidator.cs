﻿using FluentValidation;
using Hino.App.Cross.Models.General.Business;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Hino.App.Cross.Models.Validation.General.Business
{
    public class CreateEnterpriseValidator : AbstractValidator<GEEnterprisesModel>
    {
        public CreateEnterpriseValidator()
        {
            RuleFor(x => x.RazaoSocial)
                .NotEmpty()
                //.NotNull()
                .WithMessage(Resources.Validations.RequiredDefault);

            RuleFor(x => x.NomeFantasia)
                .NotEmpty()
                .NotNull()
                .WithMessage(Resources.Validations.RequiredDefault);

            RuleFor(x => x.CNPJCPF)
                .Must((cnpjcpf) =>
                {
                    if (string.IsNullOrEmpty(cnpjcpf) ||
                        string.IsNullOrWhiteSpace(cnpjcpf))
                        return true;

                    if (Validators.IsCnpj(cnpjcpf))
                        return true;

                    if (Validators.IsCPF(cnpjcpf))
                        return true;

                    return false;
                })
                .WithMessage(Resources.Validations.InvalidCNPJCPF);

            RuleFor(x => x.CNPJCPF)
                .NotEmpty()
                .NotNull()
                .WithMessage(Resources.Validations.RequiredDefault);

            RuleFor(x => x.Type)
                .NotNull()
                .WithMessage(Resources.Validations.RequiredDefault);
            
            /*RuleFor(x => x.Address)
                .NotEmpty()
                .NotNull()
                .WithMessage(Resources.Validations.RequiredDefault);

            RuleFor(x => x.UF)
                 .Must((uf) =>
                 {
                     return Validators.UFValidator(uf);
                 })
                .WithMessage(Resources.Validations.InvalidUF);

            RuleFor(x => x.CellPhone)
                 .Must((cellphone) =>
                 {
                     if (string.IsNullOrEmpty(cellphone) ||
                         string.IsNullOrWhiteSpace(cellphone))
                         return true;

                     if (Validators.PhoneValidator(cellphone))
                         return true;

                     return false;
                 })
                .WithMessage(Resources.Validations.CellphoneInvalid);

            RuleFor(x => x.Phone)
                 .Must((phone) =>
                 {
                     if (string.IsNullOrEmpty(phone) ||
                         string.IsNullOrWhiteSpace(phone))
                         return true;

                     if (Validators.PhoneValidator(phone))
                         return true;

                     return false;
                 })
                .WithMessage(Resources.Validations.PhoneInvalid);


            RuleFor(x => x.ZipCode)
                 .Must((zipcode) =>
                 {
                     if (string.IsNullOrEmpty(zipcode) ||
                         string.IsNullOrWhiteSpace(zipcode))
                         return true;

                     if (Validators.ZipCodeValidator(zipcode))
                         return true;

                     return false;
                 })
                .WithMessage(Resources.Validations.InvalidZipCode);

            RuleFor(x => x.Email)
                 .Must((email) =>
                 {
                     if (string.IsNullOrEmpty(email) ||
                         string.IsNullOrWhiteSpace(email))
                         return true;

                     if (Validators.EmailValidator(email))
                         return true;

                     return false;
                 })
                .WithMessage(Resources.Validations.InvalidZipCode);*/
        }
    }
}
