﻿using FluentValidation;
using Hino.App.Cross.Models.Auth;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.App.Cross.Models.Validation.Auth
{
    public class RegisterValidator: AbstractValidator<RegisterModel>
    {
        public RegisterValidator()
        {
            RuleFor(x => x.EstablishmentKey)
                .NotEmpty()
                .NotNull()
                .WithMessage(Resources.Validations.RequiredDefault);
            RuleFor(x => x.UserName)
                .NotEmpty()
                .NotNull()
                .WithMessage(Resources.Validations.RequiredUsername);
            RuleFor(x => x.UserKey)
                .NotEmpty()
                .NotNull()
                .WithMessage(Resources.Validations.RequiredDefault);
            RuleFor(x => x.Email)
                .NotEmpty()
                .NotNull()
                .WithMessage(Resources.Validations.RequiredEmail);
            RuleFor(x => x.Email)
                .NotEmpty()
                .NotNull()
                .EmailAddress()
                .WithMessage(Resources.Validations.InvalidEmail);
            RuleFor(x => x.Password)
                .NotEmpty()
                .NotNull()
                .WithMessage(Resources.Validations.RequiredPassword);
            RuleFor(x => x.PasswordConfirm)
                .NotEmpty()
                .NotNull()
                .WithMessage(Resources.Validations.RequiredPassword);
            RuleFor(x => x.PasswordConfirm)
                .Equal(x => x.Password)
                .WithMessage(Resources.Messages.PasswordNotEqual);
        }

    }
}
