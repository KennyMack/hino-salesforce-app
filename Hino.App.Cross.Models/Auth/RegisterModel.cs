﻿using Hino.App.Cross.Utils.Attributes;

namespace Hino.App.Cross.Models
{
    [EndPoint("Auth/Mordor/login")]
    public class RegisterModel : BaseEntity
    {
        [DisplayField]
        public new string UserKey { get; set; }
        [DisplayField]
        public string UserName { get; set; }
        [DisplayField]
        public string Email { get; set; }
        [DisplayField]
        public string Password { get; set; }
        [DisplayField]
        public string PasswordConfirm { get; set; }
        [DisplayField]
        public string OriginCreate { get; set; }
    }

}
