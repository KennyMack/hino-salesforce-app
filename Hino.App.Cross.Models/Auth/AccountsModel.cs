﻿using Hino.App.Cross.Models.General;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.App.Cross.Models.Auth
{
    public class AccountsModel
    {
        public string Initials 
        {
            get => Email?.Substring(0, 2)?.ToUpper();
        }
        public string EstablishmentKey { get; set; }
        public string RazaoSocial { get; set; }
        public string NomeFantasia { get; set; }
        public string UserKey { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string UserToken { get; set; }
        public string RefreshToken { get; set; }
        public string SessionId { get; set; }
        public UserLoggedModel User { get; set; }
        public GEEstablishmentsModel Estab { get; set; }
    }
}
