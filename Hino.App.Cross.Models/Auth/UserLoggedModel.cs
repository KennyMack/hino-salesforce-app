﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.App.Cross.Models.Auth
{
    public class UserLoggedModel: BaseEntity
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public DateTime LastLogin { get; set; }
        public int UserType { get; set; }
        public decimal PercDiscount { get; set; }
        public decimal PercCommission { get; set; }
        public new string UserKey { get; set; }
        public string UserToken { get; set; }
        public string RefreshToken { get; set; }
        public string SessionId { get; set; }
    }
}
