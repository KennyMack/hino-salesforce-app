﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.App.Cross.Models.Auth
{
    public class TokenData
    {
        public string UserKey { get; set; }
        public string UserToken { get; set; }
        public string RefreshToken { get; set; }
        public string EstablishmentKey { get; set; }
        public string SessionId { get; set; }
    }
}
