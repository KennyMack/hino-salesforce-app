﻿using Hino.App.Cross.Utils.Attributes;

namespace Hino.App.Cross.Models.Auth
{
    [EndPoint("Auth/Mordor/login")]
    public class LoginModel : BaseEntity
    {
        [DisplayField]
        public string UserOrEmail { get; set; }
        [DisplayField]
        public string Password { get; set; }
    }
}
