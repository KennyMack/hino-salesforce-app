﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.App.Cross.Models.Interfaces
{
    public interface IBaseEntity
    {
        long Id { get; set; }
        long? IdApi { get; set; }
        string EstablishmentKey { get; set; }
        string UniqueKey { get; set; }
        string UserKey { get; set; }
        DateTime Created { get; set; }
        DateTime Modified { get; set; }
        bool IsActive { get; set; }
    }
}
