﻿using Hino.App.Cross.Models.Interfaces;
using Hino.App.Cross.Utils.Attributes;
using LiteDB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Hino.App.Cross.Models
{
    public class BaseEntity : IBaseEntity, INotifyPropertyChanged
    {
        public BaseEntity()
        {

        }

        [BsonId]
        [DisplayField]
        public long Id { get; set; }
        [DisplayField]
        public long? IdApi { get; set; }
        [DisplayField]
        public string EstablishmentKey { get; set; }
        [DisplayField]
        public string UniqueKey { get; set; }
        [DisplayField]
        public string UserKey { get; set; }
        [DisplayField]
        public DateTime Created { get; set; }
        [DisplayField]
        public DateTime Modified { get; set; }
        [DisplayField]
        public bool IsActive { get; set; }

        #region INotifyPropertyChanged
        protected bool SetProperty<T>(ref T backingStore, T value,
            [CallerMemberName]string propertyName = "",
            Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(backingStore, value))
                return false;

            backingStore = value;
            onChanged?.Invoke();
            OnPropertyChanged(propertyName);
            return true;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var changed = PropertyChanged;
            if (changed == null)
                return;

            changed.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
