﻿using Hino.App.Cross.Models.Sales;
using Hino.App.Cross.Utils.Attributes;
using Hino.App.Cross.Utils.Extensions;
using LiteDB;
// using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace Hino.App.Cross.Models.General
{
    [EndPoint("General/Products/{pEstablishmentKey}")]
    public class GEProductsModel : BaseEntity
    {
        public GEProductsModel()
        {
            this.VEOrderItems = new HashSet<VEOrderItemsModel>();
            this.VESalePrice = new HashSet<VESalePriceModel>();
        }

        string _ProductKey;
        public string ProductKey { get => _ProductKey; set => SetProperty(ref _ProductKey, value); }
        string _Image;
        public string Image { get => _Image; set => SetProperty(ref _Image, value); }
        string _Name;
        public string Name { get => _Name; set => SetProperty(ref _Name, value); }
        string _Description;
        public string Description { get => _Description; set => SetProperty(ref _Description, value); }
        string _Type;
        public string Type { get => _Type; set => SetProperty(ref _Type, value); }
        string _Family;
        public string Family { get => _Family; set => SetProperty(ref _Family, value); }
        decimal _PercIPI;
        public decimal PercIPI { get => _PercIPI; set => SetProperty(ref _PercIPI, value); }
        decimal _PercMaxDiscount;
        public decimal PercMaxDiscount { get => _PercMaxDiscount; set => SetProperty(ref _PercMaxDiscount, value); }
        decimal _Value;
        public decimal Value { get => _Value; set => SetProperty(ref _Value, value); }
        decimal _StockBalance;
        public decimal StockBalance { get => _StockBalance; set => SetProperty(ref _StockBalance, value); }
        int _Status;
        public int Status { get => _Status; set => SetProperty(ref _Status, value); }
        string _NCM;
        public string NCM { get => _NCM; set => SetProperty(ref _NCM, value); }
        string _Unit;
        public string Unit { get => _Unit; set => SetProperty(ref _Unit, value); }
        decimal _Weight;
        public decimal Weight { get => _Weight; set => SetProperty(ref _Weight, value); }
        decimal _PackageQTD;
        public decimal PackageQTD { get => _PackageQTD; set => SetProperty(ref _PackageQTD, value); }

        public string DisplayName
        {
            get
            {
                return $"{ProductKey} - {Name}".SubStrTitle();
            }
        }

        [BsonIgnore]
        public virtual ICollection<VEOrderItemsModel> VEOrderItems { get; set; }
        [BsonIgnore]
        public virtual ICollection<VESalePriceModel> VESalePrice { get; set; }

        string _quantitySelected = "0";
        [BsonIgnore]
        public string QuantitySelected
        {
            get { return _quantitySelected; }
            set { SetProperty(ref _quantitySelected, value); }
        }

        string _quantityConverted = "0";
        [BsonIgnore]
        public string QuantityConverted
        {
            get { return _quantityConverted; }
            set { SetProperty(ref _quantityConverted, value); }
        }
    }
}
