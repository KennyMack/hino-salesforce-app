﻿using Hino.App.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.App.Cross.Models.General
{
    [EndPoint("General/Establishments/{pEstablishmentKey}")]
    public class GEEstablishmentsModel : BaseEntity
    {
        public GEEstablishmentsModel()
        {

        }

        public string RazaoSocial { get; set; }
        public string NomeFantasia { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string CNPJCPF { get; set; }
        public int Devices { get; set; }
        public decimal PIS { get; set; }
        public decimal COFINS { get; set; }
        public bool AllowEnterprise { get; set; }
        public bool AllowPayment { get; set; }
        public bool AllowChangePrice { get; set; }

    }
}
