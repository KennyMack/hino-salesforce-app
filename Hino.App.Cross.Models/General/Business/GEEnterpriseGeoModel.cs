﻿using Hino.App.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.App.Cross.Models.General.Business
{
    public class GEEnterpriseGeoModel : BaseEntity
    {
        // [ForeignKey("GEEnterprises")]
        [DisplayField]
        [RequiredField]
        public long EnterpriseId { get; set; }
        [DisplayField]
        [RequiredField]
        public int Type { get; set; }
        [DisplayField]
        [RequiredField]
        public string CNPJCPF { get; set; }
        [DisplayField]
        [RequiredField]
        public string IE { get; set; }
        [DisplayField]
        [RequiredField]
        public string RG { get; set; }
        [DisplayField]
        public string CellPhone { get; set; }
        [DisplayField]
        public string Phone { get; set; }
        [DisplayField]
        public string Email { get; set; }
        [DisplayField]
        public string Address { get; set; }
        [DisplayField]
        public string Complement { get; set; }
        [DisplayField]
        [RequiredField]
        public string District { get; set; }
        [DisplayField]
        [RequiredField]
        public string Num { get; set; }
        [DisplayField]
        [RequiredField]
        public string ZipCode { get; set; }

        [DisplayField]
        public string Site { get; set; }

        [DisplayField]
        [RequiredField]
        public string CountryIni { get; set; }
        [DisplayField]
        [RequiredField]
        public string CountryCode { get; set; }
        [DisplayField]
        [RequiredField]
        public string CountryName { get; set; }
        [DisplayField]
        [RequiredField]
        public string UF { get; set; }
        [DisplayField]
        [RequiredField]
        public string StateName { get; set; }
        [DisplayField]
        [RequiredField]
        public string IBGE { get; set; }
        [DisplayField]
        [RequiredField]
        public string CityName { get; set; }
        public decimal DisplayLat { get; set; }
        public decimal DisplayLng { get; set; }
        public decimal NavLat { get; set; }
        public decimal NavLng { get; set; }
        public bool DisplayPhone { get => !string.IsNullOrEmpty(Phone) || !string.IsNullOrEmpty(CellPhone); }
    }
}
