﻿using Hino.App.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.App.Cross.Models.General.Business
{
    [EndPoint("General/Business/Enterprises/Fiscal/Group/{pEstablishmentKey}")]
    public class GEEnterpriseFiscalGroupModel : BaseEntity
    {
        public string Description { get; set; }
        public string Type { get; set; }
    }
}
