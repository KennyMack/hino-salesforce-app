﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.App.Cross.Models.General.Business
{
    public class EnterpriseType
    {
        public EnterpriseType()
        {

        }

        public EnterpriseType(string pName, string pIndex)
        {
            Index = pIndex;
            Name = pName;
        }

        public string Index { get; set; }
        public string Name { get; set; }
    }
}
