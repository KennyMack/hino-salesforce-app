﻿using Hino.App.Cross.Models.General.Demograph;
using Hino.App.Cross.Models.Sales;
using Hino.App.Cross.Utils.Attributes;
using Hino.App.Cross.Utils.Extensions;
using LiteDB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Hino.App.Cross.Models.General.Business
{
    [EndPoint("General/Business/Enterprises/{pEstablishmentKey}")]
    public class GEEnterprisesModel : BaseEntity
    {
        public GEEnterprisesModel()
        {
            GEEnterpriseGeo = new List<GEEnterpriseGeoModel>();
            GEEnterpriseContacts = new HashSet<GEEnterpriseContactsModel>();
        }

        [DisplayField]
        [RequiredField]
        public string RazaoSocial { get; set; }
        [DisplayField]
        [RequiredField]
        public string NomeFantasia { get; set; }
        [DisplayField]
        [RequiredField]
        public string CNPJCPF { get; set; }
        [DisplayField]
        [RequiredField]
        public string IE { get; set; }
        [DisplayField]
        [RequiredField]
        public int Type { get; set; }
        public string TypeDescription
        {
            get => Type == 0 ? "Pessoa Física" : "Pessoa Jurídica";
        }
        [DisplayField]
        [RequiredField]
        public int Status { get; set; }
        public string StatusDesc
        {
            get
            {
                switch (Status)
                {
                    case 1:
                        return "Ativo";
                    case 2:
                        return "Inativo";
                    case 3:
                        return "Bloqueado";
                    default:
                        return "Novo";
                }
            }
        }
        
        /// <summary>
        /// 0 - Aguardando sincronizacao
        /// 1 - Sincronizado
        /// 2 - Integrado
        /// </summary>
        [DisplayField]
        [RequiredField]
        public int StatusSinc { get; set; }
        /// <summary>
        /// 0 - Aguardando sincronizacao
        /// 1 - Sincronizado
        /// 2 - Integrado
        /// </summary>
        public string StatusSincDesc
        {
            get
            {
                switch (StatusSinc)
                {
                    case 1:
                        return "Sincronizado";
                    case 2:
                        return "Integrado";
                    default:
                        return "Aguardando sincronização";
                }
            }
        }
        /// <summary>
        /// 0 - Aguardando sincronizacao
        /// 1 - Sincronizado
        /// 2 - Integrado
        /// </summary>
        public string StatusSincColor
        {
            get
            {
                switch (StatusSinc)
                {
                    case 1:
                        return "#05a05a";
                    case 2:
                        return "#89d2ff";
                    default:
                        return "#ffd700";
                }
            }
        }

        public int? Classification { get; set; }
        public long UserId { get; set; }

        // [ForeignKey("GEEnterpriseFiscalGroup")]

        [DisplayField]
        public long? FiscalGroupId { get; set; }
        [BsonIgnore]
        public virtual GEEnterpriseFiscalGroupModel GEEnterpriseFiscalGroup { get; set; }

        [DisplayField]
        // [ForeignKey(typeof(GEPaymentConditionModel))]
        // [OneToOne]
        public long? PayConditionId { get; set; }
        [BsonIgnore]
        public virtual GEPaymentConditionModel GEPaymentCondition { get; set; }

        // [ForeignKey("GEEnterpriseCategory")]
        public long? CategoryId { get; set; }
        [BsonIgnore]
        public virtual GEEnterpriseCategoryModel GEEnterpriseCategory { get; set; }

        public string Initials { get; set; }

        public string CNPJCPFType
        {
            get => $"{CNPJCPF} / {TypeDescription}";

        }

        public string DisplayName
        {
            get
            {
                if (Id < 0)
                    return $"(NOVO) - {RazaoSocial}";

                return $"{Id} - {RazaoSocial}";
            }
        }

        public bool DisplayPhone
        {
            get => CommercialAddress?.DisplayPhone ?? false;
        }

        [BsonIgnore]
        public string Phone => CommercialAddress?.Phone;
        [BsonIgnore]
        public string CellPhone => CommercialAddress?.CellPhone;
        [BsonIgnore]
        public string Email => CommercialAddress?.CellPhone;

        public long IdERP { get; set; }
        public long RegionId { get; set; }

        #region Commercial Address
        [DisplayField]
        [RequiredField]
        public string CommercialAddress_CNPJCPF { get; set; }
        [DisplayField]
        [RequiredField]
        [DefaultValue("N/INFO")]
        public string CommercialAddress_IE { get; set; }
        [DisplayField]
        public string CommercialAddress_RG { get; set; }
        [DisplayField]
        public string CommercialAddress_CellPhone { get; set; }
        [DisplayField]
        public string CommercialAddress_Phone { get; set; }
        [DisplayField]
        public string CommercialAddress_Email { get; set; }
        [DisplayField]
        public string CommercialAddress_Address { get; set; }
        [DisplayField]
        public string CommercialAddress_Complement { get; set; }
        [DisplayField]
        [RequiredField]
        public string CommercialAddress_District { get; set; }
        [DisplayField]
        [RequiredField]
        public string CommercialAddress_Num { get; set; }
        [DisplayField]
        [RequiredField]
        public string CommercialAddress_ZipCode { get; set; }
        [DisplayField]
        public string CommercialAddress_Site { get; set; }
        [DisplayField]
        [RequiredField]
        public string CommercialAddress_CountryIni { get; set; }
        [DisplayField]
        [RequiredField]
        public string CommercialAddress_CountryCode { get; set; }
        [DisplayField]
        [RequiredField]
        public string CommercialAddress_CountryName { get; set; }
        [DisplayField]
        [RequiredField]
        public string CommercialAddress_UF { get; set; }
        [DisplayField]
        [RequiredField]
        public string CommercialAddress_StateName { get; set; }
        [DisplayField]
        [RequiredField]
        public string CommercialAddress_IBGE { get; set; }
        [DisplayField]
        [RequiredField]
        public string CommercialAddress_CityName { get; set; }
        public decimal CommercialAddress_DisplayLat { get; set; }
        public decimal CommercialAddress_DisplayLng { get; set; }
        public decimal CommercialAddress_NavLat { get; set; }
        public decimal CommercialAddress_NavLng { get; set; }
        public bool CommercialAddress_DisplayPhone { get => !string.IsNullOrEmpty(Phone) || !string.IsNullOrEmpty(CellPhone); }
        #endregion

        #region Delivery Address
        [DisplayField]
        public string DeliveryAddress_CNPJCPF { get; set; }
        [DisplayField]
        [DefaultValue("N/INFO")]
        public string DeliveryAddress_IE { get; set; }
        [DisplayField]
        public string DeliveryAddress_RG { get; set; }
        [DisplayField]
        public string DeliveryAddress_CellPhone { get; set; }
        [DisplayField]
        public string DeliveryAddress_Phone { get; set; }
        [DisplayField]
        public string DeliveryAddress_Email { get; set; }
        [DisplayField]
        public string DeliveryAddress_Address { get; set; }
        [DisplayField]
        public string DeliveryAddress_Complement { get; set; }
        [DisplayField]
        public string DeliveryAddress_District { get; set; }
        [DisplayField]
        public string DeliveryAddress_Num { get; set; }
        [DisplayField]
        public string DeliveryAddress_ZipCode { get; set; }
        [DisplayField]
        public string DeliveryAddress_Site { get; set; }
        [DisplayField]
        public string DeliveryAddress_CountryIni { get; set; }
        [DisplayField]
        public string DeliveryAddress_CountryCode { get; set; }
        [DisplayField]
        public string DeliveryAddress_CountryName { get; set; }
        [DisplayField]
        public string DeliveryAddress_UF { get; set; }
        [DisplayField]
        public string DeliveryAddress_StateName { get; set; }
        [DisplayField]
        public string DeliveryAddress_IBGE { get; set; }
        [DisplayField]
        public string DeliveryAddress_CityName { get; set; }
        public decimal DeliveryAddress_DisplayLat { get; set; }
        public decimal DeliveryAddress_DisplayLng { get; set; }
        public decimal DeliveryAddress_NavLat { get; set; }
        public decimal DeliveryAddress_NavLng { get; set; }
        public bool DeliveryAddress_DisplayPhone { get => !string.IsNullOrEmpty(Phone) || !string.IsNullOrEmpty(CellPhone); }
        #endregion

        #region Levy Address
        [DisplayField]
        public string LevyAddress_CNPJCPF { get; set; }
        [DisplayField]
        [DefaultValue("N/INFO")]
        public string LevyAddress_IE { get; set; }
        [DisplayField]
        public string LevyAddress_RG { get; set; }
        [DisplayField]
        public string LevyAddress_CellPhone { get; set; }
        [DisplayField]
        public string LevyAddress_Phone { get; set; }
        [DisplayField]
        public string LevyAddress_Email { get; set; }
        [DisplayField]
        public string LevyAddress_Address { get; set; }
        [DisplayField]
        public string LevyAddress_Complement { get; set; }
        [DisplayField]
        public string LevyAddress_District { get; set; }
        [DisplayField]
        public string LevyAddress_Num { get; set; }
        [DisplayField]
        public string LevyAddress_ZipCode { get; set; }
        [DisplayField]
        public string LevyAddress_Site { get; set; }
        [DisplayField]
        public string LevyAddress_CountryIni { get; set; }
        [DisplayField]
        public string LevyAddress_CountryCode { get; set; }
        [DisplayField]
        public string LevyAddress_CountryName { get; set; }
        [DisplayField]
        public string LevyAddress_UF { get; set; }
        [DisplayField]
        public string LevyAddress_StateName { get; set; }
        [DisplayField]
        public string LevyAddress_IBGE { get; set; }
        [DisplayField]
        public string LevyAddress_CityName { get; set; }
        public decimal LevyAddress_DisplayLat { get; set; }
        public decimal LevyAddress_DisplayLng { get; set; }
        public decimal LevyAddress_NavLat { get; set; }
        public decimal LevyAddress_NavLng { get; set; }
        public bool LevyAddress_DisplayPhone { get => !string.IsNullOrEmpty(Phone) || !string.IsNullOrEmpty(CellPhone); }
        #endregion

        [BsonIgnore]
        public List<GEEnterpriseGeoModel> GEEnterpriseGeo { get; set; }
        [BsonIgnore]
        public virtual ICollection<GEEnterpriseContactsModel> GEEnterpriseContacts { get; set; }
        [BsonIgnore]
        public GEEnterpriseGeoModel CommercialAddress
        {
            get =>
                new GEEnterpriseGeoModel
                {
                    Type = 0,
                    CNPJCPF = this.CommercialAddress_CNPJCPF,
                    IE = this.CommercialAddress_IE,
                    RG = this.CommercialAddress_RG,
                    CellPhone = this.CommercialAddress_CellPhone,
                    Phone = this.CommercialAddress_Phone,
                    Email = this.CommercialAddress_Email,
                    Address = this.CommercialAddress_Address,
                    Complement = this.CommercialAddress_Complement,
                    District = this.CommercialAddress_District,
                    Num = this.CommercialAddress_Num,
                    ZipCode = this.CommercialAddress_ZipCode,
                    Site = this.CommercialAddress_Site,
                    CountryIni = this.CommercialAddress_CountryIni,
                    CountryCode = this.CommercialAddress_CountryCode,
                    CountryName = this.CommercialAddress_CountryName,
                    UF = this.CommercialAddress_UF,
                    StateName = this.CommercialAddress_StateName,
                    IBGE = this.CommercialAddress_IBGE,
                    CityName = this.CommercialAddress_CityName,
                    DisplayLat = this.CommercialAddress_DisplayLat,
                    DisplayLng = this.CommercialAddress_DisplayLng,
                    NavLat = this.CommercialAddress_NavLat,
                    NavLng = this.CommercialAddress_NavLng
                };
        }
        [BsonIgnore]
        public GEEnterpriseGeoModel DeliveryAddress
        {
            get =>
                new GEEnterpriseGeoModel
                {
                    Type = 1,
                    CNPJCPF = this.DeliveryAddress_CNPJCPF,
                    IE = this.DeliveryAddress_IE,
                    RG = this.DeliveryAddress_RG,
                    CellPhone = this.DeliveryAddress_CellPhone,
                    Phone = this.DeliveryAddress_Phone,
                    Email = this.DeliveryAddress_Email,
                    Address = this.DeliveryAddress_Address,
                    Complement = this.DeliveryAddress_Complement,
                    District = this.DeliveryAddress_District,
                    Num = this.DeliveryAddress_Num,
                    ZipCode = this.DeliveryAddress_ZipCode,
                    Site = this.DeliveryAddress_Site,
                    CountryIni = this.DeliveryAddress_CountryIni,
                    CountryCode = this.DeliveryAddress_CountryCode,
                    CountryName = this.DeliveryAddress_CountryName,
                    UF = this.DeliveryAddress_UF,
                    StateName = this.DeliveryAddress_StateName,
                    IBGE = this.DeliveryAddress_IBGE,
                    CityName = this.DeliveryAddress_CityName,
                    DisplayLat = this.DeliveryAddress_DisplayLat,
                    DisplayLng = this.DeliveryAddress_DisplayLng,
                    NavLat = this.DeliveryAddress_NavLat,
                    NavLng = this.DeliveryAddress_NavLng
                };
        }
        [BsonIgnore]
        public GEEnterpriseGeoModel LevyAddress
        {
            get =>
                new GEEnterpriseGeoModel
                {
                    Type = 2,
                    CNPJCPF = this.LevyAddress_CNPJCPF,
                    IE = this.LevyAddress_IE,
                    RG = this.LevyAddress_RG,
                    CellPhone = this.LevyAddress_CellPhone,
                    Phone = this.LevyAddress_Phone,
                    Email = this.LevyAddress_Email,
                    Address = this.LevyAddress_Address,
                    Complement = this.LevyAddress_Complement,
                    District = this.LevyAddress_District,
                    Num = this.LevyAddress_Num,
                    ZipCode = this.LevyAddress_ZipCode,
                    Site = this.LevyAddress_Site,
                    CountryIni = this.LevyAddress_CountryIni,
                    CountryCode = this.LevyAddress_CountryCode,
                    CountryName = this.LevyAddress_CountryName,
                    UF = this.LevyAddress_UF,
                    StateName = this.LevyAddress_StateName,
                    IBGE = this.LevyAddress_IBGE,
                    CityName = this.LevyAddress_CityName,
                    DisplayLat = this.LevyAddress_DisplayLat,
                    DisplayLng = this.LevyAddress_DisplayLng,
                    NavLat = this.LevyAddress_NavLat,
                    NavLng = this.LevyAddress_NavLng
                };
        }

        [BsonIgnore]
        public ICollection<GEEnterpriseContactsModel> Phones { get => this.GEEnterpriseContacts?.Where(r => !string.IsNullOrEmpty(r.Phone)).ToList(); }
        [BsonIgnore]
        public ICollection<GEEnterpriseContactsModel> Emails { get => this.GEEnterpriseContacts?.Where(r => !string.IsNullOrEmpty(r.Email)).ToList(); }

        public void LoadAddressList()
        {
            GEEnterpriseGeo.Clear();

            if (!CommercialAddress_Address.IsEmpty())
                GEEnterpriseGeo.Add(CommercialAddress);
            if (!DeliveryAddress_Address.IsEmpty())
                GEEnterpriseGeo.Add(DeliveryAddress);
            if (!LevyAddress_Address.IsEmpty())
                GEEnterpriseGeo.Add(LevyAddress);
        }
    }
}

