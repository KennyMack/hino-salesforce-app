﻿using Hino.App.Cross.Models.Sales;
using Hino.App.Cross.Utils.Attributes;
using LiteDB;
// using SQLite;
// using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.App.Cross.Models.General.Business
{
    [EndPoint("General/Business/Payment/Condition/{pEstablishmentKey}")]
    public class GEPaymentConditionModel : BaseEntity
    {
        public GEPaymentConditionModel()
        {
            //this.VEOrders = new HashSet<VEOrdersModel>();
        }

        // [ForeignKey(typeof(GEPaymentTypeModel))]
        
        public long TypePayID { get; set; }

        public long GEPaymentConditionModelId { get => TypePayID; set => TypePayID = value; }
        // [Ignore]
        // [OneToOne]
        //[BsonIgnore]
       // [BsonRef("GEPaymentTypeModel")]
        public GEPaymentTypeModel GEPaymentType { get; set; }

        public string Description { get; set; }

        [BsonIgnore]
        public string DetailDescription
        {
            get => $"{GEPaymentType?.Description}/{Description}";
        }
        //// [Ignore]
        //public virtual ICollection<VEOrdersModel> VEOrders { get; set; }
    }
}
