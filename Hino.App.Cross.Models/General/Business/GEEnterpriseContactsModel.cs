﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.App.Cross.Models.General.Business
{
    public class GEEnterpriseContactsModel : BaseEntity
    {
        public long EnterpriseId { get; set; }
        public int ReceptivityIndex { get; set; }
        public int Sector { get; set; }
        public string Email { get; set; }
        public string Contact { get; set; }
        public string Ramal { get; set; }
        public string Phone { get; set; }
        public string Note { get; set; }
    }
}
