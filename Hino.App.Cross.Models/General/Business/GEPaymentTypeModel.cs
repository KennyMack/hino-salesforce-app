﻿using Hino.App.Cross.Models.Sales;
using Hino.App.Cross.Utils.Attributes;
using LiteDB;
// using SQLite;
// using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.App.Cross.Models.General.Business
{
    [EndPoint("General/Business/Payment/Type/{pEstablishmentKey}")]
    public class GEPaymentTypeModel : BaseEntity
    {
        public GEPaymentTypeModel()
        {
            this.GEPaymentCondition = new List<GEPaymentConditionModel>();
            this.VEOrders = new List<VEOrdersModel>();
        }

        public string Description { get; set; }
        /*// [Ignore]
        [OneToMany]*/
        //[BsonRef("GEPaymentTypeModel")]
        public virtual List<GEPaymentConditionModel> GEPaymentCondition { get; set; }
        [BsonIgnore]
        public virtual List<VEOrdersModel> VEOrders { get; set; }
    }
}
