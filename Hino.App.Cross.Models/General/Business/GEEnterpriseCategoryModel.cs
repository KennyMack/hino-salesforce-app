﻿using Hino.App.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.App.Cross.Models.General.Business
{
    [EndPoint("General/Business/Enterprises/Category/{pEstablishmentKey}")]
    public class GEEnterpriseCategoryModel : BaseEntity
    {
        public string Description { get; set; }
        public string Identifier { get; set; }
    }
}
