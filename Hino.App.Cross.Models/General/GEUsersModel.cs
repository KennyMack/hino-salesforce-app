﻿using System;
using System.Collections.Generic;
using System.Text;
using Hino.App.Cross.Utils.Attributes;

namespace Hino.App.Cross.Models.General
{
    [EndPoint("General/Users/{pEstablishmentKey}")]
    public class GEUsersModel: BaseEntity
    {
        public GEUsersModel()
        {
            //this.VEOrders = new HashSet<VEOrders>();
        }
        [RequiredField]
        [DisplayField]
        public string UserName { get; set; }
        [RequiredField]
        [DisplayField]
        public string Email { get; set; }
        [RequiredField]
        [DisplayField]
        public string Password { get; set; }
        [RequiredField]
        [DisplayField]
        public DateTime LastLogin { get; set; }
        [RequiredField]
        [DisplayField]
        [IntegerRangeField(0, 5)]
        public int UserType { get; set; }
        [RequiredField]
        [DisplayField]
        [DecimalRangeField(0, 99.99)]
        public decimal PercDiscount { get; set; }
        [RequiredField]
        [DisplayField]
        [DecimalRangeField(0, 99.99)]
        public decimal PercCommission { get; set; }
        [RequiredField]
        [DisplayField]
        [Max36LengthField]
        [Min36LengthField]
        public new string UserKey { get; set; }

        //public virtual ICollection<VEOrders> VEOrders { get; set; }
    }
}
