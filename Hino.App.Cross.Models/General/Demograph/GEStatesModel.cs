﻿using Hino.App.Cross.Models.General.Business;
using Hino.App.Cross.Utils.Attributes;
using LiteDB;
// using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.App.Cross.Models.General.Demograph
{
    [EndPoint("General/Demograph/States/{pEstablishmentKey}")]
    public class GEStatesModel: BaseEntity
    {
        public GEStatesModel()
        {
            this.GECities = new HashSet<GECitiesModel>();
            this.GEEnterprises = new HashSet<GEEnterprisesModel>();

        }

        public string Name { get; set; }
        public string Initials { get; set; }
        public string IBGE { get; set; }

        public string Description
        {
            get
            {
                if (string.IsNullOrEmpty(Initials))
                    return $"{Name} ({Initials})";

                return Name;
            }
        }

        public long CountryID { get; set; }

        [BsonIgnore]
        public virtual GECountriesModel GECountries { get; set; }

        [BsonIgnore]
        public virtual ICollection<GECitiesModel> GECities { get; set; }

        [BsonIgnore]
        public virtual ICollection<GEEnterprisesModel> GEEnterprises { get; set; }
    }
}
