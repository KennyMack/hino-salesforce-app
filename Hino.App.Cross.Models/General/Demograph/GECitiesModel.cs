﻿using Hino.App.Cross.Models.General.Business;
using Hino.App.Cross.Utils.Attributes;
using LiteDB;
// using SQLite;
using System.Collections.Generic;

namespace Hino.App.Cross.Models.General.Demograph
{
    [EndPoint("General/Demograph/Cities/{pEstablishmentKey}")]
    public class GECitiesModel : BaseEntity
    {
        public GECitiesModel()
        {
            this.GEEnterprises = new HashSet<GEEnterprisesModel>();
        }

        public string Name { get; set; }
        public string IBGE { get; set; }
        public string DDD { get; set; }
        
        public long StateID { get; set; }
        [BsonIgnore]
        public virtual GEStatesModel GEStates { get; set; }
        [BsonIgnore]
        public virtual ICollection<GEEnterprisesModel> GEEnterprises { get; set; }
    }
}
