﻿using Hino.App.Cross.Models.General.Business;
using Hino.App.Cross.Utils.Attributes;
using LiteDB;
// using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.App.Cross.Models.General.Demograph
{
    [EndPoint("General/Demograph/Countries/{pEstablishmentKey}")]
    public class GECountriesModel: BaseEntity
    {
        public GECountriesModel()
        {
            this.GEEnterprises = new HashSet<GEEnterprisesModel>();
            this.GEStates = new HashSet<GEStatesModel>();
        }
        public string Initials { get; set; }
        public string Name { get; set; }
        public string BACEN { get; set; }
        public string DDI { get; set; }

        public string Description
        {
            get
            {
                if (string.IsNullOrEmpty(Initials))
                    return $"{Name} ({Initials})";

                return Name;
            }
        }

        [BsonIgnore]
        public virtual ICollection<GEEnterprisesModel> GEEnterprises { get; set; }

        [BsonIgnore]
        public virtual ICollection<GEStatesModel> GEStates { get; set; }
    }
}
