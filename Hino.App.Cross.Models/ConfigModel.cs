﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.App.Cross.Models
{
    public class ConfigModel : BaseEntity
    {
        public bool SyncWIFI { get; set; }
        public bool SyncOnStart { get; set; }
        public bool SyncOnSave { get; set; }
        public int IntervalSync { get; set; }
        public bool FirstAccess { get; set; }
        public bool FirstSyncOk { get; set; }
        public bool UseSecurity { get; set; }
        public DateTime LastSync { get; set; }
    }
}
