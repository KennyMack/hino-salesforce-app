﻿using Hino.App.Cross.Models.General;
using Hino.App.Cross.Models.General.Business;
using Hino.App.Cross.Utils.Attributes;
using System.Linq;
// using SQLite;
// using SQLiteNetExtensions.Attributes;
using System.Collections.Generic;
using LiteDB;

namespace Hino.App.Cross.Models.Sales
{
    [EndPoint("Sales/Orders/{pEstablishmentKey}")]
    public class VEOrdersModel: BaseEntity
    {
        public VEOrdersModel()
        {
            this.VEOrderItems = new List<VEOrderItemsModel>();
        }
        
        // [ForeignKey(typeof(GEEnterprisesModel))]
        public long EnterpriseID { get; set; }
        [BsonIgnore]
        // [OneToOne]
        public virtual GEEnterprisesModel GEEnterprises { get; set; }

        // [ForeignKey(typeof(GEUsersModel))]
        public long UserID { get; set; }
        [BsonIgnore]
        public virtual GEUsersModel GEUsers { get; set; }

        // [ForeignKey(typeof(GEPaymentTypeModel))]
        public long TypePaymentID { get; set; }
        [BsonIgnore]
        // [OneToOne]
        public virtual GEPaymentTypeModel GEPaymentType { get; set; }

        // [ForeignKey(typeof(GEPaymentConditionModel))]
        public long PayConditionID { get; set; }
        [BsonIgnore]
        // [OneToOne]
        public virtual GEPaymentConditionModel GEPaymentCondition { get; set; }

        public long CodPedVenda { get; set; }
        public long NumPedMob { get; set; }

        public string CodPedDesc
        {
            get
            {
                var desc = IsProposal ? "Orçamento " : "Pedido";

                if (CodPedVenda > 0)
                    return $"{desc} #{Id} / {CodPedVenda}";

                if (Id > 0)
                    return $"{desc} #{Id} / (NOVO)";

                return $"{desc} (NOVO) / (NOVO)";
            }
        }

        public System.DateTime DeliveryDate { get; set; }
        public string Note { get; set; }
        public string Status { get; set; }
        public string StatusDesc
        {
            get
            {
                if (CodPedVenda < 0)
                    return "NOVO";
                switch(Status)
                {
                    case "A":
                        return "APROVADO";
                    case "E":
                        return "ANÁLISE";
                    case "M":
                        return "ANDAMENTO";
                    case "R":
                        return "REPROVADO";
                    case "C":
                        return "CANCELADO";
                    case "O":
                        return "CONCLUÍDO";
                    case "G":
                        return "AGRUPADO";
                    default: // P
                        return "PENDENTE";
                }
            }
        }
        /// <summary>
        /// 0 - Aguardando sincronizacao
        /// 1 - Sincronizado
        /// 2 - Integrado
        /// </summary>
        public int StatusSinc { get; set; }

        /// <summary>
        /// 0 - Aguardando sincronizacao
        /// 1 - Sincronizado
        /// 2 - Integrado
        /// </summary>
        public string StatusSincDesc
        {
            get
            {
                switch (StatusSinc)
                {
                    case 1:
                        return "Sincronizado";
                    case 2:
                        return "Integrado";
                    default:
                        return "Aguardando sincronização";
                }
            }
        }

        public string StatusSincColor
        {
            get
            {
                switch (StatusSinc)
                {
                    case 1:
                        return "#05a05a";
                    case 2:
                        return "#89d2ff";
                    default:
                        return "#ffd700";
                }
            }
        }

        public bool IsProposal { get; set; }
        [BsonIgnore]
        // [OneToMany]
        public virtual List<VEOrderItemsModel> VEOrderItems { get; set; }

        public string ConditionDescription
        {
            get => $"{this.GEPaymentType?.Description} / {GEPaymentCondition?.Description}";
        }

        public decimal TotalValue
        {
            get =>
                VEOrderItems.Sum(r => r.Quantity * r.Value);
        }
    }
}
