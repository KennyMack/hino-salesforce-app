﻿using Hino.App.Cross.Utils.Attributes;
using LiteDB;
// using SQLite;
using System.Collections.Generic;

namespace Hino.App.Cross.Models.Sales
{
    [EndPoint("Sales/Region/{pEstablishmentKey}")]
    public class VERegionSaleModel : BaseEntity
    {
        public VERegionSaleModel()
        {
            this.VESalePrice = new HashSet<VESalePriceModel>();
        }

        [RequiredField]
        [DisplayField]
        public string Description { get; set; }
        
        [DisplayField]
        public long IdERP { get; set; }

        [BsonIgnore]
        public virtual ICollection<VESalePriceModel> VESalePrice { get; set; }
        
    }
}
