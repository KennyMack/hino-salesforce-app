﻿using Hino.App.Cross.Models.General;
using Hino.App.Cross.Utils.Attributes;
using LiteDB;

namespace Hino.App.Cross.Models.Sales
{
    [EndPoint("Sales/Prices/{pEstablishmentKey}")]
    public class VESalePriceModel : BaseEntity
    {
        public VESalePriceModel()
        {

        }

        [RequiredField]
        [DisplayField]
        public long CodPrVenda { get; set; }
        
        [RequiredField]
        [DisplayField]
        public long RegionId { get; set; }

        [BsonIgnore]
        public virtual VERegionSaleModel VERegionSale { get; set; }
        
        [RequiredField]
        [DisplayField]
        public long ProductId { get; set; }

        [BsonIgnore]
        public virtual GEProductsModel GEProducts { get; set; }

        [RequiredField]
        [DisplayField]
        public decimal Value { get; set; }

    }
}
