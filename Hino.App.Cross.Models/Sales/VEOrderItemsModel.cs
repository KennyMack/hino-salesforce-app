﻿using Hino.App.Cross.Models.General;
using Hino.App.Cross.Utils.Attributes;
using LiteDB;
// using SQLite;
// using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Hino.App.Cross.Models.Sales
{
    [EndPoint("Sales/Orders/Items/{pEstablishmentKey}")]
    public class VEOrderItemsModel: BaseEntity
    {
        // [ForeignKey(typeof(VEOrdersModel))]
        public long OrderID { get; set; }

        [BsonIgnore]
        public virtual VEOrdersModel VEOrders { get; set; }
        
        public long ProductID { get; set; }
        [BsonIgnore]
        public virtual GEProductsModel GEProducts { get; set; }

        public decimal TableValue { get; set; }

        [BsonIgnore]
        public decimal TotalValue
        {
            get => Value * Quantity;
        }

        public decimal PercDiscount { get; set; }
        public string Note { get; set; }

        private decimal _Value;
        public decimal Value
        {
            get => _Value;
            set
            {
                _Value = value;
                OnPropertyChanged(nameof(Value));
                OnPropertyChanged(nameof(TotalValue));
            }
        }

        private decimal _Quantity;
        public decimal Quantity
        {
            get => _Quantity;
            set
            {
                _Quantity = value;
                OnPropertyChanged(nameof(Quantity));
                OnPropertyChanged(nameof(TotalValue));
            }
        }


    }
}
