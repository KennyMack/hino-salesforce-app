﻿using Hino.App.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Hino.App.Cross.Models
{
    public class DefaultResultModel
    {
        public DefaultResultModel()
        {
            error = new List<ModelException>();
        }

        public int status { get; set; }
        public bool success { get; set; }
        public JContainer data { get; set; }
        public List<ModelException> error { get; set; }
    }
}
