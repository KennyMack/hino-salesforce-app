﻿using Hino.App.Core.Navigator.Utilities;
using Hino.App.Cross.Services.ViewModels;
using Hino.App.Cross.Utils.Extensions;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using XF.Material.Forms.UI;
using XF.Material.Forms.UI.Dialogs;

namespace Hino.App.Core.Navigator.NavPage
{
    public class BaseNavigationPage : XF.Navigation.NavBar.XFNavigationPage
    {
        private object _currentNavigationParameter;

        public BaseNavigationPage(string rootViewName, object parameter = null) : base(ViewFactory.GetView(rootViewName))
        {
            _currentNavigationParameter = parameter;
        }

        public async Task PopViewAsync()
        {
            await Navigation.PopAsync(true);
        }

        public async Task PushViewAsync(string rootViewName, object parameter = null)
        {
            _currentNavigationParameter = parameter;
            var view = ViewFactory.GetView(rootViewName);
            await base.PushAsync(view, true);
        }

        public async Task PushModalAsync(string rootViewName, object parameter = null)
        {
            _currentNavigationParameter = parameter;
            var view = ViewFactory.GetView(rootViewName);
            if (view is XF.Navigation.UI.Pages.ContentModalPage)
                await base.PushModalAsync((XF.Navigation.UI.Pages.ContentModalPage)view, true);
            else
                await Navigation.PushModalAsync(view, true);
        }

        public async Task PopModalAsync()
        {
            await base.PopModalAsync(true);
        }

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);

            if (propertyName == nameof(RootPage) && RootPage != null)
            {
                RootPage.Appearing += AppearingHandler;
            }
        }

        protected override void OnPagePush(Page page)
        {
            base.OnPagePush(page);

            if (!(page.BindingContext is BaseViewModel viewModel))
            {
                return;
            }

            viewModel?.OnViewPushed(_currentNavigationParameter);
            _currentNavigationParameter = null;

        }

        protected override void OnPagePop(Page previousPage, Page poppedPage)
        {
            base.OnPagePop(previousPage, poppedPage);

            if (poppedPage.GetType() == typeof(XF.Material.Forms.UI.Dialogs.MaterialConfirmationDialog) ||
                poppedPage.GetType() == typeof(XF.Material.Forms.UI.Dialogs.MaterialLoadingDialog) ||
                poppedPage.GetType() == typeof(XF.Material.Forms.UI.Dialogs.MaterialAlertDialog) ||
                poppedPage.GetType() == typeof(XF.Material.Forms.UI.Dialogs.MaterialInputDialog) ||
                poppedPage.GetType() == typeof(XF.Material.Forms.UI.Dialogs.MaterialMenuDialog) ||
                poppedPage.GetType() == typeof(XF.Material.Forms.UI.Dialogs.MaterialSimpleDialog) ||
                poppedPage.GetType() == typeof(XF.Material.Forms.UI.Dialogs.MaterialSnackbar) ||
                poppedPage.GetType() == typeof(XF.Material.Forms.UI.Dialogs.MaterialDialog) ||
                poppedPage.GetType() == typeof(XF.Material.Forms.UI.Dialogs.MaterialDialogFragment))
                return;

            if (previousPage.BindingContext is BaseViewModel viewModel)
            {
                viewModel.OnViewPopped();
            }
        }

        private void AppearingHandler(object sender, EventArgs e)
        {
            var viewModel = RootPage.BindingContext as BaseViewModel;
            viewModel?.OnViewPushed(_currentNavigationParameter);
            _currentNavigationParameter = null;
            RootPage.Appearing -= AppearingHandler;
        }
    }
}