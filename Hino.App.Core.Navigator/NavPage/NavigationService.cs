﻿using Hino.App.Cross.Services.Navigation;
using Hino.Salesforce.App.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Hino.App.Core.Navigator.NavPage
{
    public class NavigationService : INavigationService
    {
        private BaseNavigationPage _currentNavigationPage;

        public async Task PopAsync()
        {
            await _currentNavigationPage?.PopViewAsync();
        }

        public async Task PushAsync(string viewName, object parameter = null)
        {
            await _currentNavigationPage?.PushViewAsync(viewName, parameter);
        }

        public async Task PushModalAsync(string viewName, object parameter = null)
        {
            await _currentNavigationPage?.PushModalAsync(viewName, parameter);
        }

        public async Task PopModalAsync()
        {
            await _currentNavigationPage?.PopModalAsync();
        }


        public void SetRootView(string rootViewName, object parameter = null)
        {
            _currentNavigationPage = new BaseNavigationPage(rootViewName, parameter);



            var pagePrincipal = new MainPage();

            pagePrincipal.Detail = _currentNavigationPage;

            Application.Current.MainPage = pagePrincipal;


            /*_currentNavigationPage = new CustomNavigationPage(rootViewName, parameter);
            Application.Current.MainPage = _currentNavigationPage;*/
        }
    }
}
