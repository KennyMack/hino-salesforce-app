﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.App.Core.Navigator.Utilities
{
    public static class ViewNames
    {
        public enum Auth
        {
            AccountPage = 0,
            LoginPage = 1,
            RegisterPage = 2
        }

        public enum Enterprises
        {
            CreateEnterprisePage = 0,
            DetailEnterprisePage = 1,
            ListEnterprisesPage = 2
        }

        public enum Payment
        {
            MainPaymentPage = 0,
            ListPaymentConditionPage = 1,
            ListPaymentTypePage = 2
        }

        public enum General
        {
            EstablishmentPage = 0
        }

        public enum Products
        {
            ListProductsPage = 0,
            DetailProductsPage = 1
        }

        public enum Preferences
        {
            PreferencesPage = 0
        }

        public enum Sales
        {
            CreateSalePage = 0,
            MainSalesPage = 1
        }

        public enum Main
        {
            MainPage = 0
        }

        public enum Test
        {
            TestPage = 0
        }
    }
}
