﻿using CommonServiceLocator;
using Hino.Salesforce.App.Views.General.Business.Enterprises;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Hino.App.Core.Navigator.Utilities
{
    public class ViewFactory
    {
        public static Page GetView(string viewName)
        {
            if (viewName == ViewNames.Auth.AccountPage.ToString())
                return new Salesforce.App.Views.Auth.AccountsPage();
            else if (viewName == ViewNames.Auth.LoginPage.ToString())
                return new Salesforce.App.Views.Auth.LoginPage();
            else if (viewName == ViewNames.Auth.RegisterPage.ToString())
                return new Salesforce.App.Views.Auth.RegisterPage();
            else if (viewName == ViewNames.Enterprises.CreateEnterprisePage.ToString())
                return new Hino.Salesforce.App.Views.General.Business.Enterprises.CreateEnterprisePage();
            else if (viewName == ViewNames.Enterprises.DetailEnterprisePage.ToString())
                return new Hino.Salesforce.App.Views.General.Business.Enterprises.DetailEnterprisePage();
            else if (viewName == ViewNames.Enterprises.ListEnterprisesPage.ToString())
                return new Hino.Salesforce.App.Views.General.Business.Enterprises.ListEnterprisesPage();
            else if (viewName == ViewNames.Payment.MainPaymentPage.ToString())
                return new Hino.Salesforce.App.Views.General.Business.Payment.MainPaymentPage();
            else if (viewName == ViewNames.Payment.ListPaymentConditionPage.ToString())
                return new Hino.Salesforce.App.Views.General.Business.Payment.ListPaymentConditionPage();
            else if (viewName == ViewNames.Payment.ListPaymentTypePage.ToString())
                return new Hino.Salesforce.App.Views.General.Business.Payment.ListPaymentTypePage();
            else if (viewName == ViewNames.General.EstablishmentPage.ToString())
                return new Hino.Salesforce.App.Views.General.EstablishmentPage();
            else if (viewName == ViewNames.Preferences.PreferencesPage.ToString())
                return new Hino.Salesforce.App.Views.Preferences.PreferencesPage();
            else if (viewName == ViewNames.Sales.CreateSalePage.ToString())
                return new Hino.Salesforce.App.Views.Sales.CreateSalePage();
            else if (viewName == ViewNames.Sales.MainSalesPage.ToString())
                return new Hino.Salesforce.App.Views.Sales.MainSalesPage();
            else if (viewName == ViewNames.Products.ListProductsPage.ToString())
                return new Hino.Salesforce.App.Views.General.Products.ListProductsPage();
            else if (viewName == ViewNames.Products.DetailProductsPage.ToString())
                return new Hino.Salesforce.App.Views.General.Products.DetailProductsPage();
            else if (viewName == ViewNames.Test.TestPage.ToString())
                return new PageTest();

            return new Hino.Salesforce.App.Views.Sales.MainSalesPage();
        }

        

        public static TView GetView<TView>() where TView : ContentPage
        {
            return ServiceLocator.Current.GetInstance<TView>();
        }
    }
}
